//
//  WXApi.h
//  WXApi
//
//  Created by jhjang on 08/08/2019.
//  Copyright © 2019 jhjang. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WXApi.
FOUNDATION_EXPORT double WXApiVersionNumber;

//! Project version string for WXApi.
FOUNDATION_EXPORT const unsigned char WXApiVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WXApi/PublicHeader.h>

#import "WXApi.h"
