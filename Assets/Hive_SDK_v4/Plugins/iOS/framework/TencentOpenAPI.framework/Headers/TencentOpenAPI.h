//
//  TencentOpenAPI.h
//  TencentOpenAPI
//
//  Created by jhjang on 13/12/2019.
//  Copyright © 2019 jhjang. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for TencentOpenAPI.
FOUNDATION_EXPORT double TencentOpenAPIVersionNumber;

//! Project version string for TencentOpenAPI.
FOUNDATION_EXPORT const unsigned char TencentOpenAPIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TencentOpenAPI/PublicHeader.h>
#import <TencentOpenAPI/TencentOAuth.h>

