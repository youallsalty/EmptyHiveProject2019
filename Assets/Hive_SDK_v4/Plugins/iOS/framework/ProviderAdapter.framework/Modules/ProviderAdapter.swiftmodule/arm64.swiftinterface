// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5 (swiftlang-1300.0.31.1 clang-1300.0.29.1)
// swift-module-flags: -target arm64-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name ProviderAdapter
import Foundation
@_exported import ProviderAdapter
import Swift
import UIKit
import _Concurrency
@_inheritsConvenienceInitializers @objcMembers @objc(HIVEAuthProviderAdapter) public class AuthProviderAdapter : ObjectiveC.NSObject {
  @objc public static let shared: ProviderAdapter.AuthProviderAdapter
  @objc override dynamic public init()
  @objc override dynamic public var description: Swift.String {
    @objc get
  }
  @objc public func getAuthProvider(_ providerType: ProviderAdapter.AuthProviderType) -> ProviderAdapter.AuthProviderProtocol?
  @objc public func setDelegate(_ delegate: ProviderAdapter.AuthConfigurationProtocol?)
  @objc public func application(_ app: UIKit.UIApplication, open url: Foundation.URL, options: [UIKit.UIApplication.OpenURLOptionsKey : Any] = [:]) -> Swift.Bool
  @objc public func application(_ application: UIKit.UIApplication, continue userActivity: Foundation.NSUserActivity, restorationHandler: @escaping ([UIKit.UIUserActivityRestoring]?) -> Swift.Void) -> Swift.Bool
  @objc deinit
}
@objc(HIVEAnalyticsProviderType) public enum AnalyticsProviderType : Swift.Int, ProviderAdapter.ProviderType {
  case adjust
  case singular
  case appsflyer
  public var namespace: Swift.String {
    get
  }
  public var className: Swift.String {
    get
  }
  public var description: Swift.String {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias AllCases = [ProviderAdapter.AnalyticsProviderType]
  public typealias RawValue = Swift.Int
  public static var allCases: [ProviderAdapter.AnalyticsProviderType] {
    get
  }
  public var rawValue: Swift.Int {
    get
  }
}
@objc(HIVEAnalyticsDefineEventType) public enum AnalyticsDefineEventType : Swift.Int, Swift.CustomStringConvertible {
  case open = 1
  case login
  case install
  case update
  case purchase
  case tutorialComplete
  public var description: Swift.String {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @objc(HIVEAnalyticsAccountInfo) public class AnalyticsAccountInfo : ObjectiveC.NSObject {
  @objc public var accountId: Swift.String?
  @objc public var session: Swift.String?
  @objc public var nickName: Swift.String?
  @objc public var country: Swift.String?
  @objc public var server: Swift.String?
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc(HIVEAnalyticsRevenue) public class AnalyticsRevenue : ObjectiveC.NSObject {
  @objc public var pid: Swift.String?
  @objc public var title: Swift.String?
  @objc public var itemDescription: Swift.String?
  @objc public var currency: Swift.String?
  @objc public var amountMicros: Swift.Int64
  @objc public var formattedString: Swift.String?
  @objc public var price: Swift.String?
  @objc public var itemCount: Swift.Int
  @objc public var refId: Swift.String?
  @objc public var accountInfo: ProviderAdapter.AnalyticsAccountInfo?
  @objc public var analyticsId: Swift.String?
  @objc public var validationHash: Swift.String?
  @objc public var market: Swift.String?
  @objc override dynamic public init()
  @objc public func getPriceDouble() -> Swift.Double
  @objc deinit
}
@objc(HIVEAuthConfigurationProtocol) public protocol AuthConfigurationProtocol : ProviderAdapter.ProviderConfigurationProtocol {
  @objc func present(viewController: UIKit.UIViewController)
  @objc func dismiss(viewController: UIKit.UIViewController)
  @objc func getViewController() -> UIKit.UIViewController?
  @objc func requestNetwork(_ url: Foundation.URL, params: [Swift.String : Swift.String]?, method: Swift.String, handler: ProviderAdapter.ProviderRequestHandler?)
  @objc func getUniversalLink() -> Swift.String?
  @objc func isRealServerZone() -> Swift.Bool
}
@objc(HIVEAuthProviderProtocol) public protocol AuthProviderProtocol {
  @objc static var sdkVersion: Swift.String { get }
  @objc var providerID: Swift.String { get }
  @objc var providerType: ProviderAdapter.AuthProviderType { get }
  @objc var loginHandler: ProviderAdapter.AuthProviderHandler? { get }
  @objc var logoutHandler: ProviderAdapter.AuthProviderHandler? { get }
  @objc var getProfileHandler: ProviderAdapter.AuthProviderHandler? { get }
  @objc var isLogin: Swift.Bool { get }
  @objc var userID: Swift.String? { get }
  @objc var userToken: Swift.String? { get }
  @objc var idpAppID: Swift.String? { get }
  @objc var userName: Swift.String? { get }
  @objc var userProfileImage: Swift.String? { get }
  @objc var delegate: ProviderAdapter.AuthConfigurationProtocol? { get set }
  @objc static func application(_ app: UIKit.UIApplication, open url: Foundation.URL, options: [UIKit.UIApplication.OpenURLOptionsKey : Any]) -> Swift.Bool
  @objc static func application(_ application: UIKit.UIApplication, continue userActivity: Foundation.NSUserActivity, restorationHandler: @escaping ([UIKit.UIUserActivityRestoring]?) -> Swift.Void) -> Swift.Bool
  @objc func login(_ handler: ProviderAdapter.AuthProviderHandler?)
  @objc func logout(_ handler: ProviderAdapter.AuthProviderHandler?)
  @objc func getProfile(_ handler: ProviderAdapter.AuthProviderHandler?)
  @objc func getFriends(_ handler: ProviderAdapter.AuthProviderGetFriendsHandler?)
  @objc init(configuration: ProviderAdapter.AuthConfigurationProtocol?)
}
@objc(HIVEAuthTwitterProviderProtocol) public protocol AuthTwitterProviderProtocol : ProviderAdapter.AuthProviderProtocol {
  @objc var userSecret: Swift.String? { get }
  @objc var consumerKey: Swift.String? { get }
  @objc var consumerSecret: Swift.String? { get }
}
extension ProviderAdapter.AuthProviderProtocol {
  public func addLog(_ message: Swift.String)
  public func getConfigurationValue(key: Swift.String) -> Swift.String
  public func getConfigurationValue(key: Swift.String, migrationKey: Swift.String) -> Swift.String
}
@objc(HIVEProviderType) public enum AuthProviderType : Swift.Int, ProviderAdapter.ProviderType {
  case Guest = 0
  case HIVE
  case Facebook
  case Google
  case QQ
  case WEIBO
  case VK
  case WeChat
  case Apple
  case SignIn_Apple
  case Line
  case Twitter
  case Weverse
  case Auto = 99
  public init?(string: Swift.String)
  public var description: Swift.String {
    get
  }
  public var namespace: Swift.String {
    get
  }
  public var className: Swift.String {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias AllCases = [ProviderAdapter.AuthProviderType]
  public typealias RawValue = Swift.Int
  public static var allCases: [ProviderAdapter.AuthProviderType] {
    get
  }
  public var rawValue: Swift.Int {
    get
  }
}
@objc(HIVEAnalyticsConfigurationProtocol) public protocol AnalyticsConfigurationProtocol : ProviderAdapter.ProviderConfigurationProtocol {
  @objc func isProductionEnviroment() -> Swift.Bool
  @objc func isUseLog() -> Swift.Bool
  @objc func deviceToken() -> Foundation.Data?
}
@objc(HIVEAnalyticsProviderProtocol) public protocol AnalyticsProviderProtocol {
  @objc var providerType: ProviderAdapter.AnalyticsProviderType { get }
  @objc var delegate: ProviderAdapter.AnalyticsConfigurationProtocol? { get set }
  @objc var description: Swift.String { get }
  @objc func initTracker(did: Swift.String?, analyticsId: Swift.String)
  @objc func setEnable(isEnable: Swift.Bool)
  @objc func sendEvent(eventName: Swift.String)
  @objc func sendRevenueEvent(eventName: Swift.String, analyticsRevenue: ProviderAdapter.AnalyticsRevenue)
  @objc func onSigninEvent(accountInfo: ProviderAdapter.AnalyticsAccountInfo?)
  @objc func onSignoutEvent()
  @objc init(configuration: ProviderAdapter.AnalyticsConfigurationProtocol?)
  @objc static func application(_ app: UIKit.UIApplication, open url: Foundation.URL, options: [UIKit.UIApplication.OpenURLOptionsKey : Any]) -> Swift.Bool
  @objc static func application(_ application: UIKit.UIApplication, continue userActivity: Foundation.NSUserActivity, restorationHandler: @escaping ([Any]?) -> Swift.Void) -> Swift.Bool
  @objc static func application(_ application: UIKit.UIApplication, open url: Foundation.URL, sourceApplication: Swift.String?, annotation: Any) -> Swift.Bool
  @objc static func applicationDidBecomeActive(_ application: UIKit.UIApplication)
}
extension ProviderAdapter.AnalyticsProviderProtocol {
  public func printLog(message: Swift.String)
  public func getConfigurationValue(key: Swift.String) -> Swift.String
  public func getEvents() -> [Swift.String : Swift.String]
}
public protocol AnalyticsConfigurationKey {
  var name: Swift.String { get }
}
extension ProviderAdapter.AnalyticsProviderProtocol {
  public var providerID: Swift.String {
    get
  }
}
@objc @_inheritsConvenienceInitializers public class AuthProviderConfiguration : ObjectiveC.NSObject {
  public static let shared: ProviderAdapter.AuthProviderConfiguration
  public func initializeConfiguration(xmlDictionary: [Swift.String : Any]?)
  public func parseKey(providerName: Swift.String, providerAttributeKey attKey: Swift.String) -> Swift.String
  public func parseKey(providerName: Swift.String, providerAttributeKey attKey: Swift.String, oldKey: Swift.String?) -> Swift.String
  public func setKey(providerName: Swift.String, providerAttributeKey attKey: Swift.String, value: Swift.String)
  @objc override dynamic public init()
  @objc deinit
}
public typealias AuthProviderHandler = (_ errorCode: ProviderAdapter.AuthProviderError, _ code: ProviderAdapter.AuthProviderCode) -> ()
public typealias AuthProviderGetFriendsHandler = (_ errorCode: ProviderAdapter.AuthProviderError, _ code: ProviderAdapter.AuthProviderCode, _ providerUserIDList: [Swift.String]?) -> ()
public typealias ProviderRequestHandler = (_ result: ProviderAdapter.AuthProviderError, _ responseData: [Swift.String : Any]?) -> ()
@objc(HIVEProviderConfigurationProtocol) public protocol ProviderConfigurationProtocol {
  @objc func addLog(tag: Swift.String, message: Swift.String)
}
public protocol ProviderType : Swift.CaseIterable, Swift.RawRepresentable {
  var namespace: Swift.String { get }
  var className: Swift.String { get }
  var description: Swift.String { get }
}
@objc @_inheritsConvenienceInitializers public class AnalyticsConfiguration : ObjectiveC.NSObject {
  public static let shared: ProviderAdapter.AnalyticsConfiguration
  public func initializeConfiguration(xmlDictionary: [Swift.String : Any]?)
  public func parseKey(providerName: Swift.String, attributeKey attKey: Swift.String) -> Swift.String
  public func parseEvents(providerName: Swift.String) -> [Swift.String : Swift.String]
  public func setKey(providerName: Swift.String, providerAttributeKey attKey: Swift.String, value: Swift.String)
  public func setEvents(providerName: Swift.String)
  @objc override dynamic public init()
  @objc deinit
}
@objc public enum AuthProviderError : Swift.Int {
  case success
  case cancel
  case inProgress
  case invalidParam
  case responseFail
  case network
  case notSupported
  case unknown
  case needInitialize
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public enum AuthProviderCode : Swift.Int {
  case success
  case lineCancel
  case lineAuthorizeError
  case lineTimeOut
  case lineMissingKeyChannelId
  case twitterRespnseFail
  case wechatInProgress
  case wechatResponseFail
  case wechatResponseFailLogin
  case wechatInvalidResponseData
  case wechatNetworkError
  case wechatResponseFailUserInfo
  case wechatNetworkErrorUserInfo
  case wechatMissingKeyID
  case wechatMissingKeySecret
  case googleLoginCancel
  case googleMissingKeyClientID
  case googleMissingKeyServerClientID
  case googleMissingKeyURLScheme
  case facebookResponseError
  case facebookUserCancel
  case facebookCancel
  case facebookResponseFailGetFriends
  case facebookMissingKeyAppID
  case facebookMissingKeyURLScheme
  case vkInProgress
  case vkNotInitialized
  case vkCancel
  case vkResponseError
  case vkResponseFailUploadProfile
  case vkInvalidParamSDK
  case vkMissingKeyID
  case qqInProgress
  case qqInvalidParam
  case qqResponseFailLogin
  case qqCancel
  case qqResponseError
  case qqNetworkError
  case qqMissingKeyAppId
  case hiveInProgress
  case hiveCancel
  case hiveResponseFailSignInProvider
  case signInAppleInProgress
  case signInAppleNotSupported
  case signInAppleResponseError
  case signInAppleCancel
  case signInAppleFailed
  case signInAppleInvalidResponse
  case signInAppleNotHandled
  case appleTimeOut
  case appleResponseFailLogin
  case appleLoginCancel
  case appleCancel
  case appleResponseError
  case weverseInProgress
  case weverseMissingKeyClientID
  case weverseResponseError
  case weverseNetworkError
  case weverseNotSupported
  case weverseCancel
  case notSupportGetFriends
  case unknown
  case notSupportedProviderType
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @objc(HIVEAnalyticsProviderAdapter) public class AnalyticsProviderAdapter : ObjectiveC.NSObject {
  @objc public static let shared: ProviderAdapter.AnalyticsProviderAdapter
  @objc override dynamic public var description: Swift.String {
    @objc get
  }
  @objc override dynamic public init()
  @objc public func initTracker(did: Swift.String?, analyticsId: Swift.String)
  @objc public func setDelegate(_ delegate: ProviderAdapter.AnalyticsConfigurationProtocol?)
  @objc public func setEnableTracker(name: Swift.String, isEnable: Swift.Bool)
  @objc public func sendEvent(eventName: Swift.String)
  @objc public func sendRevenueEvent(eventName: Swift.String, analyticsRevenue: ProviderAdapter.AnalyticsRevenue)
  @objc public func onSigninEvent(accountInfo: ProviderAdapter.AnalyticsAccountInfo?)
  @objc public func onSignoutEvent()
  @objc deinit
}
extension ProviderAdapter.AnalyticsProviderAdapter {
  @objc dynamic public func applicationDidBecomeActive(_ application: UIKit.UIApplication)
  @objc dynamic public func application(_ application: UIKit.UIApplication, continue userActivity: Foundation.NSUserActivity, restorationHandler: @escaping ([Any]?) -> Swift.Void) -> Swift.Bool
  @objc dynamic public func application(_ app: UIKit.UIApplication, open url: Foundation.URL, options: [UIKit.UIApplication.OpenURLOptionsKey : Any] = [:]) -> Swift.Bool
  @objc dynamic public func application(_ application: UIKit.UIApplication, open url: Foundation.URL, sourceApplication: Swift.String?, annotation: Any) -> Swift.Bool
}
extension ProviderAdapter.AnalyticsProviderType : Swift.Equatable {}
extension ProviderAdapter.AnalyticsProviderType : Swift.Hashable {}
extension ProviderAdapter.AnalyticsDefineEventType : Swift.Equatable {}
extension ProviderAdapter.AnalyticsDefineEventType : Swift.Hashable {}
extension ProviderAdapter.AnalyticsDefineEventType : Swift.RawRepresentable {}
extension ProviderAdapter.AuthProviderType : Swift.Equatable {}
extension ProviderAdapter.AuthProviderType : Swift.Hashable {}
extension ProviderAdapter.AuthProviderError : Swift.Equatable {}
extension ProviderAdapter.AuthProviderError : Swift.Hashable {}
extension ProviderAdapter.AuthProviderError : Swift.RawRepresentable {}
extension ProviderAdapter.AuthProviderCode : Swift.Equatable {}
extension ProviderAdapter.AuthProviderCode : Swift.Hashable {}
extension ProviderAdapter.AuthProviderCode : Swift.RawRepresentable {}
