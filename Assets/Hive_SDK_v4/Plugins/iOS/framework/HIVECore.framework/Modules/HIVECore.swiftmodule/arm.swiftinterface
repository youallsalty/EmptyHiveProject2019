// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5 (swiftlang-1300.0.31.1 clang-1300.0.29.1)
// swift-module-flags: -target armv7-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name HIVECore
import CommonCrypto
import CoreTelephony
import Foundation
import Security
import StoreKit
import Swift
import UIKit
import _Concurrency
import zlib
@_inheritsConvenienceInitializers @objc public class Platform : ObjectiveC.NSObject {
  @objc override dynamic public init()
  @objc deinit
}
extension HIVECore.Platform {
  @objc public static func getAdvertisingId() -> Swift.String?
  @objc public static func getIsLimitAdTracking() -> Swift.Bool
  @objc public static func getMdn() -> Swift.String?
  @objc public static func getImei() -> Swift.String?
  @objc public static func getMcc() -> Swift.String?
  @objc public static func getMnc() -> Swift.String?
  @objc public static func getCountry() -> Swift.String?
  @objc public static func getLanguage() -> Swift.String?
  @objc public static func getOsVersion() -> Swift.String?
  @objc public static func getCpuType() -> Swift.String?
  @objc public static func getPlatform() -> Swift.String?
  @objc public static func getDeviceModel() -> Swift.String?
  @objc public static func getDeviceName() -> Swift.String
}
extension HIVECore.Platform {
  public static func getBundleVersion() -> Swift.String?
  @objc public static func getVendorId() -> Swift.String?
  @objc public static func getIsHack() -> Swift.Bool
  public static func getMemoryInfo() -> [Swift.String : Any]
}
@objc public enum PropertyType : Swift.Int {
  case main
  case local
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @objc public class PropertyManager : ObjectiveC.NSObject {
  @objc override dynamic public var description: Swift.String {
    @objc get
  }
  @objc override dynamic public init()
  @objc deinit
}
extension HIVECore.PropertyManager {
  @objc dynamic public class func getInstance(_ filename: Swift.String, type propertyType: HIVECore.PropertyType = .main) -> HIVECore.CoreProperty
}
@objc public enum HiveATTStatus : Swift.Int {
  case notDetermined = 0
  case restricted
  case denied
  case authorized
  case notSupportedOS
  case invalidStatus
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public protocol HiveATTManagerProtocol {
  @objc static func requestAuthentication(handler: @escaping ((_ status: HIVECore.HiveATTStatus) -> ()))
  @objc static var advertisingId: Swift.String { get }
  @objc static var isEnableTracking: Swift.Bool { get }
}
@_inheritsConvenienceInitializers @objc public class HiveCoreATTManager : ObjectiveC.NSObject {
  @objc public static let managerClass: HIVECore.HiveATTManagerProtocol.Type?
  @objc public static func requestAuthentication(handler: @escaping ((_ status: HIVECore.HiveATTStatus) -> ()))
  @objc public static var advertisingId: Swift.String {
    @objc get
  }
  @objc public static var isEnableTracking: Swift.Bool {
    @objc get
  }
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class Product : ObjectiveC.NSObject {
  @objc override dynamic public init()
  @objc deinit
}
extension HIVECore.Product {
  @objc public static func getAppIdentifier() -> Swift.String
  @objc public static func getBuildVersion() -> Swift.String
  @objc public static func getUrlSchemes() -> [Swift.String]?
}
@_inheritsConvenienceInitializers @objc public class Request : ObjectiveC.NSObject {
  @objc(RequestMethodType) public enum MethodType : Swift.Int, Swift.RawRepresentable {
    case POST
    case GET
    case DELETE
    case PUT
    public typealias RawValue = Swift.String
    public var rawValue: HIVECore.Request.MethodType.RawValue {
      get
    }
    public init?(rawValue: HIVECore.Request.MethodType.RawValue)
  }
  @objc(RequestContentType) public enum ContentType : Swift.Int, Swift.RawRepresentable {
    case textHtml
    case urlEncoding
    case json
    case gzip
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
    public init?(rawValue: Swift.String)
  }
  @objc public var header: [Swift.String : Swift.String]
  @objc public var method: HIVECore.Request.MethodType
  @objc public var contentType: HIVECore.Request.ContentType
  @objc public var contentTypeAdded: Swift.String
  @objc public var body: Foundation.Data?
  @objc public var connectTimeoutSeconds: Swift.Int
  @objc public var readTimeoutSeconds: Swift.Int
  @objc public var url: Swift.String
  @objc override dynamic public var description: Swift.String {
    @objc get
  }
  @objc override dynamic public init()
  @objc override dynamic public func copy() -> Any
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class CookieManager : ObjectiveC.NSObject {
  @objc public static func setCookie(_ domain: Swift.String, key: Swift.String, value: Swift.String)
  @objc public static func setCookie(_ domain: Swift.String, map: [Swift.String : Swift.String])
  @objc public static func getCookies(_ domain: Swift.String) -> [Swift.String : Swift.String]?
  @objc public static func removeCookie(_ domain: Swift.String, key: Swift.String)
  @objc public static func removeCookie(_ domain: Swift.String)
  @objc override dynamic public init()
  @objc deinit
}
@objc(HIVEZoneType) public enum ZoneType : Swift.Int, Swift.CaseIterable, Swift.CustomStringConvertible {
  case sandbox = 0
  case test = 1
  case real = 2
  case dev = 3
  public init?(stringValue: Swift.String)
  public var description: Swift.String {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias AllCases = [HIVECore.ZoneType]
  public typealias RawValue = Swift.Int
  public static var allCases: [HIVECore.ZoneType] {
    get
  }
  public var rawValue: Swift.Int {
    get
  }
}
@objc(HIVELanguage) public enum HIVELanguage : Swift.Int, Swift.CaseIterable, Swift.CustomStringConvertible {
  case DE = 0
  case EN
  case ES
  case FR
  case ID
  case IT
  case JA
  case KO
  case PT
  case RU
  case TH
  case TR
  case VI
  case ZHS
  case ZHT
  case AR
  public init?(stringValue: Swift.String)
  public var description: Swift.String {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias AllCases = [HIVECore.HIVELanguage]
  public typealias RawValue = Swift.Int
  public static var allCases: [HIVECore.HIVELanguage] {
    get
  }
  public var rawValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers public class Configuration {
  public static var zone: HIVECore.ZoneType
  public static var readTimeoutSeconds: Swift.Int
  public static var connectTimeoutSeconds: Swift.Int
  public static var retryNetwork: Swift.Bool
  public static var trackers: [[Swift.String : Any]]
  public static var serverID: Swift.String
  public static var sdkVersion: Swift.String
  public static var gameLanguage: Swift.String
  public static var language: Swift.String {
    get
  }
  public static var country: Swift.String {
    get
  }
  public static var hiveCountry: Swift.String {
    get
    set
  }
  public static var timezone: [Swift.String : Swift.String] {
    get
    set
  }
  public static var ageGateU13: Swift.Bool {
    get
    set
  }
  public static var mirror: Swift.String {
    get
    set
  }
  public static var whiteListDomain: [[Swift.String : Any]] {
    get
    set
  }
  public static var analyticsID: Swift.String {
    get
  }
  public static func migrationAnalyticsID(analyticsId: Swift.String)
  @objc deinit
}
extension StoreKit.SKProduct {
  public var hiveStoreCountry: Swift.String? {
    get
  }
  public var hiveCurrencyCode: Swift.String? {
    get
  }
}
@_inheritsConvenienceInitializers @objc public class Crypto : ObjectiveC.NSObject {
  @objc public enum CryptoType : Swift.Int {
    case AES128
    case AES256
    case MD5
    case SHA1
    case SHA256
    case RSA
    public init?(rawValue: Swift.Int)
    public typealias RawValue = Swift.Int
    public var rawValue: Swift.Int {
      get
    }
  }
  @objc override dynamic public init()
  @objc deinit
}
extension HIVECore.Crypto {
  @objc dynamic public class func md5(_ data: Foundation.Data) -> Foundation.Data
  @objc dynamic public class func sha1(_ data: Foundation.Data) -> Foundation.Data
  @objc dynamic public class func sha256(_ data: Foundation.Data) -> Foundation.Data
  @objc dynamic public class func rsa(string: Swift.String, publicKey: Swift.String?) -> Swift.String?
}
extension HIVECore.Crypto {
  public struct Crypt {
    public static func encrypt(type: HIVECore.Crypto.CryptoType, keyData: Foundation.Data, data: Foundation.Data) -> Foundation.Data?
    public static func decrypt(type: HIVECore.Crypto.CryptoType, keyData: Foundation.Data, data: Foundation.Data) -> Foundation.Data?
  }
  @objc dynamic public class func encrypt(type: HIVECore.Crypto.CryptoType, key: Foundation.Data, data: Foundation.Data) -> Foundation.Data?
  @objc dynamic public class func decrypt(type: HIVECore.Crypto.CryptoType, key: Foundation.Data, data: Foundation.Data) -> Foundation.Data?
  public class func gzipEncodedData(inputData: Foundation.Data) -> Foundation.Data?
  public class func gzipDecodedData(inputData: Foundation.Data) -> Foundation.Data?
  public class func createHash(cryptoHashType: HIVECore.Crypto.CryptoType, data: Swift.String) -> Swift.String?
  public class func createHash(cryptoHashType: HIVECore.Crypto.CryptoType, data: Swift.String) -> Foundation.Data?
}
extension HIVECore.Crypto {
  public struct KeyChain {
    public static func save(key: Swift.String, value: Swift.String) -> Swift.Bool
    public static func load(key: Swift.String) -> Swift.String?
    public static func remove(key: Swift.String) -> Swift.Bool
    public static func update(key: Swift.String, value: Swift.String) -> Swift.Bool
  }
  @objc dynamic public class func saveKeyChain(_ key: Swift.String, value: Swift.String) -> Swift.Bool
  @objc dynamic public class func loadKeyChain(_ key: Swift.String) -> Swift.String?
  @objc dynamic public class func removeKeyChain(_ key: Swift.String) -> Swift.Bool
}
@_inheritsConvenienceInitializers @objc public class StringUtil : ObjectiveC.NSObject {
  @objc override dynamic public init()
  @objc deinit
}
extension HIVECore.StringUtil {
  @objc public static func stringToByte(_ string: Swift.String) -> Foundation.Data?
  @objc public static func byteToString(_ data: Foundation.Data) -> Swift.String?
  @objc public static func byteToHexStr(_ data: Foundation.Data) -> Swift.String
  @objc public static func isEmptyString(_ value: Any?) -> Swift.Bool
}
extension HIVECore.StringUtil {
  public struct Base64 {
    public static func encode(data: Foundation.Data, options: Foundation.Data.Base64EncodingOptions = .endLineWithCarriageReturn) -> Swift.String
    public static func decode(string: Swift.String, options: Foundation.Data.Base64DecodingOptions = .ignoreUnknownCharacters) -> Foundation.Data?
  }
  @objc public static func base64Encode(_ data: Foundation.Data) -> Swift.String?
  @objc public static func base64Encode(_ data: Foundation.Data, options: Foundation.Data.Base64EncodingOptions) -> Swift.String?
  @objc public static func base64Decode(_ string: Swift.String) -> Foundation.Data?
  @objc public static func base64Decode(_ string: Swift.String, options: Foundation.Data.Base64DecodingOptions) -> Foundation.Data?
}
extension HIVECore.StringUtil {
  public struct URLCoder {
    public static func encode(_ string: Swift.String) -> Swift.String?
    public static func decode(_ string: Swift.String) -> Swift.String?
    public static func queryDictionary(_ string: Swift.String) -> [Swift.String : Swift.AnyObject]?
  }
  @objc public static func urlEncode(_ string: Swift.String) -> Swift.String?
  @objc public static func urlDecode(_ string: Swift.String) -> Swift.String?
  @objc public static func queryDictionary(_ string: Swift.String) -> [Swift.String : Swift.AnyObject]?
}
extension Swift.String {
  public func hive_trim() -> Swift.String
}
extension HIVECore.StringUtil {
  @objc public static func hexNumberStringToString(_ string: Swift.String) -> Swift.String
}
extension Foundation.Data {
  public func hexEncodedString() -> Swift.String
}
@_inheritsConvenienceInitializers @objc open class Response : ObjectiveC.NSObject {
  @objc public var request: HIVECore.Request?
  @objc public var result: HIVECore.Result?
  @objc public var httpStatusCode: Swift.Int
  @objc public var header: [Swift.AnyHashable : Any]?
  @objc public var content: Foundation.Data?
  @objc override dynamic public init()
  public init(response: HIVECore.Response)
  @objc override dynamic open var description: Swift.String {
    @objc get
  }
  @objc deinit
}
extension HIVECore.Response {
  @objc dynamic open func isSuccess() -> Swift.Bool
}
extension Swift.Dictionary where Key == Swift.String {
  public subscript(ci key: Key) -> Value? {
    get
    set
  }
}
extension Swift.Dictionary where Key : Swift.ExpressibleByStringLiteral {
  public func intValue(key: Swift.String) -> Swift.Int?
}
extension Swift.Dictionary {
  public static func += (lhs: inout [Key : Value], rhs: [Key : Value])
}
extension Swift.Collection where Self : Swift.CustomStringConvertible {
  public var metaDescription: Swift.String {
    get
  }
}
extension Swift.Dictionary where Key == Swift.String, Value == Any {
  public mutating func appendCommonIdentifier()
  public mutating func appendURLparameters(params: [Swift.String : Swift.String])
}
@objc public protocol LoggerListener {
  @objc optional func onLogEvent(_ type: HIVECore.Logger.CoreLogType, tag: Swift.String, message: Swift.String)
}
@_inheritsConvenienceInitializers @objc public class Logger : ObjectiveC.NSObject {
  public static var isEnable: Swift.Bool {
    get
  }
  @objc override dynamic public init()
  @objc deinit
}
extension HIVECore.Logger {
  @objc public enum CoreLogType : Swift.Int {
    case Error
    case Warning
    case Info
    case Debug
    case Verbose
    public init?(rawValue: Swift.Int)
    public typealias RawValue = Swift.Int
    public var rawValue: Swift.Int {
      get
    }
  }
}
extension HIVECore.Logger {
  public class func addListener<T>(_ listener: T) where T : HIVECore.LoggerListener
  public class func removeListener<T>(_ listener: T) where T : HIVECore.LoggerListener
  @objc dynamic public class func addListener(_ listener: HIVECore.LoggerListener?)
  @objc dynamic public class func removeListener(_ listener: HIVECore.LoggerListener?)
}
extension HIVECore.Logger {
  @objc dynamic public class func setLogEnable(_ enable: Swift.Bool)
}
extension HIVECore.Logger {
  public static func e<T>(tag: Swift.String? = nil, _ object: @autoclosure @escaping () -> T?, _ file: Swift.String = #file, _ function: Swift.String = #function, _ line: Swift.Int = #line)
  public static func w<T>(tag: Swift.String? = nil, _ object: @autoclosure @escaping () -> T?, _ file: Swift.String = #file, _ function: Swift.String = #function, _ line: Swift.Int = #line)
  public static func i<T>(tag: Swift.String? = nil, _ object: @autoclosure @escaping () -> T?, _ file: Swift.String = #file, _ function: Swift.String = #function, _ line: Swift.Int = #line)
  public static func d<T>(tag: Swift.String? = nil, _ object: @autoclosure @escaping () -> T?, _ file: Swift.String = #file, _ function: Swift.String = #function, _ line: Swift.Int = #line)
  public static func v<T>(tag: Swift.String? = nil, _ object: @autoclosure @escaping () -> T?, _ file: Swift.String = #file, _ function: Swift.String = #function, _ line: Swift.Int = #line)
  @objc dynamic public class func e(tag: Swift.String? = nil, _ message: Swift.String?, file: Swift.String = #file, function: Swift.String = #function, line: Swift.Int = #line)
  @objc dynamic public class func w(tag: Swift.String? = nil, _ message: Swift.String?, file: Swift.String = #file, function: Swift.String = #function, line: Swift.Int = #line)
  @objc dynamic public class func i(tag: Swift.String? = nil, _ message: Swift.String?, file: Swift.String = #file, function: Swift.String = #function, line: Swift.Int = #line)
  @objc dynamic public class func d(tag: Swift.String? = nil, _ message: Swift.String?, file: Swift.String = #file, function: Swift.String = #function, line: Swift.Int = #line)
  @objc dynamic public class func v(tag: Swift.String? = nil, _ message: Swift.String?, file: Swift.String = #file, function: Swift.String = #function, line: Swift.Int = #line)
  @objc dynamic public class func log(type: Swift.String? = nil, tag: Swift.String? = nil, message: Swift.String?, file: Swift.String = #file, function: Swift.String = #function, line: Swift.Int = #line)
}
extension UIKit.UIImage {
  public func compressFormat(fileType: Swift.String, quality: Swift.Int) -> Foundation.Data?
  public func fixOrientation() -> UIKit.UIImage
  public func resize(size: CoreGraphics.CGSize) -> UIKit.UIImage
  public func changeColor(color: UIKit.UIColor) -> UIKit.UIImage
}
@_inheritsConvenienceInitializers @objc public class Information : ObjectiveC.NSObject {
  @objc public class var Version: Swift.String {
    @objc get
  }
  @objc public class var Domain: Swift.String {
    @objc get
  }
  public class var TAG: Swift.String {
    get
  }
  @objc override dynamic public init()
  @objc deinit
}
public protocol JSONProtocol {
  func toJson() -> Swift.String
}
extension HIVECore.JSONProtocol where Self : Swift.Encodable {
  public func toJson() -> Swift.String
}
extension Swift.Optional where Wrapped == Swift.String {
  public var isEmpty: Swift.Bool {
    get
  }
  public var boolValue: Swift.Bool {
    get
  }
}
extension Swift.Optional where Wrapped == Any {
  public var int64Value: Swift.Int64? {
    get
  }
}
extension Swift.String {
  public var boolValue: Swift.Bool {
    get
  }
  public func parseItemDictionaryFromAgreementScheme() -> [Swift.String : Any]?
  public func trim() -> Swift.String
  public func localize(bundle: Foundation.Bundle, tableName: Swift.String) -> Swift.String
  public func stringByAppendingPathComponent(path: Swift.String) -> Swift.String
  public var intValue: Swift.Int? {
    get
  }
  public var int64Value: Swift.Int64? {
    get
  }
  public var hexDecimal: Foundation.Data? {
    get
  }
}
extension Swift.Int {
  public var stringValue: Swift.String {
    get
  }
}
extension Swift.Int64 {
  public var stringValue: Swift.String {
    get
  }
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc public class CoreProperty : ObjectiveC.NSObject {
  public var backup: Swift.Bool
  @objc override dynamic public var description: Swift.String {
    @objc get
  }
  @objc public func get(key: Swift.String) -> Swift.String?
  @objc public func set(key: Swift.String, value: Swift.String)
  @objc public func remove(key: Swift.String)
  @objc public func write()
  @objc public func delete()
  public func allKeys() -> [Swift.String]
  @objc deinit
}
extension HIVECore.CoreProperty {
  public func getKeyString(dataKey: Swift.String) -> Swift.String?
  public func load(fileName: Swift.String, filePath: Swift.String, keyString: Swift.String, legacy: Swift.Bool = false) -> Foundation.Data?
}
extension Foundation.URL {
  public func parseQueryString() -> [Swift.String : Swift.String]?
}
@_inheritsConvenienceInitializers @objc public class Network : ObjectiveC.NSObject {
  @objc override dynamic public init()
  @objc deinit
}
extension HIVECore.Network {
  @objc dynamic public class func sync(request: HIVECore.Request) -> HIVECore.Response
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc public class Result : ObjectiveC.NSObject {
  @objc public var code: HIVECore.Result.Code
  @objc public var message: Swift.String {
    @objc get
    @objc set
  }
  @objc override dynamic public var description: Swift.String {
    @objc get
  }
  @objc override dynamic public func copy() -> Any
  @objc(ResultCode) public enum Code : Swift.Int {
    case Success = 0
    case Failed = -100
    case InvalidParameters = -101
    case Canceled = -102
    case Timeout = -201
    case Unknown = -9999
    public init?(rawValue: Swift.Int)
    public typealias RawValue = Swift.Int
    public var rawValue: Swift.Int {
      get
    }
  }
  @objc deinit
}
extension HIVECore.Result {
  @objc convenience dynamic public init(code: HIVECore.Result.Code = .Unknown, message: Swift.String? = nil)
}
extension HIVECore.Result {
  @objc dynamic public func isSuccess() -> Swift.Bool
  public class func codeFromError(_ error: Foundation.NSError) -> HIVECore.Result.Code
  public class func codeFromHttpStatusCode(_ statusCode: Swift.Int) -> HIVECore.Result.Code
}
extension Swift.Dictionary {
  public func toJSON() -> Swift.String?
  public func toData() -> Foundation.Data?
}
extension Swift.Array {
  public func toJSON() -> Swift.String?
  public func toData() -> Foundation.Data?
}
extension Foundation.NSDictionary {
  @objc dynamic public func toJSON() -> Foundation.NSString?
  @objc dynamic public func toData() -> Foundation.NSData?
}
extension Swift.String {
  public func toDictionary() -> [Swift.String : Any]?
  public func toArray<T>() -> [T]?
}
extension Foundation.NSString {
  @objc dynamic public func toDictionary() -> Foundation.NSDictionary?
}
extension Foundation.Data {
  public func toDictionary() -> [Swift.String : Any]?
  public func toArray<T>() -> [T]?
}
extension Foundation.NSData {
  @objc dynamic public func toDictionary() -> Foundation.NSDictionary?
}
extension HIVECore.PropertyType : Swift.Equatable {}
extension HIVECore.PropertyType : Swift.Hashable {}
extension HIVECore.PropertyType : Swift.RawRepresentable {}
extension HIVECore.HiveATTStatus : Swift.Equatable {}
extension HIVECore.HiveATTStatus : Swift.Hashable {}
extension HIVECore.HiveATTStatus : Swift.RawRepresentable {}
extension HIVECore.Request.MethodType : Swift.Equatable {}
extension HIVECore.Request.MethodType : Swift.Hashable {}
extension HIVECore.Request.ContentType : Swift.Equatable {}
extension HIVECore.Request.ContentType : Swift.Hashable {}
extension HIVECore.ZoneType : Swift.Equatable {}
extension HIVECore.ZoneType : Swift.Hashable {}
extension HIVECore.ZoneType : Swift.RawRepresentable {}
extension HIVECore.HIVELanguage : Swift.Equatable {}
extension HIVECore.HIVELanguage : Swift.Hashable {}
extension HIVECore.HIVELanguage : Swift.RawRepresentable {}
extension HIVECore.Crypto.CryptoType : Swift.Equatable {}
extension HIVECore.Crypto.CryptoType : Swift.Hashable {}
extension HIVECore.Crypto.CryptoType : Swift.RawRepresentable {}
extension HIVECore.Logger.CoreLogType : Swift.Equatable {}
extension HIVECore.Logger.CoreLogType : Swift.Hashable {}
extension HIVECore.Logger.CoreLogType : Swift.RawRepresentable {}
extension HIVECore.Result.Code : Swift.Equatable {}
extension HIVECore.Result.Code : Swift.Hashable {}
extension HIVECore.Result.Code : Swift.RawRepresentable {}
