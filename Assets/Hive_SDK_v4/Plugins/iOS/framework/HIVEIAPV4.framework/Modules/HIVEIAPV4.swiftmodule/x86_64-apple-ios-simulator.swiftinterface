// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5 (swiftlang-1300.0.31.1 clang-1300.0.29.1)
// swift-module-flags: -target x86_64-apple-ios10.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name HIVEIAPV4
import CommonCrypto
import Foundation
import HIVEBase
import HIVECore
import HIVEProtocol
import HIVEUI
import SQLite3
import StoreKit
import Swift
import UIKit
import _Concurrency
public typealias IAPV4MarketConnectHandler = (_ result: HIVEBase.ResultAPI, _ marketIDs: [Swift.Int]?) -> Swift.Void
public typealias IAPV4ProductInfoHandler = (_ result: HIVEBase.ResultAPI, _ productInfoList: [HIVEIAPV4.IAPV4Product]?, _ balance: Swift.UInt) -> Swift.Void
public typealias IAPV4PurchaseHandler = (_ result: HIVEBase.ResultAPI, _ receipt: HIVEIAPV4.IAPV4Receipt?) -> Swift.Void
public typealias IAPV4RestoreHandler = (_ result: HIVEBase.ResultAPI, _ receiptList: [HIVEIAPV4.IAPV4Receipt]?) -> Swift.Void
public typealias IAPV4TransactionFinishHandler = (_ result: HIVEBase.ResultAPI, _ marketPID: Swift.String?) -> Swift.Void
public typealias IAPV4TransactionMultiFinishHandler = (_ resultList: [HIVEBase.ResultAPI], _ marketPIDList: [Swift.String]?) -> Swift.Void
public typealias IAPV4CheckPromotePurchaseHandler = (_ result: HIVEBase.ResultAPI, _ marketPID: Swift.String?) -> Swift.Void
public typealias RepaymentCompletionHandler = () -> Swift.Void
public typealias RequestRepaymentHandler = (_ reult: HIVEBase.ResultAPI) -> Swift.Void
public typealias VoidRestoreHandler = (_ result: HIVEBase.ResultAPI, _ receiptList: [HIVEIAPV4.IAPV4Receipt]?) -> Swift.Void
public typealias VoidVerifyReceiptHandler = (_ result: HIVEBase.ResultAPI) -> Swift.Void
public typealias VoidTransactionFinshHandler = (_ result: HIVEBase.ResultAPI, _ marketPID: Swift.String?) -> Swift.Void
@_hasMissingDesignatedInitializers public class Repayment : HIVEBase.Singletonable {
  public typealias T = HIVEIAPV4.Repayment
  public static var shared: HIVEIAPV4.Repayment.T
  public func initialize(completionHandler: HIVEIAPV4.RepaymentCompletionHandler?)
  @objc deinit
}
extension HIVEIAPV4.Repayment : HIVEUI.RepaymentServiceProtocol {
  public func onInquiry(url: Foundation.URL?)
  public func onVoidPurchase(pid: Swift.String)
  public func onGameStart()
}
@_inheritsConvenienceInitializers @objc(HiveRepayment) public class RepaymentInterface : ObjectiveC.NSObject {
  @objc public class func initialize(_ completionHandler: @escaping HIVEIAPV4.RepaymentCompletionHandler)
  @objc override dynamic public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers public class IAPV4Service {
  public class func setPromotePurchaseMarketPID(marketPID: Swift.String)
  public class func sendAllPurchaseLogData()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc(HIVEIAPV4) public class IAPV4Interface : ObjectiveC.NSObject {
  @objc public class func marketConnect(_ handler: HIVEIAPV4.IAPV4MarketConnectHandler?)
  @objc public class func getProductInfo(_ handler: HIVEIAPV4.IAPV4ProductInfoHandler?)
  @objc public class func purchase(_ marketPID: Swift.String, additionalInfo: Swift.String?, handler: HIVEIAPV4.IAPV4PurchaseHandler?)
  @objc public class func restore(_ handler: HIVEIAPV4.IAPV4RestoreHandler?)
  @objc public class func transactionFinish(_ marketPID: Swift.String, handler: HIVEIAPV4.IAPV4TransactionFinishHandler?)
  @objc public class func transactionMultiFinish(_ marketPIDList: [Swift.String], handler: HIVEIAPV4.IAPV4TransactionMultiFinishHandler?)
  @objc public class func checkPromotePurchase(_ handler: HIVEIAPV4.IAPV4CheckPromotePurchaseHandler?)
  @objc public class func getSubscriptionProductInfo(_ handler: HIVEIAPV4.IAPV4ProductInfoHandler?)
  @objc public class func purchaseSubscriptionUpdate(_ marketPid: Swift.String, oldMarketPid: Swift.String?, additionalInfo: Swift.String?, handler: HIVEIAPV4.IAPV4PurchaseHandler?)
  @objc public class func restoreSubscription(_ handler: HIVEIAPV4.IAPV4RestoreHandler?)
  @objc public class func iapGetVersion() -> Swift.String
  @objc public class func setPromotePurchaseHandler(_ handler: HIVEIAPV4.IAPV4CheckPromotePurchaseHandler?)
  @objc public class func setUid(_ uid: Swift.String?)
  @objc public class func getSelectedMarket() -> HIVEIAPV4.IAPV4Type
  @objc override dynamic public init()
  @objc deinit
}
@objc(HIVEIAPV4Type) public enum IAPV4Type : Swift.Int {
  case notSelected = 0
  case appStore = 1
  case google = 2
  case lebi = 3
  case oneStore = 4
  case amazon = 5
  case samsung = 6
  case huawei = 7
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers @objc(HIVEIAPV4Product) public class IAPV4Product : ObjectiveC.NSObject {
  @objc public var marketPid: Swift.String?
  @objc public var currency: Swift.String?
  @objc public var price: Swift.Double
  @objc public var title: Swift.String?
  @objc public var displayPrice: Swift.String?
  @objc public var productDescription: Swift.String?
  @objc public var formattedString: Swift.String?
  @objc public var productType: Swift.String?
  @objc override dynamic public var description: Swift.String {
    @objc get
  }
  @objc deinit
}
@_inheritsConvenienceInitializers @objc(HIVEIAPV4Receipt) public class IAPV4Receipt : ObjectiveC.NSObject {
  @objc public var type: HIVEIAPV4.IAPV4Type {
    @objc get
  }
  @objc public var product: HIVEIAPV4.IAPV4Product?
  @objc public var hiveiapReceipt: Swift.String?
  @objc public var bypassInfo: Swift.String?
  @objc public var additionalInfo: Swift.String?
  @objc public var transactionId: Swift.String?
  @objc override dynamic public var description: Swift.String {
    @objc get
  }
  public var privacySafeDescription: Swift.String {
    get
  }
  @objc override dynamic public init()
  @objc deinit
}
@objc @_inheritsConvenienceInitializers public class IAPV4ReceiptApple : HIVEIAPV4.IAPV4Receipt {
  @objc override public var type: HIVEIAPV4.IAPV4Type {
    @objc get
  }
  @objc override dynamic public init()
  @objc deinit
}
extension HIVEIAPV4.IAPV4Type : Swift.Equatable {}
extension HIVEIAPV4.IAPV4Type : Swift.Hashable {}
extension HIVEIAPV4.IAPV4Type : Swift.RawRepresentable {}
