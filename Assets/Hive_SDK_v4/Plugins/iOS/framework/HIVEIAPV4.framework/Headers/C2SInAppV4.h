#ifndef _C2SINAPP_V4_H_
#define _C2SINAPP_V4_H_

#ifdef __cplusplus
extern "C" {
#endif

// Market Id
const static int MARKET_LIST_NOT_SELECTED = 0;
const static int MARKET_LIST_APPLE_APPSTORE = 1;
const static int MARKET_LIST_GOOGLE_PLAYSTORE = 2;
const static int MARKET_LIST_HIVE_LEBI = 3;
const static int MARKET_LIST_ONESTORE = 4;
const static int MARKET_LIST_AMAZON_APPSTORE = 5;
const static int MARKET_LIST_SAMSUNG_GALAXYSTORE = 6;
const static int MARKET_LIST_HUAWEI_APPGALLERY = 7;

// IapV4Lifecycle Type
const static int IAPV4_LIFE_CYCLE_ON_SETUP_FINISHED = 0;
const static int IAPV4_LIFE_CYCLE_ON_INITIALIZE_FINISHED = 1;
const static int IAPV4_LIFE_CYCLE_ON_SIGN_IN = 2;
const static int IAPV4_LIFE_CYCLE_ON_SIGN_OUT = 3;
const static int IAPV4_LIFE_CYCLE_ON_PROCESS_URI = 4;
const static int IAPV4_LIFE_CYCLE_ON_RESET = 5;


// [ResultAPI]
typedef struct CS_IapV4Result {
	int result;
	int errorCode;
	const char* errorMessage;
} CS_IapV4Result;

// [ProductInfo]
// iOS : Apple
// Android : Google, OneStore, Lebi, Huawei, Amazon
typedef struct CS_IapV4ProductInfo {
    const char* productType;
	const char* marketPid;
	const char* currency;
	double price;
	const char* displayPrice;           //
	const char* title;
	const char* description;
	const char* originalJson;
	const char* displayOriginalPrice;
	double originalPrice;            //
	const char* iconUrl;
	int coinsReward;
} CS_IapV4ProductInfo;

// [ReceiptInfo]
typedef struct CS_IapV4ReceiptInfo {
	int type;
	const char* additionalInfo;
	const char* bypassInfo;
	const char* hiveiapReceipt;
	// ProductInfo
    const char* productType;
	const char* marketPid;
	const char* currency;
	double price;
	const char* displayPrice;           //
	const char* title;
	const char* description;
	const char* originalJson;
	const char* displayOriginalPrice;
	double originalPrice;            //
	const char* iconUrl;
    int coinsReward;
} CS_IapV4ReceiptInfo;

// [Callback]
typedef void (*MarketListCB)(CS_IapV4Result iapResult, int numOfMarketList, int* marketList);
typedef void (*ProductInfoCB)(CS_IapV4Result iapResult, int numOfProduct, CS_IapV4ProductInfo* productInfo);
typedef void (*ReceiptInfoCB)(CS_IapV4Result iapResult, CS_IapV4ReceiptInfo receiptInfo);
typedef void (*ReceiptListInfoCB)(CS_IapV4Result iapResult, int numOfReceipt, CS_IapV4ReceiptInfo* receiptInfo);
typedef void (*TransactionCB)(CS_IapV4Result iapResult, const char* marketPid);
typedef void (*BalanceCB)(CS_IapV4Result iapResult, int balance);
typedef void (*CheckPromotePurchaseCB)(CS_IapV4Result iapResult, const char* marketPid);
typedef void (*IapV4LifecycleCB)(int type, const char* data);

// [API]
void CS_IapV4MarketConnect(MarketListCB cb);
void CS_IapV4GetProductInfo(ProductInfoCB cb);
void CS_IapV4GetSubscriptionProductInfo(ProductInfoCB cb);
void CS_IapV4Purchase(const char* marketPid, const char* additionalInfo, ReceiptInfoCB cb);
void CS_IapV4PurchaseSubscription(const char* marketPid, const char* oldMarketPid, const char* additionalInfo, ReceiptInfoCB cb);
void CS_IapV4Restore(ReceiptListInfoCB cb);
void CS_IapV4RestoreSubscription(ReceiptListInfoCB cb);
void CS_IapV4TransactionFinish(const char* marketPid, TransactionCB cb);
//void CS_IapV4TransactionMultiFinish();		// not support
void CS_IapV4CheckPromotePurchase(CheckPromotePurchaseCB cb);
void CS_IapV4SetUid(const char* uid);
int CS_IapV4GetSelectedMarket();
/*
 * IAPv4 모듈 버전을 반환한다.
 */
const char* CS_IapV4GetVersion(void);

// [API] Android Only
void CS_IapV4ShowMarketSelection(MarketListCB cb);
void CS_IapV4GetBalanceInfo(BalanceCB cb);
void CS_IapV4ShowCharge(BalanceCB cb);
void CS_IapV4SetLifecycleCB(IapV4LifecycleCB cb);


#ifdef __cplusplus
};
#endif

#endif	// _C2SINAPP_V4_H_
