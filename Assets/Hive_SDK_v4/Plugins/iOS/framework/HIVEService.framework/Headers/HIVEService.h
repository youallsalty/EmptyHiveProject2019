//
//  HIVEService.h
//  HIVEService
//
//  Created by jhjang on 29/07/2019.
//  Copyright © 2019 gcp. All rights reserved.
//


#import <UIKit/UIKit.h>

//! Project version number for HIVEService.
FOUNDATION_EXPORT double HIVEServiceVersionNumber;

//! Project version string for HIVEService.
FOUNDATION_EXPORT const unsigned char HIVEServiceVersionString[];

#import "YTPlayerView.h"
