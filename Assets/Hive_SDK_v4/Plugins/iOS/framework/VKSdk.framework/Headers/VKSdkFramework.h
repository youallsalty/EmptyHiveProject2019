//
//  VKSdk.h
//  VKSdk
//
//  Created by jhjang on 08/08/2019.
//  Copyright © 2019 jhjang. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VKSdk.
FOUNDATION_EXPORT double VKSdkVersionNumber;

//! Project version string for VKSdk.
FOUNDATION_EXPORT const unsigned char VKSdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VKSdk/PublicHeader.h>
#import "VKSdk.h"
