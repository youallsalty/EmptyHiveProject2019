/**
 * @file    IAPV4.cs
 * 
 *  @date		2016-2019
 *  @copyright	Copyright © GAMEVIL COM2US PLATFORM Inc. All Right Reserved.
 *  @author		ryuvsken, hife
 *  @since		4.4.0
 * 
 */
using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;

/**
 * @defgroup IAPV4
 * @{
 */

namespace hive
{
	/**
	 * \~korean 
	 * HIVE IAP 는 마켓별 구매 프로세스의 개발 비용을 최소화 할 수 있고 구매 로그를 일관성 있게 수집하여 사업, 개발, 관리 부서에 필요한 정보를 제공할 수 있다.<br/> 
	 * 현재 HIVE IAP 가 제공하는 결제 마켓은 애플 앱스토어, 구글 플레이 스토어, 원스토어 (https://www.tstore.co.kr), HIVE 러비(Lebi) 을 제공한다.<br/>
	 * HIVE 러비(Lebi) 상점은 중국 시장에서 상점을 구성하고 상품을 구매 할 수 있는 시스템으로서 HIVE 에서 자체적으로 운영하고 있다.<br/><br/>
	 * \~english 
	 * HIVE IAP can minimize the development costs of the market-specific purchase process and collect purchase logs consistently to provide information to business, development, and management departments. <br/> * Currently, HIVE IAP supports Apple App Store, Google Play Store, One Store (https://www.tstore.co.kr) and HIVE Lobby (Lebi).<br/>
	 * HIVE LIVE (Lebi) store is a system that can organize stores and purchase goods in Chinese market. It is operated by HIVE itself.<br/><br/>
	 * \~
	* @since		4.4.0
	* @ingroup IAPV4
	* @author imsunghoon
	*/
	public class IAPV4 {

		/**
		 *  \~korean
		 * @brief HIVE IAPV4 에서 마켓의 구성을 위한 정보 조회 결과 통지
		 *
		 * @param result		API 호출 결과
		 * @param iapV4TypeList	사용 가능한 마켓 정보의 목록<br/>이 마켓 정보의 목록은 게임 클라이언트에서 사용 가능한 마켓을 의미한다.<br/>마켓 목록이 2개 이상이라면 유저가 상점을 선택할 수 있도록 게임에서 유저에게 노출할 수 있는 화면을 제공해야 한다
		 *
		 *  \~english
		 * @brief HIVE IAPV4 Market information request result callback
		 *
		 * @param result		Result of API call
		 * @param iapV4TypeList	사용 가능한 마켓 정보의 목록<br/>
		 * This list of market information means the market available on the game client.<br/>
		 * If there is more than one market list, you must provide a screen that display the available market list to the user so that the user can select a store. <br/>
		 *
		 *  \~
		 * @see #marketConnect(onIAPV4MarketInfo)
		 *
		 * @ingroup IAPV4
		 */
		public delegate void onIAPV4MarketInfo(ResultAPI result, List<IAPV4Type> iapV4TypeList);

		/**
		 *  \~korean
		 * HIVE IAPV4 상품 정보 조회 결과 통지
		 *
		 * @param result		    API 호출 결과
		 * @param iapV4ProductList  상품 정보
		 * @param balance		    HIVE Lebi 마켓의 경우 잔액 정보
		 *
		 *  \~english
		 * HIVE IAPV4 Product Information request callback
		 *
		 * @param result		    Result of API call
		 * @param iapV4ProductList  Product Information
		 * @param balance		    Balance information for HIVE Lebi Market If Lebi Market enabled.
		 *
		 *  \~
		 * @see getProductInfo(onIAPV4ProductInfo)
		 *
		 * @ingroup IAPV4
		 */
		public delegate void onIAPV4ProductInfo(ResultAPI result, List<IAPV4Product> iapV4ProductList, int balance);

		/**
		 *  \~korean
         * @brief 상품 결제 시도 결과 통지<br/>
         * HIVE IAPV4 의 상품 구매는 App Store, Google Store 에 등록된 마켓 상품 (marketPid) 을 요청하게 된다.<br/>
         * additionalInfo 의 경우 구매 요청에 대한 보충 정보로 구매 완료 콜백에 다시 포함되어 전송되지만<br/>
         * 프로모 코드 구매 처럼 앱 외부에서 구매 등의 경우에는 포함될 수 없게 된다.
		 *
		 * @param result			API 호출 결과
		 * @param iapV4Receipt  	구매 성공한 상품 정보 와 additionalInfo 를 포함한 정보
		 *
		 *  \~english
		 * @brief Purchase request result callback<br/>
         * The purchase of HIVE IAPV4 will require a marketPid registered in the App Store and Google Store.<br/>
         * additionalInfo is supplementary information for the purchase request, and is included in the purchase completion callback, <br/>
         * but, in case of purchasing outside of the app, such as a promo code purchase etc., it can not be included in iapV4Receipt.
		 *
		 * @param result			Result of API call
		 * @param iapV4Receipt  	It includes the purchased product information and additionalInfo.
		 *
		 *  \~
		 * @see #purchase(String, String, onIAPV4Purchase)
		 * @ingroup IAPV4
		 */
		public delegate void onIAPV4Purchase(ResultAPI result, IAPV4Receipt iapV4Receipt);

		/**
		 *  \~korean
		 * @brief HIVE IAPV4 상품에 대한 미지급된 아이템 지급을 요청 결과 통지<br/>
		 * 상품 구매 과정에서 여러가지 상황으로 인하여 아이템 지급을 실패하는 경우가 발생할 수 있다.<br/>
		 * 이런 상황을 대비하기 위해서 미 지급된 아이템 지급을 요청 해야 한다.
		 *
		 * @param result			API 호출 결과
		 * @param iapv4ReceiptList  		구매 성공한 상품 정보 와 additionalInfo 를 포함한 정보
		 *
		 *  \~english
		 * @brief HIVE IAPV4 Restore request result callback<br/>
		 * It may happen that the item delivery fails due to various situations in the product purchase process.<br/>
		 * In order to prepare for this situation, you should request to deliver the item that has not been delivered.
		 *
		 * @param result			Result of API call
		 * @param iapv4ReceiptList  		It includes the purchased product information and additionalInfo
		 *
		 *  \~
		 * @see #restore(onIAPV4Restore)
		 *
		 * @ingroup IAPV4
		 */
		public delegate void onIAPV4Restore(ResultAPI result, List<IAPV4Receipt> iapv4ReceiptList);

		/**
		 *  \~korean
		 * @brief HIVE IAP v4에서 상품에 대한 지급 완료 후 결제 트랜잭션을 종료하기 위한 요청 결과 통지<br/>
		 *
		 * @param result		API 호출 결과
		 * @param marketPid	트랜잭션이 종료된 상품의 marketPid
		 *
		 *  \~english
		 * @brief HIVE IAP v4 Transaction Finish request result callback<br/>
		 *
		 * @param result		Result of API call
		 * @param marketPid	 marketPid of the product whose transaction ended
		 *  
		 *  \~
		 * @see #transactionFinish(String, onIAPV4TransactionFinish)
		 *
		 * @ingroup IAPV4
		 */
		public delegate void onIAPV4TransactionFinish(ResultAPI result, String marketPid);

		/**
		 *  \~korean
		 * @brief HIVE IAP v4에서 상품에 대한 지급 완료 후 복수 개의 결제 트랜잭션을 종료하기 위한 요청 결과 통지<br/>
		 *
		 * @param resultList		트랜잭션의 종료 결과 목록
		 * @param marketPidList		트랜잭션이 종료된 상품의 marketPid 목록
		 *
		 *  \~english
		 * @brief HIVE IAP v4 Multiple Transaction Finish request result callback<br/>
		 *
		 * @param resultList		Result list of the transaction end
		 * @param marketPidList		marketPid list of the transaction end
		 *
		 *  \~
		 * @see #transactionMultiFinish(List, onIAPV4TransactionMultiFinish)
		 */
		public delegate void onIAPV4TransactionMultiFinish(List<ResultAPI> resultList, List<String> marketPidList);

#if !UNITY_EDITOR && UNITY_ANDROID

		/**
		 *  \~korean
		 * @brief HIVE 러비 상점일 경우 잔액 확인 결과 통지
		 *
		 * @param result		API 호출 결과
		 * @param balance		HIVE 러비 상점일 경우 잔액
		 *
		 *  \~english
		 * @brief HIVE Lebi Balance request result callback, if it is a Rebi store.
		 *
		 * @param result		Result of API call
		 * @param balance		HIVE Lebi balance
		 *
		 *  \~
		 * @ingroup IAPV4
		 */
		public delegate void onIAPV4Balance(ResultAPI result, int balance);

#endif

		public delegate void onIAPV4CheckPromotePurchase(ResultAPI result, String marketPid);


		/**
		*  \~korean
		* @brief IAPV4 결재 API 초기화 요청<br/>
		* HIVE IAPV4 의 초기화를 수행하면 결과 통지시 사용 가능한 마켓 정보의 목록을 전달하게 된다.<br/>
		* 이 마켓 정보의 목록은 게임 클라이언트에서 사용 가능한 마켓을 의미한다.
		*
		* @param listener	API 결과 통지
		*
		*  \~english
		* @brief IAPV4 It initialize IAPV4 Purchase module.<br/>
		* When HIVE IAPV4 is initialized, it sends a list of available market information in the result callback.<br/>
		* This list of market information means the market available on the game client.
		*
		* @param listener	Result of API call
		*
		*  \~
		* @ingroup IAPV4
		*/
		public static void marketConnect(onIAPV4MarketInfo listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "marketConnect", listener);

			HIVEUnityPlugin.callNative (jsonParam);	
		}

		/**
		*  \~korean
		* @brief HIVE IAPV4 에서 상품의 구성을 위한 정보를 조회할 수 있다. <br/>
		*
		* @param listener			API 결과 통지
		* 
		*  \~english
		* @brief HIVE IAPV4 It returns information for composition of the product.<br/>
		*
		* @param listener			Result of API call
		* 
		*  \~
		* @ingroup IAPV4
		*/
		public static void getProductInfo(onIAPV4ProductInfo listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "getProductInfo", listener);

			HIVEUnityPlugin.callNative (jsonParam);	
		}


		/**
		*  \~korean
		* @brief HIVE IAPV4 에서 구독형 상품의 구성을 위한 정보를 조회할 수 있다. <br/>
		*
		* @param listener			API 결과 통지
		* 
		*  \~english
		* @brief HIVE IAPV4 It returns information for composition of the product.<br/>
		*
		* @param listener			Result of API call
		* 
		*  \~
		* @ingroup IAPV4
		* @since 4.11.6
		*/
		public static void getSubscriptionProductInfo(onIAPV4ProductInfo listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "getSubscriptionProductInfo", listener);

			HIVEUnityPlugin.callNative (jsonParam);	
		}

		/**
		*  \~korean
		* @brief HIVE IAPV4 의 상품 구매는 App Store, Google Store 에 등록된 마켓 상품 (marketPid) 을 요청하게 된다.<br/>
		* additionalInfo 의 경우 구매 요청에 대한 보충 정보로 구매 완료 콜백에 다시 포함되어 전송되지만<br/>
		* 프로모 코드 구매 처럼 앱 외부에서 구매 등의 경우에는 포함될 수 없게 된다.
		*
		* @param marketPid			App Store, Google Store 에 등록된 하나의 상품을 지칭하는 고유 코드
		* @param additionalInfo	구매 요청에 대한 보충 정보를 포함한 문자열로, 개발자가 지정합니다.
		* @param listener			API 결과 통지
		*
		*  \~english
		* @brief HIVE IAPV4 The purchase will require a marketPid from the App Store and Google Store.<br/>
		* additionalInfo is supplementary information for the purchase request, and is included in the purchase completion callback, <br/>
        * but, in case of purchasing outside of the app, such as a promo code purchase etc., it can not be included in iapV4Receipt.
		*
		* @param marketPid			Unique code registered on the App Store or Google Store.
		* @param additionalInfo	 A string containing supplementary information about the purchase request, specified by the developer.
		* @param listener			Result of API call
		*
		*  \~
		* @ingroup IAPV4
		*/
		public static void purchase(String marketPid, String additionalInfo, onIAPV4Purchase listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "purchase", listener);
			jsonParam.AddField ("marketPid", marketPid);
			jsonParam.AddField ("additionalInfo", additionalInfo);

			HIVEUnityPlugin.callNative (jsonParam);	
		}
		
		/**
		*  \~korean
		* @brief HIVE IAPV4 의 상품 구매는 App Store, Google Store 에 등록된 구독 상품 (marketPid) 을 요청하게 된다.<br/>
		* additionalInfo 의 경우 구매 요청에 대한 보충 정보로 구매 완료 콜백에 다시 포함되어 전송되지만<br/>
		* 프로모 코드 구매 처럼 앱 외부에서 구매 등의 경우에는 포함될 수 없게 된다.
		* Android타겟의 경우 구독형 상품을 업그레이드 하거나 다운그레이드를 수행하는 경우 기존에 구독중인 상품의 고유코드 oldMarketPid를 필수로 넣어야한다.
		*
		* @param marketPid			App Store, Google Store 에 등록된 하나의 상품을 지칭하는 새로운 구독 상품의 고유 코드
		* @param oldMarketPid		App Store, Google Store 에 등록된 하나의 상품을 지칭하는 기존 구독 상품의 고유 코드
		* @param additionalInfo	구매 요청에 대한 보충 정보를 포함한 문자열로, 개발자가 지정합니다.
		* @param listener			API 결과 통지
		*
		*  \~english
		* @brief HIVE IAPV4 The purchase will require a marketPid from the App Store and Google Store.<br/>
		* additionalInfo is supplementary information for the purchase request, and is included in the purchase completion callback, <br/>
        * but, in case of purchasing outside of the app, such as a promo code purchase etc., it can not be included in iapV4Receipt.
		*
		* @param marketPid			Unique code registered on the App Store or Google Store.
		* @param oldMarketPid		Unique code registered on the App Store or Google Store.
		* @param additionalInfo	 A string containing supplementary information about the purchase request, specified by the developer.
		* @param listener			Result of API call
		*
		*  \~
		* @ingroup IAPV4
		* @since 4.11.6
		*/
		public static void purchaseSubscriptionUpdate(String marketPid, String oldMarketPid, String additionalInfo, onIAPV4Purchase listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "purchaseSubscriptionUpdate", listener);
			jsonParam.AddField ("marketPid", marketPid);
			jsonParam.AddField ("oldMarketPid", oldMarketPid);
			jsonParam.AddField ("additionalInfo", additionalInfo);

			HIVEUnityPlugin.callNative (jsonParam);	
		}

		/**
		*  \~korean
		* @brief HIVE IAPV4 상품에 대한 미지급된 아이템 지급을 요청한다.<br/>
		* 상품 구매 과정에서 여러가지 상황으로 인하여 아이템 지급을 실패하는 경우가 발생할 수 있다.<br/>
		* 이런 상황을 대비하기 위해서 미 지급된 아이템 지급을 요청 해야 한다.<br/>
		*
		* @param listener			API 결과 통지
		*
		*  \~english
		* @brief HIVE IAPV4  It restore the item that has not been delivered.<br/>
		* It may happen that the item delivery fails due to various situations in the product purchase process.<br/>
		* In order to prepare for this situation, you should request to deliver the item that has not been delivered.
		*
		* @param listener			API 결과 통지
		*
		*  \~
		* @ingroup IAPV4
		*/
		public static void restore(onIAPV4Restore listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "restore", listener);

			HIVEUnityPlugin.callNative (jsonParam);	
		}

		/**
		*  \~korean
		* @brief HIVE IAPV4 상품에 대한 미지급된 구독 상품 지급을 요청한다.<br/>
		* 상품 구매 과정에서 여러가지 상황으로 인하여 구독 상태를 갱신하지 못할 수 있다.<br/>
		* 구독 상태를 확인할 수 없는 경우, 구독 상태 복구를 요청 해야 한다.<br/>
		*
		* @param listener			API 결과 통지
		*
		*  \~english
		* @brief HIVE IAPV4  It restore the item that has not been delivered.<br/>
		* 
		* 
		*
		* @param listener            Result of API call
		*
		*  \~
		* @ingroup IAPV4
		* @since 4.11.6
		*/
		public static void restoreSubscription(onIAPV4Restore listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "restoreSubscription", listener);

			HIVEUnityPlugin.callNative (jsonParam);	
		}

		/**
		*  \~korean
		* @brief HIVE IAP v4에서 지급이 완료된 상품에 대한 구매 영수증에 대한 트랜잭션 완료 처리를 요청한다.<br/>
		* 구매나 복구 기능 사용 후 아이템 지급이 완료된 시점에 이 API를 호출하여 마켓에 구매 완료 처리를 해야한다.<br/>
	 	* 
		* @warning 만약, 지급 완료 후에도 마켓으로의 구매 완료 처리를 하지 않으면, 미지급된 상품으로 남는다.<br/>
		* 미지급된 상품은 transactionFinish API 호출 전까지 계속 복구가 되므로, 영수증 중복 여부를 확인해야 한다.<br/>
		* 
		* @param marketPid	지급이 완료되어 트랜잭션 완료 처리를 위한 상품의 고유 ID
		* @param listener	API 결과 통지
		*  
		*  \~english
		* @brief HIVE IAP v4 It requests the transaction completion processing for the purchase receipt of the item that have been delivered.<br/>
		* At the completion of item delivery after purchasing or restoring, this API should be called to complete the purchase on the market.<br/>
	 	* 
		* @warning If you do not notify the completion of the purchase to the market after payment has been made, it will be left as an unpaid item..<br/>
		* Since the unpaid items will continue to be recovered until the transactionFinish API call, you should check for duplicate receipts.<br/>
		* 
		* @param marketPid	The unique ID of the item for which the transaction is to be completed.
		* @param listener	Result of API call
		*
		*  \~
		* @see onIAPV4TransactionFinish
		* @see #transactionMultiFinish(List, onIAPV4TransactionMultiFinish)
		* 
		* @ingroup IAPV4
		*/
		public static void transactionFinish(String marketPid, onIAPV4TransactionFinish listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "transactionFinish", listener);
			jsonParam.AddField ("marketPid", marketPid);

			HIVEUnityPlugin.callNative (jsonParam);	
		}

		/**
		*  \~korean
		* @brief HIVE IAP v4에서 지급이 완료된 상품에 대한 구매 영수증에 대한 복수 개의 트랜잭션 완료 처리를 요청한다.<br/>
		* 구매나 복구 기능 사용 후 아이템 지급이 완료된 시점에 이 API를 호출하여 마켓에 구매 완료 처리를 해야한다.<br/>
		* 
		* @warning 만약, 지급 완료 후에도 마켓으로의 구매 완료 처리를 하지 않으면, 미지급된 상품으로 남는다.<br/>
		* 미지급된 상품은 transactionFinish API 호출 전까지 계속 복구가 되므로, 영수증 중복 여부를 확인해야 한다.<br/>
		* 
		* @param marketPidList	지급이 완료되어 트랜잭션 완료 처리를 위한 상품의 고유 ID 목록
		* @param listener			API 결과 통지
		* 
		*  \~english
		* @brief HIVE IAP v4 It requests the multiple transaction completion processing for the purchase receipt of the item that have been delivered.<br/>
		* At the completion of item delivery after purchasing or restoring, this API should be called to complete the purchase on the market.<br/>
	 	* 
		* @warning If you do not notify the completion of the purchase to the market after payment has been made, it will be left as an unpaid item..<br/>
		* Since the unpaid items will continue to be recovered until the transactionFinish API call, you should check for duplicate receipts.<br/>
		* 
		* @param marketPidList	The list of unique ID of the item for which the transaction is to be completed.
		* @param listener	Result of API call
		* 
		*  \~
		* @see onIAPV4TransactionMultiFinish
		* @see #transactionFinish(String, onIAPV4TransactionFinish)
		* 
		* @ingroup IAPV4
		*/
		public static void transactionMultiFinish(List<String> marketPidList, onIAPV4TransactionMultiFinish listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "transactionMultiFinish", listener);

			JSONObject jsonArray = new JSONObject();

			if(marketPidList != null) {
				foreach(String marketPid in marketPidList) {

					jsonArray.Add(marketPid.ToString());
				}
			}

			jsonParam.AddField ("marketPidList", jsonArray);

			HIVEUnityPlugin.callNative (jsonParam);	
		}

#if !UNITY_EDITOR && UNITY_ANDROID

		/**
		*  \~korean
		* @brief 러비 상점이나 구글 상점을 선택하기 위한 창을 띄운다.<br/>
		* 만약 Google Store 와 HIVE 러비(Lebi) 상점을 동시에 사용 가능한 상황이라면 유저가 상점 선택을 할 수 있도록 상점 선택 화면을 노출해야 한다
		*
		* @param listener			API 결과 통지
		*
		*  \~english
		* @brief It shows a UI to select a Lebi store or a Google store..<br/>
		* If the Google Store and the HIVE Lebi store are available at the same time, you should expose the store selection screen to allow the user to select a store.
		*
		* @param listener			Result of API call
		* 
		*  \~
		* @ingroup IAPV4
		*/
		public static void showMarketSelection(onIAPV4MarketInfo listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "showMarketSelection", listener);

			HIVEUnityPlugin.callNative (jsonParam);	
		}

		/**
		*  \~korean
		* @brief 러비 상점일 경우 잔액 정보 조회.
		*
		* @param listener			API 결과 통지
		*
		*  \~english
		* @brief It request Lebi balance information.
		*
		* @param listener			Result of API call
		*
		*  \~
		* @ingroup IAPV4
		*/
		public static void getBalanceInfo(onIAPV4Balance listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "getBalanceInfo", listener);

			HIVEUnityPlugin.callNative (jsonParam);	
		}

		/**
		*  \~korean
		* @brief 러비 충전 페이지 노출
		*
		* @param listener			API 결과 통지
		* 
		*  \~english
		* @brief It shows Lebi refill page.
		*
		* @param listener			Result of API call
		*
		*  \~
		* @ingroup IAPV4
		*/
		public static void showCharge(onIAPV4Balance listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "showCharge", listener);

			HIVEUnityPlugin.callNative (jsonParam);	
		}
#endif	

		public static void checkPromotePurchase(onIAPV4CheckPromotePurchase listener) {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "checkPromotePurchase", listener);

			HIVEUnityPlugin.callNative (jsonParam);	
		}	


		public static IAPV4Type getSelectedMarket() {

			JSONObject jsonParam = HIVEUnityPlugin.createParam("IAPV4", "getSelectedMarket", null);

			JSONObject resJsonObject = HIVEUnityPlugin.callNative (jsonParam);

			IAPV4Type iapV4Type = IAPV4Type.NOT_SELECTED;
			string iapV4TypeName = null;
			resJsonObject.GetField (ref iapV4TypeName, "iapv4Type");

			if (iapV4TypeName != null) {
				iapV4Type = getIAPV4Type(iapV4TypeName);
			} else {
				iapV4Type = IAPV4Type.NOT_SELECTED;
			}
			
			return iapV4Type;
		}


		public static IAPV4Type getIAPV4Type(String iapV4TypeName) {

			if ("NOT_SELECTED".Equals (iapV4TypeName)) {
				return IAPV4Type.NOT_SELECTED;
			}
			else if ("APPLE_APPSTORE".Equals (iapV4TypeName)) {
				return IAPV4Type.APPLE_APPSTORE;
			}
			else if ("GOOGLE_PLAYSTORE".Equals (iapV4TypeName)) {
				return IAPV4Type.GOOGLE_PLAYSTORE;
			}
			else if ("HIVE_LEBI".Equals (iapV4TypeName)) {
				return IAPV4Type.HIVE_LEBI;
			}
			else if ("ONESTORE".Equals (iapV4TypeName)) {
				return IAPV4Type.ONESTORE;
			}
			else if ("AMAZON_APPSTORE".Equals (iapV4TypeName)) {
				return IAPV4Type.AMAZON_APPSTORE;
			}
			else if ("SAMSUNG_GALAXYSTORE".Equals (iapV4TypeName)) {
				return IAPV4Type.SAMSUNG_GALAXYSTORE;
			}
			else if ("HUAWEI_APPGALLERY".Equals (iapV4TypeName)) {
				return IAPV4Type.HUAWEI_APPGALLERY;
			}

			return IAPV4Type.GOOGLE_PLAYSTORE;
		}

		public static IAPV4Receipt IAPV4ReceiptJsonTo4Receipt(JSONObject iapV4ReceiptJson) {

			if (iapV4ReceiptJson == null)
				return null;

			IAPV4Receipt iapV4Receipt = null;

			String iapV4TypeStr = "";
			iapV4ReceiptJson.GetField(ref iapV4TypeStr, "type");
			IAPV4Type type = getIAPV4Type(iapV4TypeStr);
			if (type == IAPV4Type.APPLE_APPSTORE) {					

				IAPV4ReceiptApple iapV4ReceiptApple = new IAPV4ReceiptApple(iapV4ReceiptJson);
				iapV4Receipt = (IAPV4Receipt) iapV4ReceiptApple;
			}
			else if (type == IAPV4Type.GOOGLE_PLAYSTORE) {

				IAPV4ReceiptGoogle iapV4ReceiptGoogle = new IAPV4ReceiptGoogle(iapV4ReceiptJson);
				iapV4Receipt = (IAPV4Receipt) iapV4ReceiptGoogle;
			}
			else if (type == IAPV4Type.ONESTORE) {

				IAPV4ReceiptOneStore iapV4ReceiptOneStore = new IAPV4ReceiptOneStore(iapV4ReceiptJson);
				iapV4Receipt = (IAPV4Receipt) iapV4ReceiptOneStore;
			}
			else if (type == IAPV4Type.HIVE_LEBI) {

				IAPV4ReceiptLebi iapV4ReceiptLebi = new IAPV4ReceiptLebi(iapV4ReceiptJson);
				iapV4Receipt = (IAPV4Receipt) iapV4ReceiptLebi;
			}
			else if (type == IAPV4Type.AMAZON_APPSTORE) {

				IAPV4ReceiptAmazon iapV4ReceiptAmazon = new IAPV4ReceiptAmazon(iapV4ReceiptJson);
				iapV4Receipt = (IAPV4Receipt) iapV4ReceiptAmazon;
			}
			else if (type == IAPV4Type.SAMSUNG_GALAXYSTORE) {

                IAPV4ReceiptSamsung IAPV4ReceiptSamsung = new IAPV4ReceiptSamsung(iapV4ReceiptJson);
                iapV4Receipt = (IAPV4Receipt) IAPV4ReceiptSamsung;
            }
			else if (type == IAPV4Type.HUAWEI_APPGALLERY) {

				IAPV4ReceiptHuawei iapV4ReceiptHuawei = new IAPV4ReceiptHuawei(iapV4ReceiptJson);
				iapV4Receipt = (IAPV4Receipt) iapV4ReceiptHuawei;
			}

			return iapV4Receipt;
		}

		public static void executeEngine(JSONObject resJsonObject) {

			String methodName = null;
			resJsonObject.GetField (ref methodName, "method");

			int handlerId = -1;
			resJsonObject.GetField (ref handlerId, "handler");
			object handler = (object)HIVEUnityPlugin.popHandler (handlerId);

			if (handler == null) return;

			if ("marketConnect".Equals (methodName)
#if !UNITY_EDITOR && UNITY_ANDROID			
			|| "showMarketSelection".Equals (methodName)
#endif
			) {

				List<IAPV4Type> iapV4TypeList = new List<IAPV4Type> ();

				JSONObject jsonArray = resJsonObject.GetField ("iapV4TypeList");
				if (jsonArray != null && jsonArray.IsArray) {
					List<JSONObject> jsonList = jsonArray.list;
					foreach (JSONObject jsonItem in jsonList) {

						String iapV4TypeName = jsonItem.str;
						IAPV4Type iapV4Type = getIAPV4Type(iapV4TypeName);

						iapV4TypeList.Add(iapV4Type);
					}
				}

				onIAPV4MarketInfo listener = (onIAPV4MarketInfo)handler;
				listener (new ResultAPI (resJsonObject.GetField ("resultAPI")), iapV4TypeList);
			}
			else if ("getProductInfo".Equals (methodName) || "getSubscriptionProductInfo".Equals (methodName)) {

				List<IAPV4Product> iapV4ProductList = new List<IAPV4Product> ();

				JSONObject jsonArray = resJsonObject.GetField ("iapV4ProductList");
				if (jsonArray != null && jsonArray.IsArray) {
					List<JSONObject> jsonList = jsonArray.list;
					foreach(JSONObject jsonItem in jsonList) {
						IAPV4Product iapV4Product = new IAPV4Product(jsonItem);

						iapV4ProductList.Add(iapV4Product);
					}
				}

				int balance = 0;
				resJsonObject.GetField (ref balance, "balance");

				onIAPV4ProductInfo listener = (onIAPV4ProductInfo)handler;
				listener (new ResultAPI (resJsonObject.GetField ("resultAPI")), iapV4ProductList, balance);
			}
			else if ("purchase".Equals (methodName) || "purchaseSubscriptionUpdate".Equals (methodName)) {

				JSONObject iapV4ReceiptJson = resJsonObject.GetField("iapV4Receipt");					
				IAPV4Receipt iapV4Receipt = IAPV4ReceiptJsonTo4Receipt(iapV4ReceiptJson);

				onIAPV4Purchase listener = (onIAPV4Purchase)handler;
				listener (new ResultAPI (resJsonObject.GetField ("resultAPI")), iapV4Receipt);
			}
			else if ("restore".Equals (methodName) || "restoreSubscription".Equals (methodName)) {

				List<IAPV4Receipt> iapv4ReceiptList = new List<IAPV4Receipt> ();

				JSONObject jsonArray = resJsonObject.GetField ("iapv4ReceiptList");
				if (jsonArray != null && jsonArray.IsArray) {
					List<JSONObject> iapV4ReceiptJsonList = jsonArray.list;
					foreach(JSONObject iapV4ReceiptJson in iapV4ReceiptJsonList) {

						IAPV4Receipt iapV4Receipt = IAPV4ReceiptJsonTo4Receipt(iapV4ReceiptJson);

						iapv4ReceiptList.Add(iapV4Receipt);
					}
				}

				onIAPV4Restore listener = (onIAPV4Restore)handler;
				listener (new ResultAPI (resJsonObject.GetField ("resultAPI")), iapv4ReceiptList);
			}
			else if ("transactionFinish".Equals (methodName)) {

				String marketPid = "";
				resJsonObject.GetField (ref marketPid, "marketPid");

				onIAPV4TransactionFinish listener = (onIAPV4TransactionFinish)handler;
				listener (new ResultAPI (resJsonObject.GetField ("resultAPI")), marketPid);
			}
			else if ("transactionMultiFinish".Equals (methodName)) {

				List<ResultAPI> resultList = new List<ResultAPI> ();

				JSONObject jsonArray = resJsonObject.GetField ("resultList");
				if (jsonArray != null && jsonArray.IsArray) {
					List<JSONObject> jsonList = jsonArray.list;
					foreach(JSONObject jsonItem in jsonList) {
						ResultAPI result = new ResultAPI(jsonItem);

						resultList.Add(result);
					}
				}

				List<String> marketPidList = new List<String> ();

				JSONObject jsonArray2 = resJsonObject.GetField ("marketPidList");
				if (jsonArray2 != null && jsonArray2.IsArray) {
					List<JSONObject> jsonList = jsonArray2.list;
					foreach(JSONObject jsonItem in jsonList) {

						String marketPid = jsonItem.str;
						marketPidList.Add(marketPid);
					}
				}

				onIAPV4TransactionMultiFinish listener = (onIAPV4TransactionMultiFinish)handler;
				listener (resultList, marketPidList);
			}
#if !UNITY_EDITOR && UNITY_ANDROID
			else if ("getBalanceInfo".Equals (methodName)
				|| "showCharge".Equals (methodName)) {
					
				int balance = 0;
				resJsonObject.GetField (ref balance, "balance");

				onIAPV4Balance listener = (onIAPV4Balance)handler;
				listener (new ResultAPI (resJsonObject.GetField ("resultAPI")), balance);
			}
#endif
			else if ("checkPromotePurchase".Equals (methodName)) {
					
				String marketPid = "";
				resJsonObject.GetField (ref marketPid, "marketPid");

				onIAPV4CheckPromotePurchase listener = (onIAPV4CheckPromotePurchase)handler;
				listener (new ResultAPI (resJsonObject.GetField ("resultAPI")), marketPid);
			}
		}

		public enum IAPV4Type {

			NOT_SELECTED = 0,
			APPLE_APPSTORE = 1,			///< Apple App Store
			GOOGLE_PLAYSTORE = 2,		///< Google Play Store
			HIVE_LEBI = 3,				///< HIVE Lebi
			ONESTORE = 4,				///< OneStore
			AMAZON_APPSTORE = 5,		///< Amazon App Store
			SAMSUNG_GALAXYSTORE = 6,	///< Samsung Galaxy Store
			HUAWEI_APPGALLERY = 7		///< Huawei App Gallery
		}

		public class IAPV4Product {

			public String productType;				///< \~korean consumable, subscription	 \~english 	consumable, subscription
			public String marketPid;				///< \~korean 마켓에 등록된 고유 상품 ID	 \~english 	Unique product ID registered in Market
			public String currency;					///< \~korean 통화 코드 (ex, KRW / USD)	 \~english 	Currency code (ex, KRW / USD)	
			public double price;					///< \~korean 상품 가격		 \~english 	Product price
			public String displayPrice;				///< \~korean 상품 가격 문자열 (ex. $100.00)		 \~english 	Product price string (ex. $100.00)
			public String title;					///< \~korean 상품 타이틀		 \~english 	Item Title
			public String productDescription;		///< \~korean 상품 설명		 \~english 	Product Description
			
			public String originalJson;

			//Price before Promotion Sale
			public String displayOriginalPrice;		/// 세일전 가격 문자열 Google Only 
			public double originalPrice;			/// 세일전 가격 Google Only 
			public String iconURL;					/// 상품 아이콘 (512x512) Google Only 

			// for Amazon
        	public int coinsReward;					/// 구매시 추가 지급 코인량

			/**
			* 32-bit PNG (with alpha)
			*
			* Dimensions: 512px by 512px
			*
			* Maximum file size: 1024KB
			*/

			public IAPV4Product() {
			}

			public IAPV4Product(String productType
							, String marketPid, String currency
							, double price, String displayPrice
							, String title, String productDescription
							, String originalJson) {

				this.productType = productType;
				this.marketPid = marketPid;
				this.currency = currency;
				this.price = price;
				this.displayPrice = displayPrice;
				this.title = title;
				this.productDescription = productDescription;
				this.originalJson = originalJson;
			}

			public IAPV4Product(JSONObject resJsonParam) {

				if(resJsonParam == null)
					return;

				resJsonParam.GetField (ref this.productType, "productType");
				resJsonParam.GetField (ref this.marketPid, "marketPid");
				resJsonParam.GetField (ref this.currency, "currency");
				resJsonParam.GetField (ref this.price, "price");
				resJsonParam.GetField (ref this.displayPrice, "displayPrice");
				resJsonParam.GetField (ref this.title, "title");
				resJsonParam.GetField (ref this.productDescription, "productDescription");
				
				resJsonParam.GetField (ref this.originalJson, "originalJson");

				resJsonParam.GetField (ref this.displayOriginalPrice, "displayOriginalPrice");
				resJsonParam.GetField (ref this.originalPrice, "originalPrice");
				resJsonParam.GetField (ref this.iconURL, "iconURL");

				resJsonParam.GetField (ref this.coinsReward, "coinsReward");
			}

			public String toString() {

				StringBuilder sb = new StringBuilder();

				sb.Append("IAPV4Product { marketPid = ");
				sb.Append(marketPid);
				sb.Append(", currency = ");
				sb.Append(currency);
				sb.Append(", price = ");
				sb.Append(price);
				sb.Append(", displayPrice = ");
				sb.Append(displayPrice);
				sb.Append(", title = ");
				sb.Append(title);
				sb.Append(", productDescription = ");
				sb.Append(productDescription);
				sb.Append(", originalJson = ");
				sb.Append(originalJson);
				sb.Append (" }\n");

				return sb.ToString();
			}
		}

		public class IAPV4Receipt {

			public IAPV4Type type;				///< \~korean 영수증 마켓 정보	\~english  Receipt Market Information
			public IAPV4Product product;		///< \~korean 구매 성공한 상품 정보	\~english  Purchased product information.
			public String additionalInfo;		///< \~korean 구매 요청시 추가로 입력받은 additionalInfo	\~english  AdditionalInfo entered in purchase request
			
			public String hiveiapReceipt;		///< \~korean bypassInfo가 IAP서버 영수증 검증 요청시 보낼 데이터	\~english  Receipt Data to send when requesting validation to IAP server
			public String bypassInfo;			///< \~korean 게임 쪽에서 영수증 검증 서버로 바로 전달(bypass)하는 json 데이터	\~english  Json data bypassed from game to receipt verification server
			
			public IAPV4Receipt() {}

			public IAPV4Receipt(IAPV4Type type, IAPV4Product product, String additionalInfo, String bypassInfo) {

				this.type = type;
				this.product = product;
				this.additionalInfo = additionalInfo;
				this.bypassInfo = bypassInfo;
			}

			public IAPV4Receipt(JSONObject resJsonParam) {

				if(resJsonParam == null)
					return;

				String iapV4TypeName = "";
				resJsonParam.GetField(ref iapV4TypeName, "type");
				this.type = getIAPV4Type(iapV4TypeName);

				JSONObject productJson = resJsonParam.GetField("product");
				this.product = new IAPV4Product(productJson);
				
				resJsonParam.GetField (ref this.additionalInfo, "additionalInfo");
				resJsonParam.GetField (ref this.hiveiapReceipt, "hiveiapReceipt");
				resJsonParam.GetField (ref this.bypassInfo, "bypassInfo");
			}

			public String toString() {

				StringBuilder sb = new StringBuilder();

				sb.Append("IAPV4Receipt { type = ");
				sb.Append(type.ToString());
				sb.Append(", product = ");
				sb.Append(product.toString());
				sb.Append(", additionalInfo = ");
				sb.Append(additionalInfo);
				sb.Append(", hiveiapReceipt = ");
				sb.Append(hiveiapReceipt);
				sb.Append(", bypassInfo = ");
				sb.Append(bypassInfo);
				sb.Append (" }\n");

				return sb.ToString();
			}
		}

		public class IAPV4ReceiptApple : IAPV4Receipt {
			String originReceipt;

			public IAPV4ReceiptApple() {}

			public IAPV4ReceiptApple(JSONObject receiptJson) 
			: base(receiptJson) {

				originReceipt = receiptJson.ToString();
			}

			public String toString() {
				StringBuilder builder = new StringBuilder();
				builder.Append("[IAPV4ReceiptApple]")
						.Append("\n    type: " + type.ToString())
						.Append("\n    product: " + product.toString())
						.Append("\n    additionalInfo: " + additionalInfo)
						.Append("\n")
						.Append("\n    hiveiapReceipt: " + hiveiapReceipt)
						.Append("\n")
						.Append("\n    originReceipt: " + originReceipt)
						.Append("\n")
						.Append("\n bypassInfo: " + bypassInfo)
						.Append("\n");
				return builder.ToString();
			}
		}

		public class IAPV4ReceiptGoogle : IAPV4Receipt {

			public String purchaseData;
			public String signature;

			public String itemType;  // ITEM_TYPE_INAPP or ITEM_TYPE_SUBS
			public String orderId;
			public String packageName;
			public String sku;
			public long purchaseTime;
			public int purchaseState;
			public String developerPayload;
			public String token;
			public String vid;
			public String gameCurrency;
			public double gamePrice;
			public String serverId;

			public IAPV4ReceiptGoogle() {}

			public IAPV4ReceiptGoogle(JSONObject receiptJson) 
			: base(receiptJson) {

				receiptJson.GetField(ref this.purchaseData, "purchaseData");
				receiptJson.GetField(ref this.signature, "signature");

				receiptJson.GetField(ref this.itemType, "itemType");
				receiptJson.GetField(ref this.orderId, "orderId");
				receiptJson.GetField(ref this.packageName, "packageName");
				receiptJson.GetField(ref this.sku, "sku");
				receiptJson.GetField(ref this.purchaseTime, "purchaseTime");
				receiptJson.GetField(ref this.purchaseState, "purchaseState");
				receiptJson.GetField(ref this.developerPayload, "developerPayload");
				receiptJson.GetField(ref this.token, "token");
				receiptJson.GetField(ref this.vid, "vid");
				receiptJson.GetField(ref this.gameCurrency, "gameCurrency");
				receiptJson.GetField(ref this.gamePrice, "gamePrice");
				receiptJson.GetField(ref this.serverId, "serverId");
			}

			public String toString() {

				StringBuilder builder = new StringBuilder();
				builder.Append("[IAPV4ReceiptGoogle]")
						.Append("\n    type: " + type.ToString())
						.Append("\n    product: " + product.toString())
						.Append("\n    additionalInfo: " + additionalInfo)
						.Append("\n")
						.Append("\n    hiveiapReceipt: " + hiveiapReceipt)
						.Append("\n")
						.Append("\n    purchaseData: " + purchaseData)
						.Append("\n    signature: " + signature)
						.Append("\n")
						.Append("\n    itemType: " + itemType)
						.Append("\n    orderId: " + orderId)
						.Append("\n    packageName: " + packageName)
						.Append("\n    sku: " + sku)
						.Append("\n    purchaseTime: " + purchaseTime)
						.Append("\n    purchaseState: " + purchaseState)
						.Append("\n    developerPayload: " + developerPayload)
						.Append("\n    token: " + token)
						.Append("\n    vid: " + vid)
						.Append("\n    gameCurrency: " + gameCurrency)
						.Append("\n    gamePrice: " + gamePrice)
						.Append("\n    serverId: " + serverId)
						.Append("\n")
						.Append("\n bypassInfo: " + bypassInfo)
						.Append("\n");
				return builder.ToString();
			}
		}

		public class IAPV4ReceiptLebi : IAPV4Receipt {
			public long tradeno;

			public long uid;
			public long vid;
			public String billItemId;

			public String gameName;
			public String appId;
			public int tradeMoney;
			public String tradeDate;
			public String itemName;
			public String originalJson;

			public IAPV4ReceiptLebi() {}

			public IAPV4ReceiptLebi(JSONObject receiptJson) 
			: base(receiptJson) {

				receiptJson.GetField(ref tradeno, "tradeno");

				receiptJson.GetField(ref uid, "uid");
				receiptJson.GetField(ref vid, "vid");
				receiptJson.GetField(ref billItemId, "billItemId");

				receiptJson.GetField(ref gameName, "gameName");
				receiptJson.GetField(ref appId, "appId");
				receiptJson.GetField(ref tradeMoney, "tradeMoney");
				receiptJson.GetField(ref tradeDate, "tradeDate");
				receiptJson.GetField(ref itemName, "itemName");
				receiptJson.GetField(ref originalJson, "originalJson");
			}

			public String toString() {
				StringBuilder builder = new StringBuilder();
				builder.Append("[IAPV4ReceiptLebi]")
						.Append("\n    type: " + type.ToString())
						.Append("\n    product: " + product.toString())
						.Append("\n    additionalInfo: " + additionalInfo)
						.Append("\n")
						.Append("\n    hiveiapReceipt: " + hiveiapReceipt)
						.Append("\n")
						.Append("\n    billItemId: " + billItemId)
						.Append("\n")
						.Append("\n    uid: " + uid)
						.Append("\n    vid: " + vid)
						.Append("\n    tradeno: " + tradeno)
						.Append("\n")
						.Append("\n    gameName: " + gameName)
						.Append("\n    appId: " + appId)
						.Append("\n    tradeMoney: " + tradeMoney)
						.Append("\n    tradeDate: " + tradeDate)
						.Append("\n    itemName: " + itemName)
						.Append("\n    originalJson: " + originalJson)
						.Append("\n")
						.Append("\n bypassInfo: " + bypassInfo)
						.Append("\n");
				return builder.ToString();
			}
		}
		
		public class IAPV4ReceiptOneStore : IAPV4Receipt {

			// for OneStore V6
			public String purchaseData;
			public String signature;

			public String orderId;
			public String packageName;
			public String productId;
			public long purchaseTime;
			public int acknowledgeState;
			public String purchaseToken;
			public String billingKey;
			public int purchaseState;
			public int recurringState;
			public String storeCode;

			public IAPV4ReceiptOneStore() {}

			public IAPV4ReceiptOneStore(JSONObject receiptJson) 
			: base(receiptJson) {

				receiptJson.GetField(ref purchaseData, "purchaseData");
				receiptJson.GetField(ref signature, "signature");

				receiptJson.GetField(ref orderId, "orderId");
				receiptJson.GetField(ref packageName, "packageName");
				receiptJson.GetField(ref productId, "productId");
				receiptJson.GetField(ref purchaseTime, "purchaseTime");
				receiptJson.GetField(ref acknowledgeState, "acknowledgeState");
				receiptJson.GetField(ref purchaseToken, "purchaseToken");
				receiptJson.GetField(ref billingKey, "billingKey");
				receiptJson.GetField(ref purchaseState, "purchaseState");
				receiptJson.GetField(ref recurringState, "recurringState");
				receiptJson.GetField(ref storeCode, "storeCode");
			}

			public String toString() {
				StringBuilder builder = new StringBuilder();
				builder.Append("[IAPV4ReceiptOneStore]")
						.Append("\n    type: " + type.ToString())
						.Append("\n    product: " + product.toString())
						.Append("\n    additionalInfo: " + additionalInfo)
						.Append("\n")
						.Append("\n    hiveiapReceipt: " + hiveiapReceipt)
						.Append("\n")
						.Append("\n    purchaseData: " + purchaseData)
						.Append("\n    signature: " + signature)
						.Append("\n")
						.Append("\n    orderId: " + orderId)
						.Append("\n    packageName: " + packageName)
						.Append("\n    productId: " + productId)
						.Append("\n    purchaseTime: " + purchaseTime)
						.Append("\n    acknowledgeState: " + acknowledgeState)
						.Append("\n    purchaseToken: " + purchaseToken)
						.Append("\n    billingKey: " + billingKey)
						.Append("\n    purchaseState: " + purchaseState)
						.Append("\n    recurringState: " + recurringState)
						.Append("\n    storeCode: " + storeCode)
						.Append("\n")
						.Append("\n bypassInfo: " + bypassInfo)
						.Append("\n");
				return builder.ToString();
			}
		}

		public class IAPV4ReceiptAmazon : IAPV4Receipt {

			// for Amazon
			public String userId;
			public String marketPlace;
			public String receiptId;
			public String sku;
			public String productType;
			public String purchaseDate;
			public String cancelDate;

			public IAPV4ReceiptAmazon() {}

			public IAPV4ReceiptAmazon(JSONObject receiptJson) 
			: base(receiptJson) {

				receiptJson.GetField(ref userId, "userId");
				receiptJson.GetField(ref marketPlace, "marketPlace");
				receiptJson.GetField(ref receiptId, "receiptId");
				receiptJson.GetField(ref sku, "sku");
				receiptJson.GetField(ref productType, "productType");
				receiptJson.GetField(ref purchaseDate, "purchaseDate");
				receiptJson.GetField(ref cancelDate, "cancelDate");
			}

			public String toString() {
				StringBuilder builder = new StringBuilder();
				builder.Append("[IAPV4ReceiptAmazon]")
						.Append("\n    type: " + type.ToString())
						.Append("\n    product: " + product.toString())
						.Append("\n    additionalInfo: " + additionalInfo)
						.Append("\n")
						.Append("\n    hiveiapReceipt: " + hiveiapReceipt)
						.Append("\n")
						.Append("\n    userId: " + userId)
						.Append("\n    marketPlace: " + marketPlace)
						.Append("\n    receiptId: " + receiptId)
						.Append("\n    sku: " + sku)
						.Append("\n    productType: " + productType)
						.Append("\n    purchaseDate: " + purchaseDate)
						.Append("\n    cancelDate: " + cancelDate)
						.Append("\n")
						.Append("\n bypassInfo: " + bypassInfo)
						.Append("\n");
				return builder.ToString();
			}
		}

		public class IAPV4ReceiptSamsung : IAPV4Receipt {

            // for GalaxyStore
            public String purchaseData;

            public IAPV4ReceiptSamsung() {}

            public IAPV4ReceiptSamsung(JSONObject receiptJson)
            : base(receiptJson) {

                receiptJson.GetField(ref purchaseData, "purchaseData");
            }

            public String toString() {
                StringBuilder builder = new StringBuilder();
                builder.Append("[IAPV4ReceiptSamsung]")
                        .Append("\n    type: " + type.ToString())
                        .Append("\n    product: " + product.toString())
                        .Append("\n    additionalInfo: " + additionalInfo)
                        .Append("\n")
                        .Append("\n    hiveiapReceipt: " + hiveiapReceipt)
                        .Append("\n")
                        .Append("\n    purchaseData: " + purchaseData)
                        .Append("\n")
                        .Append("\n bypassInfo: " + bypassInfo)
                        .Append("\n");
                return builder.ToString();
            }
        }

		public class IAPV4ReceiptHuawei : IAPV4Receipt {

			// for Amazon
			public String purchaseData;
			public String signature;

			public String orderId;
			public String productId;
			public String priceType;

			public IAPV4ReceiptHuawei() {}

			public IAPV4ReceiptHuawei(JSONObject receiptJson) 
			: base(receiptJson) {

				receiptJson.GetField(ref purchaseData, "purchaseData");
				receiptJson.GetField(ref signature, "signature");

				receiptJson.GetField(ref orderId, "orderId");
				receiptJson.GetField(ref productId, "productId");
				receiptJson.GetField(ref priceType, "priceType");
			}

			public String toString() {
				StringBuilder builder = new StringBuilder();
				builder.Append("[IAPV4ReceiptHuawei]")
						.Append("\n    type: " + type.ToString())
						.Append("\n    product: " + product.toString())
						.Append("\n    additionalInfo: " + additionalInfo)
						.Append("\n")
						.Append("\n    hiveiapReceipt: " + hiveiapReceipt)
						.Append("\n")
						.Append("\n    purchaseData: " + purchaseData)
						.Append("\n    signature: " + signature)
						.Append("\n    orderId: " + orderId)
						.Append("\n    productId: " + productId)
						.Append("\n    priceType: " + priceType)
						.Append("\n")
						.Append("\n bypassInfo: " + bypassInfo)
						.Append("\n");
				return builder.ToString();
			}
		}
	}
}


/** @} */



