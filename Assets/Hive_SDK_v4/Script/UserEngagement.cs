// using UnityEngine;
// using System;
// using System.Text;
// using System.Collections;
// using System.Collections.Generic;
// using System.Runtime.InteropServices;
// using System.IO;


// namespace hive
// {
// 	/**
// 	 * \~korean @brief HIVE SDK 가 특정한 조건에서 클라이언트에 개입 (Engagement) 하기 위한 이벤트 형태 정의
// 	 * \~english @brief It defines the event type for the HIVE SDK to engage the client under certain conditions.
// 	 * \~
// 	 * @ingroup UserEngagement
// 	 * @author imsunghoon
// 	 */
// 	public enum EngagementEventType {

// 		START				///< \~korean Engagement에 의해 다른 기능이 수행되기 시작함을 알림.	\~english Notice that other functions are started by Engagement.
// 		, END				///< \~korean Engagement에 의한 다른 기능 수행이 종료됨을 알림.	\~english Notice that other functions performed by Engagement are terminated.
// 		, EVENT				///< \~korean Engagement에 의해 처리될 수 없는 이벤트(host가 game인 경우)를 전달해주는 콜백.	\~english Callback that delivers events that can not be handled by Engagement (for example, the host is game).
// 		, COUPON			///< \~korean Engagement에 의해 처리된 쿠폰 소모에 대한 결과.	\~english Results for consumption of coupons processed by Engagement.

// 		/**
//		*  \~korean 
// 		* 앱이 실행중일 때, 앱 외에서 (마켓앱, 웹 등) Google PlayStore PromoCode (리딤코드) 로 상품을 구매시 불리게 되는 콜백.<br>
// 		* 콜백 내에 영수증 등의 데이터가 있는것은 아니고, 구매내역이 변경되었으니 받아가라는 (복구하라는) 신호이다.<br>
// 		* 해당 콜백이 불리면 게임에서는 아이템을 지급할 수 있는 적정한 시점에 Restore() 를 호출해 주어 처리하면 된다.
//		*
//		*  \~english
// 		* Callback that will be called when you purchase a product from Google PlayStore's PromoCode outside of the app while the app is running.<br>
// 		* There is no receipt data in the callback, but the signal is that the purchase history has been changed.<br>
// 		* If the callback is called, the game should call Restore () at the appropriate time to deliver the item.
//		* 
// 		*/
// 		, IAP_UPDATED
// 		, REMOTE_PUSH		///< \~korean 리모트 푸쉬가 도책했다고 알림. \~english  Notification that a remote push has been received.
// 		, LOCAL_PUSH		///< \~korean 로컬 푸시가 도착했다고 알림. \~english  Notification that a local push has been received.
// 		, AUTH_LOGIN		///< \~korean 유저(클라이언트)에 의해 열리지 않은 로그인 프로세스에 의한 결과를 받는 콜백. \~english  A callback that receives the results of a login process not opened by the user (client).
// 	}

// 	public class UserEngagement {

// 		/**
//		 *  \~korean
// 		 * @brief SDK 가 특정한 조건에서 클라이언트에 개입 (Engagement) 하기 위한 이벤트 리스너.<br>
// 		 * 여기서 특정한 조건은 모바일 메시지 (SMS), 푸시 알림 (Push Notification) 으로 전송된 메시지의 URL 클릭이나 프로모션 뷰에서 사용자 동작 등이 있다.
// 		 *
//		 *  \~english
// 		 * @brief An event listener for the SDK to engage clients in certain conditions.<br>
// 		 * The specific conditions are, for example, a mobile message (SMS), a URL click on a message in a push notification, or a user action in a promotional view.
// 		 *
// 		 * @ingroup UserEngagement
// 		 */
// 		public delegate void onEngagement(ResultAPI result, EngagementEventType engagementEventType, JSONObject param);

// 		/**
//		 *  \~korean
// 		 * @brief Engagement 이벤트 처리가 가능한지(게임 서버에 DB가 생성된 경우 등) 여부를 설정한다.
//  	 * true로 설정하려는 경우, <로그인 이후 & 리스너가 등록된 이후>의 조건을 만족한 상태여야 정상적으로 설정되며,
//  	 * false로 설정하려는 경우는 항상 설정 가능하다.
//		 *
// 		 * @param isReady Enganement 이벤트 처리 가능 여부.
// 		 * @return API 호출 성공 여부.
// 		 *
//		 *  \~english
// 		 * @briefIt sets whether Engagement event handling is enabled.(Such as when a DB is created in the game server)
//		 * If you want to set it to true, it must be in a state that satisfies the condition of <after login & after registering the listener>, 
//		 * if you want to set it to false, you can always set it.
//		 *
// 		 * @param isReady Whether Engagement events can be processed.
// 		 * @return Whether the API call was successful.
//     	 */
// 		public static ResultAPI setEngagementReady(Boolean isReady) {

// 			JSONObject jsonParam = HIVEUnityPlugin.createParam("UserEngagement", "setEngagementReady", null);
// 			jsonParam.AddField ("isReady", isReady);

// 			JSONObject resJsonObject = HIVEUnityPlugin.callNative (jsonParam);

// 			ResultAPI result = new ResultAPI(resJsonObject.GetField ("resultAPI"));
// 			return result;
// 		}

// 		/**
// 		 * \~korean @brief Engagement 리스너를 등록한다.
// 		 * \~english @brief It register Engagement listener.
// 		 *
// 		 * @param listener
// 		 */
// 		public static void setEngagementListener(onEngagement listener) {

// 			JSONObject jsonParam = HIVEUnityPlugin.createParam("UserEngagement", "setEngagementHandler", listener);
// 			HIVEUnityPlugin.callNative (jsonParam);
// 		}

// 		// Native 영역에서 호출된 요청을 처리하기 위한 플러그인 내부 코드
// 		public static void executeEngine(JSONObject resJsonObject) {

// 			String methodName = null;
// 			resJsonObject.GetField (ref methodName, "method");

// 			int handlerId = -1;
// 			resJsonObject.GetField (ref handlerId, "handler");

// 			// object handler = null;
// 			// if ("setEngagementReady".Equals (methodName) == false) {
// 			// 	// handler = (object)HIVEUnityPlugin.popHandler (handlerId);
// 			// 	handler = HIVEUnityPlugin.getEngagementHandler();
// 			// }

// 			if ("setEngagementHandler".Equals (methodName)) {
				
// 				object handler = HIVEUnityPlugin.getEngagementHandler();

// 				if (handler == null)
// 					return;

// 				// Engagement Event Type
// 				String strValue = "";
// 				resJsonObject.GetField (ref strValue, "engagementEventType");
// 				EngagementEventType engagementEventType = EngagementEventType.EVENT;
// 				if (strValue.Equals ("START"))
// 					engagementEventType = EngagementEventType.START;
// 				else if (strValue.Equals ("END"))
// 					engagementEventType = EngagementEventType.END;
// 				else if (strValue.Equals ("COUPON"))
// 					engagementEventType = EngagementEventType.COUPON;
// 				else if (strValue.Equals ("IAP_UPDATED"))
// 					engagementEventType = EngagementEventType.IAP_UPDATED;
// 				else if (strValue.Equals ("REMOTE_PUSH"))
// 					engagementEventType = EngagementEventType.REMOTE_PUSH;
// 				else if (strValue.Equals ("LOCAL_PUSH"))
// 					engagementEventType = EngagementEventType.LOCAL_PUSH;
// 				else if (strValue.Equals ("AUTH_LOGIN"))
// 					engagementEventType = EngagementEventType.AUTH_LOGIN;

// 				// Parameter by Engagement Event Type
// 				JSONObject param = resJsonObject.GetField ("param");
			
// 				onEngagement listener = (onEngagement)handler;
// 				listener (new ResultAPI (resJsonObject.GetField ("resultAPI")), engagementEventType, param);
// 			}
// 		}
// 	}		// end of public class UserEngagement
// }



