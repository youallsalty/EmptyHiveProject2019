/**
 * @file    HiveSDKExport.cs
 * 
 * @author  nanomech
 * Copyright 2016 GAMEVILCom2USPlatform Corp.
 * @defgroup Hive.Unity.Editor
 * @{
 * @brief HIVE SDK Export 프로세싱 <br/><br/>
 */

namespace Hive.Unity.Editor
{
    using System.Globalization;
    using System.IO;
    using UnityEditor;
    using UnityEngine;

    internal class HiveSDKExport
    {
        private const string HivePath = "Assets/Hive_SDK_v4/";
        private const string ExamplesPath = "Assets/Hive_SDK_v4_Examples/";
        private const string PluginsPath = "Assets/Hive_SDK_v4/Plugins/";

        public enum Target
        {
            DEBUG,
            RELEASE
        }

        private static string PackageName
        {
            get
            {
                return string.Format(
                    CultureInfo.InvariantCulture,
                    "Hive-unity-sdk-{0}.unitypackage",
                    "4.0.0");
                    // FacebookSdkVersion.Build);
            }
        }

        private static string OutputPath
        {
            get
            {
                DirectoryInfo projectRoot = Directory.GetParent(Directory.GetCurrentDirectory());
                var outputDirectory = new DirectoryInfo(Path.Combine(projectRoot.FullName, "out"));

                // Create the directory if it doesn't exist
                outputDirectory.Create();
                return Path.Combine(outputDirectory.FullName, HiveSDKExport.PackageName);
            }
        }

        // Exporting the *.unityPackage for Asset store
        public static string ExportPackage()
        {
            Debug.Log("Exporting HIVE Unity Package...");
            string path = OutputPath;
            try
            {
                if (!File.Exists(Path.Combine(Application.dataPath, "Temp")))
                {
                    AssetDatabase.CreateFolder("Assets", "Temp");
                }

                AssetDatabase.MoveAsset(HivePath + "Editor/Resources/HiveConfig.asset", "Assets/Temp/HiveConfig.asset");
                AssetDatabase.DeleteAsset(PluginsPath + "Android/AndroidManifest.xml");
                AssetDatabase.DeleteAsset(PluginsPath + "Android/AndroidManifest.xml.meta");

                string[] hiveFiles = (string[])Directory.GetFiles(HivePath, "*.*", SearchOption.AllDirectories);
                string[] exampleFiles = (string[])Directory.GetFiles(ExamplesPath, "*.*", SearchOption.AllDirectories);
                string[] pluginsFiles = (string[])Directory.GetFiles(PluginsPath, "*.*", SearchOption.AllDirectories);
                string[] files = new string[hiveFiles.Length + exampleFiles.Length + pluginsFiles.Length];
                hiveFiles.CopyTo(files, 0);
                exampleFiles.CopyTo(files, hiveFiles.Length);
                pluginsFiles.CopyTo(files, hiveFiles.Length + exampleFiles.Length);

                AssetDatabase.ExportPackage(
                    files,
                    path,
                    ExportPackageOptions.IncludeDependencies | ExportPackageOptions.Recurse);
            }
            finally
            {
                // Move files back no matter what
                AssetDatabase.MoveAsset("Assets/Temp/HiveConfig.asset", HivePath + "Editor/Resources/HiveConfig.asset");
                AssetDatabase.DeleteAsset("Assets/Temp");

                // regenerate the manifest
                UnityEditor.HiveEditor.HiveManifestMod.GenerateManifest();
            }

            Debug.Log("Finished exporting!");

            return path;
        }
    }
}
