﻿/**
 * @file    ExternalDependencyEditor.cs
 * 
 * @author  disker
 * Copyright 2020 GAMEVILCom2USPlatform Corp.
 * @defgroup Hive.Unity.Editor
 * @{
 * @brief HIVE External Dependency EditorWindow <br/><br/>
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;

namespace Hive.Unity.Editor
{
    using ExternalDependencyDictionary = SerializableDictionary<ExternalDependencyEditor.ExternalDependencyType, bool>;
    public class ExternalDependencyEditor : EditorWindow
    {
        public enum ExternalDependencyType
        {
            // IDP
            Line,
            VK,
            QQ,
            Wechat,

            // Analytics
            Adjust,
            Singular,
            AppsFlyer
        }
        private ExternalDependencyDictionary externalDependencyDictionary = new ExternalDependencyDictionary();
        private GUILayoutOption[] layoutOptions = { GUILayout.Width(100), GUILayout.ExpandWidth(false) };

        [MenuItem("Hive/ExternalDependency")]
        public static void create()
        {
            var editor = (ExternalDependencyEditor)EditorWindow.GetWindow(typeof(ExternalDependencyEditor));
            editor.Initialize();
            editor.Show();
        }

        public void Initialize()
        {
            minSize = new Vector2(300, 450);
            position = new Rect(UnityEngine.Screen.width / 3, UnityEngine.Screen.height / 3, minSize.x, minSize.y);

            externalDependencyDictionary = LoadExternalDependencyDictionaryFromJson();
        }

        private static ExternalDependencyDictionary LoadExternalDependencyDictionaryFromJson()
        {
            string jsonDataPath = Path.Combine(Application.dataPath, "Hive_SDK_v4/Editor/externalDependencyDictionary.json");

            if (File.Exists(jsonDataPath))
            {
                string jsonDataString = File.ReadAllText(jsonDataPath);
                return JsonUtility.FromJson<ExternalDependencyDictionary>(jsonDataString);
            }
            else
            {
                var dictionary = new ExternalDependencyDictionary();
                Reset(ref dictionary);
                return dictionary;
            }
        }

        private void SaveExternalDependencyDictionaryToJson()
        {
            string jsonDataPath = Path.Combine(Application.dataPath, "Hive_SDK_v4/Editor/externalDependencyDictionary.json");

            string externalDependencyDictionaryJsonString = JsonUtility.ToJson(externalDependencyDictionary);
            File.WriteAllText(jsonDataPath, externalDependencyDictionaryJsonString);
        }

        private static void Reset(ref ExternalDependencyDictionary dictionary)
        {
            dictionary.Clear();

            foreach (ExternalDependencyType i in Enum.GetValues(typeof(ExternalDependencyType)))
            {
                dictionary[i] = true;
            }
        }

        void OnGUI()
        {
            EditorGUILayout.BeginVertical();

            GUILayout.Label("[IDP]", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;

            // Line
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("LINE", layoutOptions);
            if(externalDependencyDictionary.ContainsKey(ExternalDependencyType.Line))
                externalDependencyDictionary[ExternalDependencyType.Line] = EditorGUILayout.Toggle(externalDependencyDictionary[ExternalDependencyType.Line], layoutOptions);
            EditorGUILayout.EndHorizontal();

            // VK
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("VK", layoutOptions);
            if(externalDependencyDictionary.ContainsKey(ExternalDependencyType.VK))
                externalDependencyDictionary[ExternalDependencyType.VK] = EditorGUILayout.Toggle(externalDependencyDictionary[ExternalDependencyType.VK], layoutOptions);
            EditorGUILayout.EndHorizontal();

            // QQ
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("QQ", layoutOptions);
            if(externalDependencyDictionary.ContainsKey(ExternalDependencyType.QQ))
                externalDependencyDictionary[ExternalDependencyType.QQ] = EditorGUILayout.Toggle(externalDependencyDictionary[ExternalDependencyType.QQ], layoutOptions);
            EditorGUILayout.EndHorizontal();

            // Wechat
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Wechat", layoutOptions);
            if(externalDependencyDictionary.ContainsKey(ExternalDependencyType.Wechat))
                externalDependencyDictionary[ExternalDependencyType.Wechat] = EditorGUILayout.Toggle(externalDependencyDictionary[ExternalDependencyType.Wechat], layoutOptions);
            EditorGUILayout.EndHorizontal();

            EditorGUI.indentLevel--;
            GUILayout.Space(20);


            GUILayout.Label("[Analytics]", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;

            // Adjust
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Adjust", layoutOptions);
            if(externalDependencyDictionary.ContainsKey(ExternalDependencyType.Adjust))
                externalDependencyDictionary[ExternalDependencyType.Adjust] = EditorGUILayout.Toggle(externalDependencyDictionary[ExternalDependencyType.Adjust], layoutOptions);
            EditorGUILayout.EndHorizontal();

            // Singular
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Singular", layoutOptions);
            if(externalDependencyDictionary.ContainsKey(ExternalDependencyType.Singular))
                externalDependencyDictionary[ExternalDependencyType.Singular] = EditorGUILayout.Toggle(externalDependencyDictionary[ExternalDependencyType.Singular], layoutOptions);
            EditorGUILayout.EndHorizontal();

            // AppsFlyer
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("AppsFlyer", layoutOptions);
            if(externalDependencyDictionary.ContainsKey(ExternalDependencyType.AppsFlyer))
                externalDependencyDictionary[ExternalDependencyType.AppsFlyer] = EditorGUILayout.Toggle(externalDependencyDictionary[ExternalDependencyType.AppsFlyer], layoutOptions);
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(190);

            if (GUILayout.Button("Reset to Defaults"))
            {
                Reset(ref externalDependencyDictionary);
            }

            GUILayout.BeginHorizontal();
            bool closeButton = GUILayout.Button("Cancel");

            bool okButton = GUILayout.Button("OK");
            closeButton |= okButton;
            if (okButton)
            {
                SetupEDM4UExternalDependency();
                SetupIncludedExternalDependency();
                SaveExternalDependencyDictionaryToJson();
                AssetDatabase.Refresh();

#if UNITY_ANDROID
                GooglePlayServices.PlayServicesResolver.Resolve(null, true, null);
                GooglePlayServices.PlayServicesResolver.ResolveSync(true);
#endif

            }
            if (closeButton) Close();
            GUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }

        private void SetupEDM4UExternalDependency()
        {
            ExternalDependencyType[] edm4uExternalDependencies =
            {
                ExternalDependencyType.Line,
                ExternalDependencyType.Wechat,
                ExternalDependencyType.VK,
                ExternalDependencyType.QQ,
                ExternalDependencyType.Adjust,
                ExternalDependencyType.AppsFlyer,
                ExternalDependencyType.Singular
            };

            string editorPath = Path.Combine(Application.dataPath, "Hive_SDK_v4/Editor");
            string dependenciesPath = Path.Combine(Application.dataPath, "Hive_SDK_v4/Dependencies");

            DirectoryCopy(dependenciesPath, editorPath);

            DirectoryInfo editorDirInfo = new DirectoryInfo(editorPath);
            foreach (ExternalDependencyType t in edm4uExternalDependencies)
            {
                if (externalDependencyDictionary[t])
                    continue;

                foreach (FileInfo f in editorDirInfo.GetFiles())
                {
                    if (f.Name.Contains(Enum.GetName(typeof(ExternalDependencyType), t)))
                        f.Delete();
                }
            }

        }

        private void SetupIncludedExternalDependency()
        {
            SetupIncludedExternalDependencyForAndroid();
            SetupIncludedExternalDependencyForIOS();
        }

        private void SetupIncludedExternalDependencyForAndroid()
        {
            var libPath = "Assets/Hive_SDK_v4/Plugins/Android/libs";

            if (!Directory.Exists(libPath))
            {
                Directory.CreateDirectory(libPath);
            }

            DirectoryInfo libDirInfo = new DirectoryInfo(libPath);
            FileInfo[] libFileInfoes = libDirInfo.GetFiles();
            if (libFileInfoes == null || libFileInfoes.Length == 0)
            {
                Debug.Log(libPath + "\n" + "FileInfo[] is null or empty");
                return;
            }

            foreach (FileInfo f in libFileInfoes)
            {
                if (f.Extension.ToLower().Equals(".meta"))
                    continue;

                PluginImporter plugin = PluginImporter.GetAtPath(libPath + "/" + f.Name) as PluginImporter;

                if (f.Name.Contains("open_sdk_"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.QQ];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.Android, enable);
                }
                if (f.Name.Contains("mid-sdk-"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.QQ];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.Android, enable);
                }
                if (f.Name.Contains("mta-sdk-"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.QQ] || externalDependencyDictionary[ExternalDependencyType.Wechat];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.Android, enable);
                }

                try
                {
                    plugin.SaveAndReimport();
                }
                catch (Exception ex)
                {
                    Debug.Log(String.Format("{0} is not allocated as PluginImporter. \n {1}", f.Name, ex.ToString()));
                }
                
            }

        }


        private void SetupIncludedExternalDependencyForIOS()
        {
            var frameworkPath = "Assets/Hive_SDK_v4/Plugins/iOS/framework";

            if (!Directory.Exists(frameworkPath))
            {
                Directory.CreateDirectory(frameworkPath);
            }

            DirectoryInfo frameworkDirInfo = new DirectoryInfo(frameworkPath);
            DirectoryInfo[] frameworkDirInfoes = frameworkDirInfo.GetDirectories();
            if (frameworkDirInfoes == null || frameworkDirInfoes.Length == 0)
            {
                Debug.Log(frameworkPath + "\n" + "DirectoryInfo[] is null or empty");
                return;
            }

            foreach (DirectoryInfo d in frameworkDirInfoes)
            {
                PluginImporter plugin = PluginImporter.GetAtPath(frameworkPath + "/" + d.Name) as PluginImporter;

                if (d.Name.Equals("WXApi.framework"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.Wechat];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.iOS, enable);

                }
                if (d.Name.Equals("TencentOpenAPI.framework"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.QQ];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.iOS, enable);
                }
                if (d.Name.Equals("VKSdk.framework"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.VK];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.iOS, enable);
                }
                if (d.Name.Equals("ProviderAdapter.framework"))
                {
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.iOS, true);
                }

                /*
                    HIVE 제공 framework
                */
                if (d.Name.Contains("ProviderWechat"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.Wechat];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.iOS, enable);
                }
                if (d.Name.Contains("ProviderVK"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.VK];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.iOS, enable);
                }
                if (d.Name.Contains("ProviderSingular"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.Singular];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.iOS, enable);
                }
                if (d.Name.Contains("ProviderQQ"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.QQ];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.iOS, enable);
                }
                if (d.Name.Contains("ProviderLine"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.Line];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.iOS, enable);
                }
                if (d.Name.Contains("ProviderAppsFlyer"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.AppsFlyer];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.iOS, enable);
                }
                if (d.Name.Contains("ProviderAdjust"))
                {
                    bool enable = externalDependencyDictionary[ExternalDependencyType.Adjust];
                    plugin.SetCompatibleWithAnyPlatform(false);
                    plugin.SetCompatibleWithEditor(false);
                    plugin.SetCompatibleWithPlatform(BuildTarget.iOS, enable);
                }

                try
                {
                    plugin.SaveAndReimport();
                }
                catch (Exception ex)
                {
                    Debug.Log(String.Format("{0} is not allocated as PluginImporter. \n {1}", d.Name, ex.ToString()));
                }
            }
        }

        private bool IsFile(string path, string file)
        {
            string[] files = Directory.GetFiles(path, file);
            return files.Length > 0 ? true : false;
        }

        private void DirectoryCopy(string sourcePath, string destPath)
        {
            DirectoryInfo dir = new DirectoryInfo(sourcePath);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + dir
                );
            }

            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                if (file.Name.Contains(".meta"))
                    continue;
                string tempPath = Path.Combine(destPath, file.Name);
                file.CopyTo(tempPath, true);
            }
        }

    }

    [Serializable]
    public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
    {
        [SerializeField]
        private List<TKey> keys = new List<TKey>();

        [SerializeField]
        private List<TValue> values = new List<TValue>();

        // save the dictionary to lists
        public void OnBeforeSerialize()
        {
            keys.Clear();
            values.Clear();
            foreach (KeyValuePair<TKey, TValue> pair in this)
            {
                keys.Add(pair.Key);
                values.Add(pair.Value);
            }
        }

        // load dictionary from lists
        public void OnAfterDeserialize()
        {
            this.Clear();

            if (keys.Count != values.Count)
                throw new System.Exception(string.Format("there are {0} keys and {1} values after deserialization. Make sure that both key and value types are serializable."));

            for (int i = 0; i < keys.Count; i++)
                this.Add(keys[i], values[i]);
        }
    }


}