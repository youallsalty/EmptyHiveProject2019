﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Android;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Globalization;


#if UNITY_2018_1_OR_NEWER
public class AndroidPostBuildProcessor : IPostGenerateGradleAndroidProject
{
    public int callbackOrder
    {
        get
        {
            return 999;
        }
    }


    void IPostGenerateGradleAndroidProject.OnPostGenerateGradleAndroidProject(string path)
    {
        Debug.Log("Bulid path : " + path);
        string gradlePropertiesFile = path + "/gradle.properties";
        if (File.Exists(gradlePropertiesFile))
        {
            File.Delete(gradlePropertiesFile);
        }
        StreamWriter writer = File.CreateText(gradlePropertiesFile);
        writer.WriteLine("org.gradle.jvmargs=-Xmx4096M");
        writer.WriteLine("android.useAndroidX=true");
        writer.WriteLine("android.enableJetifier=true");
        writer.Flush();
        writer.Close();

        #if UNITY_2019_3_OR_NEWER
        resolveLauncherBuildGradle(path);
        #endif
        #if UNITY_2020_1_OR_NEWER
        resolveMainBuildGradle(path);
        #endif

        // resolve targetSDKversion 31, JDK 11 issue
        // https://developers.google.com/ar/develop/unity-arf/android-12-build
        resolveMainBuildGradleTargetSDK31(path);
        resolveUnityResourcesBuildGradleTargetSDK31(path);

    }

    /*
    * Resolve launcher's build.gradle (#GCPSDK4-99)
    * - unity 2019.3에서 launcher, unityLibrary 로 프로젝트가 분리
    * - unityLibrary's build.gradle은 기존 mainTemplate.gradle로 커스텀할 수 있으나, launcher's build.gradle은 커스텀할 수 없는 이슈 존재
    */
    private void resolveLauncherBuildGradle(string path){
        
        string launcherPath = path + "/../launcher/";
        string launcherBuildGradle = launcherPath + "build.gradle";
        string tmpBuildGradle = launcherPath + "tmpBuild.gradle";

        if (!File.Exists(launcherBuildGradle))
        {
            Debug.Log("launcher's build.gradle is not exist");
            return;
        }

        IEnumerable<string> lines = null;
        try
        {
            lines = File.ReadAllLines(launcherBuildGradle);
        }
        catch (Exception ex)
        {
            Debug.Log(String.Format("Unable to read lines {0} ({1})", launcherBuildGradle, ex.ToString()));
        }

        StreamWriter writer = File.CreateText(tmpBuildGradle);
            
        IEnumerator ienum = lines.GetEnumerator();
        while(ienum.MoveNext())
        {
            string line = (string)ienum.Current;

            // resolve targetSDKversion 31, JDK 11 issue
            // https://developers.google.com/ar/develop/unity-arf/android-12-build
            if(line.Contains("compileSdkVersion 31")) {
                line = "compileSdkVersion 30 // Add Hive for targetSDKversion 31 issue";
            } else if(line.Contains("buildToolsVersion \'31")){
                line = "buildToolsVersion \'30.0.3\' // Add Hive for targetSDKversion 31 issue";
            }

            writer.WriteLine(line);

            // resolve multidex
            if(line.Contains("defaultConfig")){
                writer.WriteLine("// Add HIVE");
                writer.WriteLine("multiDexEnabled true");
            }
            else if(line.Contains("dependencies")){
                writer.WriteLine("// Add HIVE");
                writer.WriteLine("implementation \'androidx.multidex:multidex:2.0.1\'");
            }

        }
        writer.Flush();
        writer.Close();

        File.Delete(launcherBuildGradle);
        File.Move(tmpBuildGradle, launcherBuildGradle);

    }

    /*
    * Resolve launcher's build.gradle (#GCPTAM-391)
    * - unity 2020.1에서 aaptOptions STREAMING_ASSETS 예약어가 사라짐
    */
    private void resolveMainBuildGradle(string path){
        
        string mainPath = path + "/../unityLibrary/";
        string mainBuildGradle = mainPath + "build.gradle";
        string tmpBuildGradle = mainPath + "tmpBuild.gradle";

        if (!File.Exists(mainBuildGradle))
        {
            Debug.Log("main's build.gradle is not exist");
            return;
        }

        IEnumerable<string> lines = null;
        try
        {
            lines = File.ReadAllLines(mainBuildGradle);
        }
        catch (Exception ex)
        {
            Debug.Log(String.Format("Unable to read lines {0} ({1})", mainBuildGradle, ex.ToString()));
            return;
        }

        StreamWriter writer = File.CreateText(tmpBuildGradle);
            
        IEnumerator ienum = lines.GetEnumerator();
        while(ienum.MoveNext())
        {
            string line = (string)ienum.Current;

            // Find and Replace '**STREAMING_ASSETS**'
            if(line.Contains("**STREAMING_ASSETS**")) {
                writer.WriteLine("noCompress = ['.ress', '.resource', '.obb'] + unityStreamingAssets.tokenize(', ')");
            } else {
                writer.WriteLine(line);
            }
        }
        writer.Flush();
        writer.Close();

        File.Delete(mainBuildGradle);
        File.Move(tmpBuildGradle, mainBuildGradle);

    }

    private void resolveMainBuildGradleTargetSDK31(string path){
        
        string mainPath = path + "/../unityLibrary/";
        string mainBuildGradle = mainPath + "build.gradle";
        string tmpBuildGradle = mainPath + "tmpBuild.gradle";

        if (!File.Exists(mainBuildGradle))
        {
            Debug.Log("main's build.gradle is not exist");
            return;
        }

        IEnumerable<string> lines = null;
        try
        {
            lines = File.ReadAllLines(mainBuildGradle);
        }
        catch (Exception ex)
        {
            Debug.Log(String.Format("Unable to read lines {0} ({1})", mainBuildGradle, ex.ToString()));
            return;
        }

        StreamWriter writer = File.CreateText(tmpBuildGradle);
            
        IEnumerator ienum = lines.GetEnumerator();
        while(ienum.MoveNext())
        {
            string line = (string)ienum.Current;

            if(line.Contains("compileSdkVersion 31")) {
                line = "compileSdkVersion 30 // Add Hive for targetSDKversion 31 issue";
            } else if(line.Contains("buildToolsVersion \'31")){
                line = "buildToolsVersion \'30.0.3\' // Add Hive for targetSDKversion 31 issue";
            }

            writer.WriteLine(line);  
        }
        writer.Flush();
        writer.Close();

        File.Delete(mainBuildGradle);
        File.Move(tmpBuildGradle, mainBuildGradle);

    }

    private void resolveUnityResourcesBuildGradleTargetSDK31(string path){
        
        string mainPath = path + "/../unityLibrary/unity-android-resources/";
        string mainBuildGradle = mainPath + "build.gradle";
        string tmpBuildGradle = mainPath + "tmpBuild.gradle";

        if (!File.Exists(mainBuildGradle))
        {
            Debug.Log("main's build.gradle is not exist");
            return;
        }

        IEnumerable<string> lines = null;
        try
        {
            lines = File.ReadAllLines(mainBuildGradle);
        }
        catch (Exception ex)
        {
            Debug.Log(String.Format("Unable to read lines {0} ({1})", mainBuildGradle, ex.ToString()));
            return;
        }

        StreamWriter writer = File.CreateText(tmpBuildGradle);
            
        IEnumerator ienum = lines.GetEnumerator();
        while(ienum.MoveNext())
        {
            string line = (string)ienum.Current;

            if(line.Contains("compileSdkVersion 31")) {
                line = "compileSdkVersion 30 // Add Hive for targetSDKversion 31 issue";
            } else if(line.Contains("buildToolsVersion \'31")){
                line = "buildToolsVersion \'30.0.3\' // Add Hive for targetSDKversion 31 issue";
            }

            writer.WriteLine(line);  
        }
        writer.Flush();
        writer.Close();

        File.Delete(mainBuildGradle);
        File.Move(tmpBuildGradle, mainBuildGradle);

    }

}
#endif