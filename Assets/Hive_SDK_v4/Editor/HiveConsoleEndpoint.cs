
/**
 * @file    HiveConsoleEndpoint.cs
 * 
 * @author  nanomech
 * Copyright 2016 GAMEVILCom2USPlatform Corp.
 * @defgroup HiveConsoleEndpoint
 * @{
 * @brief HIVE PackageExport console support. <br/><br/>
 */

using Hive.Unity.Editor;

public static class HiveConsoleEndpoint
{
    public static void ExportPackage()
    {
        HiveSDKExport.ExportPackage();
    }
}
