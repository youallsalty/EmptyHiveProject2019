﻿using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using UnityEngine.Networking;

public class Postbox : MonoBehaviour {
	//singleton instance
	static Postbox _instance = null;
	//singleton method
	static public Postbox Instance
	{
		get{
			if(_instance == null)
			{
				_instance = FindObjectOfType<Postbox>();
				if(_instance == null)
				{
					//create gameobject and add component postbox
					GameObject obj = new GameObject();
					obj.name ="PostBox";
					_instance = obj.AddComponent<Postbox>();
					DontDestroyOnLoad(obj);
				}
				Debug.Assert(_instance != null,"check,create failure postbox");
			}
			return _instance;
		}
	}

	public class PostBoxResultData
	{
		public enum ErrorCodes
		{
			UNKOWN = -1,
			SUCCESS = 0,
			INTERNAL_ERROR,
			SESSION_ERROR,
			PARAM_ERROR,
			MAX
		}

		ErrorCodes errorcode;
		public void SetErrorCode(string ErrorCode)
		{
			int maxError = (int)ErrorCodes.MAX;
			string[] error_strings = {"200","500","501","502"};
			for (int i=0; i<maxError; ++i) {
				if(ErrorCode == error_strings[i])
				{
					errorcode = (ErrorCodes)i;
					break;
				}
			}
		}
		public ErrorCodes ErrorCode{get{ return errorcode; }set{ errorcode = value; }}
		public string ErrorMsg = "";
		public Dictionary<string,object> ResultData = null;
	}

	public enum ServerState
	{
		Live = 0,
		Sandbox,
		Staging,//for v1
		Test,
		Dev,
	}

	string[] URLBase = {
		"https://misample.com2us.net/",
		"https://sandbox-misample.com2us.net/",
		"https://test-misample.com2us.net/",//Staging for v1
		"https://test-misample.com2us.net/",//Test
		"https://dev-misample.com2us.net/"//Dev
	};

	enum API
	{
		LOGIN = 0
		,USER_INFO
		,REFRSH_POST
		,RECEIVE_POST
		,IAPV4_VERIFY
	}

	string[] APIUrl = {
		"account/login",
		"account/user",
		"postbox/posts",
		"postbox/receive",
		"hive/iap/verify"
	};

	string mPostBoxVID;
	string mPostBoxVIDType;
	string mPostBoxSessionToken;
	string mLoginCenterSessionKey;
	string mSelectedWorld;

	ServerState state = ServerState.Live;

	public void SetServerState(ServerState state)
	{
		this.state = state;
	}

	string getURL(API postboxAPI)
	{
		return URLBase [(int)state] + APIUrl [(int)postboxAPI];
	}

	public delegate void PostBoxCallback(PostBoxResultData receiveData);

	public void requestLoginCenter(string world,string vid, string vidType, string did,string uid,string appid,string country,string language,string appversion,string logincenterSessionToken,PostBoxCallback callback)
	{
		Debug.Log("Postbox (Game Sample Server) Action -- requestLoginCenter\n");

		mPostBoxVID = vid;
		mLoginCenterSessionKey = logincenterSessionToken;
		mSelectedWorld = world;
		mPostBoxVIDType = vidType;
		Dictionary<string,object> data = new Dictionary<string, object> ();
		data.Add ("world", world);
		data.Add ("vid", vid);
		data.Add ("vid_type", vidType);
		data.Add ("did", did);
		data.Add ("uid", uid);
		data.Add ("appid", appid);
		data.Add ("country", country);
		data.Add ("language", language);
		data.Add ("appver", appversion);
		data.Add ("lc_sessionkey", logincenterSessionToken);
		StartCoroutine (requestAPI (API.LOGIN, data, callback));
	}

	public void requestUserInfo(PostBoxCallback callback)
	{
		requestUserInfo (null, callback);
	}

	public void requestUserInfo(string vid,PostBoxCallback callback)
	{
		Dictionary<string,object> data = new Dictionary<string, object> ();
		data.Add ("world", mSelectedWorld);
		if (string.IsNullOrEmpty (vid) == false) {
			data.Add ("vid", vid);
		} else {
			data.Add("vid",mPostBoxVID);
		}
		StartCoroutine (requestAPI (API.USER_INFO, data, callback));
	}

	public void requestRefreshPost(PostBoxCallback callback)
	{
		Debug.Log("Postbox (Game Sample Server) Action -- requestRefreshPost\n");
		//Debug.Assert (string.IsNullOrEmpty(mPostBoxSessionToken) != true); //Please login
		Dictionary<string,object> data = new Dictionary<string, object> ();
		data.Add ("session_token", mPostBoxSessionToken);
		data.Add ("world", mSelectedWorld);
		data.Add ("vid", mPostBoxVID);
		data.Add ("vid_type", mPostBoxVIDType);
		StartCoroutine(requestAPI (API.REFRSH_POST, data, callback));
	}

	public void requestReceivePost(string postID,PostBoxCallback callback)
	{
		Debug.Log("Postbox (Game Sample Server) Action -- requestReceivePost\n");
		List<string> postId = new List<string> (){postID};
		requestReceivePost (postId,callback);
	}

	public void requestReceivePost(List<string> postID,PostBoxCallback callback)
	{
		Debug.Log("Postbox (Game Sample Server) Action -- requestReceivePost\n");
		//Debug.Assert (string.IsNullOrEmpty(mPostBoxSessionToken) != true); //Please login
		Dictionary<string,object> data = new Dictionary<string, object> ();
		data.Add ("vid", mPostBoxVID);
		data.Add ("session_token", mPostBoxSessionToken);
		data.Add ("world", mSelectedWorld);
		//Debug.Assert (postID.Count != 0);

		data.Add ("post_id_list", MiniJSON.Json.Serialize (postID));
		StartCoroutine (requestAPI (API.RECEIVE_POST, data, callback));
	}

	public void requestIapVerify(string itemType, string bypassInfo, string additionalInfo, PostBoxCallback callback)
	{
		Dictionary<string,object> data = new Dictionary<string, object> ();
		data.Add ("world", mSelectedWorld);
		data.Add ("vid", mPostBoxVID);
		data.Add ("session_token", mPostBoxSessionToken);
		data.Add ("purchase_bypass_info", bypassInfo);
		data.Add ("game_info", additionalInfo);
		data.Add ("item_type", itemType);

		StartCoroutine (requestAPI (API.IAPV4_VERIFY, data, callback));
	}

	IEnumerator requestAPI(API api,Dictionary<string,object> data,PostBoxCallback callback)
	{
		//Debug.Assert (callback != null);
		//Debug.Assert (data != null);

		string jsonData = MiniJSON.Json.Serialize (data);
		jsonData = jsonData.Replace("\"[\\\"", "[\"");
		jsonData = jsonData.Replace("\\\"]\"", "\"]");

		string url = getURL (api);
		byte[] byteData = System.Text.Encoding.UTF8.GetBytes(jsonData);

		Debug.Log ("PostBox SendData : " + jsonData);
		Debug.Log ("PostBox Send URL : " + url);

		UnityWebRequest www = UnityWebRequest.Post(url, jsonData);
		www.method = UnityWebRequest.kHttpVerbPOST;
		www.uploadHandler = new UploadHandlerRaw(byteData);
		www.uploadHandler.contentType = "application/json";

		if (api == API.IAPV4_VERIFY)
		{
			www.SetRequestHeader("Content-Type", "application/json");
		}
		yield return www.SendWebRequest();

		PostBoxResultData retData = new PostBoxResultData();

		if (www.isDone && String.IsNullOrEmpty(www.error))
		{
			Debug.Log("requestAPI result : " + www.downloadHandler.text);
			System.Object deserializeResult = MiniJSON.Json.Deserialize(www.downloadHandler.text);
			System.Collections.Generic.Dictionary<string, object> parseData = deserializeResult as System.Collections.Generic.Dictionary<string, object>;

			if (parseData != null)
			{

				if (parseData.ContainsKey("result_code"))
				{
					retData.SetErrorCode(parseData["result_code"] as string);
				}

				if (parseData.ContainsKey("result_msg"))
				{
					retData.ErrorMsg = parseData["result_msg"] as string;
				}

				if (parseData.ContainsKey("result_data"))
				{
					retData.ResultData = parseData["result_data"] as Dictionary<string, object>;
				}

				if (parseData.ContainsKey("session_token"))
				{
					string sessionToken = parseData["session_token"] as string;
					//if sessionToken is null, assert process?
					if (sessionToken != null)
					{
						mPostBoxSessionToken = sessionToken;
					}
				}
			}
		}
		else
		{
			retData.ErrorCode = PostBoxResultData.ErrorCodes.INTERNAL_ERROR;
			retData.ErrorMsg = www.error + "Network Error";
			Debug.Log("requestAPI result : " + www.error);
		}
		if (callback != null)
			callback (retData);
	}
}
