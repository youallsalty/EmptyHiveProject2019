﻿
// only on IAP Postbox sample.
static class PostboxInfo{

	public static string SampleAppVersion = "1.0.0";

	public static bool isCompletionHandlerMode;
	public static string UID = "";
	public static string VID = "";
	public static string VIDType = "v4";
	public static string DID = "";
	public static string hiveSessionToken = "";
	public static string serverState = "";
	public static string appID = "";
	public static string serverID = "";
	public static string loginState = "";

	public static string country = "kr";
	public static string language = "ko";
	public static string appversion="0";

	public static string UsedVID = "";
	public static string CurrentVID = "";

	public static string SelectVID = "";


	public static string QRCode = "";
	public static string CommonLink = "";

	public static string clickActionName = "";			// save this data for log

	public static string hiveiapWorldName = "";


}