﻿using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;

public class PostboxView {

	public static PostboxView instance = new PostboxView ();

	public enum PostType
	{
		CONSUMABLE = 0,
		SUBSCRIPTION,
		MAX
	}

	public List<object> conList;
	public List<object> subList;
	public PostType postType;
	public string savedSessionToken;

	String additionalInfo = "";

	public delegate void RemovePostBoxCB();

	public RemovePostBoxCB finishCallback;

	// Use this for initialization
	public void Start () {
		
	}
	
	// Update is called once per frame
	public void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			ExternalTestManager.instance.switchView (ExternalTestViewType.HIDE_VIEW);
			PostboxView.instance.finishCallback();
		}
	}

	public void showPostBox(string type, RemovePostBoxCB cb) {
		if (type == "subscription")
			postType = PostType.SUBSCRIPTION;
		else
			postType = PostType.CONSUMABLE;

		// 로그인 계정이 달라졌다면 재로그인 시도, 아니면 refresh  
		if (savedSessionToken != PostboxInfo.hiveSessionToken)
			callThePostboxLogin();
		else
			getPostBoxData();

		ExternalTestManager.instance.switchView(ExternalTestViewType.SHOW_VIEW);
		PostboxView.instance.finishCallback = cb;
	}


	public void callThePostboxLogin()
	{
		Debug.Log("Postbox initialize");
		if (PostboxInfo.serverState.ToLower() == "Sandbox".ToLower())
		{
			Postbox.Instance.SetServerState(Postbox.ServerState.Sandbox);
		}
		else if (PostboxInfo.serverState.ToLower() == "Real".ToLower())
		{
			Postbox.Instance.SetServerState(Postbox.ServerState.Live);
		}
		else if (PostboxInfo.serverState.ToLower() == "Test".ToLower())
		{
			Postbox.Instance.SetServerState(Postbox.ServerState.Test);
		}
		else if (PostboxInfo.serverState.ToLower() == "Dev".ToLower())
		{
			Postbox.Instance.SetServerState(Postbox.ServerState.Dev);
		}

		savedSessionToken = PostboxInfo.hiveSessionToken;

		Postbox.Instance.requestLoginCenter(PostboxInfo.hiveiapWorldName, PostboxInfo.VID, PostboxInfo.VIDType, PostboxInfo.DID, PostboxInfo.UID, PostboxInfo.appID, PostboxInfo.country, PostboxInfo.language, PostboxInfo.appversion, PostboxInfo.hiveSessionToken, initCB);
	}

	public void getPostBoxData()
	{

		Postbox.Instance.requestRefreshPost(getpostboxdataCB);
	}

	public void getpostboxdataCB(Postbox.PostBoxResultData resultData)
	{

		if (resultData != null)
		{
			Debug.Log("Postbox (Game Sample Server) Result -- getpostboxdataCB\n" + " // " + GetLine(resultData.ResultData));

			this.conList = resultData.ResultData["post_list"] as List<object>;
			this.subList = resultData.ResultData["post_subscription_list"] as List<object>;
			// if (Postbox.Instance.infoDict != null) {
			// 	Debug.Log("====>>>> Postbox.Instance.infoDict.Count : " + Postbox.Instance.infoDict.Count);	
			// }
		}
		else
			Debug.Log("Called getpostboxdataCB resultData is null");
	}

	public void initCB(Postbox.PostBoxResultData resultData)
	{

		if (resultData != null)
		{

			if (resultData.ResultData != null)
				Debug.Log("Postbox (Game Sample Server) Result -- initCB\n" + " // " + GetLine(resultData.ResultData));

			string makeTextStr = "";

			if (resultData.ResultData != null && resultData.ResultData.ContainsKey("result_data"))
			{
				Dictionary<string, object> resultDict = resultData.ResultData["result_data"] as Dictionary<string, object>;

				makeTextStr = GetLine(resultDict);
			}
		}
		getPostBoxData();
	}

	string GetLine(Dictionary<string, object> d)
	{
		// Build up each line one-by-one and then trim the end
		if (d == null)
		{
			return "";
		}
		StringBuilder builder = new StringBuilder();
		foreach (KeyValuePair<string, object> pair in d)
		{
			builder.Append(pair.Key).Append(":").Append(pair.Value).Append(',');
		}
		string result = builder.ToString();
		// Remove the final delimiter
		result = result.TrimEnd(',');
		return result;
	}

	public void OnGUI() {

		ExternalTestView.layout(24);

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
		if (ExternalTestView.createButton ("- Post Box-")) {
			ExternalTestManager.instance.switchView(ExternalTestViewType.HIDE_VIEW);
			PostboxView.instance.finishCallback();
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		if (Postbox.Instance != null ) {
			if (postType == PostType.CONSUMABLE && PostboxView.instance.conList != null && PostboxView.instance.conList.Count > 0) {
				foreach (Dictionary<string, object> nowdict in conList) {
					string showStr = "";
					showStr += ("postid : " + nowdict ["post_id"] as string);
					showStr += (" | asset_code : " + nowdict ["asset_code"] as string);
					showStr += ("\n time : " + nowdict ["date_added"] as string);

					if (string.IsNullOrEmpty(nowdict ["character_key"] as string) == false)
						showStr += ("\n character_key : " + nowdict ["character_key"] as string);

					if (ExternalTestView.createButton(showStr)) {
						// TODO: Item cosume.

						String postid = nowdict ["post_id"] as string;

						Postbox.Instance.requestReceivePost(postid, (receiveResultData) => {
							if (receiveResultData.ErrorCode == Postbox.PostBoxResultData.ErrorCodes.SUCCESS) {

								Postbox.Instance.requestRefreshPost( (refreshResultData) => {
									if (refreshResultData.ErrorCode == Postbox.PostBoxResultData.ErrorCodes.SUCCESS) {

										PostboxView.instance.conList = refreshResultData.ResultData ["post_list"] as List<object>;
									}
								});
							}
						});
					}
				}
			}

			// Subscritption
			if (postType == PostType.SUBSCRIPTION && PostboxView.instance.subList != null && PostboxView.instance.subList.Count > 0) {
				foreach (Dictionary<string, object> nowdict in PostboxView.instance.subList) {
					string showStr = "";
					showStr += ("product_id : " + nowdict ["product_id"] as string);
					showStr += ("\n market_id : " + nowdict ["market_id"] as string);// == 1 ? "Apple" : "Google");
					showStr += ("\n start_date : " + nowdict ["start_date"] as string);
					showStr += ("\n expire_date : " + nowdict ["expire_date"] as string);

					if (string.IsNullOrEmpty(nowdict ["character_key"] as string) == false)
						showStr += ("\n character_key : " + nowdict ["character_key"] as string);

					if (ExternalTestView.createButton(showStr)) {
						// It cannnot consume
					}
				}
			}
		}

		GUILayout.EndVertical();
	}
}
