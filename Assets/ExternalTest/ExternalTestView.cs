﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// 테스트를 수행 할 화면 형태 정의
public enum ExternalTestViewType
{
	HIDE_VIEW               // 테스트 화면 숨김
	, SHOW_VIEW             // 테스트 화면 노출
	, POSTBOX_VIEW              // 우편함 테스트 화면
}


// 테스트 뷰 화면 전환을 관리하는 기능을 제공
public class ExternalTestManager
{

	public static ExternalTestManager instance = new ExternalTestManager();

	public ExternalTestViewType currentViewType = ExternalTestViewType.POSTBOX_VIEW;    // 현재 선택 된 테스트 화면

	public bool _isShow = false;         // 테스트 화면 표시 여부

	public bool isShowConsoleLogView = false;   // 로그 뷰를 Gesture가 아닌 버튼 동작으로 노출시키기 위함


	public int screenWidth = 0;         // 화면 크기
	public int fontSize = 0;            // 폰트 크기 (가변적)
	public int buttonHeight = 0;        // 버튼 크기 (가변적)
	public int bottomBarHeight = 0;

	// 테스트 뷰 전환
	public void switchView(ExternalTestViewType testViewType)
	{

		if (testViewType == ExternalTestViewType.HIDE_VIEW)
		{
			ExternalTestManager.instance._isShow = false;
		}
		else if (testViewType == ExternalTestViewType.SHOW_VIEW)
		{
			ExternalTestManager.instance._isShow = true;
		}
		else
		{
			ExternalTestManager.instance.currentViewType = testViewType;
		}
	}
}

public class ExternalTestView : MonoBehaviour
{

	// Start is called before the first frame update
	void Start()
    {
		// 테스트 화면이 추가되면 여기에 코드를 추가한다
		PostboxView.instance.Start ();
	}

	// Update is called once per frame
	void Update()
    {
		// 테스트 화면이 추가되면 여기에 코드를 추가한다
		ExternalTestManager testViewManager = ExternalTestManager.instance;

		if (testViewManager.currentViewType == ExternalTestViewType.POSTBOX_VIEW) {
			PostboxView.instance.Update ();
		}
    }

    void OnGUI() {

		// 테스트 화면이 추가되면 여기에 코드를 추가한다
		ExternalTestManager testViewManager = ExternalTestManager.instance;

		if (testViewManager._isShow == false) {
			return;
		}

		if (testViewManager.currentViewType == ExternalTestViewType.POSTBOX_VIEW) {
			PostboxView.instance.OnGUI ();
		}
	}

    public static void layout(int contentCount) {

		ExternalTestManager testViewManager = ExternalTestManager.instance;
    	
#if !UNITY_EDITOR && UNITY_ANDROID

		testViewManager.screenWidth = Screen.width;
        if(Screen.height > 1280)
			testViewManager.fontSize = 40;
        else if(Screen.height > 800)
			testViewManager.fontSize = 35;
        else if(Screen.height > 400)
			testViewManager.fontSize = 20;
        else
			testViewManager.fontSize = 10;
        
#elif !UNITY_EDITOR && UNITY_IPHONE
		testViewManager.fontSize = 30;
#else
		testViewManager.fontSize = ((testViewManager.buttonHeight - 10) > 15) ? 15 : (testViewManager.buttonHeight - 10);
#endif
		testViewManager.screenWidth = Screen.width;
		// 높이 증가를 위한 보간 값 +10
		testViewManager.buttonHeight = (Screen.height  / contentCount) + 10 ;
		if (Screen.width >= Screen.height) {
			// Landscape 일때 버튼 높이 보간 값 +15
			testViewManager.buttonHeight += 15;
		}

		testViewManager.bottomBarHeight = testViewManager.buttonHeight + 30;
		//Debug.Log("fontSize : " + instance.fontSize);

		// 버튼 크기에 맞는 일정한 폰트 사이즈 계산
		testViewManager.fontSize = testViewManager.buttonHeight / 3;
		GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = GUI.skin.textField.fontSize = testViewManager.fontSize;
    }

	/** 단일 디자인 객체를 노출시 활용할 함수들 **/
	public static void createBox(string text) {

		ExternalTestManager testViewManager = ExternalTestManager.instance;

		GUILayout.Box(text, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxWidth(testViewManager.screenWidth));
	}


    public static void createLabel(string label) {

		ExternalTestManager testViewManager = ExternalTestManager.instance;
    
		GUIStyle style = GUI.skin.GetStyle("Label");
		style.fontSize = testViewManager.fontSize; 
		GUI.skin.box = style;

		GUILayout.Label(label);
    }


	public static bool createButton(string text) {

		ExternalTestManager testViewManager = ExternalTestManager.instance;
    
		GUIStyle style = GUI.skin.GetStyle("button");
		style.fontSize = testViewManager.fontSize; 
		GUI.skin.box = style;

		return GUILayout.Button(text, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxWidth(testViewManager.screenWidth));
    }
	
	public static bool createButton(string text, params GUILayoutOption[] options) {

		ExternalTestManager testViewManager = ExternalTestManager.instance;
		return GUILayout.Button(text, options);
    }

    public static string createTextField(string defaultString) {

		ExternalTestManager testViewManager = ExternalTestManager.instance;
    
		GUIStyle style = GUI.skin.GetStyle("TextField");
		style.fontSize = testViewManager.fontSize; 
		GUI.skin.box = style;

		return GUILayout.TextField(defaultString, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxWidth(testViewManager.screenWidth));
    }

    public static string createTextArea(string defaultString, int height)
    {
		ExternalTestManager testViewManager = ExternalTestManager.instance;

		GUIStyle style = GUI.skin.GetStyle("TextArea");
		style.fontSize = testViewManager.fontSize; 
		GUI.skin.box = style;

        return GUILayout.TextArea(defaultString, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxHeight(height), GUILayout.MaxWidth(testViewManager.screenWidth));
    }



	public static bool createToggle(bool val, string text) {

		ExternalTestManager testViewManager = ExternalTestManager.instance;
		return GUILayout.Toggle (val, text, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxWidth(testViewManager.screenWidth));
	}


	public static int selectionGrid(int selPosition, string[] texts, int count) {

		return selectionGrid(selPosition, texts, count, 2);
	}


	public static int selectionGrid(int selPosition, string[] texts, int count, int xCount) {

		ExternalTestManager testViewManager = ExternalTestManager.instance;
		return GUILayout.SelectionGrid(selPosition, texts, xCount, GUILayout.MinHeight(testViewManager.buttonHeight*((count%xCount==0)?count/xCount:count/xCount+1)), GUILayout.MaxWidth(testViewManager.screenWidth));
	}


	/** 디자인 한 줄을 노출시 활용할 함수들 **/

	public static void showSubTitle(string subTitle) {
		GUILayout.Space (15);
		GUILayout.BeginHorizontal();

		ExternalTestManager testViewManager = ExternalTestManager.instance;
		GUIStyle style = new GUIStyle("Label");
		style.fontSize = testViewManager.fontSize + 5;
		style.alignment = TextAnchor.MiddleCenter;
		style.fixedWidth = testViewManager.screenWidth;
		GUILayout.Label(subTitle, style);
		

		GUILayout.EndHorizontal();
	}

	public static void showNonEditableTextWithTitle(string title, string value) {

		ExternalTestManager testViewManager = ExternalTestManager.instance;


		GUILayout.Space (10);
		GUILayout.BeginHorizontal();


		GUIStyle titleStyle = new GUIStyle("Label");
		titleStyle.fontSize = testViewManager.fontSize;
		titleStyle.alignment = TextAnchor.MiddleRight;
		titleStyle.fixedWidth = testViewManager.screenWidth/3;
		titleStyle.wordWrap = true;
		GUILayout.Label(title + " : ", titleStyle);
		
		
		GUIStyle valueStyle = new GUIStyle("Label");
		valueStyle.fontSize = testViewManager.fontSize;
		valueStyle.alignment = TextAnchor.MiddleLeft;
		valueStyle.fixedWidth = testViewManager.screenWidth/3 * 2;
		valueStyle.wordWrap = true;
		GUILayout.Label(value, valueStyle);
		

		GUILayout.EndHorizontal();
	}

	public static void showEditableTextWithTitle(string title, ref string value) {

		ExternalTestManager testViewManager = ExternalTestManager.instance;


		GUILayout.Space (10);
		GUILayout.BeginHorizontal();


		GUIStyle titleStyle = new GUIStyle("Label");
		titleStyle.fontSize = testViewManager.fontSize;
		titleStyle.alignment = TextAnchor.MiddleRight;
		titleStyle.fixedWidth = testViewManager.screenWidth/3;
		titleStyle.wordWrap = true;
		GUILayout.Label(title + " : ", titleStyle);
		
		

		GUIStyle valueStyle = new GUIStyle("Label");
		valueStyle.fontSize = testViewManager.fontSize;
		valueStyle.alignment = TextAnchor.MiddleLeft;
		valueStyle.fixedWidth = testViewManager.screenWidth/3 * 2;
		valueStyle.wordWrap = true;

		value =  GUILayout.TextField(value, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxWidth(testViewManager.screenWidth));

		GUILayout.EndHorizontal();


	}

	public static bool showButton(string text) {
		ExternalTestManager testViewManager = ExternalTestManager.instance;

		return GUILayout.Button(text, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxWidth(testViewManager.screenWidth));
	}

}




