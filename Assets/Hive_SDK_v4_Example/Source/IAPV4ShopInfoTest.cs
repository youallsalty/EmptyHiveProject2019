﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using hive;			// HIVE SDK 네임스페이스 사용

public class IAPV4ShopInfoTestView {

	public static IAPV4ShopInfoTestView instance = new IAPV4ShopInfoTestView ();

	public void setShopInfo(List<hive.IAPV4.IAPV4Product> iapV4ProductList, int balance, bool bSubscription) {

		this.iapV4ProductList = iapV4ProductList;		

		if(bSubscription == true)
		{
			//Reset Product Subscription Group.
			subscriptionGroup.Clear();

			List<hive.IAPV4.IAPV4Product> productGroup1 = new List<hive.IAPV4.IAPV4Product>();
			List<hive.IAPV4.IAPV4Product> productGroup2 = new List<hive.IAPV4.IAPV4Product>();
			List<hive.IAPV4.IAPV4Product> productGroup3 = new List<hive.IAPV4.IAPV4Product>();
			List<hive.IAPV4.IAPV4Product> productGroup4 = new List<hive.IAPV4.IAPV4Product>();

			//divide subscription group/
			foreach (hive.IAPV4.IAPV4Product product in iapV4ProductList)
			{
				// product.marketPid.Equals
				//	presetSubscriptionGroup has 2 item(List) .
				if( ( (List<string>)presetSubscriptionGroup[0]).Contains(product.marketPid))
				{
					productGroup1.Add(product);
				}
				else if( ( (List<string>)presetSubscriptionGroup[1]).Contains(product.marketPid))
				{
					productGroup2.Add(product);
				}
				#if UNITY_IOS
				else if( ( (List<string>)presetSubscriptionGroup[2]).Contains(product.marketPid))
				{
					productGroup3.Add(product);
				}
				#endif
				else
				{
					productGroup4.Add(product);
				}
			}
			
			subscriptionGroup.Insert(0,productGroup1);
			subscriptionGroup.Insert(1,productGroup2);
			#if UNITY_IOS
			subscriptionGroup.Insert(2,productGroup3);
			if(productGroup4.Count > 0) {
				subscriptionGroup.Insert(3,productGroup4);		// productGroup4 insert to group 4th
			#else
			if(productGroup4.Count > 0) {
				subscriptionGroup.Insert(2,productGroup4);		// productGroup4 insert to group 3rd 
			#endif
			}
		}
		this.balance = balance;

		this.bSubscription = bSubscription;
	}
	
	// Product preset Group Use this for initialization
	private List<List<string>> presetSubscriptionGroup = new List<List<string>>();

	// Product Group
	private List<List<hive.IAPV4.IAPV4Product>> subscriptionGroup = new List<List<hive.IAPV4.IAPV4Product>>();

	// Use this for initialization
	public void Start () {
//	HIVE SAMPLE Market PID PRESET
	 	List<String> group1 = new List<string>();
		#if UNITY_ANDROID
		group1.Add("sub30");
		group1.Add("sub10");
		group1.Add("sub01");
		group1.Add("sub11");
		#elif UNITY_IOS
		group1.Add("com.com2us.hivesdk.normal.freefull.apple.global.ios.universal.arshive001");
		group1.Add("com.com2us.hivesdk.normal.freefull.apple.global.ios.universal.arshive002");
		group1.Add("com.com2us.hivesdk.normal.freefull.apple.global.ios.universal.arshive003");
		#endif

	 	List<String> group2 = new List<string>();
		#if UNITY_ANDROID
 		group2.Add("sub303");
		group2.Add("sub103");
		group2.Add("sub013");
		group2.Add("sub113");
		#elif UNITY_IOS
 		group2.Add("com.com2us.hivesdk.normal.freefull.apple.global.ios.universal.arshive011");
		#endif
		presetSubscriptionGroup.Insert(0,group1);
		presetSubscriptionGroup.Insert(1,group2);

		#if UNITY_IOS
	 	List<String> group3 = new List<string>();
		group3.Add("com.com2us.hivesdk.normal.freefull.apple.global.ios.universal.arshive031");
		group3.Add("com.com2us.hivesdk.normal.freefull.apple.global.ios.universal.arshive032");
		group3.Add("com.com2us.hivesdk.normal.freefull.apple.global.ios.universal.arshive033");
		presetSubscriptionGroup.Insert(1,group3);
		#endif
		
	}
	
	// Update is called once per frame
	public void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.IAP_VIEW);
		}
	}

	public void onIAPV4Purchase(ResultAPI result, hive.IAPV4.IAPV4Receipt iapV4Receipt) {

		Debug.Log("IAPV4TestView.onIAPV4Purchase() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() && iapV4Receipt != null) {

			hive.IAPV4.IAPV4Type type = iapV4Receipt.type;

			if (type == hive.IAPV4.IAPV4Type.APPLE_APPSTORE) {

				hive.IAPV4.IAPV4ReceiptApple iapV4ReceiptApple = (hive.IAPV4.IAPV4ReceiptApple)iapV4Receipt;

				Debug.Log ("iapV4ReceiptApple = \n");

				Debug.Log (iapV4ReceiptApple.toString() + "\n");
			}
			else if (type == hive.IAPV4.IAPV4Type.GOOGLE_PLAYSTORE) {

				hive.IAPV4.IAPV4ReceiptGoogle iapV4ReceiptGoogle = (hive.IAPV4.IAPV4ReceiptGoogle)iapV4Receipt;

				Debug.Log ("iapV4ReceiptGoogle = \n");

				Debug.Log (iapV4ReceiptGoogle.toString() + "\n");
			}
			else if (type == hive.IAPV4.IAPV4Type.ONESTORE) {

				hive.IAPV4.IAPV4ReceiptOneStore iapV4ReceiptOneStore = (hive.IAPV4.IAPV4ReceiptOneStore)iapV4Receipt;

				Debug.Log ("iapV4ReceiptOneStore = \n");

				Debug.Log (iapV4ReceiptOneStore.toString() + "\n");
			}
			else if (type == hive.IAPV4.IAPV4Type.HIVE_LEBI) {

				hive.IAPV4.IAPV4ReceiptLebi iapV4ReceiptLebi = (hive.IAPV4.IAPV4ReceiptLebi)iapV4Receipt;

				Debug.Log ("iapV4ReceiptLebi = \n");

				Debug.Log (iapV4ReceiptLebi.toString() + "\n");
			}
			else if (type == hive.IAPV4.IAPV4Type.AMAZON_APPSTORE) {

				hive.IAPV4.IAPV4ReceiptAmazon iapV4ReceiptAmazon = (hive.IAPV4.IAPV4ReceiptAmazon)iapV4Receipt;

				Debug.Log ("iapV4ReceiptAmazon = \n");

				Debug.Log (iapV4ReceiptAmazon.toString() + "\n");
			}
			else if (type == hive.IAPV4.IAPV4Type.SAMSUNG_GALAXYSTORE) {

                hive.IAPV4.IAPV4ReceiptSamsung iapV4ReceiptSamsung = (hive.IAPV4.IAPV4ReceiptSamsung)iapV4Receipt;

                Debug.Log ("iapV4ReceiptSamsung = \n");

                Debug.Log (iapV4ReceiptSamsung.toString() + "\n");
            }
			else if (type == hive.IAPV4.IAPV4Type.HUAWEI_APPGALLERY) {

				hive.IAPV4.IAPV4ReceiptHuawei iapV4ReceiptHuawei = (hive.IAPV4.IAPV4ReceiptHuawei)iapV4Receipt;

				Debug.Log ("iapV4ReceiptHuawei = \n");

				Debug.Log (iapV4ReceiptHuawei.toString() + "\n");
			}

			if (String.IsNullOrEmpty(iapV4Receipt.product.marketPid) == false) {

				this.marketPid += iapV4Receipt.product.marketPid + " ";

				marketPidList.Add(iapV4Receipt.product.marketPid);
			}

			Postbox.Instance.requestIapVerify("consumable", iapV4Receipt.bypassInfo, iapV4Receipt.additionalInfo, (resultData) => {

				if (resultData.ErrorCode == Postbox.PostBoxResultData.ErrorCodes.SUCCESS) {

					IAPV4.transactionFinish (iapV4Receipt.product.marketPid, onIAPV4TransactionFinish);
				}
			});
		}
	}


	// Subscription Callback
	public void onIAPV4PurchaseSubscriptionUpdate(ResultAPI result, hive.IAPV4.IAPV4Receipt iapV4Receipt) {

		Debug.Log("IAPV4TestView.onIAPV4PurchaseSubscriptionUpdate() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			hive.IAPV4.IAPV4Type type = iapV4Receipt.type;

			if (type == hive.IAPV4.IAPV4Type.APPLE_APPSTORE) {

				hive.IAPV4.IAPV4ReceiptApple iapV4ReceiptApple = (hive.IAPV4.IAPV4ReceiptApple)iapV4Receipt;

				Debug.Log ("iapV4ReceiptApple = \n");

				Debug.Log (iapV4ReceiptApple.toString() + "\n");
			}
			else if (type == hive.IAPV4.IAPV4Type.GOOGLE_PLAYSTORE) {

				hive.IAPV4.IAPV4ReceiptGoogle iapV4ReceiptGoogle = (hive.IAPV4.IAPV4ReceiptGoogle)iapV4Receipt;

				Debug.Log ("iapV4ReceiptGoogle = \n");

				Debug.Log (iapV4ReceiptGoogle.toString() + "\n");
			}
			else if (type == hive.IAPV4.IAPV4Type.ONESTORE) {

                hive.IAPV4.IAPV4ReceiptOneStore iapV4ReceiptOneStore = (hive.IAPV4.IAPV4ReceiptOneStore)iapV4Receipt;

                Debug.Log ("iapV4ReceiptOneStore = \n");

                Debug.Log (iapV4ReceiptOneStore.toString() + "\n");
            }
			else if (type == hive.IAPV4.IAPV4Type.AMAZON_APPSTORE) {

				hive.IAPV4.IAPV4ReceiptAmazon iapV4ReceiptAmazon = (hive.IAPV4.IAPV4ReceiptAmazon)iapV4Receipt;

				Debug.Log ("iapV4ReceiptAmazon = \n");

				Debug.Log (iapV4ReceiptAmazon.toString() + "\n");
			}
			else if (type == hive.IAPV4.IAPV4Type.SAMSUNG_GALAXYSTORE) {

                hive.IAPV4.IAPV4ReceiptSamsung iapV4ReceiptSamsung = (hive.IAPV4.IAPV4ReceiptSamsung)iapV4Receipt;

                Debug.Log ("iapV4ReceiptSamsung = \n");

                Debug.Log (iapV4ReceiptSamsung.toString() + "\n");
            }
			else if (type == hive.IAPV4.IAPV4Type.HUAWEI_APPGALLERY) {

				hive.IAPV4.IAPV4ReceiptHuawei iapV4ReceiptHuawei = (hive.IAPV4.IAPV4ReceiptHuawei)iapV4Receipt;

				Debug.Log ("iapV4ReceiptHuawei = \n");

				Debug.Log (iapV4ReceiptHuawei.toString() + "\n");
			}
			
			if (String.IsNullOrEmpty(iapV4Receipt.product.marketPid) == false) {

				this.marketPid += iapV4Receipt.product.marketPid + " ";

				marketPidList.Add(iapV4Receipt.product.marketPid);
			}

			Postbox.Instance.requestIapVerify("subscription", iapV4Receipt.bypassInfo, iapV4Receipt.additionalInfo, (resultData) => {

				if (resultData.ErrorCode == Postbox.PostBoxResultData.ErrorCodes.SUCCESS) {

					IAPV4.transactionFinish (iapV4Receipt.product.marketPid, onIAPV4TransactionFinish);
				}
			});
		}
	}

	// HIVE IAP 상품에 대한 미지급된 아이템 복구를 요청 결과 통지 리스너 - IAPV4.restore()
	public void onIAPV4Restore(ResultAPI result, List<hive.IAPV4.IAPV4Receipt> iapv4ReceiptList) {

		Debug.Log("IAPV4TestView.onIAPV4Restore() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			Debug.Log ("iapv4ReceiptList = \n");
			
			foreach(hive.IAPV4.IAPV4Receipt iapV4Receipt in iapv4ReceiptList) {

				hive.IAPV4.IAPV4Type type = iapV4Receipt.type;

				if (type == hive.IAPV4.IAPV4Type.APPLE_APPSTORE) {

					hive.IAPV4.IAPV4ReceiptApple iapV4ReceiptApple = (hive.IAPV4.IAPV4ReceiptApple)iapV4Receipt;

					Debug.Log ("iapV4ReceiptApple = \n");

					Debug.Log (iapV4ReceiptApple.toString() + "\n");
				}
				else if (type == hive.IAPV4.IAPV4Type.GOOGLE_PLAYSTORE) {

					hive.IAPV4.IAPV4ReceiptGoogle iapV4ReceiptGoogle = (hive.IAPV4.IAPV4ReceiptGoogle)iapV4Receipt;

					Debug.Log ("iapV4ReceiptGoogle = \n");

					Debug.Log (iapV4ReceiptGoogle.toString() + "\n");
				}
				else if (type == hive.IAPV4.IAPV4Type.ONESTORE) {

					hive.IAPV4.IAPV4ReceiptOneStore iapV4ReceiptOneStore = (hive.IAPV4.IAPV4ReceiptOneStore)iapV4Receipt;

					Debug.Log ("iapV4ReceiptOneStore = \n");

					Debug.Log (iapV4ReceiptOneStore.toString() + "\n");
				}
				else if (type == hive.IAPV4.IAPV4Type.HIVE_LEBI) {

					hive.IAPV4.IAPV4ReceiptLebi iapV4ReceiptLebi = (hive.IAPV4.IAPV4ReceiptLebi)iapV4Receipt;

					Debug.Log ("iapV4ReceiptLebi = \n");

					Debug.Log (iapV4ReceiptLebi.toString() + "\n");
				}
				else if (type == hive.IAPV4.IAPV4Type.AMAZON_APPSTORE) {

					hive.IAPV4.IAPV4ReceiptAmazon iapV4ReceiptAmazon = (hive.IAPV4.IAPV4ReceiptAmazon)iapV4Receipt;

					Debug.Log ("iapV4ReceiptAmazon = \n");

					Debug.Log (iapV4ReceiptAmazon.toString() + "\n");
				}
				else if (type == hive.IAPV4.IAPV4Type.SAMSUNG_GALAXYSTORE) {

                    hive.IAPV4.IAPV4ReceiptSamsung iapV4ReceiptSamsung = (hive.IAPV4.IAPV4ReceiptSamsung)iapV4Receipt;

                    Debug.Log ("iapV4ReceiptSamsung = \n");

                    Debug.Log (iapV4ReceiptSamsung.toString() + "\n");
                }
				else if (type == hive.IAPV4.IAPV4Type.HUAWEI_APPGALLERY) {

					hive.IAPV4.IAPV4ReceiptHuawei iapV4ReceiptHuawei = (hive.IAPV4.IAPV4ReceiptHuawei)iapV4Receipt;

					Debug.Log ("iapV4ReceiptHuawei = \n");

					Debug.Log (iapV4ReceiptHuawei.toString() + "\n");
				}

				if (marketPidList.Contains(iapV4Receipt.product.marketPid) == false) {

					this.marketPid += iapV4Receipt.product.marketPid + " ";
					
					marketPidList.Add(iapV4Receipt.product.marketPid);
				}


				if (result.errorCode != ResultAPI.ErrorCode.NOT_OWNED) {

					Postbox.Instance.requestIapVerify("consumable", iapV4Receipt.bypassInfo, iapV4Receipt.additionalInfo, (resultData) => {
						if (resultData.ErrorCode == Postbox.PostBoxResultData.ErrorCodes.SUCCESS) {

							IAPV4.transactionFinish (iapV4Receipt.product.marketPid, onIAPV4TransactionFinish);
						}
					});
				}
			}
		}
	}


	// HIVE IAP 구독 상태 복구를 요청 결과 통지 리스너 - IAPV4.restoreSubscription()
	public void onIAPV4RestoreSubscription(ResultAPI result, List<hive.IAPV4.IAPV4Receipt> iapv4ReceiptList) {

		Debug.Log("IAPV4TestView.onIAPV4RestoreSubscription() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			Debug.Log ("iapv4ReceiptList = \n");
			
			foreach(hive.IAPV4.IAPV4Receipt iapV4Receipt in iapv4ReceiptList) {

				hive.IAPV4.IAPV4Type type = iapV4Receipt.type;

				if (type == hive.IAPV4.IAPV4Type.APPLE_APPSTORE) {

					hive.IAPV4.IAPV4ReceiptApple iapV4ReceiptApple = (hive.IAPV4.IAPV4ReceiptApple)iapV4Receipt;

					Debug.Log ("iapV4ReceiptApple = \n");

					Debug.Log (iapV4ReceiptApple.toString() + "\n");
				}
				else if (type == hive.IAPV4.IAPV4Type.GOOGLE_PLAYSTORE) {

					hive.IAPV4.IAPV4ReceiptGoogle iapV4ReceiptGoogle = (hive.IAPV4.IAPV4ReceiptGoogle)iapV4Receipt;

					Debug.Log ("iapV4ReceiptGoogle = \n");

					Debug.Log (iapV4ReceiptGoogle.toString() + "\n");
				}
				else if (type == hive.IAPV4.IAPV4Type.ONESTORE) {

                    hive.IAPV4.IAPV4ReceiptOneStore iapV4ReceiptOneStore = (hive.IAPV4.IAPV4ReceiptOneStore)iapV4Receipt;

                    Debug.Log ("iapV4ReceiptOneStore = \n");

                    Debug.Log (iapV4ReceiptOneStore.toString() + "\n");
                }
				else if (type == hive.IAPV4.IAPV4Type.AMAZON_APPSTORE) {

					hive.IAPV4.IAPV4ReceiptAmazon iapV4ReceiptAmazon = (hive.IAPV4.IAPV4ReceiptAmazon)iapV4Receipt;

					Debug.Log ("iapV4ReceiptAmazon = \n");

					Debug.Log (iapV4ReceiptAmazon.toString() + "\n");
				}
				else if (type == hive.IAPV4.IAPV4Type.SAMSUNG_GALAXYSTORE) {

                    hive.IAPV4.IAPV4ReceiptSamsung iapV4ReceiptSamsung = (hive.IAPV4.IAPV4ReceiptSamsung)iapV4Receipt;

                    Debug.Log ("iapV4ReceiptSamsung = \n");

                    Debug.Log (iapV4ReceiptSamsung.toString() + "\n");
                }
				else if (type == hive.IAPV4.IAPV4Type.HUAWEI_APPGALLERY) {

					hive.IAPV4.IAPV4ReceiptHuawei iapV4ReceiptHuawei = (hive.IAPV4.IAPV4ReceiptHuawei)iapV4Receipt;

					Debug.Log ("iapV4ReceiptHuawei = \n");

					Debug.Log (iapV4ReceiptHuawei.toString() + "\n");
				}

				if (marketPidList.Contains(iapV4Receipt.product.marketPid) == false) {

					this.marketPid += iapV4Receipt.product.marketPid + " ";
					
					marketPidList.Add(iapV4Receipt.product.marketPid);
				}


				if (result.errorCode != ResultAPI.ErrorCode.NOT_OWNED) {

					Postbox.Instance.requestIapVerify("subscription", iapV4Receipt.bypassInfo, iapV4Receipt.additionalInfo, (resultData) => {
						if (resultData.ErrorCode == Postbox.PostBoxResultData.ErrorCodes.SUCCESS) {

							IAPV4.transactionFinish (iapV4Receipt.product.marketPid, onIAPV4TransactionFinish);
						}
					});
				}
			}
		}
	}

	public void onIAPV4TransactionFinish(ResultAPI result, String marketPid) {

		Debug.Log("IAPV4TestView.onIAPV4TransactionFinish() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			this.marketPidList.Clear();
			this.marketPid = "";

			Debug.Log ("marketPid = " + marketPid + "\n");
		}
	}

	public void onIAPV4TransactionMultiFinish(List<ResultAPI> resultList, List<String> marketPidList) {

		Debug.Log("IAPV4TestView.onIAPV4TransactionMultiFinish() Callback\n");

		if (resultList != null) {
			Debug.Log ("resultList = \n");
			foreach(ResultAPI each in resultList) {

				Debug.Log (each.toString() + "\n");
			}
		}

		if (marketPidList != null) {
			Debug.Log ("marketPidList = \n");
			foreach(String marketPid in marketPidList) {

				this.marketPidList.Remove(marketPid);
				this.marketPid = "";

				Debug.Log (marketPid + "\n");
			}
		}
	}

	public void onIAPV4CheckPromotePurchase(ResultAPI result, String marketPid) {

		Debug.Log("IAPV4TestView.onIAPV4CheckPromotePurchase() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess ()) {

			Debug.Log ("marketPid = " + marketPid);

			// 이후 상품 소개 및 구매 처리 필요
			MainTestView.isIAPUpdated = false;
		}
	}

	int balance = 0;
	String marketPid = "";
	bool bSubscription = false;

	List<hive.IAPV4.IAPV4Product> iapV4ProductList = null;
	List<String> marketPidList = new List<String>();
	
	String additionalInfo = "";

	public void OnGUI() {

		HIVETestView.layout(24);

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- IAPV4 SHOPINFO-")) {
			HIVETestManager.instance.switchView (TestViewType.IAPV4_VIEW);
		}
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		//if (this.isPostbox == false) {
			// PostBox
			GUILayout.BeginHorizontal();		
			if (HIVETestView.createButton ("PostBox")) {

				Debug.Log("\n\n=== PostBox 요청 ===\n");
				Debug.Log("PostBox Called\n");

				HIVETestManager.instance.switchView(TestViewType.HIDE_VIEW);

				ConfigurationTestView.instance.postboxDataSetting();
				PostboxView.instance.showPostBox(bSubscription? "subscription":"consumable", () => {
					HIVETestManager.instance.switchView(TestViewType.SHOW_VIEW);
				});
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);

			GUILayout.BeginHorizontal();
			GUILayout.Box("Balance : ");
			GUILayout.Box(this.balance.ToString());
			GUILayout.EndHorizontal();
			GUILayout.Space (2);

			GUILayout.BeginHorizontal();
			GUILayout.Box("AdditionalInfo : ");
			this.additionalInfo = HIVETestView.createTextField(this.additionalInfo);
			GUILayout.EndHorizontal();
			GUILayout.Space (2);
			
			GUILayout.BeginHorizontal();
			GUILayout.Box("- Products -");
			GUILayout.EndHorizontal();
			GUILayout.Space (8);

			// Consumable Product
			if( bSubscription == false ) {
				GUILayout.BeginHorizontal();
				if (HIVETestView.createButton("IAPV4.restore()")) {

					Debug.Log("\n\n=== HIVE IAPV4 상품에 대한 복구를 요청. ===\n");
					Debug.Log(String.Format("IAPV4.restore(onIAPV4Restore) Called\n"));

					IAPV4.restore (onIAPV4Restore);
				}
				GUILayout.EndHorizontal();
				GUILayout.Space (2);

				for (int i=0; i<iapV4ProductList.Count; ++i) {

					GUILayout.BeginHorizontal();
					if (HIVETestView.createButton(iapV4ProductList[i].marketPid + "." + 
						iapV4ProductList[i].title + "(<color=grey>" + 
						iapV4ProductList[i].displayOriginalPrice + "</color>" +
						iapV4ProductList[i].displayPrice + ")" )) {

							Debug.Log("\n\n=== HIVE IAPV4 상품에 대한 구매를 요청. ===\n");
							Debug.Log(String.Format("IAPV4.purchase(pid = {0}, additionalInfo = {1}, onIAPPurchase) Called\n", iapV4ProductList[i].marketPid, additionalInfo));

							IAPV4.purchase (iapV4ProductList[i].marketPid, additionalInfo, onIAPV4Purchase);
						}
					GUILayout.BeginVertical();
					
					GUILayout.EndVertical();
					GUILayout.EndHorizontal();
					GUILayout.Space (8);
				}
			}
			

			// Subscription Product
			if( bSubscription == true ) {
				GUILayout.BeginHorizontal();
				if (HIVETestView.createButton("IAPV4.restoreSubscription()")) {

					Debug.Log("\n\n=== HIVE IAPV4 상품에 대한 복구를 요청. ===\n");
					Debug.Log(String.Format("IAPV4.restoreSubscription(onIAPV4RestoreSubscription) Called\n"));

					IAPV4.restoreSubscription (onIAPV4RestoreSubscription);
				}
				GUILayout.EndHorizontal();
				GUILayout.Space (2);

				for (int groupIndex=0; groupIndex<subscriptionGroup.Count; ++groupIndex) //foreach(List<hive.IAPV4.IAPV4Product> productGroup in subscriptionGroup)
				{
					foreach (hive.IAPV4.IAPV4Product iapV4SubsProduct in subscriptionGroup[groupIndex])
					{
						GUILayout.Space (8);
						GUILayout.BeginHorizontal();

//	1. Check Subscription Item in Postbox
						string oldMarketPid = null;	//	Can be null or some Pid.
						if (PostboxView.instance.subList != null && PostboxView.instance.subList.Count > 0) {
							foreach (Dictionary<string, object> nowdict in PostboxView.instance.subList) {
								string market_id = nowdict ["market_id"] as string;
								string product_id = nowdict["product_id"] as string;
								if (string.IsNullOrEmpty(product_id) == false) {
//	2. Compare whether PostBox item is include in group(preset groupIndex)
									if(presetSubscriptionGroup.Count > groupIndex && presetSubscriptionGroup[groupIndex].Contains(product_id)) {
										// This Item is include in group
										oldMarketPid = product_id;
											break;
									}
								}
							}
						}

						if(iapV4SubsProduct.marketPid.Equals(oldMarketPid))
						{
							GUI.backgroundColor = Color.red;
						}

						if (HIVETestView.createButton(iapV4SubsProduct.marketPid + ".\n" + 
							iapV4SubsProduct.title + "(" + 
							iapV4SubsProduct.displayPrice + ")" )) {
								if(iapV4SubsProduct.marketPid.Equals(oldMarketPid)) {
									Debug.Log("\n\n=== HIVE IAPV4 '구독 중'인 상품에 대한 구매를 요청. ===\n");
								}
								else{
									Debug.Log("\n\n=== HIVE IAPV4 구독 상품에 대한 구매를 요청. ===\n");
									Debug.Log(String.Format("IAPV4.purchaseSubscriptionUpdate(pid = {0}, additionalInfo = {1}, onIAPV4PurchaseSubscriptionUpdate) Called\n", iapV4SubsProduct.marketPid, additionalInfo));
									//	oldMarketPid only Use on Android.
									IAPV4.purchaseSubscriptionUpdate (iapV4SubsProduct.marketPid, oldMarketPid, additionalInfo, onIAPV4PurchaseSubscriptionUpdate);
								}
							}
						GUI.backgroundColor = Color.white;
						GUILayout.BeginVertical();
						
						GUILayout.EndVertical();
						GUILayout.EndHorizontal();
						GUILayout.Space (8);
					}
					GUILayout.Space (48);		//	enough Big constant for dividing Group 

				}
			}
			

			GUILayout.BeginHorizontal();
			GUILayout.Box("marketPid : ");
			this.marketPid = HIVETestView.createTextField(this.marketPid);
			GUILayout.EndHorizontal();
			GUILayout.Space (2);

			GUILayout.BeginHorizontal();
			if (marketPidList.Count == 1) {
				if (HIVETestView.createButton("IAPV4.transactionFinish()")) {

					Debug.Log("\n\n=== HIVE IAPV4 상품에 대한 트랜젝션 종료를 요청. ===\n");
					Debug.Log(String.Format("IAPV4.transactionFinish(marketPid = {0}, onIAPV4TransactionFinish) Called\n", marketPid));

					IAPV4.transactionFinish (marketPid.Trim(), onIAPV4TransactionFinish);
				}
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (2);

			GUILayout.BeginHorizontal();
			if (marketPidList.Count >= 1) {
				if (HIVETestView.createButton("IAPV4.transactionMultiFinish()")) {

					Debug.Log("\n\n=== HIVE IAPV4 상품들에 대한 트랜젝션 종료를 요청. ===\n");
					Debug.Log(String.Format("IAPV4.transactionMultiFinish(marketPidList = {0}, onIAPV4TransactionMultiFinish) Called\n", marketPid));

					IAPV4.transactionMultiFinish (this.marketPidList, onIAPV4TransactionMultiFinish);
				}
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (2);

			GUILayout.BeginHorizontal();
			if (MainTestView.isIAPUpdated == true) {
				if (HIVETestView.createButton("IAPV4.checkPromotePurchase()")) {

					Debug.Log(String.Format("IAPV4.onIAPV4CheckPromotePurchase Called\n"));

					IAPV4.checkPromotePurchase (onIAPV4CheckPromotePurchase);
				}
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (2);
		//}
		GUILayout.EndVertical();
	}
}
