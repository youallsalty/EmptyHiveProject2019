﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using hive;			// HIVE SDK 네임스페이스 사용

public class ConfigurationAPIKeyTestView
{


	public static ConfigurationAPIKeyTestView instance = new ConfigurationAPIKeyTestView ();

	private Vector2 scrollPosition = new Vector2(0, 0);
	private int contentCount = 24;

	private static string SAMPLEKEY_GoogleServerClientID 		= "331526026701-42uadtkeght91f7saspdg92gtdill6mv.apps.googleusercontent.com";

#if UNITY_IPHONE
	private static string SAMPLEKEY_WeChatSecretKey 			= "b5a73b0ee67d546bf876851944eda308";
	private static string SAMPLEKEY_WeChatPaymentKey 			= "auILMZisSYHnEb4ISD58QYm7uETdVX6d";
    private string SAMPLEKEY_AdjustKey 					= "8szd9kddtt6o";
    private string SAMPLEKEY_AdjustSecretID 			= "1";
    private string SAMPLEKEY_AdjustInfo1 				= "2071917595";
    private string SAMPLEKEY_AdjustInfo2 				= "254356864";
    private string SAMPLEKEY_AdjustInfo3 				= "427896519";
    private string SAMPLEKEY_AdjustInfo4 				= "820983601";
#else
	private static string SAMPLEKEY_WeChatSecretKey 			= "26dcee99df49b6da27bc0fa160d15219";
	private static string SAMPLEKEY_WeChatPaymentKey 			= "auILMZisSYHnEb4ISD58QYm7uETdVX6d";
	private string SAMPLEKEY_AdjustKey 					= "f02nseiur2m8";
    private string SAMPLEKEY_AdjustSecretID 			= "1";
    private string SAMPLEKEY_AdjustInfo1 				= "1263786494";
    private string SAMPLEKEY_AdjustInfo2 				= "303424327";
    private string SAMPLEKEY_AdjustInfo3 				= "1829017077";
    private string SAMPLEKEY_AdjustInfo4 				= "1052886214";
#endif

    private string SAMPLEKEY_SingularKey 				= "9b7ff73a0fc13c6c9397a9881dba2673";
    private string SAMPLEKEY_AppsFlyerKey 				= "G6yEQjHw4CrEN8sEzF9onU";

	private string _googleServerClientID 				= "";
    private string _weChatSecretKey 					= "";
	private string _weChatPaymentKey 					= "";
    private string _adjustKey 							= "";
    private string _adjustSecretID 						= "";
    private string _adjustInfo1 						= "";
    private string _adjustInfo2 						= "";
    private string _adjustInfo3 						= "";
    private string _adjustInfo4 						= "";
    private string _singularKey 						= "";
    private string _appsFlyerKey 						= "";

    // Start is called before the first frame update
    public void Start()
    {
		setDefaultKeyValue();
    }

    // Update is called once per frame

    public void Update()
    {
        

    }



	public void OnGUI() {
		
		HIVETestView.layout(contentCount);

		HIVETestManager testViewManager = HIVETestManager.instance;

		if (testViewManager._isShow == false) {
			return;
		}
		
		float scrollViewHeight = (testViewManager .buttonHeight + 20) * contentCount;
		scrollPosition = GUI.BeginScrollView(new Rect(0, 0, Screen.width, Screen.height), scrollPosition, new Rect(0, 0, Screen.width, scrollViewHeight));
        GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.05f;
        GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.05f;

        GUILayout.BeginVertical();

		if (HIVETestView.showButton("Done - Go Next(설정 저장)") ){	
			saveValue();
			HIVETestManager.instance.switchView(TestViewType.MAIN_VIEW);
		}
		

		HIVETestView.showSubTitle("Google");
		HIVETestView.showEditableTextWithTitle("GoogleServerClientID", ref _googleServerClientID);

		HIVETestView.showSubTitle("WeChat");
		HIVETestView.showEditableTextWithTitle("WeChat SecretKey", ref _weChatSecretKey);
		HIVETestView.showEditableTextWithTitle("WeChat PaymentKey", ref _weChatPaymentKey);

		HIVETestView.showSubTitle("Adjust");
		HIVETestView.showEditableTextWithTitle("Adjust Key", ref _adjustKey);
		HIVETestView.showEditableTextWithTitle("Adjust SecretID", ref _adjustSecretID);
		HIVETestView.showEditableTextWithTitle("Adjust Info1", ref _adjustInfo1);
		HIVETestView.showEditableTextWithTitle("Adjust Info2", ref _adjustInfo2);
		HIVETestView.showEditableTextWithTitle("Adjust Info3", ref _adjustInfo3);
		HIVETestView.showEditableTextWithTitle("Adjust Info4", ref _adjustInfo4);

		HIVETestView.showSubTitle("Singular");
		HIVETestView.showEditableTextWithTitle("Singular Key", ref _singularKey);

		HIVETestView.showSubTitle("AppsFlyer");
		HIVETestView.showEditableTextWithTitle("AppsFlyer Key", ref _appsFlyerKey);


		if (HIVETestView.showButton("RESET DATA - 초기값으로 되돌림") ){
			setDefaultKeyValue();	
		}

		if (HIVETestView.showButton("Clear DATA - 모두 빈 값으로 설정") ){
			clearKeyValue();
		}

		if (HIVETestView.showButton("API 키 설정하지 않고 페이지 이동하기")) {
			HIVETestManager.instance.switchView(TestViewType.MAIN_VIEW);
		}

		

        GUILayout.EndVertical();

		GUI.EndScrollView();

    }

	private void setDefaultKeyValue() {
		_googleServerClientID 				= SAMPLEKEY_GoogleServerClientID;
		_weChatSecretKey 					= SAMPLEKEY_WeChatSecretKey;
		_weChatPaymentKey					= SAMPLEKEY_WeChatPaymentKey;
		_adjustKey 							= SAMPLEKEY_AdjustKey;
		_adjustSecretID 					= SAMPLEKEY_AdjustSecretID;
		_adjustInfo1						= SAMPLEKEY_AdjustInfo1;
		_adjustInfo2						= SAMPLEKEY_AdjustInfo2;
		_adjustInfo3						= SAMPLEKEY_AdjustInfo3;
		_adjustInfo4						= SAMPLEKEY_AdjustInfo4;
		_singularKey						= SAMPLEKEY_SingularKey;
		_appsFlyerKey						= SAMPLEKEY_AppsFlyerKey;
	}

	private void clearKeyValue() {
		_googleServerClientID 				= "";
		_weChatSecretKey 					= "";
		_weChatPaymentKey					= "";
		_adjustKey 							= "";
		_adjustSecretID 					= "";
		_adjustInfo1						= "";
		_adjustInfo2						= "";
		_adjustInfo3						= "";
		_adjustInfo4						= "";
		_singularKey						= "";
		_appsFlyerKey						= "";
	}

	private void saveValue() {
		Configuration.setConfigurations(HiveConfigType.googleServerClientId, _googleServerClientID);
		Configuration.setConfigurations(HiveConfigType.wechatSecret, _weChatSecretKey);
		Configuration.setConfigurations(HiveConfigType.wechatPaymentKey, _weChatPaymentKey);
		Configuration.setConfigurations(HiveConfigType.adjustKey, _adjustKey);
		Configuration.setConfigurations(HiveConfigType.adjustSecretId, _adjustSecretID);
		Configuration.setConfigurations(HiveConfigType.adjustInfo1, _adjustInfo1);
		Configuration.setConfigurations(HiveConfigType.adjustInfo2, _adjustInfo2);
		Configuration.setConfigurations(HiveConfigType.adjustInfo3, _adjustInfo3);
		Configuration.setConfigurations(HiveConfigType.adjustInfo4, _adjustInfo4);
		Configuration.setConfigurations(HiveConfigType.singularKey, _singularKey);
		Configuration.setConfigurations(HiveConfigType.appsflyerKey, _appsFlyerKey);
	}

    
}
