using UnityEngine;
using System.Collections;

using hive;

// 테스트 화면의 레이아웃 구성을 위한 GUI Wrapper
//
// @author ryuvsken
public class HIVETestView : MonoBehaviour {

	void Awake () {

		// DontDestroyOnLoad(gameObject);
		
		//하이브 플러그인을 위한 게임오브젝트를 생성한다.
		hive.HIVEUnityPlugin.InitPlugin();
	}


	void Start() {

		// 테스트 화면이 추가되면 여기에 코드를 추가한다
		ConfigurationAPIKeyTestView.instance.Start ();
		ConfigurationTestView.instance.Start ();
		MainTestView.instance.Start ();
		AuthTestView.instance.Start ();
		SocialHiveTestView.instance.Start ();
		SocialFacebookTestView.instance.Start ();
		SocialGoogleTestView.instance.Start ();
		PromotionTestView.instance.Start ();
		PushTestView.instance.Start ();
		IAPTestView.instance.Start ();
		IAPShopInfoTestView.instance.Start ();
		AuthV4TestView.instance.Start ();
		ProviderTestView.instance.Start ();
		IAPV4TestView.instance.Start ();
		IAPV4ShopInfoTestView.instance.Start ();
		AuthV4HelperTestView.instance.Start ();
		SocialV4TestView.instance.Start ();
	}


	void Update() {

		// 테스트 화면이 추가되면 여기에 코드를 추가한다
		HIVETestManager testViewManager = HIVETestManager.instance;

		if (testViewManager.currentViewType == TestViewType.CONFIG_VIEW) {
			ConfigurationTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.MAIN_VIEW) {
			MainTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.AUTH_VIEW) {
			AuthTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.SOCIAL_HIVE_VIEW) {
			SocialHiveTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.SOCIAL_FACEBOOK_VIEW) {
			SocialFacebookTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.SOCIAL_GOOGLE_VIEW) {
			SocialGoogleTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.PROMOTION_VIEW) {
			PromotionTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.PUSH_VIEW) {
			PushTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.IAP_VIEW) {
			IAPTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.ANALYTICS_VIEW) {
			AnalyticsTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.IAP_SHOPINFO_VIEW) {
			IAPShopInfoTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.AUTHV4_VIEW) {
			AuthV4TestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.PROVIDER_VIEW) {
			ProviderTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.IAPV4_VIEW) {
			IAPV4TestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.IAPV4_SHOPINFO_VIEW) {
			IAPV4ShopInfoTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.AUTHV4HELPER_VIEW) {
			AuthV4HelperTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.CONFIG_KEY_VIEW) {
			ConfigurationAPIKeyTestView.instance.Update ();
		}
		else if (testViewManager.currentViewType == TestViewType.SOCIALV4_VIEW) {
			SocialV4TestView.instance.Update ();
		}
	}


	void OnGUI() {

		// 테스트 화면이 추가되면 여기에 코드를 추가한다
		HIVETestManager testViewManager = HIVETestManager.instance;

		if (testViewManager._isShow == false) {
			return;
		}

		if (testViewManager.currentViewType == TestViewType.CONFIG_VIEW) {
			ConfigurationTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.MAIN_VIEW) {
			MainTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.AUTH_VIEW) {
			AuthTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.SOCIAL_HIVE_VIEW) {
			SocialHiveTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.SOCIAL_FACEBOOK_VIEW) {
			SocialFacebookTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.SOCIAL_GOOGLE_VIEW) {
			SocialGoogleTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.PROMOTION_VIEW) {
			PromotionTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.PUSH_VIEW) {
			PushTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.IAP_VIEW) {
			IAPTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.ANALYTICS_VIEW) {
			AnalyticsTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.IAP_SHOPINFO_VIEW) {
			IAPShopInfoTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.AUTHV4_VIEW) {
			AuthV4TestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.PROVIDER_VIEW) {
			ProviderTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.IAPV4_VIEW) {
			IAPV4TestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.IAPV4_SHOPINFO_VIEW) {
			IAPV4ShopInfoTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.AUTHV4HELPER_VIEW) {
			AuthV4HelperTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.CONFIG_KEY_VIEW) {
			ConfigurationAPIKeyTestView.instance.OnGUI ();
		}
		else if (testViewManager.currentViewType == TestViewType.SOCIALV4_VIEW) {
			SocialV4TestView.instance.OnGUI ();
		}

		// 서버정보 및 Auth 상태 확인용 텍스트 박스
		if (GUI.Button (new Rect (0, 
							Screen.height - testViewManager.bottomBarHeight - 10, 
							Screen.width, 
							testViewManager.bottomBarHeight), 
							"Click Here to show Log\nZone : " + Configuration.getZone().ToString() + "  Initialize : " + MainTestView.initializeState.ToString() + "   Login : " + MainTestView.isAuthLogin.ToString())) {
								HIVETestManager.instance.isShowConsoleLogView = true;
							}
	}


	public static void layout(int contentCount) {
    
		HIVETestManager testViewManager = HIVETestManager.instance;
    	
#if !UNITY_EDITOR && UNITY_ANDROID

		testViewManager.screenWidth = Screen.width;
        if(Screen.height > 1280)
			testViewManager.fontSize = 40;
        else if(Screen.height > 800)
			testViewManager.fontSize = 35;
        else if(Screen.height > 400)
			testViewManager.fontSize = 20;
        else
			testViewManager.fontSize = 10;
        
#elif !UNITY_EDITOR && UNITY_IPHONE
		testViewManager.fontSize = 30;
#else
		testViewManager.fontSize = ((testViewManager.buttonHeight - 10) > 15) ? 15 : (testViewManager.buttonHeight - 10);
#endif
		testViewManager.screenWidth = Screen.width;
		// 높이 증가를 위한 보간 값 +10
		testViewManager.buttonHeight = (Screen.height  / contentCount) + 10 ;
		if (Screen.width >= Screen.height) {
			// Landscape 일때 버튼 높이 보간 값 +15
			testViewManager.buttonHeight += 15;
		}

		testViewManager.bottomBarHeight = testViewManager.buttonHeight + 30;
		//Debug.Log("fontSize : " + instance.fontSize);

		// 버튼 크기에 맞는 일정한 폰트 사이즈 계산
		testViewManager.fontSize = testViewManager.buttonHeight / 3;
		GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = GUI.skin.textField.fontSize = testViewManager.fontSize;
    }

	/** 단일 디자인 객체를 노출시 활용할 함수들 **/
	public static void createBox(string text) {

		HIVETestManager testViewManager = HIVETestManager.instance;

		GUILayout.Box(text, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxWidth(testViewManager.screenWidth));
	}


    public static void createLabel(string label) {

		HIVETestManager testViewManager = HIVETestManager.instance;
    
		GUIStyle style = GUI.skin.GetStyle("Label");
		style.fontSize = testViewManager.fontSize; 
		GUI.skin.box = style;

		GUILayout.Label(label);
    }


	public static bool createButton(string text) {

		HIVETestManager testViewManager = HIVETestManager.instance;
    
		GUIStyle style = GUI.skin.GetStyle("button");
		style.fontSize = testViewManager.fontSize; 
		GUI.skin.box = style;

		return GUILayout.Button(text, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxWidth(testViewManager.screenWidth));
    }
	
	public static bool createButton(string text, params GUILayoutOption[] options) {
    
		HIVETestManager testViewManager = HIVETestManager.instance;
		return GUILayout.Button(text, options);
    }

    public static string createTextField(string defaultString) {

		HIVETestManager testViewManager = HIVETestManager.instance;
    
		GUIStyle style = GUI.skin.GetStyle("TextField");
		style.fontSize = testViewManager.fontSize; 
		GUI.skin.box = style;

		return GUILayout.TextField(defaultString, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxWidth(testViewManager.screenWidth));
    }

    public static string createTextArea(string defaultString, int height)
    {
		HIVETestManager testViewManager = HIVETestManager.instance;

		GUIStyle style = GUI.skin.GetStyle("TextArea");
		style.fontSize = testViewManager.fontSize; 
		GUI.skin.box = style;

        return GUILayout.TextArea(defaultString, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxHeight(height), GUILayout.MaxWidth(testViewManager.screenWidth));
    }



	public static bool createToggle(bool val, string text) {

		HIVETestManager testViewManager = HIVETestManager.instance;
		return GUILayout.Toggle (val, text, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxWidth(testViewManager.screenWidth));
	}


	public static int selectionGrid(int selPosition, string[] texts, int count) {

		return selectionGrid(selPosition, texts, count, 2);
	}


	public static int selectionGrid(int selPosition, string[] texts, int count, int xCount) {

		HIVETestManager testViewManager = HIVETestManager.instance;
		return GUILayout.SelectionGrid(selPosition, texts, xCount, GUILayout.MinHeight(testViewManager.buttonHeight*((count%xCount==0)?count/xCount:count/xCount+1)), GUILayout.MaxWidth(testViewManager.screenWidth));
	}


	/** 디자인 한 줄을 노출시 활용할 함수들 **/

	public static void showSubTitle(string subTitle) {
		GUILayout.Space (15);
		GUILayout.BeginHorizontal();
		
		HIVETestManager testViewManager = HIVETestManager.instance;
		GUIStyle style = new GUIStyle("Label");
		style.fontSize = testViewManager.fontSize + 5;
		style.alignment = TextAnchor.MiddleCenter;
		style.fixedWidth = testViewManager.screenWidth;
		GUILayout.Label(subTitle, style);
		

		GUILayout.EndHorizontal();
	}

	public static void showNonEditableTextWithTitle(string title, string value) {
		
		HIVETestManager testViewManager = HIVETestManager.instance;


		GUILayout.Space (10);
		GUILayout.BeginHorizontal();


		GUIStyle titleStyle = new GUIStyle("Label");
		titleStyle.fontSize = testViewManager.fontSize;
		titleStyle.alignment = TextAnchor.MiddleRight;
		titleStyle.fixedWidth = testViewManager.screenWidth/3;
		titleStyle.wordWrap = true;
		GUILayout.Label(title + " : ", titleStyle);
		
		
		GUIStyle valueStyle = new GUIStyle("Label");
		valueStyle.fontSize = testViewManager.fontSize;
		valueStyle.alignment = TextAnchor.MiddleLeft;
		valueStyle.fixedWidth = testViewManager.screenWidth/3 * 2;
		valueStyle.wordWrap = true;
		GUILayout.Label(value, valueStyle);
		

		GUILayout.EndHorizontal();
	}

	public static void showEditableTextWithTitle(string title, ref string value) {
		
		HIVETestManager testViewManager = HIVETestManager.instance;


		GUILayout.Space (10);
		GUILayout.BeginHorizontal();


		GUIStyle titleStyle = new GUIStyle("Label");
		titleStyle.fontSize = testViewManager.fontSize;
		titleStyle.alignment = TextAnchor.MiddleRight;
		titleStyle.fixedWidth = testViewManager.screenWidth/3;
		titleStyle.wordWrap = true;
		GUILayout.Label(title + " : ", titleStyle);
		
		

		GUIStyle valueStyle = new GUIStyle("Label");
		valueStyle.fontSize = testViewManager.fontSize;
		valueStyle.alignment = TextAnchor.MiddleLeft;
		valueStyle.fixedWidth = testViewManager.screenWidth/3 * 2;
		valueStyle.wordWrap = true;

		value =  GUILayout.TextField(value, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxWidth(testViewManager.screenWidth));

		GUILayout.EndHorizontal();


	}

	public static bool showButton(string text) {
		HIVETestManager testViewManager = HIVETestManager.instance;

		return GUILayout.Button(text, GUILayout.MinHeight(testViewManager.buttonHeight), GUILayout.MaxWidth(testViewManager.screenWidth));
	}
}



