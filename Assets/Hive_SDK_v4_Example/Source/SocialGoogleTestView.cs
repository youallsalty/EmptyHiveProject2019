using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


using hive;		// HIVE SDK 네임스페이스 사용


/**
 * 사용자 프로필, 친구 목록 테스트 화면
 * (Social Network Service API (Google))
 * 
 * @author ryuvsken
 */
public class SocialGoogleTestView {
	
	public static SocialGoogleTestView instance = new SocialGoogleTestView ();


	public void Start() {
	}


	public void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
	}


	public void OnGUI() {

		HIVETestView.layout(24);


		GUILayout.BeginVertical();

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- Social Google -")) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);
		
		GUILayout.EndVertical();
	}
}



