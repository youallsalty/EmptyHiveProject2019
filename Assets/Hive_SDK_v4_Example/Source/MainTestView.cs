using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using hive;			// HIVE SDK 네임스페이스 사용


public class MainTestView {


	public static MainTestView instance = new MainTestView ();

	// HiveSDK 초기화 여부
	// public static bool isAuthInitialize = false;
	public static InitializeState initializeState = InitializeState.NOT_INIT;
	public static bool isAuthLogin = false;
	public static bool isIAPUpdated = false;
	public static string permissionList = "android.permission.READ_PHONE_STATE\nandroid.permission.READ_PHONE_NUMBERS\nandroid.permission.WRITE_EXTERNAL_STORAGE\nandroid.permission.READ_CONTACTS\nandroid.permission.VIBRATE";


	public void Start() {

		Screen.autorotateToPortrait = true;
		Screen.autorotateToPortraitUpsideDown = true;
		Screen.autorotateToLandscapeLeft = true;
		Screen.autorotateToLandscapeRight = true;

		Screen.orientation = ScreenOrientation.AutoRotation;
	}


	public void Update() {
	}

	public enum InitializeState {
		NOT_INIT
		, AUTH_INIT
		, AUTHV4_INIT
	}

	// HIVE SDK 초기화 결과 통지 리스너 - Auth.initialize()
	public void onAuthInitializeCB(ResultAPI result, AuthInitResult authInitResult) {

		Debug.Log("MainTestView.onAuthInitializeCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log("authInitResult = " + authInitResult.toString() + "\n");

		if (result.isSuccess ()) {
			// MainTestView.isAuthInitialize = true;
			MainTestView.initializeState = InitializeState.AUTH_INIT;
		}
	}

	public void onAuthV4Setup(ResultAPI result, Boolean isAutoSignIn, String did, List<AuthV4.ProviderType> providerTypeList) {

		Debug.Log("MainTestView.onAuthV4Setup() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess ()) {
			MainTestView.initializeState = InitializeState.AUTHV4_INIT;
		}

		if (result.isSuccess() == false)
			return;

		Debug.Log("isAutoSignIn = " + isAutoSignIn + "\n");
		Debug.Log("did = " + did + "\n");

		foreach (AuthV4.ProviderType providerType in providerTypeList)
			Debug.Log("providerType = " + providerType + "\n");
	}


	// HIVE SDK 가 특정한 조건에서 클라이언트에 개입 (Engagement) 하기 위한 이벤트 핸들러 - Auth.initialize()
	// 여기서 특정한 조건은 모바일 메시지 (SMS), 푸시 알림 (Push Notification) 으로 전송된 메시지의 URL 클릭이나 프로모션 뷰에서 사용자 동작 등이 있다.
	public void onEngagementCB(ResultAPI result, EngagementEventType engagementEventType, EngagementEventState engagementEventState, JSONObject param) {

		Debug.Log("MainTestView.onEngagementCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log("engagementEventType = " + engagementEventType.ToString() + "\n");
		Debug.Log("engagementEventState = " + engagementEventState.ToString() + "\n");

		if (param != null) {
			Debug.Log ("param = " + param.ToString () + "\n");
		}

		// HiveSDK 에서 개입의 형태에 따라서 분기되는 코드
		if (engagementEventType == EngagementEventType.EVENT_TYPE) {
			// Engagement의 전체 시작과 끝.
		}
		else if (engagementEventType == EngagementEventType.PROMOTION_VIEW) {
			
		}
		else if (engagementEventType == EngagementEventType.OFFERWALL_VIEW) {
			
		}
		else if (engagementEventType == EngagementEventType.USERACQUISTION) {
			
		}
		else if (engagementEventType == EngagementEventType.COUPON) {

			// 쿠폰 사용 코드
			if (param != null) {
				int code = -1;
				param.GetField (ref code, "code");
				Debug.Log ("Coupon Code = " + code + "\n");

				// 쿠폰 사용 메시지
				String message = "";
				param.GetField (ref message, "message");
				Debug.Log ("message = " + message + "\n");
			}
			else {
				Debug.Log("Coupon param is null.");
			}
		}

		else if (engagementEventType == EngagementEventType.AUTH_LOGIN_VIEW) {

			// loginType
			String loginTypeName = "";
			param.GetField (ref loginTypeName, "loginType");
			Debug.Log ("loginType = " + loginTypeName);

			// convert to loginType
			LoginType loginType = LoginType.GUEST;
			if ("ACCOUNT".Equals (loginTypeName))
				loginType = LoginType.ACCOUNT;
			else if ("SELECT".Equals (loginTypeName))
				loginType = LoginType.SELECT;
			else if ("AUTO".Equals (loginTypeName))
				loginType = LoginType.AUTO;


			// currentAccount
			Account currentAccount = new Account(param.GetField("currentAccount"));
			Debug.Log ("currentAccount = " + currentAccount.toString() + "\n");
				
			// usedAccount
			Account usedAccount = new Account(param.GetField("usedAccount"));
			Debug.Log ("usedAccount = " + usedAccount.toString() + "\n");

			if (loginType == LoginType.SELECT) {
				JSONObject currentVidData = new JSONObject();
				currentVidData.AddField("vid", currentAccount.vid);

				JSONObject currendVidGameData = new JSONObject();
				currendVidGameData.AddField("Name", "currentName");
				currendVidGameData.AddField("Level", 10);

				currentVidData.AddField("data", currendVidGameData);

				JSONObject usedVidData = new JSONObject();
				usedVidData.AddField("vid", usedAccount.vid);

				JSONObject usedVidGameData = new JSONObject();
				usedVidGameData.AddField("Name", "usedName");
				usedVidGameData.AddField("Level", 11);

				usedVidData.AddField("data", usedVidGameData);

				Debug.Log("\n\n=== 결과 값이 LoginType.SELECT 이면 이 메서드를 호출 하여 사용자를 선택을 요청 ===\n");
				Debug.Log("Auth.showLoginSelection(currentVidData, usedVidData, onAuthLogin) Called\n");
				Debug.Log("currentVidData = " + currentVidData.ToString() + "\n");
				Debug.Log("usedVidData = " + usedVidData.ToString() + "\n");
			
				hive.Auth.showLoginSelection (currentVidData, usedVidData, delegate(ResultAPI selectResult, LoginType selectloginType, Account selectCurrentAccount, Account selectUsedAccount) {

					Debug.Log("MainTestView.onAuthLoginCB() Callback\nresult = " + selectResult.toString() + "\n");

					Debug.Log ("loginType = " + selectloginType.ToString () + "\n");
					Debug.Log ("currentAccount = " + selectCurrentAccount.toString () + "\n");
					Debug.Log ("usedAccount = " + selectUsedAccount.toString () + "\n");

					// Only on IAP Postbox Sample
					PostboxInfo.DID = selectCurrentAccount.did;
					PostboxInfo.VID = selectCurrentAccount.vid;
					PostboxInfo.UID = selectCurrentAccount.uid;
					PostboxInfo.hiveSessionToken = selectCurrentAccount.accessToken;
				});
			}
		}
		else if (engagementEventType == EngagementEventType.SOCIAL_INQUIRY_VIEW) {
			
		}

		else if (engagementEventType == EngagementEventType.EVENT) {

			// 백오피스에 등록된 API 명
			String apiName = "";
			param.GetField(ref apiName, "api");
			Debug.Log("apiName = " + apiName + "\n");

			// API 에 대한 매개 변수
			String apiParam = "";
			param.GetField (ref apiParam, "param");
			Debug.Log("apiParam = " + apiParam + "\n");

		}
		
		else if (engagementEventType == EngagementEventType.IAP_PROMOTE) {

			isIAPUpdated = true;
		}

		else if (engagementEventType == EngagementEventType.IAP_UPDATED) {
			
		}
	}

	private void setDefaultServerID() {
		if(String.IsNullOrEmpty(Configuration.getServerId()) == true) {
			Debug.LogWarning("ServerID Empty set default serverID 'kr', default language 'ko'\n");
			String defaultServerID = "kr";
			String defaultGameLanguage = "ko";
			String defaultHiveOrientation = "all";
			Configuration.setServerId(defaultServerID);
			//save default setting
			if(!File.Exists(ConfigurationTestView.configSavePath)){
				ConfigurationTestView.saveConfigurationToFile(Configuration.getAppId(), Configuration.getZone().ToString(), Configuration.getHiveTheme().ToString(), defaultHiveOrientation, Configuration.getUseLog(), defaultGameLanguage, defaultServerID);
			}
		}
		else {
			Debug.LogWarning("ServerID : " + Configuration.getServerId() + "\n");
		}
	}

	public void OnGUI() {

		HIVETestView.layout(24);

        GUILayout.BeginVertical();

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- HIVE SDK v4 Tester -")) {
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (16);


		// 설정 정보 화면 로딩
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Configuration")) {

			HIVETestManager.instance.switchView (TestViewType.CONFIG_VIEW);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		// HiveSDK 가 초기화 되지 않았으면 초기화 버튼 노출
		// if (MainTestView.isAuthInitialize == false) {
		if (MainTestView.initializeState == InitializeState.NOT_INIT) {

            
//#if !UNITY_EDITOR && UNITY_ANDROID
			//Android OS 권한 동의 팝업 재노출
			GUILayout.BeginHorizontal ();
			GUILayout.Box("Permission list :");
			permissionList =  HIVETestView.createTextArea(permissionList, 200);
			String[] requestsArray = permissionList.Split('\n');
			List<String> requests = new List<String>(requestsArray);
			GUILayout.EndHorizontal ();
			GUILayout.Space(8);

			GUILayout.BeginHorizontal ();
			if (HIVETestView.createButton ("PlatformHelper.requestUserPermissions()")) {
				PlatformHelper.requestUserPermissions( requests, (ResultAPI result, String[] granted, String[] denied) => {
					Debug.Log("\n\n=== OS 권한동의 팝업 재노출  ===\n");
					Debug.Log("ResultAPI :" + result.toString() + "\n");
					
					if (granted != null && granted.Length > 0) {
						Debug.Log("동의한 권한 목록 : \n");
						foreach (String name in granted) {
							Debug.Log(name + '\n');
						}
					}
					if (denied != null && denied.Length > 0) {
						Debug.Log("\n거절한 권한 목록: \n");
						foreach (String name in denied) {
							Debug.Log(name + '\n');
						}
					}
				});
			}
			GUILayout.EndHorizontal ();
			GUILayout.Space(8);

//#endif


            // HIVE SDK 초기화 요청
            GUILayout.BeginHorizontal ();
			if (HIVETestView.createButton ("Auth.requestPermissionViewData()")) {
				Debug.Log("\n\n=== V1 퍼미션 뷰 정보 요청. ===\n");

                //Auth.requestPermissionViewData((ResultAPI result,PermissionViewData data)=>{
                //	onRequestPermissionViewData(result,data,ResultAPI.Code.AuthSkipPermissionView);
                //});

                Auth.requestPermissionViewData((ResultAPI result, PermissionViewData data) =>
                {
                    Debug.Log("result.Errorcode = " + result.errorCode + ", result.code = " + result.code);

                    if(result.isSuccess())
                    {
                        if (result.code == ResultAPI.Code.AuthSkipPermissionView)
                        {
                            Debug.Log("Can skip permission notice");
                        }
                        else
                        {
							Debug.Log("\n제목 : " + data.contents);
							if (data.permissions.Count > 0) {
								Debug.Log("\n\n=== From permissions ===");
							}
                            for (int i = 0; i < data.permissions.Count; i++)
                            {
                                Debug.Log("\n권한명 : " + data.permissions[i].nativePermissionName + "\n타이틀 : " + data.permissions[i].title + "\n내용: " + data.permissions[i].contents + "\n권한 카테고리: " + data.permissions[i].permissionCategory);
                            }
							if (data.commons.Count > 0) {
								Debug.Log("\n\n=== From commons ===");
							}
                            for (int i = 0; i < data.commons.Count; i++)
                            {
                                Debug.Log("\n권한명 : " + data.commons[i].nativePermissionName + "\n타이틀 : " + data.commons[i].title + "\n내용: " + data.commons[i].contents + "\n권한 카테고리: " + data.commons[i].permissionCategory);
                            }
                        }
                    }
                    else
                    {
                        //AuthInProgressRequestPermissionViewData 에러는 대기, 그 외 에러는 무시하고 Auth.initialize를 호출할 경우 HIVE SDK의 기본 권한고지 팝업이 사용됩니다.
                    }
                });
			}
			GUILayout.EndHorizontal ();
			GUILayout.Space (8);

			// HIVE SDK 초기화 요청
			GUILayout.BeginHorizontal ();
			if (HIVETestView.createButton ("Auth.initialize()")) {
				setDefaultServerID();
				Configuration.setAgeGateU13(false);
				Auth.initialize (onAuthInitializeCB);
				Promotion.setEngagementListener (onEngagementCB);
				
				Debug.Log("\n\n=== HIVE SDK 초기화 요청. ===\n");
				Debug.Log("Auth.initialize(authInitializeListener) Called\n");
				Debug.Log("Promotion.setEngagementListener(engagementListener) Called\n");
			}
			GUILayout.EndHorizontal ();
			GUILayout.Space (8);

			GUILayout.BeginHorizontal ();
			if (HIVETestView.createButton ("AuthV4.requestPermissionViewData()")) {
				Debug.Log("\n\n=== V4 퍼미션 뷰 정보 요청. ===\n");
                //AuthV4.requestPermissionViewData((ResultAPI result,PermissionViewData data)=>{
                //	onRequestPermissionViewData(result,data,ResultAPI.Code.AuthV4SkipPermissionView);
                //});
                AuthV4.requestPermissionViewData((ResultAPI result, PermissionViewData data) => 
                {
                    Debug.Log("result.Errorcode = " + result.errorCode + ", result.code = " + result.code);

                    if (result.isSuccess())
                    {
                        if (result.code == ResultAPI.Code.AuthV4SkipPermissionView)
                        {
                            Debug.Log("Can skip permission notice");
                        }
                        else
                        {
							Debug.Log("\n제목 : " + data.contents);
							if (data.permissions.Count > 0) {
								Debug.Log("\n\n=== From permissions ===");
							}
                            for (int i = 0; i < data.permissions.Count; i++)
                            {
                                Debug.Log("\n권한명 : " + data.permissions[i].nativePermissionName + "\n타이틀 : " + data.permissions[i].title + "\n내용: " + data.permissions[i].contents + "\n권한 카테고리: " + data.permissions[i].permissionCategory);
                            }
							if (data.commons.Count > 0) {
								Debug.Log("\n\n=== From commons ===");
							}
                            for (int i = 0; i < data.commons.Count; i++)
                            {
                                Debug.Log("\n권한명 : " + data.commons[i].nativePermissionName + "\n타이틀 : " + data.commons[i].title + "\n내용: " + data.commons[i].contents + "\n권한 카테고리: " + data.commons[i].permissionCategory);
                            }
                        }
                    }
                    else
                    {
                        //AuthV4InProgress에러는 대기, 그 외 에러는 무시하고 AuthV4.setup을 호출할 경우 HIVE SDK의 기본 권한고지 팝업이 사용됩니다.
                    }
                });
			}
			GUILayout.EndHorizontal ();
			GUILayout.Space (8);
			// HIVE SDK V4 초기화 요청
			GUILayout.BeginHorizontal ();
			if (HIVETestView.createButton ("AuthV4.setup")) {
				setDefaultServerID();

				AuthV4.setup (onAuthV4Setup);
				hive.Promotion.setEngagementListener(onEngagementCB);

				Debug.Log("\n\n=== HIVE SDK V4 초기화 요청. ===\n");
				Debug.Log("AuthV4.setup(AuthV4SetupListener) Called\n");
				Debug.Log("Promotion.setEngagementListener(engagementListener) Called\n");
			}
			GUILayout.EndHorizontal ();
			GUILayout.Space (8);
		}
		// HIVE SDK 가 초기화 되었으면 테스트 씬 버튼 노출
		else {
			if (MainTestView.initializeState == InitializeState.AUTH_INIT) {
				GUILayout.BeginHorizontal();
				if (HIVETestView.createButton ("Auth.initialize() - 초기화 재수행")) {
					Auth.initialize (onAuthInitializeCB);
					
					Debug.Log("\n\n=== HIVE SDK 초기화 요청. ===\n");
					Debug.Log("Auth.initialize(authInitializeListener) Called\n");
					Debug.Log("Promotion.setEngagementListener(engagementListener) Called\n");
				}
				GUILayout.EndHorizontal ();
				GUILayout.Space (8);
				// 인증 테스트 화면 로딩
				GUILayout.BeginHorizontal ();
				if (HIVETestView.createButton ("Auth")) {

					HIVETestManager.instance.switchView(TestViewType.AUTH_VIEW);
				}
				GUILayout.EndHorizontal ();
				GUILayout.Space (8);

				// 소셜 테스트 화면 로딩
				GUILayout.BeginHorizontal ();
				if (HIVETestView.createButton ("Social Hive")) {

					HIVETestManager.instance.switchView (TestViewType.SOCIAL_HIVE_VIEW);
				}
				GUILayout.EndHorizontal ();
				GUILayout.Space (8);

				GUILayout.BeginHorizontal ();
				if (HIVETestView.createButton ("Social Facebook")) {

					HIVETestManager.instance.switchView (TestViewType.SOCIAL_FACEBOOK_VIEW);
				}
				GUILayout.EndHorizontal ();
				GUILayout.Space (8);

#if !UNITY_EDITOR && UNITY_ANDROID
				GUILayout.BeginHorizontal ();
				if (HIVETestView.createButton ("Social Google")) {

					HIVETestManager.instance.switchView (TestViewType.SOCIAL_GOOGLE_VIEW);
				}
				GUILayout.EndHorizontal ();
				GUILayout.Space (8);
#endif		// #if !UNITY_EDITOR && UNITY_ANDROID
			}
			else if (MainTestView.initializeState == InitializeState.AUTHV4_INIT) {
				GUILayout.BeginHorizontal ();
				if (HIVETestView.createButton ("AuthV4.setup - 초기화 재수행")) {
					AuthV4.setup (onAuthV4Setup);

					Debug.Log("\n\n=== HIVE SDK V4 초기화 재요청. ===\n");
					Debug.Log("AuthV4.setup(AuthV4SetupListener) Called\n");
					Debug.Log("Promotion.setEngagementListener(engagementListener) Called\n");
				}
				GUILayout.EndHorizontal ();
				GUILayout.Space (8);
				// 인증 테스트 화면 로딩
				GUILayout.BeginHorizontal ();
				if (HIVETestView.createButton ("AuthV4")) {

					HIVETestManager.instance.switchView(TestViewType.AUTHV4_VIEW);
				}
				GUILayout.EndHorizontal ();
				GUILayout.Space (8);


				// 인증 테스트 화면 로딩
				GUILayout.BeginHorizontal ();
				if (HIVETestView.createButton ("AuthV4Helper")) {

					HIVETestManager.instance.switchView(TestViewType.AUTHV4HELPER_VIEW);
				}
				GUILayout.EndHorizontal ();
				GUILayout.Space (8);

				// Provider 테스트 화면 로딩
				GUILayout.BeginHorizontal ();
				if (HIVETestView.createButton ("Provider Test")) {

					HIVETestManager.instance.switchView(TestViewType.PROVIDER_VIEW);
				}
				GUILayout.EndHorizontal ();
				GUILayout.Space (8);

				// SocialV4 테스트 화면 로딩
				GUILayout.BeginHorizontal ();
				if (HIVETestView.createButton ("SocialV4 Test")) {

					HIVETestManager.instance.switchView(TestViewType.SOCIALV4_VIEW);
				}
				GUILayout.EndHorizontal ();
				GUILayout.Space (8);
			}

			// 프로모션 테스트 화면 로딩
			GUILayout.BeginHorizontal ();
			if (HIVETestView.createButton ("Promotion")) {

				HIVETestManager.instance.switchView (TestViewType.PROMOTION_VIEW);
			}
			GUILayout.EndHorizontal ();
			GUILayout.Space (8);


			// 푸시 테스트 화면 로딩
			GUILayout.BeginHorizontal ();
			if (HIVETestView.createButton ("Push")) {

				HIVETestManager.instance.switchView (TestViewType.PUSH_VIEW);
			}
			GUILayout.EndHorizontal ();
			GUILayout.Space (8);


			// IAP 테스트 화면 로딩
			GUILayout.BeginHorizontal ();
			if (HIVETestView.createButton ("IAP")) {

				ConfigurationTestView.instance.postboxDataSetting();
				PostboxView.instance.callThePostboxLogin();

				HIVETestManager.instance.switchView (TestViewType.IAP_VIEW);
			}
			GUILayout.EndHorizontal ();
			GUILayout.Space (8);

			// IAPV4 테스트 화면 로딩
			GUILayout.BeginHorizontal ();
			if (HIVETestView.createButton ("IAPV4")) {

				ConfigurationTestView.instance.postboxDataSetting();
				PostboxView.instance.callThePostboxLogin();

				HIVETestManager.instance.switchView (TestViewType.IAPV4_VIEW);
			}
			GUILayout.EndHorizontal ();
			GUILayout.Space (8);


			// Analytics 테스트 화면 로딩
			GUILayout.BeginHorizontal ();
			if (HIVETestView.createButton ("Analytics")) {

				HIVETestManager.instance.switchView (TestViewType.ANALYTICS_VIEW);
			}
			GUILayout.EndHorizontal ();
		}


        GUILayout.EndVertical();
    }

}



