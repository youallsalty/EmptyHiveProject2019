﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class TransactionCleaner
{

#if !UNITY_EDITOR && UNITY_IPHONE
    [DllImport("__Internal")]
    private static extern int finishAllTransactions();
    public static int FinishAllTransactions() {
        return finishAllTransactions();
    }
#endif

}
