using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using System.IO;

using hive;			// HIVE SDK 네임스페이스 사용


/**
 * 프로모션 뷰 (공지, 이벤트), 보상 (쿠폰, 딥링크), 종료 팝업 (안드로이드), 무료 충전소, 리뷰 유도 팝업 테스트 화면
 * (Notice/Event Webview, Game Exit/Game Review  Popup View API) 
 * 
 * @author ryuvsken
 */
public class PromotionTestView {
    public static PromotionTestView instance = new PromotionTestView();
    private Dictionary<string, PromotionCampaignType> dicCampaignType = new Dictionary<string, PromotionCampaignType>();
    private List<string> listCampaignType = new List<string>();
    private int selectedGridIntCampaignType = 0;
    private bool btnCampaignTypeIsSelected = false;

    private Dictionary<string, PromotionBannerType> dicBannerType = new Dictionary<string, PromotionBannerType>();
    private List<string> listBannerType = new List<string>();
    private int selectedGridIntBannerType = 0;
    private bool btnBannerTypeIsSelected = false;
    private bool showGameLanguageButton = false;

    private bool disableOnGUI = false;

    private int contentCount = 28;
    private Vector2 scrollPosition = Vector2.zero;

    public void Start() {
        //initialize campaignType
        dicCampaignType.Add("EVENT", PromotionCampaignType.EVENT);
        dicCampaignType.Add("NOTICE", PromotionCampaignType.NOTICE);

        //initialize bannerType
        dicBannerType.Add("GREAT", PromotionBannerType.GREAT);
        dicBannerType.Add("ROLLING", PromotionBannerType.ROLLING);
        dicBannerType.Add("SMALL", PromotionBannerType.SMALL);

        foreach (string key in dicCampaignType.Keys)
        {
            listCampaignType.Add(key);
        }

        foreach (string key in dicBannerType.Keys)
        {
            listBannerType.Add(key);
        }

    }



	public void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
	}


	// PromotionView 결과 통지 리스너 - Promotion.onPromotionView()
	public void onPromotionViewCB(ResultAPI result, PromotionEventType promotionEventType) {
		
		Debug.Log ("PromotionTestView.onPromotionViewCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("promotionEventType = " + promotionEventType.ToString () + "\n");
	}


	// ContentsKey에 해당하는 컨텐츠 조회 결과 통지 리스너 - Promotion.getViewInfo()
	public void onPromotionViewInfoCB(ResultAPI result, List<PromotionViewInfo> promotionViewInfoList) {

		Debug.Log ("PromotionTestView.onPromotionViewInfoCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("promotionViewInfoList = \n");
		foreach(PromotionViewInfo each in promotionViewInfoList) {
			Debug.Log (each.toString() + "\n");
		}
		// Debug.Log ("promotionViewInfo = " + promotionViewInfo!=null?promotionViewInfo.toString():"null" + "\n");
	}


	// 뱃지 정보 조회 결과 통지 리스너 - Promotion.getBadgeInfo()
	public void onPromotionBadgeInfoCB(ResultAPI result, List<PromotionBadgeInfo> badgeInfoList) {

		Debug.Log ("PromotionTestView.onPromotionBadgeInfoCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("badgeInfoList = \n");
		foreach(PromotionBadgeInfo each in badgeInfoList) {
			Debug.Log (each.toString() + "\n");
		}
	}

	 public int GetSDKLevel() {
		var clazz = AndroidJNI.FindClass("android/os/Build$VERSION");
		var fieldID = AndroidJNI.GetStaticFieldID(clazz, "SDK_INT", "I");
		var sdkLevel = AndroidJNI.GetStaticIntField(clazz, fieldID);
     	return sdkLevel;
	}
    // 롤링 프로모션 배너 정보 조회 리스너 - Promotion.getBannerInfo()
    public void onPromotionBannerInfoCB(ResultAPI result, List<PromotionBannerInfo> bannerInfoList)
    {

        Debug.Log("PromotionTestView.onPromotoinBannerInfoCB() Callback\nresult = " + result.toString() + "\n");

        if (bannerInfoList.Count <= 0)
        {
            Debug.Log("bannerInfoList empty");
            return;
        }

        Debug.Log("bannerInfoList = \n");
        List<PromotionBannerInfoSample> bannerInfoListSample = new List<PromotionBannerInfoSample>();
        foreach (PromotionBannerInfo each in bannerInfoList)
        {
            Debug.Log(each.toString() + "\n");
            // Copy
            PromotionBannerInfoSample copyObject = new PromotionBannerInfoSample();
            copyObject.pid = each.pid;
            copyObject.imageUrl = each.imageUrl;
            copyObject.linkUrl = each.linkUrl;
            copyObject.displayStartDate = each.displayStartDate;
            copyObject.displayEndDate = each.displayEndDate;
            copyObject.utcStartDate = each.utcStartDate;
            copyObject.utcEndDate = each.utcEndDate;
            copyObject.typeLink = each.typeLink;
            copyObject.typeBanner = each.typeBanner;
            copyObject.interworkData = each.interworkData;
            bannerInfoListSample.Add(copyObject);
        }

        //OnGUI 활성화를 막고
        HIVETestManager testViewManager = HIVETestManager.instance;
        testViewManager._isShow = false;

        //샘플앱 프로모션 롤링배너 뷰를 띄워준다.
        GameObject bannerObject = MonoBehaviour.Instantiate(Resources.Load("Prefabs/PromotionBannerView"), new Vector2(0, 0), Quaternion.identity) as GameObject;
        bannerObject.GetComponent<PromotionBannerView>().initWithData(SDKtype.v4, bannerInfoListSample, (int resultValue, string logStr) => {
            Debug.Log("resultValue = " + resultValue + '\n' + "logStr = " + logStr);
            testViewManager._isShow = true;
        });
    }

	// 앱 초대 정보 요청에 대한 결과 통지 리스너 - Promotion.getAppInvitationData()
	public void onAppInvitationDataCB (ResultAPI result, AppInvitationData appInvitationData) {

		String qrcodeUrl = PlatformHelper.saveQrcodeToPng (appInvitationData.qrcode);

		_shareInviteMessage = appInvitationData.inviteMessage;
		_shareInviteLink = appInvitationData.inviteCommonLink;

#if !UNITY_EDITOR && UNITY_ANDROID

		// Android 8.0 이상인 경우
		if (GetSDKLevel() > 25) {

			_shareImageUrl = "content://" + Configuration.getAppId() + ".provider/external_files";

			String[] urlArray = qrcodeUrl.Split('/');
			
			for (int i=4; i<urlArray.Length; ++i) {
				_shareImageUrl += "/";
				_shareImageUrl += urlArray[i];
			}
		}
		else
			_shareImageUrl = "file://" + qrcodeUrl;

#elif !UNITY_EDITOR && UNITY_IPHONE

		_shareImageUrl = "file://" + qrcodeUrl;
#endif
		
		Debug.Log("PromotionTestView.onAppInvitationDataCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("appInvitationData = \n" + appInvitationData.toString());
	}

	public void onShareCB(ResultAPI result) {

		Debug.Log("PromotionTestView.onShareCB() Callback\nresult = " + result.toString() + "\n");
	}

	// 프로모션 화면 노출 형태 (BANNER, NEWS, NOTICE)
	private String[] _promotionTypes = new String[] { "BANNER", "NEWS", "NOTICE", "BANNERLEGACY" };
	private String[] _customTypes = new String[] { "VIEW", "BOARD", "SPOT", "DIRECT" };
	private int _selPromotionTypes = 0;
	private int _selCustomTypes = 0;

	// 프로모션 대화상자의 강제 노출 여부
	private String[] _isForced = new String[] { "True", "False" };
	private int _selIsForced = 0;

	private String _contentsKey = "";				// contentsKey for Custom Promotion View

    private String _newsMenuName = "";

	private String _additionalInfo = "{\\\"complete\\\":[202533,202514,202498]}";			// additional Info for Promotion View

	private String _shareText = "";
	private String _shareImageUrl = "";

	private String _shareInviteMessage = "";
	private String _shareInviteLink = "";

    private String _processURI = "interwork://hive/socialinquery";

	//public Vector2 scrollPosition = Vector2.zero;
	public void OnGUI() {	

        HIVETestView.layout(contentCount);

		HIVETestManager testViewManager = HIVETestManager.instance;

		if (testViewManager._isShow == false) {
			return;
		}
		
		float scrollViewHeight = (testViewManager .buttonHeight + 20) * contentCount;
		scrollPosition = GUI.BeginScrollView(new Rect(0, 0, Screen.width, Screen.height - testViewManager.bottomBarHeight - 20), scrollPosition, new Rect(0, 0, Screen.width, scrollViewHeight));
        GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.05f;
        GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.05f;
        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton ("- Promotion -")) {
            HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);

        // 프로모션 화면 노출 형태 (NOTICE, EVENT, FULL)
        GUILayout.BeginHorizontal();
        GUILayout.Box("PromotionType : ");
        _selPromotionTypes = HIVETestView.selectionGrid(_selPromotionTypes, _promotionTypes, _promotionTypes.Length);
        GUILayout.EndHorizontal();
        GUILayout.Space (2);

        // 프로모션 대화상자의 강제 노출 여부
        GUILayout.BeginHorizontal();
        GUILayout.Box("Promotion Force Type : ");
        _selIsForced = HIVETestView.selectionGrid(_selIsForced, _isForced, _isForced.Length);
        GUILayout.EndHorizontal();
        GUILayout.Space (2);

        // 이벤트, 프로모션 화면 노출
        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton ("Promotion.showPromotion(" + _promotionTypes[_selPromotionTypes] + ", " + _isForced[_selIsForced] + ")")) {

            PromotionType promotionType = PromotionType.BANNER;
            if(_selPromotionTypes == 1)
                promotionType = PromotionType.NEWS;
            else if(_selPromotionTypes == 2)
                promotionType = PromotionType.NOTICE;
            else if(_selPromotionTypes == 3)
                promotionType = PromotionType.BANNERLEGACY;

            Debug.Log("\n\n=== 이벤트, 프로모션 화면 노출. ===\n");
            Debug.Log(String.Format("Promotion.showPromotion(promotionType = {0}, isForced = {1}, onPromotionView) Called\n"
                , promotionType.ToString(), _selIsForced==0?"true":"false"));
            
            Promotion.showPromotion (promotionType, (_selIsForced==0), onPromotionViewCB);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);

        // 커스텀 컨텐츠 노출 형태 (VIEW, BOARD, SPOT, DIRECT)
        GUILayout.BeginHorizontal();
        GUILayout.Box("CustomType : ");
        _selCustomTypes = HIVETestView.selectionGrid(_selCustomTypes, _customTypes, _customTypes.Length);
        GUILayout.EndHorizontal();
        GUILayout.Space (2);

        // 커스텀 컨텐츠 노출
        GUILayout.BeginHorizontal();
        GUILayout.Box("ContentsKey : ");
        _contentsKey = HIVETestView.createTextField (_contentsKey);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton ("Promotion.showCustomContents(" + _customTypes[_selCustomTypes] + ", " + _contentsKey + ")")) {

            PromotionCustomType customType = PromotionCustomType.VIEW;
            if(_selCustomTypes == 1)
                customType = PromotionCustomType.BOARD;
            else if(_selCustomTypes == 2)
                customType = PromotionCustomType.SPOT;
            else if(_selCustomTypes == 3)
                customType = PromotionCustomType.DIRECT;


            Debug.Log("\n\n=== 커스텀 컨텐츠 화면 노출. ===\n");
            Debug.Log(String.Format("Promotion.showCustomContents(customType = {0}, contentsKey = {1}, onPromotionView) Called\n"
                , customType.ToString(), _contentsKey));
            
            Promotion.showCustomContents (customType, _contentsKey, onPromotionViewCB);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);


        // showNews 메뉴 명
        GUILayout.BeginHorizontal();
        GUILayout.Box("Menu Name : ");
        _newsMenuName = HIVETestView.createTextField (_newsMenuName);
        GUILayout.EndHorizontal();
        GUILayout.Space (2);

        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton ("Promotion.showNews(" +  _newsMenuName + ")")) {


            Debug.Log("\n\n=== 커스텀 컨텐츠 화면 노출. ===\n");
            Debug.Log(String.Format("Promotion.showNews(contentsKey = {0}, onPromotionView) Called\n"
                , _newsMenuName));
            
            Promotion.showNews ( _newsMenuName, onPromotionViewCB);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);

        

        // 게임내 오퍼월(무료 충전소) 을 호출 할 수 있는 버튼 노출 가능 상태 정보 조회
        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton ("Promotion.getOfferwallState()")) {

            Debug.Log("\n\n=== 게임내 오퍼월(무료 충전소) 노출 가능 여부 조회 ===\n");

            OfferwallState offerwallState = Promotion.getOfferwallState ();

            Debug.Log("Promotion.getOfferwallState() Called\nresult = " + offerwallState.ToString() + "\n");
        }

        // 무료 충전소 (Offerwall) 화면 노출
        if (HIVETestView.createButton ("Promotion.showOfferwall()")) {

            Debug.Log("\n\n=== 게임내 오퍼월(무료 충전소) 화면 노출. ===\n");
            Debug.Log("Promotion.showOfferwall(onPromotionView) Called\n");

            Promotion.showOfferwall (onPromotionViewCB);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);

        // HIVE SDK 4.10.0+ 리뷰 팝업 화면 노출
        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton("Promotion.showNativeReview()")) {

            Debug.Log("\n\n== 게임 리뷰 팝업 대화 상자 노출 (HIVE SDK 4.10.0+) ==\n");
            Debug.Log("Promotion.showNativeReview() Called\n");

            Promotion.showNativeReview();
        }

        GUILayout.EndHorizontal();
        GUILayout.Space(8);

#if !UNITY_EDITOR && UNITY_ANDROID
        // 종료 팝업 (추가 게임 다운로드 뷰 노출) (Android only)
        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton ("Promotion.showExit()")) {

            Debug.Log("\n\n=== 게임 종료 대화 상자 노출 (Android only) ===\n");
            Debug.Log("Promotion.showExit(onPromotionView) Called\n");

            Promotion.showExit (onPromotionViewCB);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);
#endif		// #if !UNITY_EDITOR && UNITY_ANDROID


        // ContentsKey에 해당하는 컨텐츠 조회
        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton ("Promotion.getViewInfo(" + _customTypes[_selCustomTypes] + ", " + _contentsKey + ")")) {

            PromotionCustomType customType = PromotionCustomType.VIEW;
            if(_selCustomTypes == 1)
                customType = PromotionCustomType.BOARD;
            else if(_selCustomTypes == 2)
                customType = PromotionCustomType.SPOT;
            else if(_selCustomTypes == 3)
                customType = PromotionCustomType.DIRECT;

            Debug.Log("\n\n=== contentsKey에 해당하는 프로모션 컨텐츠 조회. ===\n");
            Debug.Log(String.Format("Promotion.getViewInfo(customType = {0}, contentsKey = {1}, onPromotionView) Called\n", customType.ToString(), _contentsKey));

            Promotion.getViewInfo (customType, _contentsKey, onPromotionViewInfoCB);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);

        // 뱃지 정보 조회
        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton ("Promotion.getBadgeInfo()")) {

            Debug.Log("\n\n=== 게임내 프로모션 버튼의 부각을 위한 정보 (Badge) 조회 ===\n");
            Debug.Log("Promotion.getBadgeInfo(onPromotionBadgeInfo) Called\n");

            Promotion.getBadgeInfo (onPromotionBadgeInfoCB);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);

        // 롤링 프로모션 배너 정보 조회
        GUILayout.BeginHorizontal();
        // 롤링 배너 캠페인 타입 선택 버튼
        GUILayout.Box("CampaignType:");

        GUILayout.BeginVertical();
        if (!btnCampaignTypeIsSelected)
        {
            //Debug.Log("selected false, selectedGridIntCampaignType = " + selectedGridIntCampaignType + '\n');
            if (HIVETestView.createButton(listCampaignType[selectedGridIntCampaignType]))
            {
                btnCampaignTypeIsSelected = true;
            }
        }
        else
        {
            //Debug.Log("selected true, selectedGridIntCampaignType = " + selectedGridIntCampaignType + '\n');
            selectedGridIntCampaignType = HIVETestView.selectionGrid(-1, listCampaignType.ToArray(), listCampaignType.Count, 1);

            if (selectedGridIntCampaignType != -1)
            {
                btnCampaignTypeIsSelected = false;
            }
        }
        GUILayout.EndVertical();

        GUILayout.EndHorizontal();
        GUILayout.Space (8);

        /// 롤링 배너 배너 타입 선택 버튼
        GUILayout.BeginHorizontal();
        GUILayout.Box("BannerType:");

        GUILayout.BeginVertical();
        if (!btnBannerTypeIsSelected)
        {
            //Debug.Log("selected false, selectedGridIntBannerType = " + selectedGridIntBannerType + '\n');
            if (HIVETestView.createButton(listBannerType[selectedGridIntBannerType]))
            {
                btnBannerTypeIsSelected = true;
            }
        }
        else
        {
            //Debug.Log("selected true, selectedGridIntBannerType = " + selectedGridIntBannerType + '\n');
            selectedGridIntBannerType = HIVETestView.selectionGrid(-1, listBannerType.ToArray(), listBannerType.Count, 1);

            if (selectedGridIntBannerType != -1)
            {
                btnBannerTypeIsSelected = false;
            }
        }
        GUILayout.EndVertical();

        GUILayout.EndHorizontal();
        GUILayout.Space(8);


        // 프로모션 뷰 출력시 추가 정보 입력
        GUILayout.BeginHorizontal();
        GUILayout.Box("AdditionalInfo : ");
        _additionalInfo = HIVETestView.createTextField (_additionalInfo);
        GUILayout.EndHorizontal();
        GUILayout.Space (2);

        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton ("Promotion.setAdditionalInfo(" + _additionalInfo + ")")) {

            Debug.Log("\n\n=== GMS 등을 통해 별도의 가공 없이 게임 서버 API 또는 게임 서버의 중계용 API 등 그에 준하는 API에 전달될 값 설정 ===\n");
            Debug.Log(String.Format("Promotion.setAddtionalInfo(additionalInfo = {0}) Called\n", _additionalInfo));

            Promotion.setAdditionalInfo (_additionalInfo);
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton ("Promotion.updatePromotionData()")) 
        {
            Debug.Log("Promotion.updatePromotionDate()");
            Promotion.updatePromotionData();
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Box("Game Language : ");
        
        ConfigurationTestView.instance.gameLanguage = HIVETestView.createTextField(ConfigurationTestView.instance.gameLanguage);

        if (HIVETestView.createButton ("updateGameLanguage(" + ConfigurationTestView.instance.gameLanguage + ")")) 
        {
            Debug.Log("Configuration.updateGameLanguage(" + ConfigurationTestView.instance.gameLanguage + ")");
            Configuration.updateGameLanguage(ConfigurationTestView.instance.gameLanguage);
            if(File.Exists(ConfigurationTestView.configSavePath)){
                ConfigurationTestView.saveConfigurationToFile(Configuration.getAppId(), Configuration.getZone().ToString(), Configuration.getHiveTheme().ToString(), ConfigurationTestView.instance.hiveOrientation, Configuration.getUseLog(), ConfigurationTestView.instance.gameLanguage, ConfigurationTestView.instance.gameServerIdTextField);
            }
        }
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Box("Game Server ID : ");
        
        ConfigurationTestView.instance.gameServerIdTextField = HIVETestView.createTextField(ConfigurationTestView.instance.gameServerIdTextField);

        if (HIVETestView.createButton ("updateServerId(" + ConfigurationTestView.instance.gameServerIdTextField + ")")) 
        {
            Debug.Log("Configuration.updateServerId(" + ConfigurationTestView.instance.gameServerIdTextField + ")");
            Configuration.updateServerId(ConfigurationTestView.instance.gameServerIdTextField);
            if(File.Exists(ConfigurationTestView.configSavePath)){
                ConfigurationTestView.saveConfigurationToFile(Configuration.getAppId(), Configuration.getZone().ToString(), Configuration.getHiveTheme().ToString(), ConfigurationTestView.instance.hiveOrientation, Configuration.getUseLog(), ConfigurationTestView.instance.gameLanguage, ConfigurationTestView.instance.gameServerIdTextField);
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton("Random GiftBox")) {
            Promotion.getBannerInfo(PromotionCampaignType.ALL, PromotionBannerType.SMALL, (result, bannerInfoList) => {
                int bannerCount = bannerInfoList.Count;
                if( bannerCount > 0) {
                    int randomBanner = UnityEngine.Random.Range(0,bannerCount);
                    Debug.Log("Random " + randomBanner);

                    PromotionBannerInfo info = bannerInfoList[randomBanner];
                    _additionalInfo = "{\\\"complete\\\":["+ info.pid +"]}";
                }

            });

        }
        if (HIVETestView.createButton("Reset AdditionalInfo")) {
            _additionalInfo = "";
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (16);

        // 롤링 배너 API 호출 부튼
        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton("Promotion.getBannerInfo()"))
        {
            Debug.Log("\n\n===  프로모션 롤링 배너에 대한 정보 조회 ===\n");
            Debug.Log("Promotion.getBannerInfo(onPromotionBannerInfoCB) Called\n");

            PromotionCampaignType campaignType = dicCampaignType[listCampaignType[selectedGridIntCampaignType]];
            PromotionBannerType bannerType = dicBannerType[listBannerType[selectedGridIntBannerType]];
            Debug.Log("campaignType = " + campaignType.ToString() + '\n' + "bannerType = " + bannerType.ToString() + '\n');
            Promotion.getBannerInfo(campaignType, bannerType, onPromotionBannerInfoCB);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space(8);

        GUILayout.BeginHorizontal();
        // 앱 초대를 위한 데이터 조회
        if (HIVETestView.createButton ("Promotion.getAppInvitationData()")) {

            Debug.Log("\n\n=== 앱 초대를 위한 데이터 조회 (qrCode, inviteLink) ===\n");
            Debug.Log("Promotion.getAppInvitationData(onAppInvitationData) Called\n");

            Promotion.getAppInvitationData (onAppInvitationDataCB);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);

        GUILayout.BeginHorizontal();
        GUILayout.Box("ShareText : ");
        _shareText = HIVETestView.createTextField (_shareText);

        if (HIVETestView.createButton ("ShareText")) {

            // PlatformHelper.shareText (_shareText, onShareCB);

            PlatformShare platformShare = new PlatformShare();
            platformShare.setSharetype(PlatformShare.ShareType.TEXT);
            platformShare.setSubject("Sample Test");
            platformShare.setText(_shareText);

            PlatformHelper.share(platformShare, onShareCB);
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Box("ShareMedia Path : ");
        _shareImageUrl = HIVETestView.createTextField (_shareImageUrl);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        if (HIVETestView.createButton ("ShareMedia")) {

            // PlatformHelper.shareMedia (new String[]{_shareImageUrl}, _shareText, onShareCB);

            PlatformShare platformShare = new PlatformShare();
            platformShare.setSharetype(PlatformShare.ShareType.MEDIA);
            platformShare.setText(_shareText);
            platformShare.setMedia(new String[]{_shareImageUrl});

            PlatformHelper.share(platformShare, onShareCB);
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        if (HIVETestView.createButton ("UA Share")) {

            Promotion.showUAShare(_shareInviteMessage, _shareInviteLink, onShareCB);
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Box("uri : ");
        _processURI = HIVETestView.createTextField (_processURI);
        GUILayout.EndHorizontal();

        if (HIVETestView.createButton ("Promotion.processURI()"))
        {
            Boolean result = Promotion.processURI(_processURI);
            Debug.Log("Promotion.processURI() = " + result + "\n");
        }

        GUILayout.EndVertical();

        GUI.EndScrollView();
    }
}




