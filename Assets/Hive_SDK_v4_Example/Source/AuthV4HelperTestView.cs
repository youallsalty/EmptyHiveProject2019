using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
// using hive.AuthV4;

using hive;		// HIVE SDK 네임스페이스 사용

public class AuthV4HelperTestView {

	public static AuthV4HelperTestView instance = new AuthV4HelperTestView ();
	private int contentCount = 24;
	private Vector2 scrollPosition = Vector2.zero;

#if !UNITY_EDITOR && UNITY_ANDROID
	String _achievmentId1 = "";
	String _achievmentId2 = "";

	String _achievmentId3 = "";
	String _value1 = "";

	String _leaderboardId = "";
	String _score = "";
#endif

	public void Start() {}

	public void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
	}

	public void onAuthV4SignInHelper(ResultAPI result, AuthV4.PlayerInfo playerInfo) {
		
		Debug.Log("AuthV4HelperTestView.onAuthV4SignInHelper() Callback\nresult = " + result.toString() + "\n");

		if (result.code == ResultAPI.Code.AuthV4NotRegisteredDevice) {
			MainTestView.isAuthLogin = false;
			isConflict = false;
			return;
		}

		if (playerInfo != null)
			Debug.Log("playerInfo : " + playerInfo.toString());

		if ((result.code == ResultAPI.Code.AuthV4HelperImplifiedLoginFail) && (result.errorCode == ResultAPI.ErrorCode.INVALID_SESSION) ) {
			isImplifiedLogin = true;
		}

		checkConflict(result, playerInfo);
		if (result.isSuccess() == false)
			return;

		// Only on IAP Postbox Sample
		PostboxInfo.DID = playerInfo.did;
		PostboxInfo.VID = playerInfo.playerId.ToString();
		PostboxInfo.UID = playerInfo.playerName;
		PostboxInfo.hiveSessionToken = playerInfo.playerToken;

		AuthV4TestView.instance._playerInfo = playerInfo;
		MainTestView.isAuthLogin = true;
		isImplifiedLogin = false;
	}

	public void onAuthV4SignOutHelper(ResultAPI result, AuthV4.PlayerInfo playerInfo) {
		
		Debug.Log("AuthV4HelperTestView.onAuthV4SignOutHelper() Callback\nresult = " + result.toString() + "\n");

		if (playerInfo != null)
			Debug.Log("playerInfo : " + playerInfo.toString());

		if (result.isSuccess() == false)
			return;

		MainTestView.isAuthLogin = false;
		isImplifiedLogin = true;
	}

	public void onAuthV4Helper(ResultAPI result, AuthV4.PlayerInfo playerInfo) {
		
		Debug.Log("AuthV4HelperTestView.onAuthV4Helper() Callback\nresult = " + result.toString() + "\n");


		if (result.code == ResultAPI.Code.AuthV4NotRegisteredDevice) {
			MainTestView.isAuthLogin = false;
			isConflict = false;
			return;
		}

		if (result.code == ResultAPI.Code.AuthV4GoogleLogout)
			MainTestView.isAuthLogin = false;

		if (playerInfo != null)
			Debug.Log("playerInfo : " + playerInfo.toString());


		checkConflict(result, playerInfo);

		if (result.isSuccess() == false)
			return;

		isImplifiedLogin = false;
	}

	public void onAuthGameCenter(Boolean dismiss) {
		Debug.Log("onAuthGameCenter");

		Debug.Log("AuthV4TestView.onGameCenterCancel");
		if (dismiss) {
			Debug.Log("Dissmiss true");
		} else {
			Debug.Log("Dismiss False");
		}

	}
	private String[] _providerTypes = new String[] { "GUEST", "HIVE", "FACEBOOK", "GOOGLE", "QQ", "WEIBO", "VK", "WECHAT", "APPLE", "SIGNIN_APPLE", "LINE", "TWITTER", "WEVERSE"};
	private int _selProviderTypes = 0;

	private String[] _isShows = new String[] { "true", "false" };
	private int _selIsShow = 0;

	private String[] _isReady = new String[] { "true", "false" };
	private int _selIsReady = 0;

	public AuthV4.ProviderType getProviderType() {

		// AuthV4.ProviderType providerType = AuthV4.ProviderType.PROVIDER_GUEST;
		// if (_selProviderTypes == 1)
		// 	providerType = AuthV4.ProviderType.PROVIDER_HIVE;
		// else if (_selProviderTypes == 2)
		// 	providerType = AuthV4.ProviderType.PROVIDER_FACEBOOK;
		// else if (_selProviderTypes == 3)
		// 	providerType = AuthV4.ProviderType.PROVIDER_GOOGLE;
		// else if (_selProviderTypes == 4)
		// 	providerType = AuthV4.ProviderType.PROVIDER_QQ;
		// else if (_selProviderTypes == 5)
		// 	providerType = AuthV4.ProviderType.PROVIDER_WEIBO;
		// else if (_selProviderTypes == 6)
		// 	providerType = AuthV4.ProviderType.PROVIDER_VK;
		// else if (_selProviderTypes == 7)
		// 	providerType = AuthV4.ProviderType.PROVIDER_WECHAT;
		// else if (_selProviderTypes == 8)
		// 	providerType = AuthV4.ProviderType.PROVIDER_APPLE;
		// else if (_selProviderTypes == 9)
		// 	providerType = AuthV4.ProviderType.PROVIDER_AUTO;

		AuthV4.ProviderType providerType = AuthV4.ProviderType.GUEST;
		if (_selProviderTypes == 1)
			providerType = AuthV4.ProviderType.HIVE;
		else if (_selProviderTypes == 2)
			providerType = AuthV4.ProviderType.FACEBOOK;
		else if (_selProviderTypes == 3)
			providerType = AuthV4.ProviderType.GOOGLE;
		else if (_selProviderTypes == 4)
			providerType = AuthV4.ProviderType.QQ;
		else if (_selProviderTypes == 5)
			providerType = AuthV4.ProviderType.WEIBO;
		else if (_selProviderTypes == 6)
			providerType = AuthV4.ProviderType.VK;
		else if (_selProviderTypes == 7)
			providerType = AuthV4.ProviderType.WECHAT;
		else if (_selProviderTypes == 8)
			providerType = AuthV4.ProviderType.APPLE;
		else if (_selProviderTypes == 9)
			providerType = AuthV4.ProviderType.SIGNIN_APPLE;
		else if (_selProviderTypes == 10)
			providerType = AuthV4.ProviderType.LINE;
		else if (_selProviderTypes == 11)
			providerType = AuthV4.ProviderType.TWITTER;
		else if (_selProviderTypes == 12)
			providerType = AuthV4.ProviderType.WEVERSE;
		else if (_selProviderTypes == 13)
			providerType = AuthV4.ProviderType.AUTO;
			
		return providerType;
	}

	private void checkConflict(ResultAPI result, AuthV4.PlayerInfo playerInfo){
		// 충돌 상황인지 체크
		if (result.errorCode == ResultAPI.ErrorCode.CONFLICT_PLAYER) {
			AuthV4TestView.instance._conflictPlayer = playerInfo;
			isConflict = true;
		}

		// 충돌상황이 해결되었는지 체크
		// AuthV4PlayerResolved AuthV4PlayerChange
		if ( (result.code == ResultAPI.Code.AuthV4PlayerResolved)  || (result.code == ResultAPI.Code.AuthV4PlayerChange) ) {
			AuthV4TestView.instance._conflictPlayer = null;
			isConflict = false;

		// Only on IAP Postbox Sample
		PostboxInfo.DID = playerInfo.did;
		PostboxInfo.VID = playerInfo.playerId.ToString();
		PostboxInfo.UID = playerInfo.playerName;
		PostboxInfo.hiveSessionToken = playerInfo.playerToken;
		}
	}

	AuthV4.PlayerInfo _playerInfo = null;
	AuthV4.PlayerInfo _conflictPlayer = null;

	Boolean isAutoLogin = false;
	Boolean isConflict = false;
	Boolean isImplifiedLogin = false;
	
	String _playerId = "";
	String _selectedPlayerId = "";
	String _playerIdList = "";

	public void OnGUI() {

		HIVETestView.layout(contentCount);

		HIVETestManager testViewManager = HIVETestManager.instance;

		if (testViewManager._isShow == false) {
			return;
		}
		
		float scrollViewHeight = (testViewManager .buttonHeight + 20) * contentCount;
		scrollPosition = GUI.BeginScrollView(new Rect(0, 0, Screen.width, Screen.height - testViewManager.bottomBarHeight - 20), scrollPosition, new Rect(0, 0, Screen.width, scrollViewHeight));
        GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.05f;
        GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.05f;

		GUILayout.BeginVertical();

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- AuthV4Helper -")) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4Helper.getIDPList")) {
			Debug.Log("AuthV4Helper.getIDPList");

			List<AuthV4.ProviderType> idpList = AuthV4.Helper.getIDPList();
			
			foreach (AuthV4.ProviderType idp in idpList) {
				Debug.Log("ProviderType : " + idp);
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		if (!isImplifiedLogin) {
			if (HIVETestView.createButton ("AuthV4Helper.signIn")) {

				AuthV4.Helper.signIn(onAuthV4SignInHelper);
			}

			if (HIVETestView.createButton ("AuthV4Helper.signOut")) {
				AuthV4.Helper.signOut (onAuthV4SignOutHelper);
			}
		}
		if (isImplifiedLogin) {
			if (HIVETestView.createButton("AuthV4.showSignIn")) {
				AuthV4.showSignIn(onAuthV4SignInHelper);
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (15);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4Helper.connect")) {
			
			AuthV4.ProviderType providerType = getProviderType();

			AuthV4.Helper.connect (providerType, onAuthV4Helper);
		}
		if (HIVETestView.createButton ("AuthV4Helper.disconnect")) {
				
			AuthV4.ProviderType providerType = getProviderType();

			AuthV4.Helper.disconnect (providerType, onAuthV4Helper);
		}
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
				
		GUILayout.Box("Connect / Disconnect \nProviderType");
		_selProviderTypes = HIVETestView.selectionGrid(_selProviderTypes, _providerTypes, _providerTypes.Length);
		// GUILayout.EndHorizontal();
		// GUILayout.Space (8);

		GUILayout.BeginVertical();
		GUILayout.EndVertical();
		GUILayout.Space (8);
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

#if !UNITY_EDITOR
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4Helper.syncAccount")) {
			AuthV4.Helper.syncAccount(
	#if UNITY_ANDROID
				AuthV4.ProviderType.GOOGLE, 
	#elif UNITY_IPHONE
				AuthV4.ProviderType.APPLE, 
	#endif
				onAuthV4Helper);
		}
		// if (HIVETestView.createButton ("AuthV4Helper.resolveConflict")) {
		// 	AuthV4.Helper.resolveConflict(onAuthV4Helper);
		// }
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		// GUILayout.BeginHorizontal();
		// if (HIVETestView.createButton ("AuthV4Helper.showConflict")) {
		// 	AuthV4.Helper.showConflict(onAuthV4Helper);
		// }
		// GUILayout.EndHorizontal();
		// GUILayout.Space (8);
#endif
		GUILayout.EndVertical();
		#if UNITY_IPHONE
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4Helper.showGameCenterLoginCancelDialog")) {
			AuthV4.Helper.showGameCenterLoginCancelDialog(onAuthGameCenter);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space(8);
		#endif

		// if (isConflict) {
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("AuthV4Helper.showConflict")) {
				AuthV4.Helper.showConflict(onAuthV4Helper);
			}
			if (HIVETestView.createButton ("AuthV4Helper.showConflict ForGame")) {

				AuthV4.PlayerInfo conflictPlayer = AuthV4TestView.instance._conflictPlayer;
				AuthV4.Helper.ConflictSingleViewInfo viewInfo = new AuthV4.Helper.ConflictSingleViewInfo(conflictPlayer == null ? 0 : conflictPlayer.playerId);
				viewInfo.setValue("ID", conflictPlayer == null ? 0 : conflictPlayer.playerId);
				viewInfo.setValue("Name", conflictPlayer == null ? "" : conflictPlayer.playerName);
				viewInfo.setValue("Token",conflictPlayer == null ? "" : conflictPlayer.playerToken);
				AuthV4.Helper.showConflict(viewInfo, onAuthV4Helper);
			}
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("AuthV4Helper.resolveConflict")) {
				AuthV4.Helper.resolveConflict(onAuthV4Helper);
			}
			if (HIVETestView.createButton ("AuthV4Helper.switchAccount")) {
				AuthV4.Helper.switchAccount(onAuthV4Helper);
			}
			GUILayout.EndHorizontal();
		// }

		GUILayout.Space (8);

#if !UNITY_EDITOR && UNITY_ANDROID

		GUILayout.BeginHorizontal();
		GUILayout.Box("achievmentId : ");
		_achievmentId1 = HIVETestView.createTextField (_achievmentId1);
		if (HIVETestView.createButton ("AuthV4Helper.achievementsReveal")) {

			AuthV4.Helper.achievementsReveal(_achievmentId1, onAuthV4Helper);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box("achievmentId : ");
		_achievmentId2 = HIVETestView.createTextField (_achievmentId2);
		if (HIVETestView.createButton ("AuthV4Helper.achievementsUnlock")) {

			AuthV4.Helper.achievementsUnlock(_achievmentId2, onAuthV4Helper);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);
		
		GUILayout.BeginHorizontal();
		GUILayout.Box("achievmentId : ");
		_achievmentId3 = HIVETestView.createTextField (_achievmentId3);
		GUILayout.Box("value : ");
		_value1 = HIVETestView.createTextField (_value1);
		if (HIVETestView.createButton ("AuthV4Helper.achievementsIncrement")) {

			int value = 0;
			value = Convert.ToInt32(_value1);
			AuthV4.Helper.achievementsIncrement(_achievmentId3, value, onAuthV4Helper);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4Helper.showAchievements")) {

			AuthV4.Helper.showAchievements(onAuthV4Helper);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box("leaderboardId : ");
		_leaderboardId = HIVETestView.createTextField (_leaderboardId);
		GUILayout.Box("score : ");
		_score = HIVETestView.createTextField (_score);
		if (HIVETestView.createButton ("AuthV4Helper.leaderboardsSubmitScore")) {

			int score = 0;
			score = Convert.ToInt32(_score);
			AuthV4.Helper.leaderboardsSubmitScore(_leaderboardId, score, onAuthV4Helper);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4Helper.showLeaderboards")) {

			AuthV4.Helper.showLeaderboard(onAuthV4Helper);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

#else
		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();
		if (HIVETestView.createButton ("AuthV4Helper.showAchievements")) {
			
			AuthV4.Helper.showAchievements(onAuthV4Helper);
		}
		GUILayout.EndVertical();
		GUILayout.BeginVertical();
		if (HIVETestView.createButton ("AuthV4Helper.showLeaderboard")) {
			
			AuthV4.Helper.showLeaderboard(onAuthV4Helper);
		}
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();

		
#endif
		GUI.EndScrollView();
	}
}