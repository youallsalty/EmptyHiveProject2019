using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


using hive;			// HIVE SDK 네임스페이스 사용


/**
 * 사용자 프로필, 친구 목록, 친구 초대, SNS 포스팅 테스트 화면
 * (Social Network Service API (Facebook))
 * 
 * @author ryuvsken
 */
public class SocialFacebookTestView {
	
	public static SocialFacebookTestView instance = new SocialFacebookTestView ();

	private int contentCount = 24;
	private Vector2 scrollPosition = Vector2.zero;

	public void Start() {
	}


	public void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
	}


	// Facebook 프로필 정보 통지 결과 리스너 - SocialFacebook.getMyProfile()
	public void onProfileFacebookCB(ResultAPI result, List<ProfileFacebook> profileList) {

		Debug.Log ("SocialFacebookTestView.onProfileFacebookCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("profileList = \n");
		foreach (ProfileFacebook each in profileList) {
			Debug.Log (each.toString() + "\n");
		}
	}


	// Facebook 프로필 정보 통지 결과 리스너 - SocialFacebook.getFriends()
	public void onFriendsFacebookCB(ResultAPI result, List<ProfileFacebook> profileList) {

		Debug.Log ("SocialFacebookTestView.onFriendsFacebookCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("profileList = \n");
		foreach (ProfileFacebook each in profileList) {
			Debug.Log (each.toString() + "\n");
		}
	}


	// Facebook 친구 메시지 전송 통지 결과 리스너 - SocialFacebook.sendMessageFacebook()
	public void onSendMessageFacebookCB(ResultAPI result) {

		Debug.Log ("SocialFacebookTestView.onSendMessageFacebookCB() Callback\nresult = " + result.toString() + "\n");
	}


	// Facebook 친구 초대 대화 상자 결과 리스너 (SocialFacebook.showInvitationDialogFacebook())
	public void onShowInvitationDialogFacebookCB(ResultAPI result, List<String> invitedUserList) {

		Debug.Log ("SocialFacebookTestView.onShowInvitationDialogFacebookCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("invitedUserList = \n");
		foreach (String each in invitedUserList) {
			Debug.Log (each + "\n");
		}
	}


	// Facebook 담벼락 포스팅 결과 리스너 - SocialFacebook.postFacebook()
	public void onPostFacebookCB(ResultAPI result) {

		Debug.Log ("SocialFacebookTestView.onPostFacebookCB() Callback\nresult = " + result.toString() + "\n");
	}


	private String _vid = "vid";					// vid (vid = HIVE UserKey) for SocialFacebook.sendMessage()
	private String _message = "Message";			// message for SocialFacebook.sendMessage()

	private String _invitationMessage = "Invitation Message";		// message for SocialFacebook.showInvitationDialog()

	private String _postMessage = "Post Message";	// message for SocialFacebook.postFacebook()


	public void OnGUI() {

		HIVETestView.layout(contentCount);

		HIVETestManager testViewManager = HIVETestManager.instance;

		if (testViewManager._isShow == false) {
			return;
		}
		
		float scrollViewHeight = (testViewManager .buttonHeight + 20) * contentCount;
		scrollPosition = GUI.BeginScrollView(new Rect(0, 0, Screen.width, Screen.height - testViewManager.bottomBarHeight - 20), scrollPosition, new Rect(0, 0, Screen.width, scrollViewHeight));
        GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.05f;
        GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.05f;


		GUILayout.BeginVertical();

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- Social Facebook -")) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);
		
		GUILayout.BeginHorizontal();
		GUILayout.Box("isLogin : ");

		if (HIVETestView.createButton ("facebook isLogin")) {

			Boolean isLogin = SocialFacebook.isLogin ();

			Debug.Log("\n\n=== Facebook isLogin ===\n" + isLogin);
		}

		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		// Facebook 프로필 정보 조회
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("SocialFacebook.getMyProfile()")) {

			Debug.Log("\n\n=== Facebook 프로필 정보 조회 ===\n");
			Debug.Log("SocialFacebook.getMyProfile(onProfileFacebook) Called\n");

			SocialFacebook.getMyProfile (onProfileFacebookCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// Facebook 친구 목록 조회
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("SocialFacebook.getFriends()")) {

			Debug.Log("\n\n=== Facebook 친구 목록 조회 ===\n");
			Debug.Log("SocialFacebook.getFriends(onFriendsFacebook) Called\n");

			SocialFacebook.getFriends (onFriendsFacebookCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// Facebook 인증 사용자의 친구에게 메시지를 전송
		GUILayout.BeginHorizontal();
		GUILayout.Box("vid (vid is HIVE UserKey) : ");
		_vid = HIVETestView.createTextField (_vid);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Message : ");
		_message = HIVETestView.createTextField (_message);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("SocialFacebook.sendMessage(" + _vid + ", " + _message + ")")) {

			List<String> recipients = new List<String> ();
			recipients.Add (_vid);

			String dialogTitle = "Send Message", message = _message, data = "hidden data";
			FacebookMessage messageContents = new FacebookMessage (recipients, dialogTitle, message, data);


			Debug.Log("\n\n=== Facebook 인증 사용자의 친구에게 메시지를 전송 ===\n");
			Debug.Log("SocialFacebook.sendMessageFacebook(content, onSendMessageFacebook) Called\ncontent = "
				+ messageContents!=null?messageContents.toString():"null" + "\n");
			
			SocialFacebook.sendMessageFacebook (messageContents, onSendMessageFacebookCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// Facebook 인증 사용자의 친구에게 초대 요청을 위한 대화상자 호출
		GUILayout.BeginHorizontal();
		GUILayout.Box("Invitation Message : ");
		_invitationMessage = HIVETestView.createTextField (_invitationMessage);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("SocialFacebook.showInvitationDialog(" + _invitationMessage + ")")) {

			String dialogTitle = "Invitation Dialog", message = _invitationMessage, data = "hidden data";

			// TODO: recipients
			List<String> recipients = new List<String> ();

			FacebookMessage inviteContents = new FacebookMessage (recipients, dialogTitle, message, data);


			Debug.Log("\n\n=== Facebook 인증 사용자의 친구에게 초대 요청을 위한 대화상자 호출. ===\n");
			Debug.Log("SocialFacebook.sendMessageFacebook(content, onShowInvitationDialogFacebook) Called\ncontent = "
				+ inviteContents!=null?inviteContents.toString():"null" + "\n");
			
			SocialFacebook.showInvitationDialog (inviteContents, onShowInvitationDialogFacebookCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// Facebook 게시글 등록
		GUILayout.BeginHorizontal();
		GUILayout.Box("Post Message : ");
		_postMessage = HIVETestView.createTextField (_postMessage);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("SocialFacebook.postFacebook(" + _postMessage + ")")) {

			String title = "Content Post", contentDescription = _postMessage, contentURL = "", imageURL = "";
			contentURL = "http://withhive.com"; // content URL
			imageURL = "http://gw.com2us.com/gw_resource/css/gw_skin01/images_gw/logo.png";	// 샘플 이미지 URL

			Debug.Log("\n\n=== Facebook Posting. ===\n");
			Debug.Log("SocialFacebook.postFacebookWithContentURL(imageURL, onPostFacebook) Called\n"
				+ imageURL + "\n");

			SocialFacebook.postFacebookWithContentURL(imageURL, onPostFacebookCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.EndVertical();
		GUI.EndScrollView();
	}
}



