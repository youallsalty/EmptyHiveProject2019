using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


using hive;			// HIVE SDK 네임스페이스 사용


/**
 * 결제 (Billing) API 테스트 화면
 * 
 * @author ryuvsken
 */
public class IAPV4TestView {

	public static IAPV4TestView instance = new IAPV4TestView ();


	public void Start() {
	}


	public void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
	}

	private bool isIAPInitialize = false;
	private String locationCode = "main";

	public void onIAPV4MarketInfo(ResultAPI result, List<hive.IAPV4.IAPV4Type> iapV4TypeList) {

		Debug.Log("IAPV4TestView.onIAPV4MarketInfo() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			Debug.Log ("iapV4TypeList = \n");
			foreach(hive.IAPV4.IAPV4Type each in iapV4TypeList) {

				Debug.Log (each.ToString() + "\n");
			}

			isIAPInitialize = true;
		}
	}

	public void onIAPV4ProductInfo(ResultAPI result, List<hive.IAPV4.IAPV4Product> iapV4ProductList, int balance) {

		Debug.Log("IAPV4TestView.onIAPV4ProductInfo() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			Debug.Log ("iapV4ProductList = \n");
			foreach(hive.IAPV4.IAPV4Product each in iapV4ProductList) {

				Debug.Log (each.toString() + "\n");
			}

			Debug.Log ("balance = " + balance + "\n");

			IAPV4ShopInfoTestView.instance.setShopInfo(iapV4ProductList, balance, false);
			HIVETestManager.instance.switchView (TestViewType.IAPV4_SHOPINFO_VIEW);
		}
	}

	public void onIAPV4SubscrptionProductInfo(ResultAPI result, List<hive.IAPV4.IAPV4Product> iapV4ProductList, int balance) {

		Debug.Log("IAPV4TestView.onIAPV4SubscrptionProductInfo() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			Debug.Log ("iapV4ProductList = \n");
			foreach(hive.IAPV4.IAPV4Product each in iapV4ProductList) {

				Debug.Log (each.toString() + "\n");
			}

			Debug.Log ("balance = " + balance + "\n");

			IAPV4ShopInfoTestView.instance.setShopInfo(iapV4ProductList, balance, true);
			HIVETestManager.instance.switchView (TestViewType.IAPV4_SHOPINFO_VIEW);
		}
	}



	// 러비 잔액 확인 결과 통지 리스너 - IAPV4.getBalance()
	public void onIAPV4Balance(ResultAPI result, int balance) {

		Debug.Log("IAPV4TestView.onIAPV4BalanceCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("balance = " + balance + "\n");
	}

	public void OnGUI()
    {
		HIVETestView.layout(24);


        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- IAPV4 -")) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Location Code : ");
		this.locationCode = HIVETestView.createTextField(this.locationCode);
		GUILayout.EndHorizontal();
		GUILayout.Space (8);
		
		
		//iOS IAP restore 불가능한 현상을 해소하기 위한 모든 트랜잭션 finsih 처리. SDK로 제공하는 API 아님
	#if !UNITY_EDITOR && UNITY_IPHONE
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("FinishAllTransactions (iOS Only)")) {
			int count = TransactionCleaner.FinishAllTransactions();
			if (count == -1 ){
				Debug.Log("Not Supported OS");
			} else {
				Debug.Log(count + " transactions finished");
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);
	#else
	#endif

		// IAP 결재 API 초기화 요청
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("IAPV4.marketConnect()")) {

			Debug.Log("\n\n=== IAPV4 결재 API 초기화 요청 ===\n");
			Debug.Log("IAPV4.marketConnect(listener) Called\n");

			IAPV4.marketConnect(onIAPV4MarketInfo);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		if (isIAPInitialize == true) {

			// 선택된 마켓 조회
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("IAPV4.getSelectedMarket()")) {

				Debug.Log("\n\n=== 선택된 마켓 조회 ===\n");
				Debug.Log(String.Format("IAPV4.getSelectedMarket() = {0}, Called\n", IAPV4.getSelectedMarket().ToString()));

			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);
			
			// 상점 정보 조회
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("IAPV4.getProductInfo()")) {

				Debug.Log("\n\n=== 상점 정보 조회 ===\n");
				Debug.Log(String.Format("IAPV4.getProductInfo(locationCode = {0}, listener) Called\n", locationCode));

				IAPV4.getProductInfo(onIAPV4ProductInfo);
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);


			// 상점 구독 정보 조회
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("IAPV4.getSubscriptionProductInfo()")) {

				Debug.Log("\n\n=== 상점 구독 정보 조회 ===\n");
				Debug.Log(String.Format("IAPV4.getSubscriptionProductInfo(locationCode = {0}, listener) Called\n", locationCode));

				IAPV4.getSubscriptionProductInfo(onIAPV4SubscrptionProductInfo);
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);

			// // HIVE IAP 상품에 대한 미지급된 아이템 복구를 요청한다.
			// GUILayout.BeginHorizontal();
			// if (HIVETestView.createButton ("IAPV4.restore()")) {

			// 	Debug.Log("\n\n=== HIVE IAP 상품에 대한 미지급된 아이템 복구를 요청. ===\n");
			// 	Debug.Log("IAPV4.restore(onIAPV4Restore) Called\n");

			// 	IAPV4.restore (onIAPV4Restore);
			// }
			// GUILayout.EndHorizontal();
			// GUILayout.Space (8);

			// GUILayout.BeginHorizontal();
			// if (HIVETestView.createButton ("IAPV4.transactionFinish()")) {

			// 	Debug.Log("IAPV4.transactionFinish(onIAPV4TransactionFinish) Called\n");

			// 	IAPV4.transactionFinish (onIAPV4TransactionFinish);
			// }
			// GUILayout.EndHorizontal();
			// GUILayout.Space (8);

			// GUILayout.BeginHorizontal();
			// if (HIVETestView.createButton ("IAPV4.transactionMultiFinish()")) {

			// 	Debug.Log("IAPV4.transactionMultiFinish(onIAPV4TransactionMultiFinish) Called\n");

			// 	IAPV4.transactionMultiFinish (onIAPV4TransactionMultiFinish);
			// }
			// GUILayout.EndHorizontal();
			// GUILayout.Space (8);

#if !UNITY_EDITOR && UNITY_ANDROID

			// 러비 상점이나 구글 상점을 선택하기 위한 창을 띄운다.
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("IAPV4.showMarketSelection()")) {

				Debug.Log("\n\n=== 러비 상점이나 구글 상점을 선택하기 위한 창을 띄운다. ===\n");
				Debug.Log("IAPV4.showPayment(onIAPV4MarketInfo) Called\n");

				IAPV4.showMarketSelection (onIAPV4MarketInfo);
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);


			// 리비 상점일 경우 잔액 정보 조회.
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("IAPV4.getBalanceInfo()")) {

				Debug.Log("\n\n=== HIVE 러비 상점일 경우 잔액 정보 조회. ===\n");
				Debug.Log("IAPV4.getBalance(onIAPV4Balance) Called\n");

				IAPV4.getBalanceInfo (onIAPV4Balance);
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);


			// 러비 충전 페이지 호출
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("IAPV4.showCharge()")) {

				Debug.Log("\n\n=== 러비 충전 페이지 호출. ===\n");
				Debug.Log("IAPV4.showCharge(onIAPV4Balance) Called\n");

				IAPV4.showCharge (onIAPV4Balance);
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);

#endif			// #if !UNITY_EDITOR && UNITY_ANDROID

		}

        GUILayout.EndVertical();
    }

}



