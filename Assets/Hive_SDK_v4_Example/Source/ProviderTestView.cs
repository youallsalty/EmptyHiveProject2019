using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
// using hive.AuthV4;

using hive;		// HIVE SDK 네임스페이스 사용

public class ProviderTestView
{

    public static ProviderTestView instance = new ProviderTestView();

    public void Start() { }

    public void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            HIVETestManager.instance.switchView(TestViewType.MAIN_VIEW);
        }
    }

#if !UNITY_EDITOR && UNITY_ANDROID

	public void onAchievementsReveal(ResultAPI result) {

		Debug.Log("ProviderTestView.onAchievementsReveal() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	public void onAchievementsUnlock(ResultAPI result) {

		Debug.Log("ProviderTestView.onAchievementsUnlock() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	public void onAchievementsIncrement(ResultAPI result) {

		Debug.Log("ProviderTestView.onAchievementsIncrement() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	public void onShowAchievement(ResultAPI result) {

		Debug.Log("ProviderTestView.onShowAchievement() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	public void onLeaderboardsSubmitScore(ResultAPI result){

		Debug.Log("ProviderTestView.onLeaderboardsSubmitScore() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	public void onShowLeaderboards(ResultAPI result) {

		Debug.Log("ProviderTestView.onShowLeaderboards() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	public void onGooglePlayerIdResult(ResultAPI result, String googlePlayerId, String authCode) {

		Debug.Log("ProviderTestView.onGooglePlayerIdResult() Callback\nresult = " + result.toString() + "\n");
		Debug.Log("GooglePlayerId : " + googlePlayerId);
		Debug.Log("AuthCode : " + authCode);
	}
#elif !UNITY_EDITOR && UNITY_IPHONE

	public void onReportLeaderboard(ResultAPI result) {

		Debug.Log("ProviderTestView.onReportLeaderboard() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	public void onShowLeaderboard(ResultAPI result) {

		Debug.Log("ProviderTestView.onShowLeaderboard() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	public void onLoadAchievements(ResultAPI result, List<ProviderApple.Achievement> achievementList) {

		if (result.isSuccess() == false) {
			Debug.Log("ProviderTestView.onLoadAchievements() Callback\nresult = " + result.toString() + "\n");
			return;
		}

		String log = "ProviderTestView.onLoadAchievements() Callback\nresult = " + result.toString() + "\n";


		foreach (ProviderApple.Achievement achievementData in achievementList) {
			log += "[" + achievementData.identifier + ", " + achievementData.percent + "%, isCompleted : " + achievementData.completed + "]\n";
		}

		Debug.Log(log);
	}

	public void onReportAchievement(ResultAPI result) {

		Debug.Log("ProviderTestView.onReportAchievement() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	public void onShowAchievement(ResultAPI result) {

		Debug.Log("ProviderTestView.onShowAchievement() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	public void onResetAchievements(ResultAPI result) {

		Debug.Log("ProviderTestView.onResetAchievements() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

#endif // !UNITY_EDITOR && UNITY_ANDROID or !UNITY_EDITOR && UNITY_IPHONE

#if !UNITY_EDITOR && UNITY_ANDROID

	String _achievmentId1 = "CgkIzev2g9MJEAIQAQ";
	String _achievmentId2 = "CgkIzev2g9MJEAIQAw";

	String _achievmentId3 = "CgkIzev2g9MJEAIQAg";
	String _value1 = "10";

	String _leaderboardId = "CgkIzev2g9MJEAIQBw";
	String _score = "10";

#elif !UNITY_EDITOR && UNITY_IPHONE

	String _score = "11";
	String _leaderboardIdentifier = "grp.com.com2us.hivesdk.rank01";

	String _percent = "70";
	private String[] _showsCompletionBanner = new String[] { "true", "false" };
	private int _selShowsCompletionBanner = 0;
	String _achievementIdentifier = "grp.com.com2us.hivesdk.10hit";

#endif // !UNITY_EDITOR && UNITY_ANDROID or !UNITY_EDITOR && UNITY_IPHONE

    public void OnGUI()
    {

        HIVETestView.layout(24);

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton("- ProviderTest -"))
        {
            HIVETestManager.instance.switchView(TestViewType.MAIN_VIEW);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);

#if !UNITY_EDITOR && UNITY_ANDROID

		GUILayout.BeginHorizontal();
		GUILayout.Box("achievmentId : ");
		_achievmentId1 = HIVETestView.createTextField (_achievmentId1);
		if (HIVETestView.createButton ("ProviderGoogle.achievementsReveal")) {

			ProviderGoogle.achievementsReveal(_achievmentId1, onAchievementsReveal);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box("achievmentId : ");
		_achievmentId2 = HIVETestView.createTextField (_achievmentId2);
		if (HIVETestView.createButton ("ProviderGoogle.achievementsUnlock")) {

			ProviderGoogle.achievementsUnlock(_achievmentId2, onAchievementsUnlock);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);
		
		GUILayout.BeginHorizontal();
		GUILayout.Box("achievmentId : ");
		_achievmentId3 = HIVETestView.createTextField (_achievmentId3);
		GUILayout.Box("value : ");
		_value1 = HIVETestView.createTextField (_value1);
		if (HIVETestView.createButton ("ProviderGoogle.achievementsIncrement")) {

			int value = 0;
			value = Convert.ToInt32(_value1);
			ProviderGoogle.achievementsIncrement(_achievmentId3, value, onAchievementsIncrement);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("ProviderGoogle.showAchievements")) {

			ProviderGoogle.showAchievements(onShowAchievement);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box("leaderboardId : ");
		_leaderboardId = HIVETestView.createTextField (_leaderboardId);
		GUILayout.Box("score : ");
		_score = HIVETestView.createTextField (_score);
		if (HIVETestView.createButton ("ProviderGoogle.leaderboardsSubmitScore")) {

			int score = 0;
			score = Convert.ToInt32(_score);
			ProviderGoogle.leaderboardsSubmitScore(_leaderboardId, score, onLeaderboardsSubmitScore);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("ProviderGoogle.showLeaderboards")) {

			ProviderGoogle.showLeaderboards(onShowLeaderboards);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("ProviderGoogle.getGooglePlayerId")) {

			ProviderGoogle.getGooglePlayerId(onGooglePlayerIdResult);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

#elif !UNITY_EDITOR && UNITY_IPHONE

		GUILayout.BeginHorizontal();
		GUILayout.Box("score : ");
		_score = HIVETestView.createTextField (_score);

		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();

		GUILayout.Box("leaderboardIdentifier : ");
		_leaderboardIdentifier = HIVETestView.createTextField (_leaderboardIdentifier);

		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		
		if (HIVETestView.createButton ("ProviderApple.reportScore")) {

			ProviderApple.reportScore(_score, _leaderboardIdentifier, onReportLeaderboard);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (16);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("ProviderApple.showLeaderboard")) {

			ProviderApple.showLeaderboard(onShowLeaderboard);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (24);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("ProviderApple.loadAchievements")) {

			ProviderApple.loadAchievements(onLoadAchievements);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (16);

		GUILayout.BeginHorizontal();
		GUILayout.Box("percent : ");
		_percent = HIVETestView.createTextField (_percent);
		GUILayout.Box("ShowsCompletionBanner : ");
		_selShowsCompletionBanner = HIVETestView.selectionGrid(_selShowsCompletionBanner, _showsCompletionBanner, _showsCompletionBanner.Length);
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box("achievementIdentifier : ");
		_achievementIdentifier = HIVETestView.createTextField (_achievementIdentifier);
	
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		
		if (HIVETestView.createButton ("ProviderApple.reportAchievement")) {

			Boolean showsCompletionBanner = (_selShowsCompletionBanner == 0);
			ProviderApple.reportAchievement(_percent, showsCompletionBanner, _achievementIdentifier, onReportAchievement);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (16);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("ProviderApple.showAchievements")) {

			ProviderApple.showAchievements(onShowAchievement);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (16);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("ProviderApple.resetAchievements")) {

			ProviderApple.resetAchievements(onResetAchievements);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);
		

#endif // !UNITY_EDITOR && UNITY_ANDROID or !UNITY_EDITOR && UNITY_IPHONE

        GUILayout.EndVertical();
    }
}