using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
// using hive.AuthV4;

using hive;		// HIVE SDK 네임스페이스 사용

public class AuthV4TestView {

	public static AuthV4TestView instance = new AuthV4TestView ();
	private int contentCount = 24;
	private Vector2 scrollPosition = Vector2.zero;

	public void Start() {}

	public void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
	}

	public void onAuthV4Setup(ResultAPI result, Boolean isAutoSignIn, String did, List<AuthV4.ProviderType> providerTypeList) {

		Debug.Log("AuthV4TestView.onAuthV4Setup() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;

		Debug.Log("isAutoSignIn = " + isAutoSignIn + "\n");
		Debug.Log("did = " + did + "\n");

		foreach (AuthV4.ProviderType providerType in providerTypeList)
			Debug.Log("providerType = " + providerType + "\n");
	}

	public void onAuthV4SignIn(ResultAPI result, AuthV4.PlayerInfo playerInfo) {

		Debug.Log("AuthV4TestView.onAuthV4SignIn() Callback\nresult = " + result.toString() + "\n");

		if (result.code == ResultAPI.Code.AuthV4NotRegisteredDevice) {
			MainTestView.isAuthLogin = false;
			isConflict = false;
			return;
		} else if ((result.isSuccess() == false) && !(result.code == ResultAPI.Code.AuthV4PlayerChange))
			return;
		else {
			MainTestView.isAuthLogin = true;
			isConflict = false;
		}

		Debug.Log("playerInfo = " + playerInfo.toString() + "\n");

		// Only on IAP Postbox Sample
		PostboxInfo.DID = playerInfo.did;
		PostboxInfo.VID = playerInfo.playerId.ToString();
		PostboxInfo.UID = playerInfo.playerName;
		PostboxInfo.hiveSessionToken = playerInfo.playerToken;
		ConfigurationTestView.instance.postboxDataSetting();
		PostboxView.instance.callThePostboxLogin();

		_playerInfo = playerInfo;
	}

	public void onAuthV4SignOut(ResultAPI result) {

		Debug.Log("AuthV4TestView.onAuthV4SignOut() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
		else
			MainTestView.isAuthLogin = false;
	}

	public void onAuthV4Connect(ResultAPI result, AuthV4.PlayerInfo conflictPlayer) {

		Debug.Log("AuthV4TestView.onAuthV4Connect() Callback\nresult = " + result.toString() + "\n");

		if (result.errorCode == ResultAPI.ErrorCode.CONFLICT_PLAYER) {
			_conflictPlayer = conflictPlayer;
			_selectedPlayerId = _conflictPlayer.playerId.ToString();
			isConflict = true;

			Debug.Log("conflictPlayerInfo = " + conflictPlayer.toString() + "\n");
		}

		if (result.isSuccess() == false)
			return;
	}

	public void onAuthV4Disconnect(ResultAPI result) {

		Debug.Log("AuthV4TestView.onAuthV4Disconnect() Callback\nresult = " + result.toString() + "\n");
	}

	public void onAuthV4GetProfile(ResultAPI result, List<AuthV4.ProfileInfo> profileInfoList) {

		Debug.Log("AuthV4TestView.onAuthV4GetProfile() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;

		foreach (AuthV4.ProfileInfo profileInfo in profileInfoList)
			Debug.Log("profileInfo = " + profileInfo.toString() + "\n");
	}

	public void onAuthV4ShowProfile(ResultAPI result) {

		Debug.Log("AuthV4TestView.onAuthV4ShowProfile() Callback\nresult = " + result.toString() + "\n");
	}

	public void onAuthV4ShowInquiry(ResultAPI result) {

		Debug.Log("AuthV4TestView.onAuthV4ShowInquiry() Callback\nresult = " + result.toString() + "\n");
	}

	public void onAuthV4ShowMyInquiry(ResultAPI result) {

		Debug.Log("AuthV4TestView.onAuthV4ShowMyInquiry() Callback\nresult = " + result.toString() + "\n");
	}

	public void onAuthV4ShowChatbotInquiry(ResultAPI result) {
		
		Debug.Log("AuthV4TestView.onAuthV4ShowChatbotInquiry() Callback\nresult = " + result.toString() + '\n');
	}

	public void onAuthV4ShowTerms(ResultAPI result) {

		Debug.Log("AuthV4TestView.onAuthV4ShowTerms() Callback\nresult = " + result.toString() + "\n");
	}

	public void onAuthV4AdultConfirm(ResultAPI result) {

		Debug.Log("AuthV4TestView.onAuthV4AdultConfirm() Callback\nresult = " + result.toString() + "\n");
	}

	public void onDeviceProviderInfo(ResultAPI result, AuthV4.ProviderInfo providerInfo) {

		Debug.Log("AuthV4TestView.onDeviceProviderInfo() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;

		Debug.Log("providerInfo = " + providerInfo.toString() + "\n");
	}

	public void onAuthV4CheckBlacklist(ResultAPI result, List<AuthV4.AuthV4MaintenanceInfo> maintenanceInfoList) {

		Debug.Log("AuthV4TestView.onAuthV4CheckBlacklist() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;

		foreach (AuthV4.AuthV4MaintenanceInfo maintenanceInfo in maintenanceInfoList)
			Debug.Log("maintenanceInfo = " + maintenanceInfo.toString() + "\n");
	}

	public void onAuthV4Maintenance(ResultAPI result, List<AuthV4.AuthV4MaintenanceInfo> maintenanceInfoList) {

		Debug.Log("AuthV4TestView.onAuthV4Maintenance() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;

		foreach (AuthV4.AuthV4MaintenanceInfo maintenanceInfo in maintenanceInfoList)
			Debug.Log("maintenanceInfo = " + maintenanceInfo.toString() + "\n");
	}

	public void onGameCenterCancel(Boolean dismiss) {
		Debug.Log("AuthV4TestView.onGameCenterCancel");
		if (dismiss) {
			Debug.Log("Dissmiss true");
		} else {
			Debug.Log("Dismiss False");
		}

	} 
	public void onGetProviderFriendsList(ResultAPI result, AuthV4.ProviderType providerType, Dictionary<String, Int64> providerUserIdList) {

		Debug.Log("AuthV4TestView.onGetProviderFriendsList() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;

		Debug.Log("providerType = " + providerType + "\n");

		foreach (KeyValuePair<String, Int64> providerUserId in providerUserIdList )
			Debug.Log("providerUserId = " + providerUserId.Key.ToString() + ", " + providerUserId.Value.ToString() + "\n");
	}

	public void onAuthV4ResolveConflict(ResultAPI result) {

		Debug.Log("AuthV4TestView.onAuthV4ResolveConflict() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	public void onAuthV4ShowDeviceManagement(ResultAPI result) {

		Debug.Log("AuthV4TestView.onAuthV4ShowDeviceManagement() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	private String[] _providerTypes = new String[] { "GUEST", "HIVE", "FACEBOOK", "GOOGLE", "QQ", "WEIBO", "VK", "WECHAT", "APPLE", "SIGNIN_APPLE", "LINE", "TWITTER", "WEVERSE", "AUTO" };
	private int _selProviderTypes = 0;

	private String[] _isShows = new String[] { "true", "false" };
	private int _selIsShow = 0;

	private String[] _isReady = new String[] { "true", "false" };
	private int _selIsReady = 0;

	public AuthV4.ProviderType getProviderType() {

		AuthV4.ProviderType providerType = AuthV4.ProviderType.GUEST;
		if (_selProviderTypes == 1)
			providerType = AuthV4.ProviderType.HIVE;
		else if (_selProviderTypes == 2)
			providerType = AuthV4.ProviderType.FACEBOOK;
		else if (_selProviderTypes == 3)
			providerType = AuthV4.ProviderType.GOOGLE;
		else if (_selProviderTypes == 4)
			providerType = AuthV4.ProviderType.QQ;
		else if (_selProviderTypes == 5)
			providerType = AuthV4.ProviderType.WEIBO;
		else if (_selProviderTypes == 6)
			providerType = AuthV4.ProviderType.VK;
		else if (_selProviderTypes == 7)
			providerType = AuthV4.ProviderType.WECHAT;
		else if (_selProviderTypes == 8)
			providerType = AuthV4.ProviderType.APPLE;
		else if (_selProviderTypes == 9)
			providerType = AuthV4.ProviderType.SIGNIN_APPLE;
		else if (_selProviderTypes == 10)
			providerType = AuthV4.ProviderType.LINE;
		else if (_selProviderTypes == 11)
			providerType = AuthV4.ProviderType.TWITTER;
		else if (_selProviderTypes == 12)
			providerType = AuthV4.ProviderType.WEVERSE;
		else if (_selProviderTypes == 13)
			providerType = AuthV4.ProviderType.AUTO;

		return providerType;
	}

	public AuthV4.PlayerInfo _playerInfo = null;
	public AuthV4.PlayerInfo _conflictPlayer = null;

	Boolean isAutoLogin = false;
	Boolean isConflict = false;
	
	String _playerId = "";
	String _selectedPlayerId = "";
	String _playerIdList = "";

	public void OnGUI() {

		GUILayoutOption btnMinHeight = GUILayout.MinHeight(HIVETestManager.instance.buttonHeight);
		GUILayoutOption btnMaxWidth = GUILayout.MaxWidth(HIVETestManager.instance.screenWidth / 2);

		HIVETestView.layout(contentCount);


		HIVETestManager testViewManager = HIVETestManager.instance;

		if (testViewManager._isShow == false) {
			return;
		}
		
		float scrollViewHeight = (testViewManager .buttonHeight + 20) * contentCount;
		scrollPosition = GUI.BeginScrollView(new Rect(0, 0, Screen.width, Screen.height - testViewManager.bottomBarHeight - 20), scrollPosition, new Rect(0, 0, Screen.width, scrollViewHeight));
        GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.05f;
        GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.05f;

		GUILayout.BeginVertical();


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- AuthV4 -")) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);
		
		GUILayout.Box("ProviderType");
		_selProviderTypes = HIVETestView.selectionGrid(_selProviderTypes, _providerTypes, _providerTypes.Length, 4);
		GUILayout.Space (16);


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.showSignIn", btnMinHeight, btnMaxWidth)) {
			
			AuthV4.showSignIn(onAuthV4SignIn);
		}
		if (HIVETestView.createButton ("AuthV4.isAutoSignIn", btnMinHeight, btnMaxWidth)) {
			
			Boolean isAutoSignIn = AuthV4.isAutoSignIn();

			Debug.Log("AuthV4TestView.isAutoSignIn() \nisAutoSignIn = " + isAutoSignIn + "\n");
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.signIn", btnMinHeight, btnMaxWidth)) {

			AuthV4.ProviderType providerType = getProviderType();

			AuthV4.signIn (providerType, onAuthV4SignIn);
		}
		if (HIVETestView.createButton ("AuthV4.signOut", btnMinHeight, btnMaxWidth)) {
			AuthV4.signOut (onAuthV4SignOut);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.connect", btnMinHeight, btnMaxWidth)) {
			
			AuthV4.ProviderType providerType = getProviderType();

			AuthV4.connect (providerType, onAuthV4Connect);
		}
		if (HIVETestView.createButton ("AuthV4.disconnect", btnMinHeight, btnMaxWidth)) {
				
			AuthV4.ProviderType providerType = getProviderType();

			AuthV4.disconnect (providerType, onAuthV4Disconnect);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.getProviderFriendsList", btnMinHeight, btnMaxWidth)) {
			
			AuthV4.ProviderType providerType = getProviderType();

			AuthV4.getProviderFriendsList (providerType, onGetProviderFriendsList);
		}
		if (HIVETestView.createButton ("AuthV4.getPlayerInfo", btnMinHeight, btnMaxWidth)) {

			AuthV4.PlayerInfo playerInfo = AuthV4.getPlayerInfo();

			if (playerInfo != null)
				Debug.Log("AuthV4TestView.getPlayerInfo() \nplayerInfo = " + playerInfo.toString() + "\n");
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);
		

		if (isConflict == true) {
			GUILayout.BeginHorizontal();
			GUILayout.Box("selectedPlayerId : ");
			_selectedPlayerId = HIVETestView.createTextField (_selectedPlayerId);
			
			if (HIVETestView.createButton ("AuthV4.selectConflict")) {
				
				Int64 selectedPlayerId = Convert.ToInt64(_selectedPlayerId);

				AuthV4.selectConflict(selectedPlayerId, onAuthV4SignIn);
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);
		}


		GUILayout.BeginHorizontal();
		GUILayout.Box("playerIdList : ");
		_playerIdList = HIVETestView.createTextField (_playerIdList);
		if (HIVETestView.createButton ("AuthV4.getProfile")) {
			
			char[] delimiterChars = { ' ', ',', '.', ':', '\t' };
			List<Int64> playerIdList = new List<Int64>();

			foreach (String playerId in _playerIdList.Split(delimiterChars)) {
				
				Int64 playerIdInt64 = 0;
				if (String.IsNullOrEmpty(playerId) == false)
					playerIdInt64 = Convert.ToInt64(playerId);

				playerIdList.Add(Convert.ToInt64(playerIdInt64));
			}

			AuthV4.getProfile(playerIdList, onAuthV4GetProfile);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		if (isConflict == true) {
			GUILayout.BeginHorizontal();

			GUILayout.Box("playerId : ");
			_playerInfo.playerId = Convert.ToInt64(HIVETestView.createTextField (_playerInfo.playerId.ToString()));
			GUILayout.Box("conflictPlayerId : ");
			_conflictPlayer.playerId = Convert.ToInt64(HIVETestView.createTextField (_conflictPlayer.playerId.ToString()));

			if (HIVETestView.createButton ("AuthV4.showConflictSelection")) {
				
				JSONObject currentPlayerData = null;

				if (_playerInfo.playerId > 0) {
					currentPlayerData = new JSONObject();
					currentPlayerData.AddField("player_id", _playerInfo.playerId);

					JSONObject currendVidGameData = new JSONObject();
					currendVidGameData.AddField("playerId", _playerInfo.playerId);
					currendVidGameData.AddField("Name", "currentName");
					currendVidGameData.AddField("Level", 10);

					currentPlayerData.AddField("game_data", currendVidGameData);
				}

				JSONObject conflictPlayerData = null;

				if (_conflictPlayer.playerId > 0) {
					conflictPlayerData = new JSONObject();
					conflictPlayerData.AddField("player_id", _conflictPlayer.playerId);

					JSONObject usedVidGameData = new JSONObject();
					usedVidGameData.AddField("playerId", _conflictPlayer.playerId);
					usedVidGameData.AddField("Name", "usedName");
					usedVidGameData.AddField("Level", 11);

					conflictPlayerData.AddField("game_data", usedVidGameData);
				}

				AuthV4.showConflictSelection(currentPlayerData, conflictPlayerData, onAuthV4SignIn);
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);
		}


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.resolveConflict")) {
			
			AuthV4.resolveConflict(onAuthV4ResolveConflict);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		GUILayout.Box("playerId : ");
		_playerId = HIVETestView.createTextField (_playerId);
		if (HIVETestView.createButton ("AuthV4.showProfile")) {
			
			Int64 playerId = 0;
			if (String.IsNullOrEmpty(_playerId) == false)
				playerId = Convert.ToInt64(_playerId);
			else
				playerId = AuthV4.getPlayerInfo().playerId;

			AuthV4.showProfile(playerId, onAuthV4ShowProfile);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.showInquiry")) {
			
			AuthV4.showInquiry(onAuthV4ShowInquiry);
		}
		if (HIVETestView.createButton ("AuthV4.showMyInquiry")) {
			
			AuthV4.showMyInquiry(onAuthV4ShowMyInquiry);
		}

		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.showChatbotInquiry")) {

			String chatbotJsonString = "{\"evt_code\":1000,\"p1\":\"random\"}";	// 챗봇 페이지 바로가기를 위해 약속된 JSON 형식의 String 데이터
			AuthV4.showChatbotInquiry(chatbotJsonString, onAuthV4ShowChatbotInquiry);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.showTerms")) {
			
			AuthV4.showTerms(onAuthV4ShowTerms);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.showAdultConfirm")) {
			
			AuthV4.showAdultConfirm(onAuthV4AdultConfirm);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.checkProvider")) {
			
			AuthV4.ProviderType providerType = getProviderType();

			AuthV4.checkProvider (providerType, onDeviceProviderInfo);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		GUILayout.Box("isShow : ");
		_selIsShow = HIVETestView.selectionGrid(_selIsShow, _isShows, _isShows.Length);
		if (HIVETestView.createButton ("AuthV4.checkMaintenance")) {

			Boolean isShow = (_selIsShow == 0);

			AuthV4.checkMaintenance(isShow, onAuthV4Maintenance);
		}
		if (HIVETestView.createButton ("AuthV4.checkBlacklist")) {
			
			Boolean isShow = (_selIsShow == 0);

			AuthV4.checkBlacklist(isShow, onAuthV4CheckBlacklist);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		#if UNITY_IOS
		if (HIVETestView.createButton("AuthV4.showGameCenterLoginCancelDialog")) {
			AuthV4.showGameCenterLoginCancelDialog(onGameCenterCancel);
		}
		#endif


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.setProviderChangedListener")) {
			
			Debug.Log("AuthV4TestView.setProviderChangedListener() registered\n");
			AuthV4.setProviderChangedListener (onDeviceProviderInfo);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		GUILayout.Box("isReady : ");
		_selIsReady = HIVETestView.selectionGrid(_selIsReady, _isReady, _isReady.Length);
		if (HIVETestView.createButton ("AuthV4.setEngagementReady")) {
			
			Boolean isReady = (_selIsReady == 0);
			ResultAPI result = Promotion.setEngagementReady(isReady);

			Debug.Log("AuthV4TestView.setEngagementReady()\nresult = " + result.toString() + "\n");
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.getAgeGateU13")) {
			
			Boolean ageGateU13 = AuthV4.getAgeGateU13();

			Debug.Log("AuthV4TestView.getAgeGateU13()\nresult = " + (ageGateU13 ? "YES" : "NO") + "\n");
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.showDeviceManagement")) {
			
			Debug.Log("AuthV4TestView.showDeviceManagement()\n");
			AuthV4.showDeviceManagement (onAuthV4ShowDeviceManagement);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("AuthV4.reset")) {
			
			AuthV4.reset();
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.EndVertical();

		GUI.EndScrollView();
	}
}