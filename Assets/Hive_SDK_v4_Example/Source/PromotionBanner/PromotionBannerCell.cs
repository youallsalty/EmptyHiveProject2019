﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Networking;

using hive;

public class PromotionBannerCell : MonoBehaviour
{
    //http://image-glb.qpyou.cn/hivepromotion/banner/1907/9838_1563932370_jaa.jpg
    // Start is called before the first frame update
    private GameObject imageObj;
    private GameObject textObj;
    private long pid = 0;
    private string imageUrl;
    private string displayStartDate;
    private string displayEndDate;
    private string interworkData;
    private SDKtype sdkType = SDKtype.v4;

    public void initWithData(SDKtype sdkType, PromotionBannerInfoSample bannerInfo) {
        this.sdkType = sdkType;
        pid = bannerInfo.pid;
        imageUrl = bannerInfo.imageUrl;
        displayStartDate = bannerInfo.displayStartDate;
        displayEndDate = bannerInfo.displayEndDate;
        interworkData = bannerInfo.interworkData;
    }

    void Start()
    {
        //이미지
        imageObj = transform.Find("Image").gameObject;
        if (imageObj == null) Debug.Log("Gameobject image null");
        else {
            #if UNITY_2017_1_OR_NEWER
                StartCoroutine(setImageFromUrl(imageUrl));
            #else
                StartCoroutine(setImageFromUrlOldVersion(imageUrl));
            #endif
        }

        //텍스트
        textObj = transform.Find("Text").gameObject;
        if (textObj == null) Debug.Log("GameObject text null");
        else {
            string info = "pid : " + pid + "\n\n" + "startDate : " + displayStartDate + "\n\n" + "endDate : " + displayEndDate;
            textObj.GetComponent<Text>().text = info;
        }

        this.GetComponent<Button>().onClick.AddListener(onClickCell);
    }

    // Update is called once per frame
    void Update()
    {

    }
#if UNITY_2017_1_OR_NEWER
    IEnumerator setImageFromUrl(string url)
    {
        Debug.Log("[PromotionBannerCell] setImageFromUrl url(string) =" + url);
         
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError) {
            Debug.Log("reqiest.error = " + request.error);
        }
        else {
            Texture2D texture2D = ((DownloadHandlerTexture) request.downloadHandler).texture;
            Rect rect = new Rect(0, 0, texture2D.width, texture2D.height);
            imageObj.GetComponent<Image>().sprite = Sprite.Create(texture2D, rect, new Vector2(0.5f, 0.5f));
        }
    }
#else
    IEnumerator setImageFromUrlOldVersion(string url)
    {
        WWW www = new WWW(url);
        
        // Wait for download to complete
        yield return www;

        // assign texture
        Renderer renderer = GetComponent<Renderer>();
        Texture2D texture2D = www.texture;
        Rect rect = new Rect(0, 0, texture2D.width, texture2D.height);
        imageObj.GetComponent<Image>().sprite = Sprite.Create(texture2D, rect, new Vector2(0.5f, 0.5f));

    }
 #endif
    void onClickCell()
    {
        if (!string.IsNullOrEmpty(interworkData)) {
            // 특정 게임의 Scene으로 이동
            Debug.Log("intworkData exist. will not show CustomWebView.");
            return;
        }

        switch(this.sdkType) {
            case SDKtype.v1 :
                // v1 샘플앱에서 호출시
                if (PromotionBannerView.isUseHivePromotion) {
                    // HivePromotion
                    // TODO : v4 Unity 샘플앱 배포시에 ActionCall을 참조할 수 없으므로, Delegate(Listener) 호출 방식으로 변경 필요.
                    // ActionCall.hivePromotionShowActInPromotionBannerCell(pid.ToString());
                } else {
                    // Mercury
                    // ActionCall.promotionShowActInPromotionBannerCell(pid.ToString());
                    /*
                    * 현재 Mercury에서 getBannerInfo API를 통해 전달받은 pid 값을 활용하여 추가로 노출 시킬 수 있는 UI 이벤트가 없음. 기획 논의 필요.
                    */
                }
                break;
            case SDKtype.v4 :
                // v4 샘플앱에서 호출시 
                Promotion.showCustomContents(PromotionCustomType.DIRECT, pid.ToString(), (ResultAPI result, PromotionEventType type)=>{
                    Debug.Log("[Promotion.showCustomContents] pid = " + pid.ToString() + '\n' +  result.toString() + '\n' + type.ToString());
                });
                break;
            default:
                break;
        }
    }
}
