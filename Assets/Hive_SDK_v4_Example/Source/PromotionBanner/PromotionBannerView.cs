﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Text;

// using hive;

public enum SDKtype: int {
    v0 = 0,     // 개별모듈
    v1 = 1,     // v1
    v4 = 4      // v4
} 

public class PromotionBannerInfoSample
{
    public long pid = 0;            ///< \~korean 프로모션 ID \~english Promotion ID
    public string imageUrl;         ///< \~korean 이미지 URL \~english Image Url
    public string linkUrl;          ///< \~korean 배너 클릭 시 이동 URL \~english Banner click Url
    public string displayStartDate; ///< \~korean "2016-11-01 10:00:00" \~english "2016-11-01 10:00:00"
    public string displayEndDate;   ///< \~korean "2016-11-31 10:00:00" \~english "2016-11-31 10:00:00"
    public long utcStartDate = 0;   ///< \~korean 프로모션 시작 시간 (Unixtimestamp) \~english Promotion start time (Unixtimestamp)
    public long utcEndDate = 0;     ///< \~korean 프로모션 종료 시간 (Unixtimestamp) \~english Promotion end time (Unixtimestamp)
    public string typeLink;         ///< \~korean "webview", "webbrowser", "market", "notice", "text", "none" \~english "webview", "webbrowser", "market", "notice", "text", "none"
    public string typeBanner;       ///< \~korean "great", "small", "rolling" \~english "great", "small", "rolling"
    public string interworkData;	///< \~korean 롤링배너 클릭 시 게임의 특정위치로 이동하기 위해 게임에서 입력한 인터워크 정보"
    public string toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("PromotionBannerInfoSample = {");
        sb.Append("pid = " + pid + '\n');
        sb.Append("imageUrl = " + imageUrl + '\n');
        sb.Append("linkUrl = " + linkUrl + '\n');
        sb.Append("displayStartDate = " + displayStartDate + '\n');
        sb.Append("displayEndDate = " + displayEndDate + '\n');
        sb.Append("utcStartDate = " + utcStartDate + '\n');
        sb.Append("utcEndDate = " + utcEndDate + '\n');
        sb.Append("typeLink = " + typeLink + '\n');
        sb.Append("typeBanner = " + typeBanner + '\n');
        sb.Append("interworkData = " + interworkData + '\n');
        sb.Append(" }\n");

        return sb.ToString();
    }

}

public class PromotionBannerView : MonoBehaviour
{
    private List<PromotionBannerInfoSample> bannerInfoList = new List<PromotionBannerInfoSample>();
    private GameObject content;
    private GameObject buttonOK;
    private float promotionBannerCellPosY = 0;

    public delegate void CompleteHandler(int result, string logStr);
    private CompleteHandler viewHandler;
    private SDKtype sdkType = SDKtype.v4;
    public static bool isUseHivePromotion = true;

    public void initWithData(SDKtype sdkType, List<PromotionBannerInfoSample> bannerInfos, CompleteHandler handler) {
        Debug.Log("[PromotionBannerView] initWithData");
        bannerInfoList = bannerInfos;
        viewHandler = handler;
        this.sdkType = sdkType;
    }

    // Start is called before the first frame update.
    void Start()
    {

        content = GameObject.Find("ScrollView/Viewport/Content");
        if (content == null) Debug.Log("[PromotionBannerView] Gameobject 'Content' null");

        buttonOK = GameObject.Find("ButtonOK");
        if (buttonOK == null) Debug.Log("[PromotionBannerView] Gameobjet 'ButtonOK' null");
        
        //Debug.Log("[PromotionBannerView] bannerInfoList.Count = " + bannerInfoList.Count);

        foreach(PromotionBannerInfoSample bannerInfo in bannerInfoList) {

            GameObject cell = Instantiate(Resources.Load("Prefabs/PromotionBannerCell"), new Vector3(content.transform.position.x, content.transform.position.y - 5  - promotionBannerCellPosY, 1), Quaternion.identity) as GameObject;
            cell.GetComponent<PromotionBannerCell>().initWithData(sdkType, bannerInfo);

            cell.transform.SetParent(content.transform);
            //배치할 위치 계산
            promotionBannerCellPosY += cell.GetComponent<RectTransform>().rect.height;
        }
        //content View 사이즈 계산
        content.GetComponent<RectTransform>().sizeDelta = new Vector2(640, promotionBannerCellPosY);

        //button 이벤트
        buttonOK.GetComponent<Button>().onClick.AddListener(onCickButtonOK);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void onCickButtonOK()
    {
        Debug.Log("[PromotionBannerView] OK Button clicked");
        Destroy(gameObject);

        if (viewHandler != null) {
            viewHandler(1, "destroy from ok button clicked");
        }
    }
}
