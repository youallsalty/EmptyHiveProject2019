using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
// using hive.AuthV4;

using hive;		// HIVE SDK 네임스페이스 사용

public class SocialV4TestView
{

    public static SocialV4TestView instance = new SocialV4TestView();

    public void Start() { }

    public void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            HIVETestManager.instance.switchView(TestViewType.SOCIALV4_VIEW);
        }
    }

	public void onShowCommunity(ResultAPI result) {

		Debug.Log("SocialV4TestView.onShowCommunity() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}

	public void onSharePhoto(ResultAPI result) {

		Debug.Log("SocialV4TestView.onSharePhoto() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess() == false)
			return;
	}


    public void OnGUI()
    {

        HIVETestView.layout(24);

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        if (HIVETestView.createButton("- SocialV4TestView -"))
        {
            HIVETestManager.instance.switchView(TestViewType.MAIN_VIEW);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);


		GUILayout.BeginHorizontal();
        if (HIVETestView.createButton("SocialV4.showCommunity()"))
        {
			SocialV4.ProviderType providerType = SocialV4.ProviderType.HIVE;
            SocialV4.showCommunity(providerType, onShowCommunity);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);


		GUILayout.BeginHorizontal();
        if (HIVETestView.createButton("SocialV4.sharePhoto()"))
        {
			SocialV4.ProviderType providerType = SocialV4.ProviderType.FACEBOOK;
            SocialV4.sharePhoto(providerType, onSharePhoto);
        }
        GUILayout.EndHorizontal();
        GUILayout.Space (8);

        GUILayout.EndVertical();
    }
}