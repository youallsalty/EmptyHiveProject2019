using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


using hive;			// HIVE SDK 네임스페이스 사용


/**
 * 앱과 사용자를 트래킹하고 분석하기 위한 기능 테스트 화면
 * (User Tracking Tool Wrapper & Callect Game Log API)
 * 
 * @author ryuvsken
 */
public class AnalyticsTestView {

	public static AnalyticsTestView instance = new AnalyticsTestView ();


	public void Start() {
	}

    float deltaTime = 0;
    float fpsDeltaTime = 0.0f;
    bool isSendingLog = false;

    uint remain = 0;

	public void Update() {

		if (Input.GetKeyDown(KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}

        remain = Analytics.getRemainAnalyticsLogCount();

		fpsDeltaTime += (Time.unscaledDeltaTime - fpsDeltaTime) * 0.1f;
		deltaTime+=Time.deltaTime;
        if(deltaTime > 0.1 && isSendingLog) {
            sendLog();
            deltaTime = 0;
        }
	}


	// Mobile App Tracking 을 위한 미리 정의된 이벤트 타입 (Install, Undate, Login, Purchase, TutorialComplete)
	private String[] _eventNames = new String[] { "Install", "Update", "Login", "TutorialComplete" };
	private int _selEventNames = 0;

	// Hive Analytics Log Event 타입
	private String[] _analyticsEvents = new String[] { "Account_Join", "Account_Login", "초당 120개 전송" };
	private int _selAnalyticsEvents = 0;
	private String _time = "1";
	private String _sendLimit = "100";
	private String _queueLimit = "500";

	private String _logCreate = "10";
	private int logCreate = 10;

	public void OnGUI() {
		
		HIVETestView.layout(24);


        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- Analytics -")) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Event Name : ");
		_selEventNames = HIVETestView.selectionGrid(_selEventNames, _eventNames, _eventNames.Length);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Analytics.sendEvent(" + _eventNames[_selEventNames] + ")")) {

			Debug.Log("\n\n=== 서드파티 이벤트 전송 테스트 ===\n");
			Debug.Log("Analytics.sendEvent(" + _eventNames[_selEventNames] + ") Called\n");

			Analytics.sendEvent (_eventNames[_selEventNames]);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		GUILayout.Box("Analytics Log : ");
		_selAnalyticsEvents = HIVETestView.selectionGrid(_selAnalyticsEvents, _analyticsEvents, _analyticsEvents.Length);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Log Amount : ");
		_logCreate = HIVETestView.createTextField (_logCreate);
		GUILayout.Box("Cycle(sec) : ");
		_time = HIVETestView.createTextField (_time);
		GUILayout.Box("Send limit : ");
		_sendLimit = HIVETestView.createTextField (_sendLimit);
		GUILayout.Box("Queue limit : ");
		_queueLimit = HIVETestView.createTextField (_queueLimit);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Analytics.sendAnalyticsLog(" + _analyticsEvents[_selAnalyticsEvents] + ")")) {

			Debug.Log("\n\n=== Analytics Log 전송 테스트 ===\n");
			switch(_selAnalyticsEvents)  {
				case 0: {
					JSONObject logData = new JSONObject ();
					logData.AddField ("logId", 1);			// 계정
					logData.AddField ("logSubId", 1);		// 가입
					logData.AddField ("userId", "user001");
					logData.AddField ("nickname", "nickname001");
					Analytics.sendAnalyticsLog (logData);
				}
				break;
				case 1: {
					JSONObject logData = new JSONObject();
					logData.AddField ("logId", "1");		// 계정
					logData.AddField ("logSubId", "2");		// 로그인
					logData.AddField ("userId", "user001");
					logData.AddField ("currentGold", 1000);
					Analytics.sendAnalyticsLog(logData);
				}
				break;
				case 2:
					if(!isSendingLog) {
                        isSendingLog = true;
                        
                        float time = Convert.ToSingle(_time);
						Configuration.setAnalyticsSendCycleSeconds(time);
                        UInt32 sendLimit = Convert.ToUInt32(_sendLimit);
						Configuration.setAnalyticsSendLimit(sendLimit);
                        UInt32 queueLimit = Convert.ToUInt32(_queueLimit);
						Configuration.setAnalyticsQueueLimit(queueLimit);
						logCreate = Convert.ToInt32(_logCreate);
                        
                    } else {
                        isSendingLog = false;
                    }
				break;
			}
		}
        GUILayout.EndVertical();

		float fps = 1.0f / fpsDeltaTime;
        GUILayout.BeginHorizontal();
        GUILayout.Label("Sending Log : " + remain + "/" + Configuration.getAnalyticsQueueLimit() + "fps : " + fps );
        GUILayout.EndVertical();

    }

	void sendLog(){
//		for(int i=0;i<6;++i) {		// 27~30 fps
//		for(int i=0;i<12;++i) { // 23~29fps (ios 30fps)
//		for(int i=0;i<24;++i) {	// 19~21 fps (ios 22~27fps)
//		for(int i=0;i<48;++i) {	//6~8fps (ios 10~15fps)
		for(int i=0;i<logCreate;++i) {
            log_hivesdk_asset_var_log(7,0 ,0 ,80 ,105 ,@"" ,-1 ,8 ,7 ,163865 ,"명예의전장" ,"전투력" ,0 ,57753907 ,0 ,@"" ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0);
        }
		
	}

	/** 
     * 재화 변동 로그 v2
     *
     * @param amountCurr amount_curr
     * @param characterTypeId character_type_id
     * @param characterId character_id
     * @param accountLevel account_level
     * @param assetId asset_id
     * @param itemName item_name
     * @param amountVar amount_var
     * @param amountPrev amount_prev
     * @param actionId action_id
     * @param accountId account_id
     * @param actionName action_name
     * @param assetName asset_name
     * @param itemId item_id
     * @param channelUid channel_uid
     * @param characterLevel character_level
     * @param extraLogBoby 추가로 저장할 row 를 같이 담은 컬럼 (매출로그+게임서버로그 등에 활용)
     * @param amount_free_prev amount_free_prev
     * @param amount_free_var amount_free_var
     * @param amount_free_curr amount_free_curr
     * @param amount_paid_prev amount_paid_prev
     * @param amount_paid_var amount_paid_var
     * @param amount_paid_curr amount_paid_curr
     * @param account_id account_id
     * @param deviceid deviceid
     * @param isEmulator 에뮬레이터 판단 여부
     */
    public static void log_hivesdk_asset_var_log( 
        int amountCurr, 
        int characterTypeId, 
        int characterId, 
        int accountLevel, 
        int assetId, 
        String itemName, 
        int amountVar, 
        int amountPrev, 
        int actionId, 
        int accountId, 
        String actionName, 
        String assetName, 
        int itemId, 
        int channelUid, 
        int characterLevel, 
        String extraLogBoby, 
        int amount_free_prev, 
        int amount_free_var, 
        int amount_free_curr, 
        int amount_paid_prev, 
        int amount_paid_var, 
        int amount_paid_curr, 
        int account_id, 
        int deviceid, 
        int isEmulator
        )
    {
        
        JSONObject logData = new JSONObject ();
        logData.AddField("category", "hivesdk_asset_var_log");
        logData.AddField("amountCurr", amountCurr );
        logData.AddField("characterTypeId", characterTypeId );
        logData.AddField("characterId", characterId );
        logData.AddField("accountLevel", accountLevel );
        logData.AddField("assetId", assetId );
        logData.AddField("itemName", itemName );
        logData.AddField("amountVar", amountVar );
        logData.AddField("amountPrev", amountPrev );
        logData.AddField("actionId", actionId );
        logData.AddField("accountId", accountId );
        logData.AddField("actionName", actionName );
        logData.AddField("assetName", assetName );
        logData.AddField("itemId", itemId );
        logData.AddField("channelUid", channelUid );
        logData.AddField("characterLevel", characterLevel );
        logData.AddField("extraLogBoby", extraLogBoby );
        logData.AddField("amount_free_prev", amount_free_prev );
        logData.AddField("amount_free_var", amount_free_var );
        logData.AddField("amount_free_curr", amount_free_curr );
        logData.AddField("amount_paid_prev", amount_paid_prev );
        logData.AddField("amount_paid_var", amount_paid_var );
        logData.AddField("amount_paid_curr", amount_paid_curr );
        logData.AddField("account_id", account_id );
        logData.AddField("deviceid", deviceid );
        logData.AddField("isEmulator", isEmulator );

        Analytics.sendAnalyticsLog(logData);
    }

}



