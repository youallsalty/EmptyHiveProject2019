using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


using hive;			// HIVE SDK 네임스페이스 사용


/**
 * 푸시 토큰 취합, 푸시 활성/비활성 (공지/야간 푸시 on/off), 공지 푸시, 인게임 푸시 테스트 화면
 * (Mobile Push Notification API)
 * 
 * @author ryuvsken
 */
public class PushTestView {
	

	public static PushTestView instance = new PushTestView ();
	private int contentCount = 28;
	private Vector2 scrollPosition = Vector2.zero;
	
	// 로컬 푸시 스트레스 테스트. 등록 가능한 최대 개수
	private int stressLocalPushCount = 64;
	// 일정 시간 간격으로 스트레스 로컬 푸시를 수신 받도록 설정(5초)
	private int stressLocalPushTimeInterval = 5;
	private List<int> stressLocalPushNoticeIDs = new List<int>();
	// 현재 시간을 밀리세컨드 단위까지 표시
	private string currentTime = "";


	public void Start() {

		this.remotePush.isAgreeNotice = true;
		this.remotePush.isAgreeNight = false;

		this.localPush.title = "Local Push Title";
		this.localPush.msg = "Local Push Message";
		this.localPush.bigmsg = "Local Push Big Message";
		this.localPush.noticeId = 0;
		this.localPush.after = 5;
		this.localPush.buckettype = 0;
		this.localPush.bucketsize = 1;

		pushSetting.useForegroundLocalPush = true;
		pushSetting.useForegroundRemotePush = true;

		for (int i=1; i<=stressLocalPushCount; i++) {
			stressLocalPushNoticeIDs.Add(i);
		}
	}


	public void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}

		currentTime = DateTime.Now.ToString("yyyy, MM dd, HH:mm:ss.fff");
	}

	RemotePush remotePush = new RemotePush();
	LocalPush localPush = new LocalPush();
	PushSetting pushSetting = new PushSetting();
	
	// Local Push Bucket type(Overwrite, Inboxing, BigText)
	private String[] _bucketTypes = new String[] {"Overwrite", "Inboxing", "BigText"};
	
#if !UNITY_EDITOR && UNITY_ANDROID
	String icon_color_r = "0";
	String icon_color_g = "0";
	String icon_color_b = "0";
#endif

	// Local Push Notification 결과 통지 리스너 (Push.registerLocalPush())
	public void onLocalPushCB(ResultAPI result, LocalPush localPush) {

		if (localPush != null) {
			this.localPush.active = localPush.active; 
		}
		
		Debug.Log ("PushTestView.onLocalPushCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("localPush = " + localPush!=null?localPush.toString():"null" + "\n");
	}


	// Remote Push Notification 결과 통지 리스너 (Push.getPushToken(), Push.setPushToken())
	public void onRemotePushCB(ResultAPI result, RemotePush remotePush) {
		
		if( remotePush != null ) {
			this.remotePush.isAgreeNotice = remotePush.isAgreeNotice;
			this.remotePush.isAgreeNight = remotePush.isAgreeNight;
		} 

		Debug.Log ("PushTestView.onRemotePushCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("remotePush = " + remotePush!=null?remotePush.toString():"null" + "\n");
	}

	public void onPushSettingCB(ResultAPI result, PushSetting setting) {
		if (setting != null) {
			this.pushSetting.useForegroundLocalPush = setting.useForegroundLocalPush;
			this.pushSetting.useForegroundRemotePush = setting.useForegroundRemotePush;
		}


		Debug.Log ("PushTestView.onPushSettingCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("setting = " + setting!=null?setting.toString():"null" + "\n");

	}

	public void OnGUI() {
		
		HIVETestView.layout(contentCount);

		HIVETestManager testViewManager = HIVETestManager.instance;

		if (testViewManager._isShow == false) {
			return;
		}
		
		float scrollViewHeight = (testViewManager .buttonHeight + 20) * contentCount;
		scrollPosition = GUI.BeginScrollView(new Rect(0, 0, Screen.width, Screen.height), scrollPosition, new Rect(0, 0, Screen.width, scrollViewHeight));
        GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.05f;
        GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.05f;

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- Push Notification -")) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box ("- Request Push Token -");
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Push.requestPushPermission()")) {
			Debug.Log("\n\n=== Request Push Permission===\n");
			Push.requestPushPermission();
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		GUILayout.Box ("- Remote Push -");
        GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// 리모트 푸시 정보 조회
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Push.getRemotePush()")) {

			Debug.Log("\n\n=== 리모트 푸시 정보 조회 ===\n");
			Debug.Log("Push.getRemotePush(onRemotePush) called\n");

			Push.getRemotePush (onRemotePushCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// 리모트 푸시 정보 설정
		GUILayout.BeginHorizontal();
		GUILayout.Box("Notice : ");
		if (remotePush.isAgreeNotice == true) {
			if ( HIVETestView.createButton("ON") ) {
				remotePush.isAgreeNotice = false;
			}
		}
		else {
			if ( HIVETestView.createButton("OFF") ) {
				remotePush.isAgreeNotice = true;
			}
		}
		
		GUILayout.Box("Night : ");
		if (remotePush.isAgreeNight == true) {
			if ( HIVETestView.createButton("ON") ) {
				remotePush.isAgreeNight = false;
			}
		}
		else {
			if ( HIVETestView.createButton("OFF") ) {
				remotePush.isAgreeNight = true;
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// 리모트 푸시 정보 설정
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Push.setRemotePush(remotePush)")) {

			Debug.Log("\n\n=== 리모트 푸시 정보 설정 ===\n");
			Debug.Log("Push.setRemotePush(remotePush, onRemotePush) called\nremotePush = " + remotePush.toString() + "\n");

			Push.setRemotePush (remotePush, onRemotePushCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (24);


		GUILayout.BeginHorizontal();
		GUILayout.Box ("- Local Push -");
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box ("Title : ");
		this.localPush.title = HIVETestView.createTextField(this.localPush.title);
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box ("Message : ");
		this.localPush.msg = HIVETestView.createTextField(this.localPush.msg);
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box ("Big Message : ");
		this.localPush.bigmsg = HIVETestView.createTextField(this.localPush.bigmsg);
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Bucket Type : ");
		this.localPush.buckettype = HIVETestView.selectionGrid(this.localPush.buckettype, _bucketTypes, _bucketTypes.Length);
		GUILayout.EndHorizontal();
		GUILayout.Space(8);


		GUILayout.BeginHorizontal();
		GUILayout.Box ("Bucket Size : ");
		String bucketSize = HIVETestView.createTextField(this.localPush.bucketsize.ToString());
		this.localPush.bucketsize = Convert.ToInt32(bucketSize);
        
		GUILayout.Box ("Push NO : ");
		String pushId = HIVETestView.createTextField(this.localPush.noticeId.ToString());
		this.localPush.noticeId = Convert.ToInt32(pushId);

		GUILayout.Box ("After : ");
		String after = HIVETestView.createTextField(this.localPush.after.ToString());
		this.localPush.after = Convert.ToInt64(after);
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

#if !UNITY_EDITOR && UNITY_ANDROID
		GUILayout.BeginHorizontal();
		GUILayout.Box ("icon color");
        GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Box ("R : ");
		icon_color_r  = HIVETestView.createTextField(icon_color_r);
		GUILayout.Box ("G : ");
		icon_color_g  = HIVETestView.createTextField(icon_color_g);
		GUILayout.Box ("B : ");
		icon_color_b  = HIVETestView.createTextField(icon_color_b);
        GUILayout.EndHorizontal();
		GUILayout.Space (8);
#endif


		// 로컬 푸시 등록
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Push.registerLocalPush(localPush, listener)")) {



			Debug.Log("\n\n=== 로컬 푸시 등록 ===\n");
			Debug.Log("Push.registerLocalPush(localPush, onLocalPush) called\nlocalPush = "
				+ localPush!=null?localPush.toString():"null" + "\n");

#if !UNITY_EDITOR && UNITY_ANDROID

			int r = 0;
			int g = 0;
			int b = 0;

			if (String.IsNullOrEmpty(icon_color_r) == false)
				r = Convert.ToInt32(icon_color_r);
						
			if (String.IsNullOrEmpty(icon_color_g) == false)
				g = Convert.ToInt32(icon_color_g);

			if (String.IsNullOrEmpty(icon_color_b) == false)
				b = Convert.ToInt32(icon_color_b);

			JSONObject jsonIconColor = new JSONObject();

			jsonIconColor.AddField ("r", r);
			jsonIconColor.AddField ("g", g);
			jsonIconColor.AddField ("b", b);

			this.localPush.icon_color = jsonIconColor.ToString();
#endif

			Push.registerLocalPush (this.localPush, onLocalPushCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// 로컬 푸시 해제
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Push.unregisterLocalPush(noticeId)")) {

			int noticeId = 1;
			noticeId = this.localPush.noticeId;

			Debug.Log("\n\n=== 로컬 푸시 해제 ===\n");
			Debug.Log(String.Format("Push.unregisterLocalPush(noticeID = {0}) called\n", noticeId));

			Push.unregisterLocalPush (noticeId);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		// 로컬 푸시 배열 해제
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Push.unregisterLocalPushes(noticeIds)")) {

			int noticeId = 1;
			noticeId = this.localPush.noticeId;
			List<int> noticeIds = new List<int>();
			noticeIds.Add(noticeId);

			Debug.Log("\n\n=== 로컬 푸시 배열 해제 ===\n");
			Debug.Log(String.Format("Push.unregisterLocalPushes(noticeIds = [{0}]) called\n", noticeId));

			Push.unregisterLocalPushes (noticeIds);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		// 앱 활성화시 푸쉬 수신 여부 설정
		GUILayout.Space (24);

		// 대량 로컬 푸시 등록, 해제 테스트 영역
		GUILayout.BeginHorizontal();
		GUILayout.Box (String.Format("- Local Push stress Test ({0}) -", currentTime));
		
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		// 대량 로컬 푸시 등록
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Push.registerLocalPush(localPush, listener)")) {
			Debug.Log("\n\n=== 대량 로컬 푸시 등록 ===\n");

			for (int i=1; i<=stressLocalPushCount; i++) {
				LocalPush localPush = new LocalPush();
				localPush.title = "stress_title" + i;
				localPush.msg = "stress_message" + i;
				localPush.noticeId = i;
				// 5초 간격
				localPush.after = 5*i;

				Push.registerLocalPush(localPush, onLocalPushCB);
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.Space(8);

		// 대량 로컬 푸시 해제
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Push.unregisterLocalPush(noticeID)")) {
			Debug.Log("\n\n=== 대량 로컬 푸시 해제 ===\n");

			for (int i=1; i<=stressLocalPushCount; i++) {
				Push.unregisterLocalPush(i);
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.Space(8);

		// 대량 로컬 푸시 배열 해제
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Push.unregisterLocalPushes(noticeIDs)"))	{
			Debug.Log("\n\n=== 대량 로컬 푸시 배열 해제 ===\n");

			Push.unregisterLocalPushes(stressLocalPushNoticeIDs);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space(8);		

		// Hive 로컬푸시를 포함한 모든 로컬 푸시 해제
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Push.unregisterAllLocalPushes")) {
			Debug.Log("\n\n=== Hive 로컬푸시를 포함한 모든 로컬 푸시 해제 ===\n");

			Push.unregisterAllLocalPushes();
		}
		GUILayout.EndHorizontal();
		GUILayout.Space(8);	


		GUILayout.Space (24);


		GUILayout.BeginHorizontal();
		GUILayout.Box ("- Push Setting when activated -");
        GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		if(HIVETestView.createButton("getForegroundPush")) {

			Debug.Log("\n\n=== 푸시 활성화 정보 조회 ===\n");
			Debug.Log("Push.getForegroundPush(onPushSettingCB) called\n");

			Push.getForegroundPush (onPushSettingCB);

		}

		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Box("Local : ");
		if (pushSetting.useForegroundLocalPush == true) {
			if ( HIVETestView.createButton("ON") ) {
				pushSetting.useForegroundLocalPush = false;
			}
		}
		else {
			if ( HIVETestView.createButton("OFF") ) {
				pushSetting.useForegroundLocalPush = true;
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (2);
		GUILayout.BeginHorizontal();
		GUILayout.Box("Remote : ");
		if (pushSetting.useForegroundRemotePush == true) {
			if ( HIVETestView.createButton("ON") ) {
				pushSetting.useForegroundRemotePush = false;
			}
		}
		else {
			if ( HIVETestView.createButton("OFF") ) {
				pushSetting.useForegroundRemotePush = true;
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if(HIVETestView.createButton("setForegroundPush") ) {
			
			Debug.Log("\n\n=== 푸시 활성화 정보 설정 ===\n");
			Debug.Log("Push.setForegroundPush(pushSetting, onPushSettingCB) called\npushSetting = " + pushSetting.toString() + "\n");

			Push.setForegroundPush (pushSetting, onPushSettingCB);
		}

		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.EndVertical();

		GUI.EndScrollView();
    }

}



