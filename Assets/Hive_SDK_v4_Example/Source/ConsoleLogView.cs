/**
 * https://github.com/mminer/consolation
 * 
 * Unity's editor console is indispensible, but retrieving debug logs and warnings outside the editor is difficult.
 * To make it easier, this console displays output from the Debug class, as well as any errors and exceptions thrown, in the game itself.
 * This is especially useful on mobile devices.
 * 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 Matthew Miner
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * @author mminer
 * @modify ryuvsken
 */


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;


// Unity3D 영역에서 기록하는 로그를 보여주는 UI
public class ConsoleLogView : MonoBehaviour
{
	struct Log
	{
		public string message;
		public string stackTrace;
		public LogType type;
	}

	#region Inspector Settings

	/// <summary>
	/// The hotkey to show and hide the console window.
	/// </summary>
	public KeyCode toggleKey = KeyCode.Menu;

	/// <summary>
	/// Whether to open the window by shaking the device (mobile-only).
	/// </summary>
	public bool shakeToOpen = false;

	/// <summary>
	/// The (squared) acceleration above which the window should open.
	/// </summary>
	public float shakeAcceleration = 3f;

	public int numOfCircleToShow = 1;

	/// <summary>
	/// Whether to only keep a certain number of logs.
	///
	/// Setting this can be helpful if memory usage is a concern.
	/// </summary>
	public bool restrictLogCount = false;

	/// <summary>
	/// Number of logs to keep before removing old ones.
	/// </summary>
	public int maxLogs = 1000;

	#endregion

	readonly List<Log> logs = new List<Log>();
	Vector2 scrollPosition;
	bool visible;
	bool collapse;

	// Visual elements:

	static readonly Dictionary<LogType, Color> logTypeColors = new Dictionary<LogType, Color>
	{
		{ LogType.Assert, Color.white },
		{ LogType.Error, Color.red },
		{ LogType.Exception, Color.red },
		{ LogType.Log, Color.white },
		{ LogType.Warning, Color.yellow },
	};

	const string windowTitle = "Console";
	const int margin = 20;
	const int topMargin = 100;
	static readonly GUIContent clearLabel = new GUIContent("Clear", "Clear the contents of the console.");
	static readonly GUIContent collapseLabel = new GUIContent("Collapse", "Hide repeated messages.");
	static readonly GUIContent closeLabel = new GUIContent("Close", "Close to console.");
	static readonly GUIContent copyLabel = new GUIContent("COPY Log", "Copy Log");

	readonly Rect titleBarRect = new Rect(0, 0, 10000, 20);
	Rect windowRect = new Rect(margin, topMargin, Screen.width - (margin * 2), Screen.height - (margin + topMargin));


	void OnEnable ()
	{
		// Unity C# 스크립트의 로그를 찍을 때 StackTrace 를 꺼버린다.
		// Application.stackTraceLogType = StackTraceLogType.None;

		Application.logMessageReceived += HandleLog;

		// 스크린 회전 이벤트 받기
		ConsoleLogView.OnOrientationChange += MyOrientationChangeCode;
	}

	void OnDisable ()
	{
		Application.logMessageReceived -= HandleLog;

		ConsoleLogView.OnOrientationChange -= MyOrientationChangeCode;
	}


	// 스크린 회전 이벤트
	void MyOrientationChangeCode(DeviceOrientation orientation) {

		//Debug.Log ("MyOrientationChangeCode()");

		windowRect = new Rect(margin, topMargin, Screen.width - (margin * 2), Screen.height - (margin + topMargin));
	}


	void Update ()
	{
/*
		// 안드로이드에서 백 키로 로그 뷰를 활성화 하고 싶을 때
		if (Application.platform == RuntimePlatform.Android) {
			
			if (Input.GetKeyDown (toggleKey)) {
				
				visible = !visible;

				if (visible) {
					HIVETestManager.instance.switchView (TestViewType.HIDE_VIEW);
				}
				else {
					HIVETestManager.instance.switchView (TestViewType.SHOW_VIEW);
				}
			}

		}
*/

/*
		// 흔들기로 로그 뷰를 활성화 하고 싶을 때
		if (shakeToOpen && Input.acceleration.sqrMagnitude > shakeAcceleration) {
			visible = true;
		}
*/

		if (isGestureDone() || HIVETestManager.instance.isShowConsoleLogView ) {
			if (!visible) {
				// make scroll bottom when show the view
				ScrollToBottom ();
			}
			
			visible = true;

			HIVETestManager.instance.switchView (TestViewType.HIDE_VIEW);
		}

		if (visible) {
			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {

				// Get movement of the finger since last frame
				Touch touch = Input.touches[0];
				float dt = Time.deltaTime / touch.deltaTime;
				if (dt == 0 || float.IsNaN(dt) || float.IsInfinity(dt))
					dt = 1.0f;
				
				Vector2 gdt = touch.deltaPosition * dt;
				scrollPosition = new Vector2 (0, scrollPosition.y + gdt.y);
			}
		}
	}


	void OnGUI ()
	{
		if (!visible) {
			return;
		}

		HIVETestView.layout (30);

		windowRect = GUILayout.Window(123456, windowRect, DrawConsoleWindow, windowTitle);
	}

	/// <summary>
	/// Displays a window that lists the recorded logs.
	/// </summary>
	/// <param name="windowID">Window ID.</param>
	void DrawConsoleWindow (int windowID)
	{
		DrawLogsList();
		DrawToolbar();

		// Allow the window to be dragged by its title bar.
		GUI.DragWindow(titleBarRect);
	}

	/// <summary>
	/// Displays a scrollable list of logs.
	/// </summary>
	void DrawLogsList ()
	{
		GUI.skin.label.fontSize = 20;

		scrollPosition = GUILayout.BeginScrollView(scrollPosition);

		// Used to determine height of accumulated log labels.
		GUILayout.BeginVertical();

			// Iterate through the recorded logs.
			for (var i = 0; i < logs.Count; i++) {
				var log = logs[i];

				// Combine identical messages if collapse option is chosen.
				if (collapse && i > 0) {
					var previousMessage = logs[i - 1].message;

					if (log.message == previousMessage) {
						continue;
					}
				}

				GUI.contentColor = logTypeColors[log.type];
				HIVETestView.createLabel(log.message);
			}

		GUILayout.EndVertical();
		var innerScrollRect = GUILayoutUtility.GetLastRect();
		GUILayout.EndScrollView();
		var outerScrollRect = GUILayoutUtility.GetLastRect();

		// If we're scrolled to bottom now, guarantee that it continues to be in next cycle.
		if (Event.current.type == EventType.Repaint && IsScrolledToBottom(innerScrollRect, outerScrollRect)) {
			ScrollToBottom();
		}

		// Ensure GUI colour is reset before drawing other components.
		GUI.contentColor = Color.white;
	}

	/// <summary>
	/// Displays options for filtering and changing the logs list.
	/// </summary>
	void DrawToolbar ()
	{
		GUILayout.BeginHorizontal();

		if (HIVETestView.createButton("Clear")) {
			logs.Clear();
		}

		// collapse = GUIsLayout.Toggle(collapse, collapseLabel, GUILayout.ExpandWidth(false));

		if (HIVETestView.createButton("COPY")) {
			StringBuilder sb = new StringBuilder();
			// Iterate through the recorded logs.
			for (var i = 0; i < logs.Count; i++) {
				var log = logs[i];

				// Combine identical messages if collapse option is chosen.
				if (collapse && i > 0) {
					var previousMessage = logs[i - 1].message;

					if (log.message == previousMessage) {
						continue;
					}
				}

				sb.Append(log.message);
				// GUI.contentColor = logTypeColors[log.type];
				// GUILayout.Label(log.message);
				GUIUtility.systemCopyBuffer = sb.ToString();
			}
		}

		if (HIVETestView.createButton("Close")) {
			
			visible = false;

			HIVETestManager.instance.isShowConsoleLogView = false;
			// 로그뷰가 닫힐 때 ExternalTestView도 닫음 (상단 View로 이동)
			ExternalTestManager.instance.switchView(ExternalTestViewType.HIDE_VIEW);
			HIVETestManager.instance.switchView (TestViewType.SHOW_VIEW);
		}

		GUILayout.EndHorizontal();
	}


	List<Vector2> gestureDetector = new List<Vector2>();
	Vector2 gestureSum = Vector2.zero;
	float gestureLength = 0;
	int gestureCount = 0;
	bool isGestureDone()
	{
		if (Application.platform == RuntimePlatform.Android ||
			Application.platform == RuntimePlatform.IPhonePlayer) {
			if (Input.touches.Length != 1) {
				gestureDetector.Clear();
				gestureCount = 0;
			}
			else {
				if (Input.touches[0].phase == TouchPhase.Canceled || Input.touches[0].phase == TouchPhase.Ended)
					gestureDetector.Clear();
				else if (Input.touches[0].phase == TouchPhase.Moved) {
					Vector2 p = Input.touches[0].position;
					if (gestureDetector.Count == 0 || (p - gestureDetector[gestureDetector.Count - 1]).magnitude > 10)
						gestureDetector.Add(p);
				}
			}
		}
		else {
			if (Input.GetMouseButtonUp(0)) {
				gestureDetector.Clear();
				gestureCount = 0;
			}
			else {
				if (Input.GetMouseButton(0)) {
					Vector2 p = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
					if (gestureDetector.Count == 0 || (p - gestureDetector[gestureDetector.Count - 1]).magnitude > 10)
						gestureDetector.Add(p);
				}
			}
		}

		if (gestureDetector.Count < 10)
			return false;

		gestureSum = Vector2.zero;
		gestureLength = 0;
		Vector2 prevDelta = Vector2.zero;
		for (int i = 0; i < gestureDetector.Count - 2; i++) {

			Vector2 delta = gestureDetector[i + 1] - gestureDetector[i];
			float deltaLength = delta.magnitude;
			gestureSum += delta;
			gestureLength += deltaLength;

			float dot = Vector2.Dot(delta, prevDelta);
			if (dot < 0f) {
				gestureDetector.Clear();
				gestureCount = 0;
				return false;
			}

			prevDelta = delta;
		}

		int gestureBase = (Screen.width + Screen.height) / 4;

		if (gestureLength > gestureBase && gestureSum.magnitude < gestureBase / 2) {
			gestureDetector.Clear();
			gestureCount++;
			if (gestureCount >= numOfCircleToShow)
				return true;
		}

		return false;
	}

	/// <summary>
	/// Records a log from the log callback.
	/// </summary>
	/// <param name="message">Message.</param>
	/// <param name="stackTrace">Trace of where the message came from.</param>
	/// <param name="type">Type of message (error, exception, warning, assert).</param>
	void HandleLog (string message, string stackTrace, LogType type)
	{
		logs.Add(new Log {
			message = message,
			stackTrace = stackTrace,
			type = type,
		});

		TrimExcessLogs();
	}

	/// <summary>
	/// Determines whether the scroll view is scrolled to the bottom.
	/// </summary>
	/// <param name="innerScrollRect">Rect surrounding scroll view content.</param>
	/// <param name="outerScrollRect">Scroll view container.</param>
	/// <returns>Whether scroll view is scrolled to bottom.</returns>
	bool IsScrolledToBottom (Rect innerScrollRect, Rect outerScrollRect) {
		var innerScrollHeight = innerScrollRect.height;

		// Take into account extra padding added to the scroll container.
		var outerScrollHeight = outerScrollRect.height - GUI.skin.box.padding.vertical;

		// If contents of scroll view haven't exceeded outer container, treat it as scrolled to bottom.
		if (outerScrollHeight > innerScrollHeight) {
			return true;
		}

		var scrolledToBottom = Mathf.Approximately(innerScrollHeight, scrollPosition.y + outerScrollHeight);
		return scrolledToBottom;
	}

	/// <summary>
	/// Moves the scroll view down so that the last log is visible.
	/// </summary>
	void ScrollToBottom ()
	{
		scrollPosition = new Vector2(0, Int32.MaxValue);
	}

	/// <summary>
	/// Removes old logs that exceed the maximum number allowed.
	/// </summary>
	void TrimExcessLogs ()
	{
		if (!restrictLogCount) {
			return;
		}

		var amountToRemove = Mathf.Max(logs.Count - maxLogs, 0);

		if (amountToRemove == 0) {
			return;
		}

		logs.RemoveRange(0, amountToRemove);
	}



	// 스크린 회전 이벤트 받기
	// https://forum.unity3d.com/threads/device-screen-rotation-event.118638/
	public static event Action<Vector2> OnResolutionChange;
	public static event Action<DeviceOrientation> OnOrientationChange;
	public static float CheckDelay = 0.5f;        // How long to wait until we check again.

	static Vector2 resolution;                    // Current Resolution
	static DeviceOrientation orientation;        // Current Device Orientation
	static bool isAlive = true;                    // Keep this script running?

	void Start() {
		StartCoroutine(CheckForChange());
	}

	IEnumerator CheckForChange(){
		resolution = new Vector2(Screen.width, Screen.height);
		orientation = Input.deviceOrientation;

		while (isAlive) {

			// Check for a Resolution Change
			if (resolution.x != Screen.width || resolution.y != Screen.height ) {
				resolution = new Vector2(Screen.width, Screen.height);
				if (OnResolutionChange != null) OnResolutionChange(resolution);
			}

			// Check for an Orientation Change
			switch (Input.deviceOrientation) {
			case DeviceOrientation.Unknown:            // Ignore
			case DeviceOrientation.FaceUp:            // Ignore
			case DeviceOrientation.FaceDown:        // Ignore
				break;
			default:
				if (orientation != Input.deviceOrientation) {
					orientation = Input.deviceOrientation;
					if (OnOrientationChange != null) OnOrientationChange(orientation);
				}
				break;
			}

			yield return new WaitForSeconds(CheckDelay);
		}
	}

	void OnDestroy(){
		isAlive = false;
	}
}



