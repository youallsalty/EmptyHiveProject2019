using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;


using hive;				// HIVE SDK 네임스페이스 사용


/**
 * 사용자 프로필, 친구 목록, 친구 초대, SNS 포스팅 테스트 화면
 * (Social Network Service API (Hive))
 * 
 * @author ryuvsken
 */
public class SocialHiveTestView {

	public static SocialHiveTestView instance = new SocialHiveTestView ();
	private int contentCount = 24;
	private Vector2 scrollPosition = Vector2.zero;


	public void Start() {
	}


	public void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
	}


	// Hive 프로필 정보 통지 결과 리스너 - SocialHive.getMyProfile(), SocialHive.setMyProfile()
	public void onProfileHiveCB(ResultAPI result, List<ProfileHive> profileList) {

		Debug.Log ("SocialHiveTestView.onProfileHiveCB() Callback\nresult = " + result.toString() + "\n");

		if (profileList == null)
			return;

		Debug.Log ("profileList = \n");
		foreach (ProfileHive each in profileList) {
			Debug.Log (each.toString() + "\n");
		}
	}


	// Hive 프로필 정보 통지 결과 리스너 - SocialHive.getFriends()
	public void onFriendsHiveCB(ResultAPI result, List<ProfileHive> profileList) {

		Debug.Log ("SocialHiveTestView.onProfileHiveCB() Callback\nresult = " + result.toString() + "\n");

		if (profileList == null)
			return;

		Debug.Log ("profileList = \n");
		foreach (ProfileHive each in profileList) {
			Debug.Log (each.toString() + "\n");
		}
	}


	// Hive 친구 메시지 전송 통지 결과 리스너 (SocialHive.sendMessage())
	public void onSendMessageHiveCB(ResultAPI result) {

		Debug.Log ("SocialHiveTestView.onSendMessageHiveCB() Callback\nresult = " + result.toString() + "\n");
	}


	// HIVE 웹뷰 대화상자 결과 통지 리스너 (SocialHive.showHiveDialog())
	public void onShowHiveDialogCB(ResultAPI result) {

		Debug.Log ("SocialHiveTestView.onShowHiveDialogCB() Callback\nresult = " + result.toString() + "\n");
	}

	public void onGetSocialBadgeCB(ResultAPI result, SocialBadge socialBadge) {

		Debug.Log ("SocialHiveTestView.onGetSocialBadgeCB() Callback\nresult = " + result.toString() + "\n");
		Debug.Log ("socialBadge = " + socialBadge.toString() + "\n");
	}


	private String _comment = "Comment";				// comment for SocialHive.setMyProfile()


	// 친구 요청 형태 (IN_GAME, OUT_GAME, INVITED, ALL_GAME)
	private String[] _friendTypes = new String[] { "IN_GAME", "OUT_GAME", "INVITED", "ALL_GAME" };
	private int _selFriendTypes = 0;


	private String _uid = "uid";								// uid (uid = HIVE UserKey) for SocialHive.sendMessage()
	private String _vid = "vid";								// vid (vid = HIVE UserKey) for SocialHive.sendMessage()
	private String _message = "Message";						// message for SocialHive.sendMessage()

	private String _invitationUid = "Invitation uid";			// uid for SocialHive.sendInvitationMessage()
	private String _invitationMessage = "Invitation Message";	// message for SocialHive.sendInvitationMessage()

	// HIVE 웹뷰 대화상자 형태 (HOME, INQUIRY)
	private String[] _hiveDialogTypes = new String[] { "HOME", "GAME", "USER", "INQUIRY", "MESSAGE", "CHATBOT", "MYINQUIRY" };
	private int _selHiveDialogTypes = 0;

	private String _vidHiveDialog = "vid";			// vid (vid = HIVE UserKey) for SocialHive.showHiveDialog()

	long lCheck = 0;

	public void OnGUI() {
		
		HIVETestView.layout(contentCount);

		HIVETestManager testViewManager = HIVETestManager.instance;

		if (testViewManager._isShow == false) {
			return;
		}
		
		float scrollViewHeight = (testViewManager .buttonHeight + 20) * contentCount;
		scrollPosition = GUI.BeginScrollView(new Rect(0, 0, Screen.width, Screen.height - testViewManager.bottomBarHeight - 20), scrollPosition, new Rect(0, 0, Screen.width, scrollViewHeight));
        GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.05f;
        GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.05f;


        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- Social HIVE -")) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		// HIVE 인증 사용자의 자기 프로필 조회
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("SocialHive.getMyProfile()")) {

			Debug.Log("\n\n=== HIVE 인증 사용자의 자기 프로필 조회 ===\n");
			Debug.Log("SocialHive.getMyProfile(onProfileHive) Called\n");

			SocialHive.getMyProfile (onProfileHiveCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// HIVE 인증 사용자의 자기 대화명 수정
		GUILayout.BeginHorizontal();
		GUILayout.Box("Comment : ");
		_comment = HIVETestView.createTextField (_comment);
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("SocialHive.setMyProfile(" + _comment + ")")) {

			Debug.Log("\n\n=== HIVE 인증 사용자의 자기 대화명 수정 ===\n");
			Debug.Log(String.Format("SocialHive.setMyProfile(comment = {0}, onProfileHive) Called\n", _comment));

			SocialHive.setMyProfile (_comment, onProfileHiveCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// HIVE 인증 사용자의 친구 목록 조회
		GUILayout.BeginHorizontal();
		GUILayout.Box("FriendType : ");
		_selFriendTypes = HIVETestView.selectionGrid(_selFriendTypes, _friendTypes, _friendTypes.Length);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		// Social 친구 요청
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("SocialHive.getFriends(" + _friendTypes[_selFriendTypes] + ")")) {

			FriendType friendType = FriendType.IN_GAME;
			if(_selFriendTypes == 1)
				friendType = FriendType.OUT_GAME;
			else if(_selFriendTypes == 2)
				friendType = FriendType.INVITED;
			else if(_selFriendTypes == 3)
				friendType = FriendType.ALL_GAME;


			Debug.Log("\n\n=== HIVE 인증 사용자의 친구 목록 조회 ===\n");
			Debug.Log(String.Format("SocialHive.getFriends(friendType = {0}, onFriendsHive) Called\n", friendType.ToString()));

			SocialHive.getFriends (friendType, onFriendsHiveCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// Hive 친구에게 메시지 전송 요청
		GUILayout.BeginHorizontal();
		GUILayout.Box("uid (uid is HIVE UserKey) : ");
		_uid = HIVETestView.createTextField (_uid);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("vid (vid is HIVE UserKey) : ");
		_vid = HIVETestView.createTextField (_vid);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Message : ");
		_message = HIVETestView.createTextField (_message);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("SocialHive.sendMessage(" + (long.TryParse(_uid, out lCheck) ? _uid : "") + ", " + (long.TryParse(_vid, out lCheck) ? _vid : "") + ", " + _message + ")")) {

			MessageContent messageContent = new MessageContent ();
			messageContent.uid = (long.TryParse(_uid, out lCheck) ? _uid : "");
			messageContent.vid = (long.TryParse(_vid, out lCheck) ? _vid : "");
			messageContent.message = _message;


			Debug.Log("\n\n=== Hive 친구에게 메시지 전송 요청 ===\n");
			Debug.Log("SocialHive.sendMessage(messageContent, onSendMessageHive) Called\nmessageContent = "
				+ messageContent.toString() + "\n");
			
			SocialHive.sendMessage (messageContent, onSendMessageHiveCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// Hive 친구에게 초대 메시지 전송 요청
		GUILayout.BeginHorizontal();
		GUILayout.Box("Invitation uid (uid is HIVE UserKey) : ");
		_invitationUid = HIVETestView.createTextField (_invitationUid);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Invitation Message : ");
		_invitationMessage = HIVETestView.createTextField (_invitationMessage);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("SocialHive.sendInvitationMessage(" + (long.TryParse(_invitationUid, out lCheck) ? _invitationUid : "") + ", " + _invitationMessage + ")")) {

			MessageContent messageContent = new MessageContent ();
			messageContent.uid = (long.TryParse(_invitationUid, out lCheck) ? _invitationUid : "");
			messageContent.message = _invitationMessage;


			Debug.Log("\n\n=== Hive 친구에게 초대 메시지 전송 요청 ===\n");
			Debug.Log("SocialHive.sendInvitationMessage(messageContent, onSendMessageHive) Called\nmessageContent = "
				+ messageContent.toString() + "\n");
			
			SocialHive.sendInvitationMessage (messageContent, onSendMessageHiveCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// HIVE 웹뷰 대화상자 호출
		GUILayout.BeginHorizontal();
		GUILayout.Box("HiveDialogType : ");
		_selHiveDialogTypes = HIVETestView.selectionGrid(_selHiveDialogTypes, _hiveDialogTypes, _hiveDialogTypes.Length);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("vid (vid is HIVE UserKey) : ");
		_vidHiveDialog = HIVETestView.createTextField (_vidHiveDialog);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		// HIVE 웹뷰 대화상자 노출
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("SocialHive.showHiveDialog(" + _hiveDialogTypes[_selHiveDialogTypes] + ", " + (long.TryParse(_vidHiveDialog, out lCheck) ? _vidHiveDialog : "") + ")")) {

			HiveDialogType hiveDialogType = HiveDialogType.HOME;
			if(_selHiveDialogTypes == 1)
				hiveDialogType = HiveDialogType.GAME;
			else if(_selHiveDialogTypes == 2)
				hiveDialogType = HiveDialogType.USER;
			else if(_selHiveDialogTypes == 3)
				hiveDialogType = HiveDialogType.INQUIRY;
			else if(_selHiveDialogTypes == 4)
				hiveDialogType = HiveDialogType.MESSAGE;
			else if (_selHiveDialogTypes == 5)
				hiveDialogType = HiveDialogType.CHATBOT;
			else if (_selHiveDialogTypes == 6)
				hiveDialogType = HiveDialogType.MYINQUIRY;

			Debug.Log("\n\n=== HIVE 웹뷰 대화상자 호출 ===\n");

			if (hiveDialogType == HiveDialogType.CHATBOT) {
				
				String _chatbotAdditionalInfo =  "{\"evt_code\":1000,\"p1\":\"random\"}";	// 챗봇 페이지 바로가기를 위해 약속된 JSON 형식의 String 데이터

				Debug.Log(String.Format("SocialHive.showHiveDialog(hiveDialogType = {0}, vid = {1}, additionalInfo = {2}, onShowHiveDialog) Called\n"
				, hiveDialogType.ToString(), _vidHiveDialog, _chatbotAdditionalInfo));

				SocialHive.showHiveDialog (hiveDialogType, (long.TryParse(_vidHiveDialog, out lCheck) ? _vidHiveDialog : ""), _chatbotAdditionalInfo, onShowHiveDialogCB);
				
			} else {
				Debug.Log(String.Format("SocialHive.showHiveDialog(hiveDialogType = {0}, vid = {1}, onShowHiveDialog) Called\n"
				, hiveDialogType.ToString(), _vidHiveDialog));
				SocialHive.showHiveDialog (hiveDialogType,  (long.TryParse(_vidHiveDialog, out lCheck) ? _vidHiveDialog : ""), onShowHiveDialogCB);
			}
		}
		GUILayout.EndHorizontal();

		// HIVE 메세지 배지 조회
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("SocialHive.getBadgeInfo()")) {

			Debug.Log("\n\n=== HIVE 메세지 배지 조회 ===\n");
			Debug.Log("SocialHive.getBadgeInfo(onGetSocialBadgeCB) Called\n");

			SocialHive.getBadgeInfo (onGetSocialBadgeCB);
		}
		GUILayout.EndHorizontal();

        GUILayout.EndVertical();

		GUI.EndScrollView();
    }
}



