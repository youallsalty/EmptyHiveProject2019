using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using System.IO;

using hive;			// HIVE SDK 네임스페이스 사용


/**
 * HIVE SDK 의 설정 정보 확인
 * 
 * @author ryuvsken
 */
public class ConfigurationTestView {

	public static ConfigurationTestView instance = new ConfigurationTestView ();
	private Vector2 scrollPosition = new Vector2(0, 0);
	private int contentCount = 24;
	private List<string> appIdList, gameServerList;
	private bool showAppIdButton, showGameServerButton = false;
	private int selectedAppIdInt, selectedGameServerInt = -1;

	public void Start() {

		this.loadConfiguration ();
		this.loadConfigurationFromFile ();

		selGameLanguage.Add("en", new LanguageDisplayContent("en", "English"));
		selGameLanguage.Add("ko", new LanguageDisplayContent("ko", "한국어"));
		selGameLanguage.Add("zh-hans", new LanguageDisplayContent("zh-hans", "简体中文"));
		selGameLanguage.Add("zh-hant", new LanguageDisplayContent("zh-hant", "繁體中文"));
		selGameLanguage.Add("ja", new LanguageDisplayContent("ja", "日本語"));
		selGameLanguage.Add("ru", new LanguageDisplayContent("ru", "Pусский"));
		selGameLanguage.Add("fr", new LanguageDisplayContent("fr", "Français"));
		selGameLanguage.Add("de", new LanguageDisplayContent("de", "Deutsch"));
		selGameLanguage.Add("es", new LanguageDisplayContent("es", "Español"));
		selGameLanguage.Add("pt", new LanguageDisplayContent("pt", "português"));
		selGameLanguage.Add("id", new LanguageDisplayContent("id", "Bahasa Indonesia"));
		selGameLanguage.Add("ms", new LanguageDisplayContent("ms", "Bahasa Melayu"));
		selGameLanguage.Add("th", new LanguageDisplayContent("th", "ไทย"));
		selGameLanguage.Add("vi", new LanguageDisplayContent("vi", "Tiếng Việt"));
		selGameLanguage.Add("it", new LanguageDisplayContent("it", "Italiano"));
		selGameLanguage.Add("tr", new LanguageDisplayContent("tr", "Türkçe"));
		selGameLanguage.Add("ar", new LanguageDisplayContent("ar", " الْعَرَبِيَّةُ"));
		selGameLanguage.Add("null", new LanguageDisplayContent("null", "Not set"));

		// 선택 가능한 AppId 리스트 초기화
		#if !UNITY_EDITOR && UNITY_IPHONE
			appIdList = new List<string>() {
				"com.com2us.hivesdk.normal.freefull.apple.global.ios.universal",
				"com.com2us.misample.normal.freefull.apple.global.ios.universal"
			};
		#elif !UNITY_EDITOR && UNITY_ANDROID
			appIdList = new List<string>() {
				"com.com2us.hivesdk.normal.freefull.google.global.android.common",
				"com.com2us.misample.normal.freefull.google.global.android.common",
				"com.com2us.hivesdk.normal.freefull.onestore.kr.android.common",
				"com.com2us.hivesdk.android.huawei.global.normal.huawei",
				"com.com2us.hivesdk.android.amazon.global.normal"
			};
		#else 
			appIdList = new List<string>() {
				"Window",
				"Mac"
			};
		#endif
		appId = appIdList[0];

		// 선택 가능한 Game Server 리스트 초기화
		gameServerList = new List<string>() {
			"kr",
			"server_001",
			"server_002",
			"server_003"
		};
		gameServerId = gameServerList[0];

		int index = 0;
		foreach (KeyValuePair<string, LanguageDisplayContent> item in selGameLanguage ) {
			selLanguageCode[index] = item.Key.ToString();
			selLanguageDisplayContent[index++] = item.Value.ToString();
		}
	}


	public void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
	}

	// HIVE MetaData 결과 통지 리스너 (getMetaData)
	public void onGetMetaDataCB(ResultAPI result, String value)
	{
		Debug.Log("ConfigurationTestView.onGetMetaDataCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log("value = " + value + "\n");
	}


	private String hiveSDKVersion = "";
	private String appId = "";
	private String appIdTextField = "";
	private String zone = ZoneType.SANDBOX.ToString();
	private String hiveTheme = HiveThemeType.hiveLight.ToString();
	public String hiveOrientation = "all";
	public String gameLanguage = "en";
	private String targetLanguage = "en";
	private String gameServerId = "kr";
	internal String gameServerIdTextField = "kr";
	private String market = "";
	private String hiveCountry = "";
	private String hiveTimeZone = "";

	private bool useLog = true;

	private bool useAgeGateU13 = false;
	private bool isOnHive = true;
	private String hiveCertificationKey; // GCPSDK4-284

	public int selGridInt = 0;

	private String getMetaData = "game_server_url";
	private bool forceReload = true;


	class LanguageDisplayContent {
		string	languageCode = "";
		string	displayLanguage = "";

		public LanguageDisplayContent(string languageCode, string displayLanguage) {
			this.languageCode = languageCode;
			this.displayLanguage = displayLanguage;
		}
		
		public override String ToString() {
			if (String.IsNullOrEmpty(displayLanguage))
				return languageCode;
			else
				return displayLanguage + "(" + languageCode + ")";
		}
	}

	private string[] selLanguageDisplayContent = new string[18];
	private string[] selLanguageCode = new string[18];
	
	private Dictionary<string, LanguageDisplayContent> selGameLanguage = new Dictionary<string, LanguageDisplayContent>();
	private bool showGameLanguageButton = false;
	
	//HIVE SDK getPermissionViewData에 사용되는 Language 설정
	private bool showTargetLanguageButton = false;

	private String[] hiveOrientationList = {"NotSet", "undefined", "all", "portrait", "landscape"};

	private void loadConfiguration() {
		String jsonResString = Configuration.getConfiguration ();
		
		JSONObject jsonConfiguration = new JSONObject (jsonResString);
		jsonConfiguration.GetField (ref this.hiveSDKVersion, "hiveSDKVersion");
		jsonConfiguration.GetField (ref this.appIdTextField, "appId");
		jsonConfiguration.GetField (ref this.zone, "zone");
		jsonConfiguration.GetField (ref this.useLog, "useLog");
		jsonConfiguration.GetField (ref this.gameLanguage, "gameLanguage");
		jsonConfiguration.GetField (ref this.gameServerIdTextField, "gameServerId");
		jsonConfiguration.GetField (ref this.market, "market");
		jsonConfiguration.GetField (ref this.getMetaData, "metaDataKey");

		this.useAgeGateU13 = Configuration.getAgeGateU13();
		this.hiveCertificationKey = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJIaXZlIiwiaWF0IjoxNjAyMDU2NzI2LCJqdGkiOiIxODczMTExMzIwIn0.3soFiHTPlObCoqR5xX9ZeOQTSvnHrHDHWmopP3QfWtY"; // GCPSDK4-284
	}
	public static String configSavePath = Application.persistentDataPath + "/hiveSampleConfig.txt";
	private void loadConfigurationFromFile() {
		if(File.Exists(configSavePath)){
			String configString = File.ReadAllText(configSavePath);
			JSONObject jsonConfiguration = new JSONObject (configString);
			jsonConfiguration.GetField (ref this.appIdTextField, "appId");
			jsonConfiguration.GetField (ref this.zone, "zone");
			jsonConfiguration.GetField (ref this.hiveTheme, "hiveTheme");
			jsonConfiguration.GetField (ref this.hiveOrientation, "hiveOrientation");
			jsonConfiguration.GetField (ref this.useLog, "useLog");
			jsonConfiguration.GetField (ref this.gameLanguage, "gameLanguage");
			jsonConfiguration.GetField (ref this.gameServerIdTextField, "gameServerId");
			jsonConfiguration.GetField (ref this.getMetaData, "metaDataKey");
			Debug.LogWarning("Try load Saved Configuration and Set.\n" );
			if (this.hiveTheme == "") {
				this.hiveTheme = HiveThemeType.hiveLight.ToString();
			}
			this.saveConfiguration ();
		}
	}
	private void saveConfiguration() {
		
		ZoneType zoneType = (ZoneType) Enum.Parse(typeof(ZoneType), this.zone, true);
		HiveThemeType hiveThemeType = (HiveThemeType) Enum.Parse(typeof(HiveThemeType), this.hiveTheme, true);

		Configuration.setAppId(this.appIdTextField);
		Configuration.setServerId(this.gameServerIdTextField);
		Configuration.setZone(zoneType);
		Configuration.setHiveTheme(hiveThemeType);
		Configuration.setUseLog(this.useLog);
		if ( MainTestView.initializeState == MainTestView.InitializeState.NOT_INIT) {
			Configuration.setAgeGateU13(this.useAgeGateU13);
		}

		if (!String.Equals(this.gameLanguage,"null"))
		{
			Configuration.setGameLanguage(this.gameLanguage);
		}

		if (!String.Equals(this.hiveOrientation, "NotSet"))
		{
			Configuration.setHiveOrientation(this.hiveOrientation);
		}

		Configuration.setHiveCertificationKey(this.hiveCertificationKey); // GCPSDK4-284

		postboxDataSetting();

		saveConfigurationToFile(this.appIdTextField, this.zone, this.hiveTheme, this.hiveOrientation, this.useLog, this.gameLanguage, this.gameServerIdTextField, this.getMetaData);
	}

	public static void saveConfigurationToFile(String appID, String zone, String hiveTheme, String hiveOrientation, bool useLog, String gameLanguage, String gameServerId)
	{
		saveConfigurationToFile(appID, zone, hiveTheme, hiveOrientation, useLog, gameLanguage, gameServerId, "game_server_url");
	}

	public static void saveConfigurationToFile(String appID, String zone, String hiveTheme, String hiveOrientation, bool useLog, String gameLanguage, String gameServerId, String metaDataKey) {
		JSONObject saveConfigJson = new JSONObject();
		saveConfigJson.AddField("appId", appID);
		saveConfigJson.AddField("zone", zone);
		saveConfigJson.AddField("hiveTheme", hiveTheme);
		saveConfigJson.AddField("hiveOrientation", hiveOrientation);
		saveConfigJson.AddField("useLog", useLog);
		saveConfigJson.AddField("gameLanguage", gameLanguage);
		saveConfigJson.AddField("gameServerId", gameServerId);
		saveConfigJson.AddField("metaDataKey", metaDataKey);

		String saveConfigData = saveConfigJson.Print();
		File.WriteAllText(configSavePath, saveConfigData);
	}

	public void postboxDataSetting() {
		Debug.Log("PostBox Data Setting API is called");
		// only on IAP Postbox Sample.
		PostboxInfo.appID = this.appIdTextField;
		PostboxInfo.serverState = this.zone.ToString();
		PostboxInfo.hiveiapWorldName = this.gameServerIdTextField;
		PostboxInfo.country = this.gameLanguage;
		PostboxInfo.language = this.gameLanguage;
		PostboxInfo.appversion = this.hiveSDKVersion;

		if (MainTestView.initializeState == MainTestView.InitializeState.AUTH_INIT)
		{
			PostboxInfo.VIDType = "v1";
		}
		else if (MainTestView.initializeState == MainTestView.InitializeState.AUTHV4_INIT)
		{
			PostboxInfo.VIDType = "v4";
		}

	}

	public void OnGUI() {
		
		HIVETestView.layout(contentCount);

		HIVETestManager testViewManager = HIVETestManager.instance;

		if (testViewManager._isShow == false) {
			return;
		}
		
		float scrollViewHeight = (testViewManager .buttonHeight + 20) * (contentCount + 10);
		scrollPosition = GUI.BeginScrollView(new Rect(0, 0, Screen.width, Screen.height - testViewManager.bottomBarHeight - 20), scrollPosition, new Rect(0, 0, Screen.width, scrollViewHeight));
        GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.05f;
        GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.05f;


        GUILayout.BeginVertical();

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- Configuration -")) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		// 설정 화면 구성
		// GUILayout.BeginHorizontal();
		// if (HIVETestView.createButton ("getConfiguration")) {

		// 	this.loadConfiguration ();
		// }
		// GUILayout.EndHorizontal();
		// GUILayout.Space (16);


		GUILayout.BeginHorizontal();
		GUILayout.Box("HIVE SDK v4 : ");
		GUILayout.Box(this.hiveSDKVersion);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);


		GUILayout.BeginHorizontal();
		GUILayout.Box("SDK Ver : ");
		GUILayout.BeginVertical();


		String[] refSDKVerStrings = Configuration.getReferenceSDKVersion().Split(new string[]{Environment.NewLine, "\\n"}, StringSplitOptions.None);		
		foreach ( String refSDKVerString in refSDKVerStrings ) {
			GUILayout.Box(refSDKVerString);	
		}


		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
		GUILayout.Space (2);
		#if UNITY_IOS
		GUILayout.BeginHorizontal();
		GUILayout.Box("IDFA : ");
		GUILayout.Box(UnityEngine.iOS.Device.advertisingIdentifier);
		if( HIVETestView.createButton("COPY") ) {
				GUIUtility.systemCopyBuffer = UnityEngine.iOS.Device.advertisingIdentifier;
		}
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Box("IDFV : ");
		GUILayout.Box(UnityEngine.iOS.Device.vendorIdentifier);
		if( HIVETestView.createButton("COPY") ) {
				GUIUtility.systemCopyBuffer = UnityEngine.iOS.Device.vendorIdentifier;
		}
		GUILayout.EndHorizontal();
		#endif
		GUILayout.BeginHorizontal();
		GUILayout.Box("AppId : ");
		GUILayout.BeginVertical("Box");
		if (!showAppIdButton) {
			if ( HIVETestView.createButton(appId)) {
				showAppIdButton = true;
			}
		}
		else {
			selectedAppIdInt = HIVETestView.selectionGrid(-1, appIdList.ToArray(), appIdList.Count, 1);

			if( selectedAppIdInt != -1 ) {
				appId = appIdList[selectedAppIdInt];
				appIdTextField = appId;
				showAppIdButton = false;
			}
		}
		GUILayout.Space (2);

		// 선택한 AppId 텍스트필드에 작성
		GUILayout.BeginHorizontal();
		this.appIdTextField = HIVETestView.createTextField(this.appIdTextField);
		GUILayout.EndHorizontal();

		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		// GCPSDK4-284
		GUILayout.BeginHorizontal();
		GUILayout.Box("HiveCertificationKey : ");
		this.hiveCertificationKey = HIVETestView.createTextField(this.hiveCertificationKey);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Zone : ");
		if( this.zone.Equals(ZoneType.REAL.ToString()) ) {
			if( HIVETestView.createButton(ZoneType.REAL.ToString()) ) {
				this.zone = ZoneType.TEST.ToString();
			}
		} 
		else if ( this.zone.Equals(ZoneType.TEST.ToString()) ) {
			if ( HIVETestView.createButton(ZoneType.TEST.ToString()) ) {
				this.zone = ZoneType.DEV.ToString();
			}
		}
		else if ( this.zone.Equals(ZoneType.DEV.ToString()) ) {
			if ( HIVETestView.createButton(ZoneType.DEV.ToString()) ) {
				this.zone = ZoneType.SANDBOX.ToString();
			}
		}
		else {
			if( HIVETestView.createButton(ZoneType.SANDBOX.ToString()) ) {
				this.zone = ZoneType.REAL.ToString();
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("HiveTheme : ");
		if( this.hiveTheme.Equals(HiveThemeType.hiveLight.ToString()) ) {
			if( HIVETestView.createButton(HiveThemeType.hiveLight.ToString()) ) {
				this.hiveTheme = HiveThemeType.hiveDark.ToString();
			}
		} 
		else {
			if( HIVETestView.createButton(HiveThemeType.hiveDark.ToString()) ) {
				this.hiveTheme = HiveThemeType.hiveLight.ToString();
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		// hiveOrientation
		GUILayout.BeginHorizontal();
		GUILayout.Box("HiveOrientation : ");

		for(int i = 0; i<hiveOrientationList.Length; i++) {
			if(this.hiveOrientation.Equals(hiveOrientationList[i])) {
				if( HIVETestView.createButton(hiveOrientationList[i]) ) {
					this.hiveOrientation = hiveOrientationList[ (i+1) % hiveOrientationList.Length ];
				}
				break;
			}
		}

		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Game Language : ");
		GUILayout.BeginVertical("Box");

		if (showGameLanguageButton == false) {
			String selectGameLanguage = "";
			try {
				selectGameLanguage = this.selGameLanguage[this.gameLanguage].ToString();
			} catch(System.Exception) {

			}
			if ( HIVETestView.createButton(selectGameLanguage) ) {
				showGameLanguageButton = true;
			}
		}
		else {
			selGridInt = HIVETestView.selectionGrid(-1, selLanguageDisplayContent, selLanguageDisplayContent.Length, 1);

			if( selGridInt != -1 ) {
				gameLanguage = selLanguageCode[selGridInt];
				showGameLanguageButton = false;
			}
		}
		
        GUILayout.EndVertical();
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Game Server ID : ");
		GUILayout.BeginVertical("Box");
		if (showGameServerButton == false) {
			if ( HIVETestView.createButton(gameServerId) ) {
				showGameServerButton = true;
			}
		}
		else {
			selectedGameServerInt = HIVETestView.selectionGrid(-1, gameServerList.ToArray(), gameServerList.Count, 1);

			if( selectedGameServerInt != -1 ) {
				gameServerId = gameServerList[selectedGameServerInt];
				gameServerIdTextField = gameServerId;
				showGameServerButton = false;
			}
		}
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		// 선택한 gameServerId 텍스트필드에 작성
		GUILayout.BeginHorizontal();
		this.gameServerIdTextField = HIVETestView.createTextField(this.gameServerIdTextField);
		GUILayout.EndHorizontal();
		GUILayout.Space(2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Market : ");
		GUILayout.Box(this.market);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Hive Country : ");
		this.hiveCountry = HIVETestView.createTextField(this.hiveCountry);

		if (HIVETestView.createButton ("getHiveCountry")) {
			this.hiveCountry = Configuration.getHiveCountry();
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (2);


		// GCPSDK4-292
		GUILayout.BeginHorizontal();
		GUILayout.Box("Hive TimeZone : ");
		this.hiveTimeZone = HIVETestView.createTextField(this.hiveTimeZone);

		if (HIVETestView.createButton ("getHiveTimeZone")) {
			this.hiveTimeZone = Configuration.getHiveTimeZone();
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (2);


		GUILayout.BeginHorizontal();
		GUILayout.Box("useLog : ");
		if( useLog == true ) {
			if( HIVETestView.createButton("ON") ) {
				useLog = false;
			}
		}
		else {
			if( HIVETestView.createButton("OFF") ) {
				useLog = true;
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (2);


		if (MainTestView.initializeState == MainTestView.InitializeState.NOT_INIT) {
			GUILayout.BeginHorizontal();
			GUILayout.Box("Use  AgeGateU13 : ");
			if( useAgeGateU13 == true ) {
				if( HIVETestView.createButton("ON") ) {
					useAgeGateU13 = false;
				}
			}
			else {
				if( HIVETestView.createButton("OFF") ) {
					useAgeGateU13 = true;
				}
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (2);
		}

				GUILayout.BeginHorizontal ();
		GUILayout.Box("isOn : ");
		if ( isOnHive == true) {
			if (HIVETestView.createButton("ON")) {
				isOnHive = false;
			}
		}
		else {
			if (HIVETestView.createButton("OFF")) {
				isOnHive = true;
			}
		}

		//HIVE SDK 권한고지 팝업 노출 여부 설정
		GUILayout.Space(2);
		if (HIVETestView.createButton ("Configuration.setHivePermissionViewOn")) {
			Configuration.setHivePermissionViewOn(isOnHive);

			Debug.Log("\n\n=== HIVE SDK 권한고지 팝업 노출 설정. === \n");
			Debug.Log("Configuration.setHivePermissionViewOn Called\n");
			Debug.Log(isOnHive ? "isOn : true\n" : "isOn : false\n");
		}
		GUILayout.EndHorizontal ();
		GUILayout.Space(2);

		//HIVE SDK 권한고지 리소스 파일 읽기
        GUILayout.BeginHorizontal();
		GUILayout.Box("Select Language : ");
		GUILayout.BeginVertical("Box");
		
		if (showTargetLanguageButton == false) {
			if ( HIVETestView.createButton(this.selGameLanguage[this.targetLanguage].ToString()) ) {
				showTargetLanguageButton = true;
			}
		}
		else {
			selGridInt = HIVETestView.selectionGrid(-1, selLanguageDisplayContent, selLanguageDisplayContent.Length, 1);

			if( selGridInt != -1 ) {
				targetLanguage = selLanguageCode[selGridInt];
				showTargetLanguageButton = false;
			}
		}
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
		GUILayout.Space(2);

		GUILayout.BeginHorizontal();
        if (HIVETestView.createButton("Configuration.getPermissionViewData()"))
        {
            Debug.Log("\n\n=== Configuration 퍼미션 뷰 정보 요청. ===\n");

			HIVELanguage languageType = HIVELanguage.HIVELanguageEN;
			if (targetLanguage.Equals("ar")) {
				languageType = HIVELanguage.HIVELanguageAR;
			}
			else if (targetLanguage.Equals("de")) {
				languageType = HIVELanguage.HIVELanguageDE;
			}
            else if (targetLanguage.Equals("en")){
                languageType = HIVELanguage.HIVELanguageEN;
            }
			else if (targetLanguage.Equals("es")){
                languageType = HIVELanguage.HIVELanguageES;
            }
            else if (targetLanguage.Equals("fr")){
                languageType = HIVELanguage.HIVELanguageFR;
            }
            else if (targetLanguage.Equals("id")){
                languageType = HIVELanguage.HIVELanguageID;
            }
            else if (targetLanguage.Equals("it")){
                languageType = HIVELanguage.HIVELanguageIT;
            }
            else if (targetLanguage.Equals("ja")){
                languageType = HIVELanguage.HIVELanguageJA;
            }
            else if (targetLanguage.Equals("ko")){
                languageType = HIVELanguage.HIVELanguageKO;
            }
            else if (targetLanguage.Equals("pt")){
                languageType = HIVELanguage.HIVELanguagePT;
            }
            else if (targetLanguage.Equals("ru")){
                languageType = HIVELanguage.HIVELanguageRU;
            }
            else if (targetLanguage.Equals("th")){
                languageType = HIVELanguage.HIVELanguageTH;
            }
            else if (targetLanguage.Equals("tr")){
                languageType = HIVELanguage.HIVELanguageTR;
            }
            else if (targetLanguage.Equals("vi")){
                languageType = HIVELanguage.HIVELanguageVI;
            }
            else if (targetLanguage.Equals("zh-hans")){
                languageType = HIVELanguage.HIVELanguageZHS;
            }
            else if (targetLanguage.Equals("zh-hant")){
                languageType = HIVELanguage.HIVELanguageZHT;
            }

            PermissionViewData data = Configuration.getPermissionViewData(languageType);

            if (data.contents != null)
            {
                Debug.Log("\n제목 : " + data.contents);
            }

            for (int i = 0; i < data.permissions.Count; i++)
            {
                Debug.Log("\n권한명 : " + data.permissions[i].nativePermissionName + "\n타이틀 : " + data.permissions[i].title + "\n내용: " + data.permissions[i].contents + "\n권한 카테고리: " + data.permissions[i].permissionCategory);
            }
            for (int i = 0; i < data.commons.Count; i++)
            {
                Debug.Log("\n권한명 : " + data.commons[i].nativePermissionName + "\n타이틀 : " + data.commons[i].title + "\n내용: " + data.commons[i].contents + "\n권한 카테고리: " + data.commons[i].permissionCategory);
            }
        }
        GUILayout.EndHorizontal();
        GUILayout.Space(2);

		// MetaData
		GUILayout.BeginHorizontal();
		GUILayout.Box("MetaData : ");
		this.getMetaData = HIVETestView.createTextField(this.getMetaData);
		if (this.forceReload == true)
		{
			if (HIVETestView.createButton("ON"))
			{
				this.forceReload = false;
			}
		}
		else
		{
			if (HIVETestView.createButton("OFF"))
			{
				this.forceReload = true;
			}
		}

		if (HIVETestView.createButton("getMetaData(key: forceReload:)"))
		{
			this.saveConfiguration();
			Configuration.getMetaData(this.getMetaData, this.forceReload, onGetMetaDataCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space(2);

		GUILayout.BeginHorizontal();
		if( HIVETestView.createButton("Reset") ) {
			this.loadConfiguration ();
		}

		if( HIVETestView.createButton("OK") ) {
			this.saveConfiguration ();
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
		GUILayout.EndHorizontal();

        GUILayout.EndVertical();

		GUI.EndScrollView();
    }

}



