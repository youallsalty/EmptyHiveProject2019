using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


using hive;		// HIVE SDK 네임스페이스 사용


/**
 * 플랫폼 초기화, 약관 노출 및 사용자 동의, DID 발급 (단말 식별), 세션 통계 (동접 계산), 버전 체크, VID/UID 발급 (게스트 로그인/계정 로그인), 외부 인증 연결/해제 테스트 화면
 * (Platform Initialize, Device / User Identity, Session Counting, Check Version, Social Authentication)
 * 
 * @author ryuvsken
 */
public class AuthTestView {

	public static AuthTestView instance = new AuthTestView ();
	private int contentCount = 24;
	private Vector2 scrollPosition = Vector2.zero;


	public void Start() {
	}


	public void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
	}


	// HIVE 로그인 결과 통지 리스너 - Auth.login(), Auth.selectLogin()
	public void onAuthLoginCB(ResultAPI result, LoginType loginType, Account currentAccount, Account usedAccount) {

		Debug.Log("AuthTestView.onAuthLoginCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("loginType = " + loginType.ToString () + "\n");
		Debug.Log ("currentAccount = " + currentAccount.toString () + "\n");
		Debug.Log ("usedAccount = " + usedAccount.toString () + "\n");
		
		// LoginType이 SELECT라면 selectLogin을 호출해줘야 한다.
		// 이를 위해서 전역 변수에 vid 저장
		if (loginType == LoginType.SELECT) {

			_currentVid = currentAccount.vid;
			_usedVid = usedAccount.vid;
		}

		if (result.isSuccess()) {
			MainTestView.isAuthLogin = true;
			
			// Only on IAP Postbox Sample
			PostboxInfo.DID = currentAccount.did;
			PostboxInfo.VID = currentAccount.vid;
			PostboxInfo.UID = currentAccount.uid;
			PostboxInfo.hiveSessionToken = currentAccount.accessToken;
			ConfigurationTestView.instance.postboxDataSetting();
			PostboxView.instance.callThePostboxLogin();
		}
	}


	// HIVE 로그아웃 결과 통지 리스너 - Auth.logout()
	public void onAuthLogoutCB(ResultAPI result) {
		
		Debug.Log ("AuthTestView.onAuthLogoutCB() Callback\nresult = " + result.toString() + "\n");

		MainTestView.isAuthLogin = false;
	}


	// 약관 노출 통지 리스너 (Auth.showTerms())
	public void onAuthShowTermsCB(ResultAPI result) {

		Debug.Log ("AuthTestView.onAuthShowTermsCB() Callback\nresult = " + result.toString() + "\n");
	}


	// 점검 팝업 결과 통지 리스너 - Auth.checkMaintenance()
	public void onAuthMaintenanceCB(ResultAPI result, AuthMaintenanceInfo authMaintenanceInfo) {

		Debug.Log ("AuthTestView.onAuthMaintenanceCB() Callback\nresult = " + result.toString() + "\n");
		Debug.Log (authMaintenanceInfo.toString());
	}


	public void onAuthAdultConfirmCB(ResultAPI result) {

		Debug.Log("AuthTestView.onAuthAdultConfirmCB() Callback\nresult = " + result.toString() + "\n");
	}


	// 로그인 수행을 위한 로그인 타입 (GUEST, ACCOUNT, SELECT, AUTO)
	private String[] _loginTypes = new String[] { "GUEST", "ACCOUNT", "SELECT", "AUTO" };
	private int _selLoginTypes = 0;

	private String _currentVid = "currentVid";
	private String _usedVid = "_usedVid";

	private String[] _isShows = new String[] { "true", "false" };
	private int _selIsShow = 0;

	private String[] _isReady = new String[] { "true", "false" };
	private int _selIsReady = 0;

	long lCheckVid = 0;
	
	public void OnGUI() {

		HIVETestView.layout(contentCount);

		HIVETestManager testViewManager = HIVETestManager.instance;

		if (testViewManager._isShow == false) {
			return;
		}
		
		float scrollViewHeight = (testViewManager .buttonHeight + 20) * contentCount;
		scrollPosition = GUI.BeginScrollView(new Rect(0, 0, Screen.width, Screen.height - testViewManager.bottomBarHeight - 20), scrollPosition, new Rect(0, 0, Screen.width, scrollViewHeight));
        GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.05f;
        GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.05f;

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- Auth -")) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		// HIVE 로그인 타입 선택
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Auth.getLoginType()")) {

			LoginType loginType = Auth.getLoginType ();
			if (loginType == LoginType.ACCOUNT)
				_selLoginTypes = 1;
			else if (loginType == LoginType.SELECT)
				_selLoginTypes = 2;
			else if (loginType == LoginType.AUTO)
				_selLoginTypes = 3;
			else
				_selLoginTypes = 0;

			Debug.Log ("\n\n=== 로그인 가능 형태 조회 (GUEST, ACCOUNT, AUTO, SELECT) ===\n");
			Debug.Log ("Auth.getLoginType() Called\nloginType = \n" + loginType.ToString() + "\n");

		}
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("LoginType : ");
		_selLoginTypes = HIVETestView.selectionGrid(_selLoginTypes, _loginTypes, _loginTypes.Length);
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// HIVE 로그인 시도
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Auth.login(" + _loginTypes[_selLoginTypes] + ")")) {

			LoginType loginType = LoginType.GUEST;
			if (_selLoginTypes == 1)
				loginType = LoginType.ACCOUNT;
			else if (_selLoginTypes == 2)
				loginType = LoginType.SELECT;
			else if (_selLoginTypes == 3)
				loginType = LoginType.AUTO;


			Debug.Log("\n\n=== 선택된 로그인 타입으로 로그인을 요청 ===\n");
			Debug.Log(String.Format("Auth.login(loginType = {0}, onAuthLogin) Called\n", loginType.ToString()));

			Auth.login (loginType, onAuthLoginCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// HIVE SDK 초기화의 결과 통지 시 LoginType 이 LoginType.SELECT 일 경우 사용자를 선택을 요청한다.
		GUILayout.BeginHorizontal();
		GUILayout.Box("Current vid : ");
		_currentVid = HIVETestView.createTextField ((long.TryParse(_currentVid, out lCheckVid) ? _currentVid : ""));
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Select vid : ");
		_usedVid = HIVETestView.createTextField (_usedVid);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Auth.showLoginSelection(" + _currentVid + ", " + _usedVid + ")")) {

			JSONObject currentVidData = new JSONObject();
			currentVidData.AddField("vid", _currentVid);

			JSONObject currendVidGameData = new JSONObject();
			currendVidGameData.AddField("Name", "currentName");
			currendVidGameData.AddField("Level", 10);

			currentVidData.AddField("data", currendVidGameData);

			JSONObject usedVidData = new JSONObject();
			usedVidData.AddField("vid", _usedVid);

			JSONObject usedVidGameData = new JSONObject();
			usedVidGameData.AddField("Name", "usedName");
			usedVidGameData.AddField("Level", 11);

			usedVidData.AddField("data", usedVidGameData);

			// Dictionary<String, String> currentVidData = new Dictionary<string, string>();
			// currentVidData.Add ("vid", _currentVid);

			// Dictionary<String, String> usedVidData = new Dictionary<string, string>();
			// usedVidData.Add ("vid", _usedVid);

			Debug.Log("\n\n=== Auth.login() 메서드를 호출 후 결과 값이 LoginType.SELECT 이면 이 메서드를 호출 하여 사용자를 선택을 요청 ===\n");
			Debug.Log("Auth.showLoginSelection(currentVidData, usedVidData, onAuthLogin) Called\n");
			Debug.Log("currentVidData = " + currentVidData.ToString() + "\n");
			Debug.Log("usedVidData = " + usedVidData.ToString() + "\n");

			Auth.showLoginSelection (currentVidData, usedVidData, onAuthLoginCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Auth.bindLogin(" + _usedVid + ")")) {
			
			Debug.Log("\n\n=== Auth.login() 메서드를 호출 후 결과 값이 LoginType.SELECT 이면 이 메서드를 호출하여 계정 바인딩 요청 (usedVid) ===\n");
			Debug.Log("Auth.bindLogin()\n");
			Debug.Log("vid = " + _usedVid);

			Auth.bindLogin(_usedVid, onAuthLoginCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// HIVE 로그 아웃 요청
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Auth.logout()")) {

			Debug.Log("\n\n=== 로그 아웃 요청 ===\n");
			Debug.Log("Auth.logout(onAuthLogout) Called\n");

			Auth.logout (onAuthLogoutCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// HIVE 인증된 사용자 정보 조회
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Auth.getAccount()")) {

			Account account = Auth.getAccount ();

			Debug.Log("\n\n=== 인증된 사용자 정보 조회 ===\n");
			Debug.Log("Auth.getAccount() Called\naccount = " + account!=null?account.toString():"null" + "\n");
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);

		// HIVE 인게이지먼트 이벤트 처리 상태 변경 
		GUILayout.BeginHorizontal();
		GUILayout.Box("isReady : ");
		_selIsReady = HIVETestView.selectionGrid(_selIsReady, _isReady, _isReady.Length);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Auth.setEngagementReady(" + _isReady[_selIsReady] + ")")) {

			Boolean isReady = (_selIsReady == 0);

			Debug.Log("\n\n=== 인게이지먼트 이벤트 처리 상태 변경 및 성공 여부 반환 ===\n");
			Debug.Log(String.Format("Auth.setEngagementReady(isShow = {0}) Called\n", isReady?"true":"false"));

			// Boolean retIsReady = Auth.setEngagementReady (isReady);
			ResultAPI result = Promotion.setEngagementReady (isReady);

			Debug.Log(String.Format("Auth.setEngagementReady(isShow = {0}) Ret = {1}\n", isReady?"true":"false", result.toString()));
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// HIVE 약관 표시
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Auth.showTerms()")) {

			Debug.Log("\n\n=== 약관 정보 표시 ===\n");
			Debug.Log("Auth.showTerms(onAuthShowTerms) Called\n");

			Auth.showTerms (onAuthShowTermsCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// 점검 상태 표시 및 데이터 반환
		GUILayout.BeginHorizontal();
		GUILayout.Box("isShow : ");
		_selIsShow = HIVETestView.selectionGrid(_selIsShow, _isShows, _isShows.Length);
		GUILayout.EndHorizontal();
		GUILayout.Space (2);

		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Auth.checkMaintenance(" + _isShows[_selIsShow] + ")")) {

			Boolean isShow = (_selIsShow == 0);

			Debug.Log("\n\n=== 점검 상태 표시 및 데이터 반환 ===\n");
			Debug.Log(String.Format("Auth.checkMaintenance(isShow = {0}, onAuthMaintenance) Called\n", isShow?"true":"false"));

			Auth.checkMaintenance (isShow, onAuthMaintenanceCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// 성인 인증 표시
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Auth.showAdultConfirm()")) {

			Debug.Log("\n\n=== 성인 인증 화면 호출 ===\n");
			Debug.Log("Auth.showAdultConfirm(onAuthAdultConfirm) Called\n");

			Auth.showAdultConfirm (onAuthAdultConfirmCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		// HIVE SDK 의 로컬 정보 초기화
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Auth.reset()\nWARNING - Reset Data")) {

			Debug.Log("\n\n=== 인증 정보를 포함하여 SDK 에서 사용하는 모든 데이터 초기화 (테스트 용) ===\n");
			Debug.Log("Auth.reset() Called\n");

			Auth.reset();
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);
		
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("Auth.setEmergencyMode()")) {

			Boolean isEmergencyMode = Auth.setEmergencyMode();

			Debug.Log("\n\n=== 긴급 모드 ===\n");
			Debug.Log("Auth.setEmergencyMode() Called\nisEmergencyMode : " + isEmergencyMode);

			if(isEmergencyMode) {

				Account currentAccount = Auth.getAccount();

				// Only on IAP Postbox Sample
				PostboxInfo.DID = currentAccount.did;
				PostboxInfo.VID = currentAccount.vid;
				PostboxInfo.UID = currentAccount.uid;
				PostboxInfo.hiveSessionToken = currentAccount.accessToken;
				ConfigurationTestView.instance.postboxDataSetting();
				PostboxView.instance.callThePostboxLogin();

				MainTestView.isAuthLogin = true;
			}

		}
		GUILayout.EndHorizontal();


        GUILayout.EndVertical();

		GUI.EndScrollView();
    }

}



