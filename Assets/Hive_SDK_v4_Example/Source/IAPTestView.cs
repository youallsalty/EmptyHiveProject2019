using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


using hive;			// HIVE SDK 네임스페이스 사용


/**
 * 결제 (Billing) API 테스트 화면
 * 
 * @author ryuvsken
 */
public class IAPTestView {

	public static IAPTestView instance = new IAPTestView ();


	public void Start() {
	}


	public void Update() {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
	}

	private bool isIAPInitialize = false;
	private String locationCode = "main";


	// Apple, Google, OneStore 와 같은 마켓에 등록한 마켓 정보 통지 리스너 - IAP.initialize() / IAP.showPayment()
	public void onIAPMarketInfoCB(ResultAPI result, List<IAPType> iapTypeList) {

		Debug.Log("IAPTestView.onIAPMarketInfoCB() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			isIAPInitialize = true;

			Debug.Log ("iapTypeList = \n");
			foreach(IAPType each in iapTypeList) {

				Debug.Log (each.ToString() + "\n");
			}
		}
	}


	// HIVE IAP 백오피스에서 구성한 상점 정보 통지 리스너 - IAP.getShopInfo()
	public void onIAPShopInfoCB(ResultAPI result, IAPShop iapShop, int balance) {

		Debug.Log("IAPTestView.onIAPShopInfoCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("iapShop = " + iapShop!=null?iapShop.toString():"null" + "\n");
		Debug.Log ("balance = " + balance + "\n");

		if (result.isSuccess()) {

			IAPShopInfoTestView.instance.setShopInfo(iapShop, balance);
			HIVETestManager.instance.switchView (TestViewType.IAP_SHOPINFO_VIEW);
		}
	}


	// 결제 시도 결과 통지 리스너 - IAP.purchase()
	public void onIAPPurchaseCB(ResultAPI result, IAPProduct iapProduct, String iapTransactionId) {

		Debug.Log("IAPTestView.onIAPPurchaseCB() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			if (iapProduct != null)
				Debug.Log ("iapProduct = " + iapProduct.toString() + "\n");
			
			if (iapTransactionId != null)
				Debug.Log ("iapTransactionId = " + iapTransactionId + "\n");
		}
	}

	public void onIAPPurchaseReceipt(ResultAPI result, IAPReceipt iapReceipt) {

		Debug.Log("IAPTestView.onIAPPurchaseReceipt() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			if (iapReceipt != null)
				Debug.Log ("iapReceipt = " + iapReceipt.toString() + "\n");
		}
	}

	// HIVE IAP 상품에 대한 미지급된 아이템 지급을 요청 결과 통지 리스너 - IAP.restore()
	public void onIAPRestoreCB(ResultAPI result, List<IAPProduct> productList, List<String> iapTransactionIdList) {

		Debug.Log("IAPTestView.onIAPRestoreCB() Callback\nresult = " + result.toString() + "\n");

		if (productList != null) {
			foreach (IAPProduct product in productList) {
				Debug.Log ("product = " + product.toString() + "\n");
			}
		}

		if (iapTransactionIdList != null) {
			foreach (String iapTransactionId in iapTransactionIdList) {
				Debug.Log ("iapTransactionId = " + iapTransactionId + "\n");
			}
		}
	}

	public void onIAPRestoreReceipt(ResultAPI result, List<IAPReceipt> receiptList) {

		Debug.Log("IAPTestView.onIAPRestoreReceipt() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			if (receiptList != null) {
				foreach (IAPReceipt receipt in receiptList) {
					Debug.Log ("receipt = " + receipt.toString() + "\n");
				}
			}
		}
	}

	// 러비 잔액 확인 결과 통지 리스너 - IAP.getBalance()
	public void onIAPBalanceCB(ResultAPI result, int balance) {

		Debug.Log("IAPTestView.onIAPBalanceCB() Callback\nresult = " + result.toString() + "\n");

		Debug.Log ("balance = " + balance + "\n");
	}




	public void OnGUI()
    {
		HIVETestView.layout(24);


        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- IAP -")) {
			HIVETestManager.instance.switchView (TestViewType.MAIN_VIEW);
		}
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		GUILayout.BeginHorizontal();
		GUILayout.Box("Location Code : ");
		this.locationCode = HIVETestView.createTextField(this.locationCode);
		GUILayout.EndHorizontal();
		GUILayout.Space (8);
		

		// IAP 결재 API 초기화 요청
		GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("IAP.initialize()")) {

			Debug.Log("\n\n=== IAP 결재 API 초기화 요청 ===\n");
			Debug.Log("IAP.initialize(listener) Called\n");

			IAP.initialize (onIAPMarketInfoCB);
		}
		GUILayout.EndHorizontal();
		GUILayout.Space (8);


		if (isIAPInitialize == true) {
			
			// 상점 정보 조회
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("IAP.getShopInfo()")) {

				Debug.Log("\n\n=== 상점 정보 조회 ===\n");
				Debug.Log(String.Format("IAP.getShopInfo(locationCode = {0}, listener) Called\n", locationCode));

				IAP.getShopInfo (this.locationCode, onIAPShopInfoCB);
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);


			// // HIVE IAP 상품에 대한 구매를 요청
			// GUILayout.BeginHorizontal();
			// if (HIVETestView.createButton ("IAP.purchase()")) {

			// 	// TODO: parameter
			// 	String pid = "";

			// 	Debug.Log("\n\n=== HIVE IAP 상품에 대한 구매를 요청. ===\n");
			// 	Debug.Log(String.Format("IAP.purchase(pid = {0}, onIAPPurchase) Called\n", pid));

			// 	IAP.purchase (pid, onIAPPurchaseCB);
			// }
			// GUILayout.EndHorizontal();
			// GUILayout.Space (8);


			// HIVE IAP 상품에 대한 미지급된 아이템 지급을 요청한다.
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("IAP.restore()")) {

				Debug.Log("\n\n=== HIVE IAP 상품에 대한 미지급된 아이템 지급을 요청. ===\n");
				Debug.Log("IAP.restore(onIAPRestoreReceipt) Called\n");

				// IAP.restore (onIAPRestoreCB);
				IAP.restore (onIAPRestoreReceipt);
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);


#if !UNITY_EDITOR && UNITY_ANDROID

			// 러비 상점이나 구글 상점을 선택하기 위한 창을 띄운다.
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("IAP.showMarketSelection()")) {

				Debug.Log("\n\n=== 러비 상점이나 구글 상점을 선택하기 위한 창을 띄운다. ===\n");
				Debug.Log("IAP.showPayment(onIAPMarketInfo) Called\n");

				IAP.showMarketSelection (onIAPMarketInfoCB);
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);


			// 리비 상점일 경우 잔액 정보 조회.
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("IAP.getBalanceInfo()")) {

				Debug.Log("\n\n=== HIVE 러비 상점일 경우 잔액 정보 조회. ===\n");
				Debug.Log("IAP.getBalance(onIAPBalance) Called\n");

				IAP.getBalanceInfo (onIAPBalanceCB);
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);


			// 러비 충전 페이지 호출
			GUILayout.BeginHorizontal();
			if (HIVETestView.createButton ("IAP.showCharge()")) {

				Debug.Log("\n\n=== 러비 충전 페이지 호출. ===\n");
				Debug.Log("IAP.showCharge(onIAPBalance) Called\n");

				IAP.showCharge (onIAPBalanceCB);
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);

#endif			// #if !UNITY_EDITOR && UNITY_ANDROID

		}

        GUILayout.EndVertical();
    }

}



