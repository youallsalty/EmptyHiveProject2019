using UnityEngine;
using System.Collections;



// 테스트를 수행 할 화면 형태 정의
public enum TestViewType {

	HIDE_VIEW				// 테스트 화면 숨김
	, SHOW_VIEW				// 테스트 화면 노출
	, CONFIG_VIEW			// 설정 화면
	, MAIN_VIEW				// 메인 테스트 화면
	, AUTH_VIEW				// 인증 테스트 화면
	, SOCIAL_HIVE_VIEW		// HIVE 소셜 테스트 화면
	, SOCIAL_FACEBOOK_VIEW	// Facebook 소셜 테스트 화면
	, SOCIAL_GOOGLE_VIEW	// Google 소셜 테스트 화면
	, PROMOTION_VIEW		// 프로모션 테스트 화면
	, PUSH_VIEW				// 푸시 테스트 화면
	, IAP_VIEW				// 결제 테스트 화면
	, ANALYTICS_VIEW		// 유저 및 게임 데이터 분석 테스트 화면
	, IAP_SHOPINFO_VIEW		// 상품 정보 화면 
	, AUTHV4_VIEW			// v4 인증 테스트 화면
	, PROVIDER_VIEW			// Provider 테스트 화면
	, IAPV4_VIEW			// IAPV4 결제 테스트 화면
	, IAPV4_SHOPINFO_VIEW		// 상품 정보 화면 
	, AUTHV4HELPER_VIEW		// AuthV4 Helper 
	, CONFIG_KEY_VIEW		// Configuration 인증키 API 세팅 테스트 화면
	, SOCIALV4_VIEW			// SocialV4 테스트 화면
}


// 테스트 뷰 화면 전환을 관리하는 기능을 제공
public class HIVETestManager {

	public static HIVETestManager instance = new HIVETestManager();


	public TestViewType currentViewType = TestViewType.CONFIG_KEY_VIEW;	// 현재 선택 된 테스트 화면

	public bool _isShow = true;			// 테스트 화면 표시 여부

	public bool isShowConsoleLogView = false;	// 로그 뷰를 Gesture가 아닌 버튼 동작으로 노출시키기 위함


	public int screenWidth = 0;			// 화면 크기
	public int fontSize = 0;			// 폰트 크기 (가변적)
	public int buttonHeight = 0;		// 버튼 크기 (가변적)
	public int bottomBarHeight = 0;


	// 테스트 뷰 전환
	public void switchView(TestViewType testViewType) {

		if (testViewType == TestViewType.HIDE_VIEW) {
			HIVETestManager.instance._isShow = false;
		}
		else if (testViewType == TestViewType.SHOW_VIEW) {
			HIVETestManager.instance._isShow = true;
		}
		else {
			HIVETestManager.instance.currentViewType = testViewType;
		}
	}

}



