﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using hive;			// HIVE SDK 네임스페이스 사용

public class IAPShopInfoTestView {

	public static IAPShopInfoTestView instance = new IAPShopInfoTestView ();

	public void setShopInfo(IAPShop iapShop, int balance) {

		iapProductList = iapShop.productList;		

		this.locationCode = iapShop.locationCode;
		this.shopId = iapShop.shopId;
		this.shopType = iapShop.shopType;
		this.balance = balance;
	}
	// Use this for initialization
	public void Start () {
	
	}
	
	// Update is called once per frame
	public void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			HIVETestManager.instance.switchView (TestViewType.IAP_VIEW);
		}
	}

	public void onIAPPurchaseCB(ResultAPI result, IAPProduct iapProduct, String iapTransactionId) {

		Debug.Log("IAPShopInfoView.onIAPPurchaseCB() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			if (iapProduct != null)
				Debug.Log ("iapProduct = " + iapProduct.toString() + "\n");
			
			if (iapTransactionId != null)
				Debug.Log ("iapTransactionId = " + iapTransactionId + "\n");
		}
	}

	public void onIAPPurchaseReceipt(ResultAPI result, IAPReceipt iapReceipt) {

		Debug.Log("IAPShopInfoView.onIAPPurchaseReceipt() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess()) {

			if (iapReceipt != null)
				Debug.Log ("iapReceipt = " + iapReceipt.toString() + "\n");
		}
	}

	public void onIAPCheckPromotePurchase(ResultAPI result, String gamePid) {

		Debug.Log("IAPShopInfoView.onIAPCheckPromotePurchase() Callback\nresult = " + result.toString() + "\n");

		if (result.isSuccess ()) {

			Debug.Log ("gamePid = " + gamePid);

			// 이후 상품 소개 및 구매 처리 필요
			MainTestView.isIAPUpdated = false;
		}
	}

	string locationCode = "";
	int shopId = 0;
	string shopType = "";
	int balance = 0;

	List<IAPProduct> iapProductList = null;
	
	String additionalInfo = "";

	public void OnGUI() {

		HIVETestView.layout(24);

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
		if (HIVETestView.createButton ("- IAP SHOPINFO-")) {
			HIVETestManager.instance.switchView (TestViewType.IAP_VIEW);
		}
        GUILayout.EndHorizontal();
		GUILayout.Space (8);

		//if (this.isPostbox == false) {
			// PostBox
			GUILayout.BeginHorizontal();		
			if (HIVETestView.createButton ("PostBox")) {

				Debug.Log("\n\n=== PostBox 요청 ===\n");
				Debug.Log("PostBox Called\n");

				HIVETestManager.instance.switchView(TestViewType.HIDE_VIEW);

				ConfigurationTestView.instance.postboxDataSetting();
				PostboxView.instance.showPostBox("consumable", () => {
					HIVETestManager.instance.switchView(TestViewType.SHOW_VIEW);
				});
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (8);

			GUILayout.BeginHorizontal();
			GUILayout.Box("LocationCode : ");
			GUILayout.Box(this.locationCode);
			GUILayout.EndHorizontal();
			GUILayout.Space (2);

			GUILayout.BeginHorizontal();
			GUILayout.Box("ShopId : ");
			GUILayout.Box(this.shopId.ToString());
			GUILayout.EndHorizontal();
			GUILayout.Space (2);

			GUILayout.BeginHorizontal();
			GUILayout.Box("ShopType : ");
			GUILayout.Box(this.shopType);
			GUILayout.EndHorizontal();
			GUILayout.Space (2);

			GUILayout.BeginHorizontal();
			GUILayout.Box("Balance : ");
			GUILayout.Box(this.balance.ToString());
			GUILayout.EndHorizontal();
			GUILayout.Space (2);

			GUILayout.BeginHorizontal();
			GUILayout.Box("character_key : ");
			this.additionalInfo = HIVETestView.createTextField(this.additionalInfo);
			GUILayout.EndHorizontal();
			GUILayout.Space (2);
			
			GUILayout.BeginHorizontal();
			GUILayout.Box("- Products -");
			GUILayout.EndHorizontal();
			GUILayout.Space (8);

			GUILayout.BeginHorizontal();
			if (MainTestView.isIAPUpdated == true) {
				if (HIVETestView.createButton("IAP.checkPromotePurchase()")) {

					Debug.Log(String.Format("IAP.onIAPCheckPromotePurchase Called\n"));

					IAP.checkPromotePurchase (onIAPCheckPromotePurchase);
				}
			}
			GUILayout.EndHorizontal();
			GUILayout.Space (2);

			for (int i=0; i<iapProductList.Count; ++i) {

				GUILayout.BeginHorizontal();
				if (HIVETestView.createButton(iapProductList[i].gamePid + "." + 
					"(" + 
					iapProductList[i].displayPrice + ")" )) {

						Debug.Log("\n\n=== HIVE IAP 상품에 대한 구매를 요청. ===\n");
						Debug.Log(String.Format("IAP.purchase(pid = {0}, onIAPPurchase) Called\n", iapProductList[i].gamePid));

						// IAP.purchase (iapProductList[i].gamePid, onIAPPurchaseCB); // 구매 요청
						
						String additionalInfoStr = "{\\\"character_key\\\":\\\"" + additionalInfo + "\\\"}";

						IAP.purchase (iapProductList[i].gamePid, additionalInfoStr, onIAPPurchaseReceipt);
					}
				GUILayout.BeginVertical();
				
				GUILayout.EndVertical();
				GUILayout.EndHorizontal();
				GUILayout.Space (8);
			}
		//}

		GUILayout.EndVertical();
	}
}
