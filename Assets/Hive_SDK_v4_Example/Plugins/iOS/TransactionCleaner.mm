//
//  TransactionCleaner.m
//
//
//  Created by sjyong on 2021/02/17.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@interface TransactionCleanerPlugin : NSObject
+ (int)finishAllTransactions;
@end

@implementation TransactionCleanerPlugin

+ (int)finishAllTransactions {

    int finishedTransactionCount = 0;
    NSArray<SKPaymentTransaction *> *transactions    = [[SKPaymentQueue defaultQueue] transactions];

    for (SKPaymentTransaction * transaction in transactions) {

        if(transaction.transactionState == SKPaymentTransactionStatePurchased || transaction.transactionState == SKPaymentTransactionStateRestored) {
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            finishedTransactionCount++;
        }
    }

    return finishedTransactionCount;
}

@end


// Unity 영역에서 호출
extern "C" {

int finishAllTransactions() {
    return [TransactionCleanerPlugin finishAllTransactions];
}

}


